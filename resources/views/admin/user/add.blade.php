@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-users"></i> Pengguna dan Peranan</h3>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route ('admin.index') }}">Pentadbiran Sistem</a></li>
          <li class="breadcrumb-item active">Pengguna</li>
          <li class="breadcrumb-item active">Tambah</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="fas fa-plus-circle"></i> Tambah Pengguna</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.user.store') }}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label">Nama Pengguna <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-user"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Pengguna" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="login_id_label">ID Pengguna <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-lock"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="login_id" name="login_id" placeholder="ID Pengguna" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="email_label">E-mel <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-email"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="contoh@contoh.com" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="status_label">Status<span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-info"></i>
                                                </span>
                                            </div>
                                            <select class="form-control" id="roles" name="status" id='status'>
                                                <option value=''>Sila Pilih</option>
                                                <option value='ACTIVE'>Aktif</option>
                                                <option value='DEACTIVATED'>Tidak Aktif</option>
                                                <option value='LOCKED'>Kunci Akaun</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="password_label">Kata Laluan <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-key"></i>
                                                </span>
                                            </div>
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Kata Laluan" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="password_confirmation_label">Pengesahan Kata Laluan <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-key"></i>
                                                </span>
                                            </div>
                                            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Pengesahan Kata Laluan" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="role_label"> Peranan</label>
                                        <select class="select-role form-control" id="roles" name="roles[]" data-placeholder="Pilih Peranan" multiple>
                                            @foreach($roles as $role)
                                            <option value="{{ $role['name'] }}">{{ $role['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route('admin.user.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
<link href="{{ asset('dualListbox/dist/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
@endpush

@push('js')
<script src="{{ asset('dualListbox/dist/jquery.bootstrap-duallistbox.js') }}"></script>
<script>
    $('.select-role').bootstrapDualListbox({
        nonSelectedListLabel: 'Belum Dipilih',
        selectedListLabel: 'Sudah Dipilih',
        preserveSelectionOnMove: 'Pindahkan',
        infoText: 'Tunjuk semua {0}',
        infoTextFiltered: '<span class="label label-warning">Ditapis</span> {0} dari {1}',
        filterPlaceHolder: 'Carian',
        infoTextEmpty: 'Senarai yang kosong',
        selectorMinimalHeight: 300,
        moveOnSelect: true,
    });
</script>
@endpush
