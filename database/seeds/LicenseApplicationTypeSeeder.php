<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\ApprovalLetterApplicationType;

class LicenseApplicationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'BAHARU',
            'PEMBAHARUAN',
            'PENGGANTIAN',
            'PERUBAHAN MAKLUMAT SYARIKAT',
            'PERUBAHAN MAKLUMAT PREMIS',
            'PERUBAHAN MAKLUMAT STOR',
            'PERUBAHAN MAKLUMAT PERMOHONAN LESEN',
            'PEMBATALAN',
            'PEMBAHARUAN DAN PERUBAHAN',
        ];

        foreach ($data as $item) {
            LicenseApplicationType::create([
                'name'  =>  $item,
                'code'  =>  str_replace(' ', '_', $item),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item
            ]);
        }


        $data = [
            'BAHARU'
        ];

        foreach ($data as $item) {
            ApprovalLetterApplicationType::create([
                'name'  =>  $item,
                'code'  =>  $item,
                'description'   =>  'Data ini merujuk kepada data jenis '.$item
            ]);
        }
    }
}
