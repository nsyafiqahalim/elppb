<?php

use Illuminate\Database\Seeder;

use App\Models\Portal\Faq;

class PortalFaqSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('portal_faqs')->delete();

        \DB::table('portal_faqs')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => 'Berapakah jumlah bayaran yang dikenakan bagi setiap lesen?',
                'description' => '<p>Bayaran yang dikenakan bagi lesen yang dipohon adalah setiap berikut:-</p>
<p style="padding-left: 30px;">i.&nbsp;Lesen Runcit</p>
<p style="padding-left: 30px;">ii. Lesen Borong</p>
<p style="padding-left: 30px;">iii. Lesen Import</p>
<p style="padding-left: 30px;">iv. Lesen Eksport</p>
<p style="padding-left: 30px;">v. Lesen Membeli Padi</p>
<p style="padding-left: 30px;">vi. Lesen Kilang Padi Komersial</p>
<p style="padding-left: 30px;">vii. Lesen Bagi Mongering/Mengilang Untuk Kegunaan Sendiri</p>',
                'created_by' => 1,
                'updated_by' => 1,
                'is_published' => 1,
                'created_at' => '2020-03-18 06:33:11',
                'updated_at' => '2020-03-26 16:40:15',
            ),
            1 =>
            array (
                'id' => 2,
                'title' => 'Apakah syarat yang dikenakan untuk saya memohon lesen?',
                'description' => '<p>Hello</p>',
                'created_by' => 1,
                'updated_by' => 1,
                'is_published' => 1,
                'created_at' => '2020-03-18 06:33:11',
                'updated_at' => '2020-03-26 16:38:30',
            ),
            2 =>
            array (
                'id' => 3,
                'title' => 'Apakah dokumen yang diperlukan semasa membuat permohonan lesen?',
            'description' => '<p><strong>a) Lesen Runcit</strong></p>
<p style="padding-left: 30px;">i. Enterprise</p>
<p style="padding-left: 60px;">1) Kad Pengenalan;</p>
<p style="padding-left: 60px;">2)&nbsp;Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan &ldquo;Beras&rdquo; serta alamat premis yang dipohon atau;</p>
<p style="padding-left: 60px;">3)&nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan Beras bagi negeri Sarawak atau;</p>
<p style="padding-left: 60px;">4)&nbsp;Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan beras bagi negeri Sabah.</p>
<p style="padding-left: 60px;">&nbsp;</p>
<p style="padding-left: 30px;">ii. Sendirian Berhad</p>
<p style="padding-left: 60px;">1)&nbsp;Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan beras bagi negeri Sabah.</p>
<p style="padding-left: 60px;">2)&nbsp;Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan &ldquo;beras&rdquo;.</p>
<p style="padding-left: 60px;">3)&nbsp;Koperasi/Pertubuhan</p>
<p style="padding-left: 90px;">i) Sijil&nbsp;Koperasi/Pertubuhan</p>
<p style="padding-left: 90px;">ii)&nbsp;Buku Undang-Undang Kecil Tubuh;</p>
<p style="padding-left: 90px;">iii)&nbsp;Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon lesen runcit.</p>
<p><strong>b) Lesen Borong</strong></p>
<p style="padding-left: 30px;">i. Enterprise</p>
<p style="padding-left: 60px;">1) Kad Pengenalan;</p>
<p style="padding-left: 60px;">2)&nbsp;Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan &ldquo;Beras&rdquo; serta alamat premis yang dipohon atau;</p>
<p style="padding-left: 60px;">3)&nbsp;Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan Beras bagi negeri Sarawak atau;</p>
<p style="padding-left: 60px;">4)&nbsp;Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan beras bagi negeri Sabah.</p>
<p style="padding-left: 60px;">5)&nbsp;Surat perjanjian sewa, perjanjian jual beli, surat perjanjian menggunakan stor atau tanah;</p>
<p style="padding-left: 60px;">6)&nbsp;Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama pemohon atau perniagaan yang didaftarkan dengan baki tidak kurang daripada RM100,000 (Bumiputera) atau RM150,000 (Bukan Bumiputera).</p>
<p style="padding-left: 30px;">ii. Sendirian Berhad</p>
<p style="padding-left: 60px;">1) Kad Pengenalan;</p>
<p style="padding-left: 60px;">2) Maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan &ldquo;pemborong beras&rdquo;. Dimana modal berbayar hendaklah &nbsp;RM 100,000.00 (Bumiputera) atau RM 150,000.00 (Bukan Bumiputera);</p>
<p style="padding-left: 60px;">3)&nbsp;Surat perjanjian sewa, perjanjian jual beli, surat perjanjian menggunakan stor atau tanah;</p>
<p style="padding-left: 60px;">4)&nbsp;Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama perniagaan yang didaftarkan dengan baki tidak kurang daripada RM 100,000.00 (Bumiputera) atau RM 150,000.00 (Bukan Bumiputera).</p>
<p style="padding-left: 30px;">iii. Koperasi/Pertubuhan</p>
<p style="padding-left: 60px;">1) Sijil Koperasi/Pertubuhan;</p>
<p style="padding-left: 60px;">2) Buku Undang-Undang Kecil Tubuh;</p>
<p style="padding-left: 60px;">3)&nbsp;Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon lesen borong beras dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut;</p>
<p style="padding-left: 60px;">4)&nbsp;Surat perjanjian sewa, perjanjian jual beli, surat perjanjian menggunakan stor atau tanah;</p>
<p style="padding-left: 60px;">5)&nbsp;Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama pemohon atau perniagaan yang didaftarkan dengan baki tidak kurang daripada RM 100,000.00 (Bumiputera) atau RM 150,000.00 (Bukan Bumiputera).</p>
<p><strong>c) Lesen Import</strong></p>
<p>Hanya&nbsp;dibuka kepada pemohon yang telah memiliki Lesen Borong yang masih sah tempoh sahaja.</p>
<p style="padding-left: 30px;">i. Enterprise</p>
<p style="padding-left: 60px;">1) Kad pengenalan;</p>
<p style="padding-left: 60px;">2) Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan &ldquo; pemborong beras atau produk berasaskan beras&rdquo; atau;</p>
<p style="padding-left: 60px;">3) Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan &ldquo;pemborong beras atau produk berasaskan beras&rdquo; bagi negeri Sarawak atau;</p>
<p style="padding-left: 60px;">4)&nbsp;Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan &ldquo;pemborong beras atau produk berasaskan beras&rdquo; bagi negeri Sabah.</p>
<p style="padding-left: 30px;">ii. Sendirian Berhad</p>
<p style="padding-left: 60px;">1) Kad pengenalan;</p>
<p style="padding-left: 60px;">2) Maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan &ldquo;pemborong beras atau produk berasaskan beras&rdquo;. Dimana modal berbayar hendaklah RM 100,000.00 (Bumiputera) atau RM 150,000.00 (Bukan Bumiputera).</p>
<p style="padding-left: 30px;">iii. Koperasi/Pertubuhan</p>
<p style="padding-left: 60px;">1) Sijil Koperasi/Pertubuhan;</p>
<p style="padding-left: 60px;">2) Buku Undang-Undang Kecil Tubuh;</p>
<p style="padding-left: 60px;">3)&nbsp;Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Import dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut.</p>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-18 06:33:11',
'updated_at' => '2020-03-26 16:35:35',
),
3 =>
array (
'id' => 4,
'title' => 'Bagaimana cara proses pendaftaran id pengguna dilaksanakan?',
'description' => '<p>Pemohon perlu mengisi maklumat seperti nombor kad pengenalan, nombor telefon dan e-mel. Katalaluan dan id pengguna akan dihantar melalui e-mel serta merta setelah pendaftaran dibuat.</p>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-18 06:33:11',
'updated_at' => '2020-03-26 16:13:41',
),
4 =>
array (
'id' => 5,
'title' => 'Bagaimana proses pendaftaran id pengguna bagi sistem eLPPB 2.0?',
'description' => '<div>
<p>Pendaftaran id pengguna ini boleh dibuat secara atas talian melalui laman web <a href="http://www.elesen.moa.gov.my/" target="_blank" rel="noopener">e-lesen</a>.</p>
</div>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-18 06:33:11',
'updated_at' => '2020-03-26 16:12:56',
),
5 =>
array (
'id' => 6,
'title' => 'Bagaimana kaedah serta cara bayaran yang boleh dilakukan?',
'description' => '<p>Bayaran lesen adalah secara wong pos atau bank draf. Pembayaran secara tunai tidak diterima. Pembayaran boleh dilakukan dimana &ndash; mana pejabat atau cawangan Kawalselia Padi dan Beras yang berhampiran.</p>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-26 16:40:38',
'updated_at' => '2020-03-26 16:40:38',
),
6 =>
array (
'id' => 7,
'title' => 'Siapakan yang boleh memohon permit tersebut?',
'description' => '<p>Untuk makluman, terdapat dua (2) kategori pemohon iaitu pemohon merupakan pemegang apa &ndash; apa jenis lesen yang dikeluarkan oleh pihak Kawalselia Padi dan Beras atau orang perseorangan.</p>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-26 16:41:12',
'updated_at' => '2020-03-26 16:42:22',
),
7 =>
array (
'id' => 8,
'title' => 'Berapa jenis permit yang boleh dipohon?',
'description' => '<p>a) Permit Pemindahan Beras Dan Hasil Sampingan Antara Negeri</p>
<p>b) Permit Pemindahan Padi Antara Negeri</p>',
'created_by' => 1,
'updated_by' => 1,
'is_published' => 1,
'created_at' => '2020-03-26 16:42:12',
'updated_at' => '2020-03-26 16:42:12',
),
));


    }
}
