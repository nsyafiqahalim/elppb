<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPremiseContactDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_premises', function ($table) {
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email');
        });

        Schema::table('license_application_company_premises', function ($table) {
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email');
        });
        

        Schema::table('approval_letter_application_company_premises', function ($table) {
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
