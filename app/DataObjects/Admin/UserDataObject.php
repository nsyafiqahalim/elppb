<?php

namespace App\DataObjects\Admin;

use DB;
use App\User;

class UserDataObject
{
    public const USER_TYPE_MOA = 'MOA';

    public static function addUser($param)
    {
        $user = null;

        DB::transaction(function () use ($param, &$user) {
            $user = User::create([
                'name'          =>  $param['name'],
                'login_id'      =>  $param['login_id'],
                'email'      =>  $param['email'],
                'type'      =>  $param['type'],
                'status'      =>  $param['status'],
                'password'      => bcrypt($param['password']),
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);

            foreach ($param['roles'] as $role) {
                $user->assignRole($role);
            }
        });

        return $user;
    }

    public static function updateUser($param, $id)
    {
        $user = null;

        DB::transaction(function () use ($param, $id,  &$user) {
            $user = User::find($id);

            $user->update([
                'name'          =>  $param['name'],
                'login_id'      =>  $param['login_id'],
                'status'      =>  $param['status'],
                'email'         =>  $param['email'],
                'updated_at'    =>  $param['updated_at'],
            ]);

            if (isset($param['roles'])) {
                $user->syncRoles([]);

                foreach ($param['roles'] as $role) {
                    $user->assignRole($role);
                }
            };
        });

        return $user;
    }

    public static function findAllUserUsingDatatableFormat($param)
    {
        $users = User::distinct()->orderby('id', 'desc')
                        ->when(isset($param['login_id']) && $param['login_id'], function ($query) use ($param) {
                            $query->where('login_id', 'like', '%'.$param['login_id'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['status']), function ($query) use ($param) {
                            $query->where('status', $param['status']);
                        });
        
        return datatables()->of($users)
        ->addIndexColumn()
        ->addColumn('action', function ($user) {
            return view('admin.user.partials.datatable-button', compact('user'))->render();
        })
        ->addColumn('status', function ($user) {
            $status = $user->status;

            return view('admin.user.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findUserById($id)
    {
        return User::find($id);
    }
}
