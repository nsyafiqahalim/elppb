<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\UserDataObject;
use App\DataObjects\Admin\RoleDataObject;

class UserController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.user.index');
    }

    public function add()
    {
        $roles = RoleDataObject::findAllRoles();

        return view('admin.user.add', compact('roles'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $userDAO = UserDataObject::findUserById($decryptedId);
        $roles = RoleDataObject::findAllRoles();

        return view('admin.user.edit', compact('roles', 'userDAO'));
    }
}
