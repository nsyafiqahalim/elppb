<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateMockApplicants extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:applicant {amount}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to create mock user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amt = $this->argument('amount');

        if (!preg_match('/^\d+$/', $amt)) {
            $this->error("Please enter an integer!");
            return 0;
        }

        $this->info("Creating {$amt} mock applicants...");
        factory(\App\User::class, (int) $amt)->create()->each(function ($u) {
            $u->assignRole('Pemohon');
        });

        $this->info("Successfully created {$amt} mock applicants!");
    }
}
