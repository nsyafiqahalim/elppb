<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id_identifier')->unique();
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('building_type_id')->index();
            $table->unsignedBigInteger('store_ownership_type_id')->index();
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('district_id')->index();
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email')->nullable();
            $table->enum('type', ['MAILING'])->default('MAILING');

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('store_ownership_type_id')->references('id')->on('store_ownership_types');
            $table->foreign('building_type_id')->references('id')->on('building_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_stores');
    }
}
