<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\LandType;

class LandTypeDataObject
{
    public static function addLandType($param)
    {
        $landType = null;

        DB::transaction(function () use ($param, &$landType) {
            $landType = LandType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $landType;
    }

    public static function updateLandType($param, $id)
    {
        $landType = null;

        DB::transaction(function () use ($param, $id,  &$landType) {
            $landType = LandType::find($id);

            $landType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $landType;
    }

    public static function findAllLandTypeUsingDatatableFormat($param)
    {
        $landType = LandType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($landType)
        ->addIndexColumn()
        ->addColumn('action', function ($landType) {
            return view('admin.land-type.partials.datatable-button', compact('landType'))->render();
        })
        ->addColumn('status', function ($landType) {
            $status= ($landType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.land-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findLandTypeById($id)
    {
        return LandType::find($id);
    }
}
