<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationGeneratedLicenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_include_licenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('license_type_id')->index('lail_lt_idx');
            $table->foreign('license_type_id','fk_lail_lt')->references('id')->on('license_types');
            $table->unsignedBigInteger('license_application_id')->index('lail_la_idx');
            $table->foreign('license_application_id', 'fk_lail_la')->references('id')->on('license_applications');
            $table->unsignedBigInteger('license_application_type_id')->index('lail_lat_idx');
            $table->foreign('license_application_type_id', 'fk_lail_lat')->references('id')->on('license_application_types');
            $table->unsignedBigInteger('status_id')->index();
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->timestamps();
        });

        Schema::create('license_application_include_license_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('current_license_number')->nullable();
            $table->string('old_license_number')->nullable();
            $table->unsignedBigInteger('license_type_id')->index('laili_lt_idx');
            $table->unsignedBigInteger('current_license_id')->nullable()->index('lail_cli_idx');
            $table->unsignedBigInteger('old_license_id')->nullable()->index('lail_oli_idx');
            $table->foreign('current_license_id','fk_lail_cli')->references('id')->on('licenses');
            $table->foreign('old_license_id','fk_lail_oli')->references('id')->on('licenses');
            $table->foreign('license_type_id','fk_laili_lt')->references('id')->on('license_types');
            $table->unsignedBigInteger('status_id')->index();
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->unsignedBigInteger('license_application_id')->index('laili_la_idx');
            $table->foreign('license_application_id', 'fk_laili_la')->references('id')->on('license_applications');
            $table->unsignedBigInteger('license_application_include_license_id')->index('laili_lail_idx');
            $table->foreign('license_application_include_license_id', 'fk_laili_lail')->references('id')->on('license_application_include_licenses');
            $table->tinyInteger('has_print_original')->default(0);
            $table->tinyInteger('has_print_copy')->default(0);
            $table->timestamps();
        });

        Schema::create('approval_letter_application_include_licenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('approval_letter_type_id')->index('alail_lt_idx');
            $table->foreign('approval_letter_type_id','fk_alail_lt')->references('id')->on('approval_letter_types');
            $table->unsignedBigInteger('approval_letter_application_id')->index('alail_ala_idx');
            $table->foreign('approval_letter_application_id', 'fk_alail_ala')->references('id')->on('approval_letter_applications');
            $table->unsignedBigInteger('approval_letter_application_type_id')->index('alail_alat_idx');
            $table->foreign('approval_letter_application_type_id', 'fk_alail_alat')->references('id')->on('approval_letter_application_types');
            $table->unsignedBigInteger('status_id')->index();
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->timestamps();
        });

        Schema::create('approval_letter_application_include_approval_letter_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('current_approval_letter_number')->nullable();
            $table->unsignedBigInteger('approval_letter_type_id')->index('alaili_lt_idx');
            $table->unsignedBigInteger('current_approval_letter_id')->nullable()->index('alail_cli_idx');
            $table->unsignedBigInteger('old_approval_letter_id')->nullable()->index('alail_oli_idx');
            $table->foreign('current_approval_letter_id','fk_alail_cli')->references('id')->on('licenses');
            $table->foreign('approval_letter_type_id','fk_alaili_lt')->references('id')->on('approval_letter_types');
            $table->unsignedBigInteger('status_id')->index('alail_si_idx');
            $table->foreign('status_id','fk_alail_si')->references('id')->on('statuses');
            $table->unsignedBigInteger('approval_letter_application_id')->index('alaili_ala_idx');
            $table->foreign('approval_letter_application_id', 'fk_alaili_ala')->references('id')->on('approval_letter_applications');
            $table->unsignedBigInteger('approval_letter_application_include_approval_letter_id')->index('alaili_alail_idx');
            $table->foreign('approval_letter_application_include_approval_letter_id', 'fk_alaili_alail')->references('id')->on('approval_letter_application_include_licenses');
            $table->tinyInteger('has_print_original')->default(0);
            $table->tinyInteger('has_print_copy')->default(0);
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
