<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MakeIcNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_partners', function ($table) {
            $table->string('nric')->nullable()->change();
        });

        Schema::table('license_application_company_partners', function ($table) {
            $table->string('nric')->nullable()->change();
        });

        Schema::table('approval_letter_application_company_partners', function ($table) {
            $table->string('nric')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
