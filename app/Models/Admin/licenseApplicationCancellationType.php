<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationCancellationType extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_cancellation_types';
}
