<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));


    return [
        'name' => $faker->name,
        'login_id' => $faker->myKadNumber(),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('123'),
        'status'    =>  'ACTIVE',
        'type'  =>  'APPLICANT',
        'remember_token' => Str::random(10),
    ];
});
