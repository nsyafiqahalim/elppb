<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseApplicationCancellationType;

class licenseApplicationCancellationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LicenseApplicationCancellationType::create([
            'name'  =>  'Tutup Syarikat',
            'code'  =>  'TUTUP_SYARIKAT',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Syarikat Tutup'
        ]);

        LicenseApplicationCancellationType::create([
            'name'  =>  'Muflis',
            'code'  =>  'MUFLIS',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Syarikat Tutup'
        ]);

        LicenseApplicationCancellationType::create([
            'name'  =>  'Syarikat Dibeli',
            'code'  =>  'SYARIKAT_DIBELI',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Syarikat Dibeli'
        ]);

        LicenseApplicationCancellationType::create([
            'name'  =>  'Lain - Lain',
            'code'  =>  'LAIN_-_LAIN',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Syarikat Dibeli'
        ]);
    }
}
