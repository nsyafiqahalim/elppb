<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\FactoryTypeDataObject;

class FactoryTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addFactoryType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $factoryType = FactoryTypeDataObject::addFactoryType($param);

        return response()->json([
            'message'   =>  'Jenis kilang berjaya ditambah',
            'data'      =>  $factoryType,
            'success'   =>  true,
            'route'     =>  route('admin.factory_type.index')
        ], 200);
    }

    public function updateFactoryType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $factoryType = FactoryTypeDataObject::updateFactoryType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis kilang berjaya dikemaskini',
            'data'      =>  $factoryType,
            'success'   =>  true,
            'route'     =>  route('admin.factory_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return FactoryTypeDataObject::findAllFactoryTypeUsingDatatableFormat($param);
    }
}
