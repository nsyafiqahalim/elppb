<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class UpdateUserValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = decrypt($request->id);

        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$id],
            'login_id' => ['required', 'string', 'max:255', 'unique:users,login_id,'.$id],
        ], [
            'name.required' => ['Medan Nama diperlukan'],
            'email.unique' => ['Emel sudah direkokdkan didalam sistem'],
            'email.required' => ['Medan Emel diperlukan'],
            'login_id.unique' => ['ID Pengguna sudah direkokdkan didalam sistem'],
            'login_id.required' => ['Medan ID Pengguna diperlukan'],
        ]);

        return $next($request);
    }
}
