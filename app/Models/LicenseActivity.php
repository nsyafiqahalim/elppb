<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


use App\User;
use App\Models\Admin\Status;
use App\Models\LicenseApplication;

class LicenseActivity extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_activities';

    private const ACTIVE = 1;

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }
}
