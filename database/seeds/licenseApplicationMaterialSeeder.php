<?php

use Illuminate\Database\Seeder;
use App\Models\LicenseApplication\Material;

class licenseApplicationMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //RUNCIT BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'RUNCIT_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Surat Kelulusan/ Pengesahan Pemilikan Entiti Asing daripada KPDNHEP (tertakluk untuk syarikat yang mempunyai pemilikan entiti asing)',
            'code'      =>  'KPDNHEP',
            'domain'    =>  'RUNCIT_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'RUNCIT_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Runcit Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'RUNCIT_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        //BORONG BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak);',
            'code'      =>  'SSM',
            'domain'    =>  'BORONG_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak; dan',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'BORONG_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini;',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'BORONG_BAHARU_SYARIKAT',
            'order'     =>  3
        ]);

       Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran;',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'BORONG_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu; dan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'BORONG_BAHARU_KOPERASI',
            'order'     =>  2
        ]); 

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'APPROVAL_LETTER',
            'domain'    =>  'BORONG_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'BORONG_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        //IMPORT BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'IMPORT_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'IMPORT_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'IMPORT_BAHARU_SYARIKAT',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'IMPORT_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'IMPORT_BAHARU_KOPERASI',
            'order'     =>  2
        ]); 

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'APPROVAL_LETTER',
            'domain'    =>  'IMPORT_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'IMPORT_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        //EKSPORT BAHARU

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'EKSPORT_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'EKSPORT_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'EKSPORT_BAHARU_SYARIKAT',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'EKSPORT_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'EKSPORT_BAHARU_KOPERASI',
            'order'     =>  2
        ]); 

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'APPROVAL_LETTER',
            'domain'    =>  'EKSPORT_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'EKSPORT_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        //BELI PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'BELI_PADI_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'BELI_PADI_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'BELI_PADI_BAHARU_SYARIKAT',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Surat Kelulusan Khas Jabatan Pertanian/ Surat Lantikan Pengeluar Benih Padi Sah (bagi maksud dibuat biji benih) daripada Industri Padi Beras',
            'code'      =>  'SPECIAL_APPROVAL_LETTER',
            'domain'    =>  'BELI_PADI_BAHARU_SYARIKAT',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'BELI_PADI_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'BELI_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]); 

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'APPROVAL_LETTER',
            'domain'    =>  'BELI_PADI_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'BELI_PADI_BAHARU_KOPERASI',
            'order'     =>  4
        ]);

         Material::create([
            'label'     =>  'Surat Kelulusan Khas Jabatan Pertanian/ Surat Lantikan Pengeluar Benih Padi Sah (bagi maksud dibuat biji benih) daripada Industri Padi Beras',
            'code'      =>  'SPECIAL_APPROVAL_LETTER',
            'domain'    =>  'BELI_PADI_BAHARU_KOPERASI',
            'order'     =>  5
        ]);

        //KILANG PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan Jabatan Alam Sekitar.',
            'code'      =>  'NATURE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan ubah syarat tapak kilang kepada Industri/ kilang padi dari Pejabat Tanah atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'GRANT_COPY',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa Tempatan.',
            'code'      =>  'BUILD_FACTORY_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia',
            'code'      =>  'FIRE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  7
        ]);

        Material::create([
            'label'     =>  'Surat Kebenaran Khas Membeli Padi bagi Tujuan Ujian Mengisar (Test Milling)',
            'code'      =>  'FIRE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_SYARIKAT',
            'order'     =>  8
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Kilang Padi (Komersial)',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank tiga (3) bulan terkini',
            'code'      =>  'BANK_STATEMENT',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan Jabatan Alam Sekitar.',
            'code'      =>  'NATURE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan ubah syarat tapak kilang kepada Industri/ kilang padi dari Pejabat Tanah atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'GRANT_COPY',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa Tempatan.',
            'code'      =>  'BUILD_FACTORY_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa Tempatan',
            'code'      =>  'PBT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  7
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia',
            'code'      =>  'FIRE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  7
        ]);

        Material::create([
            'label'     =>  'Surat Kebenaran Khas Membeli Padi bagi Tujuan Ujian Mengisar (Test Milling)',
            'code'      =>  'FIRE_DEPARTMENT_APPROVAL_LETTER',
            'domain'    =>  'KILANG_PADI_BAHARU_KOPERASI',
            'order'     =>  8
        ]);

        //Pembaharuan Runcit
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'PEMBAHARUAN_RUNCIT_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon pembaharuan Lesen Runcit',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_RUNCIT_KOPERASI',
            'order'     =>  2
        ]);

        //Pembaharuan Borong
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //Pembaharuan IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //Pembaharuan ESKPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_ESKPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Pejanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_ESKPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'REGISTRATION_CERTIFICATE',
            'domain'    =>  'PEMBAHARUAN_ESKPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Baharu',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_ESKPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Geran atau Salinan Kebenaran Menggunakan Tapak',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_ESKPORT_KOPERASI',
            'order'     =>  1
        ]);

        //Pembaharuan Beli Padi
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa atau Salinan Pemilikan Stor atau Salinan Kebenaran Menggunakan Tapak Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Penyata Belian/ Jualan Padi tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);
        
        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon pembaharuan Lesen Membeli Padi.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa atau Salinan Pemilikan Stor atau Salinan Kebenaran Menggunakan Tapak Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Penyata Belian/ Jualan Padi tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  3
        ]);

        //Pembaharuan KILANG Padi
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa atau Salinan Pemilikan Stor atau Salinan Kebenaran Menggunakan Tapak Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_KILANG_PADI_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);
        
        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon pembaharuan Lesen Kilang Padi (Komersial).',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa atau Salinan Pemilikan Stor atau Salinan Kebenaran Menggunakan Tapak Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  2
        ]);


        //PERUBAHAN ALAMAT PERNIAGAAN RUNCIT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);
        
        
        //PERUBAHAN ALAMAT PERNIAGAAN BORONG
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PERNIAGAAN IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PERNIAGAAN EKSPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PERNIAGAAN BELI PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PERNIAGAAN KILANG PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PERNIAGAAN MENGERING PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon perubahan nama perniagaan.',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERNIAGAAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS RUNCIT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);
        
        //PERUBAHAN ALAMAT PREMIS BORONG
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS EKSPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS BELI PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS KILANG PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT PREMIS MENGERING PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa Premis',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PREMIS_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR BORONG
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR EKSPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR BELI PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR KILANG PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN ALAMAT STOR MENGERING PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Alamat Premis Perniagaan',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli /Sewa stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_STOR_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN RUNCIT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Runcit',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN BORONG
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN EKSPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN BELI PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN KILANG PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PERUBAHAN HAD MUATAN MENGERING PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Perubahan Muatan Stor',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_MAKLUMAT_PERMOHONAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN RUNCIT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN BORONG
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN IMPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN EKSPORT
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN BELI PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN KILANG PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PEMBAHARUAN DAN PERUBAHAN MENGERING PADI
        
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran.',
            'code'      =>  'SSM',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Pembaharuan dan Perubahan Lesen',
            'code'      =>  'MINUTE_MEETING',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/ jualan tiga (3) bulan terkini',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Perjanjian Jual Beli/ Sewa Stor',
            'code'      =>  'STOCK_STATEMENT',
            'domain'    =>  'PERUBAHAN_DAN_PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN RUNCIT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN BORONG
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN IMPORT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN EKSPORT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN BELI PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN KILANG PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        //PENGGANTIAN MENGERING PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN BORONG
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN IMPORT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN EKSPORT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN BELI PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN KILANG PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //PENGGANTIAN MENGERING PADI
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan Lesen Penjaja (Sabah) / I.R.D No. 7 (Sarawak) / Salinan Sijil Pendaftaran Jenis Syarikat Koperasi atau Pertubuhan',
            'code'      =>  'SSM',
            'domain'    =>  'PENGGANTIAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //RUNCIT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_RUNCIT_SYARIKAT',
            'order'     =>  1
        ]);

        //BORONG PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_BORONG_SYARIKAT',
            'order'     =>  1
        ]);

        //IMPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_IMPORT_SYARIKAT',
            'order'     =>  1
        ]);

        //EXPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_EKSPORT_SYARIKAT',
            'order'     =>  1
        ]);

        //KILANG PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_KILANG_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         //BELI PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_BELI_PADI_SYARIKAT',
            'order'     =>  1
        ]);

         //MENGERING PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        //RUNCIT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_RUNCIT_KOPERASI',
            'order'     =>  1
        ]);

        //BORONG PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_BORONG_KOPERASI',
            'order'     =>  1
        ]);

        //IMPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_IMPORT_KOPERASI',
            'order'     =>  1
        ]);

        //EXPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_EKSPORT_KOPERASI',
            'order'     =>  1
        ]);

        //KILANG PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_KILANG_PADI_KOPERASI',
            'order'     =>  1
        ]);

         //BELI PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_BELI_PADI_KOPERASI',
            'order'     =>  1
        ]);

         //MENGERING PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'PEMBATALAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        //MENGERING PADI BAHARU
        Material::create([
            'label'     =>  'Salinan No. Kad Pengenalan',
            'code'      =>  'NRIC',
            'domain'    =>  'MENGERING_PADI_BAHARU_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan lesen penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'MENGERING_PADI_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'MENGERING_PADI_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Geran Tanah',
            'code'      =>  'LAND_GRANT',
            'domain'    =>  'MENGERING_PADI_BAHARU_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan No. Kad Pengenalan',
            'code'      =>  'NRIC',
            'domain'    =>  'MENGERING_PADI_BAHARU_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan lesen penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'MENGERING_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'MENGERING_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Geran Tanah',
            'code'      =>  'LAND_GRANT',
            'domain'    =>  'MENGERING_PADI_BAHARU_KOPERASI',
            'order'     =>  2
        ]);

        //PEMBAHARUAN MENGERING PADI
        Material::create([
            'label'     =>  'Salinan No. Kad Pengenalan',
            'code'      =>  'NRIC',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan lesen penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Geran Tanah',
            'code'      =>  'LAND_GRANT',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_SYARIKAT',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan No. Kad Pengenalan',
            'code'      =>  'NRIC',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM / Salinan lesen penjaja (Sabah) / I.R.D No. 7 (Sarawak)',
            'code'      =>  'SSM',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/ sewaan stor dan premis atau Salinan Pemilikan Kilang atau Salinan Kebenaran Menggunakan Tapak Kilang',
            'code'      =>  'SALE_AGREEMENT',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan Geran Tanah',
            'code'      =>  'LAND_GRANT',
            'domain'    =>  'PEMBAHARUAN_MENGERING_PADI_KOPERASI',
            'order'     =>  2
        ]);
    }
}
