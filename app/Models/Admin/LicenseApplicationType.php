<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationType extends Model
{
    public $guarded = ['id'];
}
