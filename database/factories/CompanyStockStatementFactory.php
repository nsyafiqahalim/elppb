<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\ReferenceData\CompanyStockStatement;
use App\Models\Admin\StockCategory;
use App\Models\Admin\SpinType;
use App\Models\Admin\TotalGrade;
use App\Models\Admin\RiceGrade;
use App\Models\Admin\StockType;

$stockCategories    = StockCategory::all();
$spinTypes          = SpinType::all();
$totalGrades        = TotalGrade::all();
$riceGrades         = RiceGrade::all();
$stockTypes         = StockType::all();

$factory->define(CompanyStockStatement::class, function (Faker $faker) use ($stockCategories, $spinTypes, $totalGrades, $riceGrades, $stockTypes) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));

    $sc = $stockCategories->random();
    $spt = $spinTypes->random();
    $tg = $totalGrades->random();
    $rg = $riceGrades->random();
    $st =   $stockTypes->random();

    $initial = rand(10,200);
    $sell = rand(10,100);
    $buy = rand(10,300);
    $balance = $initial + $buy - $sell;

    return [
            'stock_category_id'                     =>  $sc->id,
            'stock_type_id'                         =>  $st->id,
            'spin_type_id'                          =>  $spt->id,
            'total_grade_id'                        =>  $tg->id,
             //'company_id'                         =>  $p->id,
            //'license_type_id'                     =>  $p->id,
            'rice_grade_id'                         =>  $rg->id,
            //'stock_statement_period_id'             =>  $faker->township,
            //'company_stock_statement_account_id'    =>  $faker->townState,
            'initial_stock'                         =>  $initial,
            'total_buy'                             =>  $buy,
            'buying_price'                          =>  mt_rand(0,100),
            'total_spin'                            =>  mt_rand(0,200),
            'stock_balance'                         =>  $balance,
            'spin_result'                           =>   mt_rand(0,200),
            'total_sell'                            =>  $sell,
            'selling_price'                         =>  mt_rand(0,200),
    ];
});
