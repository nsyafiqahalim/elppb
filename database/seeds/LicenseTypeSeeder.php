<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseType;

use App\Models\Admin\ApprovalLetterType;

class LicenseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Import',
          'Eksport',
          'Borong',
          'Borong(Kilang)',
          'Runcit',
          'Membeli Padi',
          'Kilang Padi Komersil',
      ];

        $shortCode = [
          'LI',
          'LE',
          'LB',
          'LB(K)',
          'LR',
          'LMP',
          'LKPK',
      ];

        $code = [
          'A',
          'B',
          'C',
          'CF',
          'D',
          'E',
          'F',
      ];

      
        foreach ($data as $index => $item) {
            LicenseType::create([
              'name'  =>  $item,
              'short_code'  =>  $shortCode[$index],
              'code'  =>  $code[$index],
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
        }

        $data = [
          'Membeli Padi',
          'Kilang Padi Komersil',
      ];

        $shortCode = [
          'LMP',
          'LKPK',
      ];

        $code = [
          'E',
          'F',
      ];

      
        foreach ($data as $index => $item) {
            ApprovalLetterType::create([
              'name'  =>  $item,
              'short_code'  =>  $shortCode[$index],
              'code'  =>  $code[$index],
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
        }
    }
}
