<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\PermitTypeDataObject;

class PermitTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addPermitType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $permitType = PermitTypeDataObject::addPermitType($param);

        return response()->json([
            'message'   =>  'Jenis permit berjaya ditambah',
            'data'      =>  $permitType,
            'success'   =>  true,
            'route'     =>  route('admin.permit_type.index')
        ], 200);
    }

    public function updatePermitType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $permitType = PermitTypeDataObject::updatePermitType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis permit berjaya dikemaskini',
            'data'      =>  $permitType,
            'success'   =>  true,
            'route'     =>  route('admin.permit_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return PermitTypeDataObject::findAllPermitTypeUsingDatatableFormat($param);
    }
}
