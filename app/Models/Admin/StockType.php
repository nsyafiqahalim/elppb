<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class StockType extends Model
{
    public $guarded = ['id'];

    public $table = 'stock_types';
}
