<?php

namespace App\Http\Controllers\PermitApplication;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function index()
    {
        return view('permit-application.index');
    }
}
