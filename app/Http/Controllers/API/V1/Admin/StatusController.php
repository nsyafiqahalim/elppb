<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\StatusDataObject;

class StatusController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addStatus(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $status = StatusDataObject::addStatus($param);

        return response()->json([
            'message'   =>  'Status berjaya ditambah',
            'data'      =>  status,
            'success'   =>  true,
            'route'     =>  route('admin.status.index')
        ], 200);
    }

    public function updateStatus(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $status = StatusDataObject::updateStatus($param, $decryptedId);

        return response()->json([
            'message'   =>  'Status berjaya dikemaskini',
            'data'      =>  status,
            'success'   =>  true,
            'route'     =>  route('admin.status.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return StatusDataObject::findAllStatusUsingDatatableFormat($param);
    }
}
