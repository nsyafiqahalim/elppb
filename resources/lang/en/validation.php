<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute perlu diterima.',
    'active_url' => ':attribute bukan URL yang sah.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => ':attribute mestilah tarikh selepas atau sama dengan :date.',
    'alpha' => ':attribute hanya boleh mengandungi huruf.',
    'alpha_dash' => ':attribute hanya boleh mengandungi huruf, nombor, garis tengah dan garis bawah.',
    'alpha_num' => ':attribute hanya boleh mengandungi huruf dan nombor.',
    'array' => ':attribute mestilah dalam susunan teratur.',
    'before' => ':attribute mestilah tarikh sebelum :date.',
    'before_or_equal' => ':attribute mestilah tarikh sebelum atau sama dengan :date.',
    'between' => [
        'numeric' => ':attribute diantara :min dan :max.',
        'file' => ':attribute diantara :min dan :max kilobytes.',
        'string' => ':attribute diantara :min dan :max karakter.',
        'array' => ':attribute mestilah mempunyai antara :min dan :max item.',
    ],
    'boolean' => 'Medan :attribute mesti betul atau salah.',
    'confirmed' => ':attribute pengesahan tidak sepadan.',
    'date' => ':attribute bukan tarikh yang sah.',
    'date_equals' => ':attribute mestilah tarikh yang sama dengan :date.',
    'date_format' => ':attribute tidak sepadan dengan format :format.',
    'different' => ':attribute dan :other mestilah berlainan.',
    'digits' => ':attribute perlu dalam :digits digit.',
    'digits_between' => ':attribute diantara :min dan :max digit.',
    'dimensions' => ':attribute mempunyai dimensi imej yang tidak sah.',
    'distinct' => 'Medan :attribute mempunyai nilai pendua.',
    'email' => ':attribute mesti alamat e-mel yang sah. Contoh: contoh@contoh.com.',
    'ends_with' => ':attribute mesti berakhir dengan salah satu daripada yang berikut: :values',
    'exists' => ':attribute yang diplih tidak sah.',
    'file' => ':attribute mesti dalam format fail.',
    'filled' => 'Medan :attribute mesti mempunyai nilai.',
    'gt' => [
        'numeric' => 'The :attribute must be greater than :value.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'string' => 'The :attribute must be greater than :value characters.',
        'array' => 'The :attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => ':attribute mesti lebih besar atau sama dengan :value.',
        'file' => ':attribute mesti lebih besar atau sama dengan :value kilobytes.',
        'string' => ':attribute mesti lebih besar atau sama dengan :value karakter.',
        'array' => ':attribute perlu mempunyai :value item atau lebih.',
    ],
    'image' => ':attribute dalam format imej.',
    'in' => ':attribute yang dipilih tidak sah.',
    'in_array' => 'Medan :attribute tidak wujud dalam :other.',
    'integer' => ':attribute ini perlu dalam format integer.',
    'ip' => ':attribute ini mestilah dalam alamat IP yang sah.',
    'ipv4' => ':attribute ini mestilah dalam alamat IPv4 yang sah.',
    'ipv6' => ':attribute ini mestilah dalam alamat IPv6 yang sah.',
    'json' => ':attribute perlu dalam format JSON yang sah.',
    'lt' => [
        'numeric' => ':attribute mesti kurang daripada :value.',
        'file' => ':attribute mesti kurang daripada :value kilobytes.',
        'string' => ':attribute mesti kurang daripada :value karakter.',
        'array' => ':attribute mesti kurang daripada :value item.',
    ],
    'lte' => [
        'numeric' => ':attribute mesti kurang atau lebih :value.',
        'file' => ':attribute mesti kurang atau lebih :value kilobytes.',
        'string' => ':attribute mesti kurang atau lebih :value karakter.',
        'array' => ':attribute mestilah tidak lebih daripada :value item.',
    ],
    'max' => [
        'numeric' => ':attribute mestilah tidak lebih daripada :max.',
        'file' => ':attribute mestilah tidak lebih daripada :max kilobyte.',
        'string' => ':attribute mestilah tidak lebih daripada :max karakter.',
        'array' => ':attribute mestilah tidak lebih daripada :max item.',
    ],
    'mimes' => ':attribute mesti dalam jenis fail: :values.',
    'mimetypes' => ':attribute mesti dalam jenis fail: :values.',
    'min' => [
        'numeric' => ':attribute mestilah kurang daripada :min.',
        'file' => ':attribute mestilah kurang daripada :min kilobyte.',
        'string' => ':attribute mestilah kurang daripada :min karakter.',
        'array' => ':attribute mestilah kurang daripada :min item.',
    ],
    'not_in' => ':attribute yang dipilih tidak sah.',
    'not_regex' => ':attribute ini mempunyai format tidak sah.',
    'numeric' => ':attribute mestilah dalam angka.',
    'present' => ':attribute perlu ada.',
    'regex' => ':attribute mempunyai format tidak sah.',
    'required' => 'Medan :attribute diperlukan.',
    'required_if' => 'Medan :attribute diperlukan apabila :other adalah :value.',
    'required_unless' => 'Medan :attribute diperlukan kecuali :other adalah dalam :values.',
    'required_with' => 'Medan :attribute diperlukan apabila :values ada.',
    'required_with_all' => 'Medan :attribute diperlukan apabila :values ada.',
    'required_without' => 'Medan :attribute diperlukan apabila :values tiada.',
    'required_without_all' => 'Medan :attribute apabila tiada :values.',
    'same' => ':attribute dan :other mesti sepadan.',
    'size' => [
        'numeric' => ':attribute perlu :size.',
        'file' => ':attribute perlu :size kilobytes.',
        'string' => ':attribute perlu :size karakter.',
        'array' => ':attribute perlu mengandungi :size item.',
    ],
    'starts_with' => ':attribute perlu bermula dengan salah satu daripada yang berikut: :values',
    'string' => ':attribute dalam format string.',
    'timezone' => ':attribute dalam format zon yang sah.',
    'unique' => ':attribute telah diambil. Sila cuba yang lain.',
    'uploaded' => ':attribute gagal untuk dimuatnaik. Sila cuba sekali lagi.',
    'url' => ':attribute dalam format yang tidak sah. Sila pastikan URL anda dalam format yang betul.',
    'uuid' => ':attribute mesti UUID yang sah.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
