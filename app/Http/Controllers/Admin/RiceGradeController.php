<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RiceGradeDataObject;

class RiceGradeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.rice-grade.index');
    }

    public function add()
    {
        return view('admin.rice-grade.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $riceGrade = RiceGradeDataObject::findRiceGradeById($decryptedId);

        return view('admin.rice-grade.edit', compact('riceGrade'));
    }
}
