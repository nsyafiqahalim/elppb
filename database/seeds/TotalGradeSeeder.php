<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\TotalGrade;

class TotalGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '5%',
            '10%',
            '15%',
            'Hancur'
        ];

        $code = [
            '5_PERCENT',
            '10_PERCENT',
            '15_PERCENT',
            'HANCUR'
        ];

      foreach ($data as $index  => $item) {
          TotalGrade::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $code[$index])),
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
      }
    }
}
