<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyOldRegistrationNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function ($table) {
            $table->string('old_registration_number')->nullable();
        });

        Schema::table('license_application_companies', function ($table) {
            $table->string('old_registration_number')->nullable();
        });

        Schema::table('approval_letter_application_companies', function ($table) {
            $table->string('old_registration_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
