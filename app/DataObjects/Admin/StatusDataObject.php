<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Status;

class StatusDataObject
{
    public static function addStatus($param)
    {
        $status = null;

        DB::transaction(function () use ($param, &$status) {
            $status = Status::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $status;
    }

    public static function updateStatus($param, $id)
    {
        $status = null;

        DB::transaction(function () use ($param, $id,  &$status) {
            $status = Status::find($id);

            $status->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $status;
    }

    public static function findAllStatusUsingDatatableFormat()
    {
        return datatables()->of(Status::query())
        ->addColumn('action', function ($status) {
            return view('admin.status.partials.datatable-button', compact('status'))->render();
        })
        ->addColumn('status', function ($status) {
            return ($status->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })->make(true);
    }

    public static function findStatusById($id)
    {
        return Status::find($id);
    }
}
