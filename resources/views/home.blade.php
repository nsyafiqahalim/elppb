@extends('layouts.main-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-home"></i> Dashboard</h3>
        <ol class="breadcrumb">
        </ol>
    </div>
</div>

<div class="row">
    <!-- Column -->
    <div class="col-xs-6 col-md-4">
        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">2376 <i class="ti-angle-down font-14 text-danger"></i></span>
                        <h6>Jumlah Pelawat</h6>
                    </div>
                    <div class="col-4 align-self-center text-right  pl-0">
                        <div id="sparklinedash3"><canvas width="51" height="50" style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-xs-6 col-md-4">
        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">3670 <i class="ti-angle-up font-14 text-success"></i></span>
                        <h6>Halaman Dikunjungi</h6>
                    </div>
                    <div class="col-4 align-self-center text-right pl-0">
                        <div id="sparklinedash"><canvas width="51" height="50" style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
    <div class="col-xs-6 col-md-4">
        <div class="card">
            <div class="card-body">
                <!-- Row -->
                <div class="row">
                    <div class="col-8"><span class="display-6">1562 <i class="ti-angle-up font-14 text-success"></i></span>
                        <h6>Pengguna Baru</h6>
                    </div>
                    <div class="col-4 align-self-center text-right pl-0">
                        <div id="sparklinedash2"><canvas width="51" height="50" style="display: inline-block; width: 51px; height: 50px; vertical-align: top;"></canvas></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Column -->
</div>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-8">
        <div class="card">
            <div class="card-body">
                <button class="float-right btn btn-sm btn-rounded btn-success" data-toggle="modal" data-target="#myModal">Tambah Tugasan</button>
                <h4 class="card-title">Senarai Tugasan</h4>
                <!-- ============================================================== -->
                <!-- To do list widgets -->
                <!-- ============================================================== -->
                <div class="to-do-widget mt-3">
                    <!-- .modal for add task -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Tambah Tugasan</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label>Tajuk</label>
                                            <input type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label>Butiran</label>
                                            <textarea class="form-control" rows="5" spellcheck="false"></textarea>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                    <button type="button" class="btn btn-success" data-dismiss="modal">Hantar</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <ul class="list-task todo-list list-group mb-0" data-role="tasklist">
                        <li class="list-group-item" data-role="task">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="inputForward" name="inputCheckboxesForward">
                                <label for="inputForward" class=""> <span>Hantar semua tugasan kepada Ketua HQ</span> <span class="label label-light-warning">2 bulan</span> </label>
                            </div>
                            <div class="item-date"> 26 Jun 2020</div>
                        </li>
                        <li class="list-group-item" data-role="task">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="inputRecieve" name="inputCheckboxesRecieve">
                                <label for="inputRecieve" class=""> <span>Menerima penghantaran beras</span> <span class="label label-light-danger">Hari ini</span></label>
                            </div>
                            <div class="item-date"> 26 April 2020</div>
                        </li>
                        <li class="list-group-item" data-role="task">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="inputpayment" name="inputCheckboxespayment">
                                <label for="inputpayment" class=""> <span>Membuat pembayaran lesen</span> </label>
                            </div>
                            <div class="item-date"> 14 Mei 2020</div>
                        </li>
                        <li class="list-group-item" data-role="task">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="inputForward2" name="inputCheckboxesd">
                                <label for="inputForward2" class=""> <span>Tugasan Penting</span> <span class="label label-light-success">2 minggu</span> </label>
                            </div>
                        </li>
                        <li class="list-group-item" data-role="task">
                            <div class="checkbox checkbox-info">
                                <input type="checkbox" id="inputForward2" name="inputCheckboxesd">
                                <label for="inputForward2" class=""> <span>Cuti Tahunan</span></label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div> <!-- end To do List -->

    <div class="col-xs-6 col-md-4">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Statistik Pelayar Web</h4>
          <table class="table browser mt-4 no-border">
            <tbody>
              <tr>
                <td style="width:40px"><img src="../assets/images/browser/chrome-logo.png" alt="logo"></td>
                <td>Google Chrome</td>
                <td class="text-right"><span class="label label-light-info">65%</span></td>
              </tr>
              <tr>
                <td><img src="../assets/images/browser/edge.png" alt="logo"></td>
                <td>Microsoft Edge</td>
                <td class="text-right"><span class="label label-light-success">14%</span></td>
              </tr>
              <tr>
                <td><img src="../assets/images/browser/firefox-logo.png" alt="logo"></td>
                <td>Mozila Firefox</td>
                <td class="text-right"><span class="label label-light-primary">12%</span></td>
              </tr>
              <tr>
                <td><img src="../assets/images/browser/opera-logo.png" alt="logo"></td>
                <td>Opera Mini</td>
                <td class="text-right"><span class="label label-light-warning">2%</span></td>
              </tr>
              <tr>
                <td><img src="../assets/images/browser/safari-logo.png" alt="logo"></td>
                <td>Safari</td>
                <td class="text-right"><span class="label label-light-danger">2%</span></td>
              </tr>
              <tr>
                <td><img src="../assets/images/browser/internet-logo.png" alt="logo"></td>
                <td>Internet Explorer</td>
                <td class="text-right"><span class="label label-light-megna">5%</span></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div> <!-- end analytic browser -->
</div> <!-- End 2nd Row -->
@endsection

@push('css')
<style typw="text/css">
    .main-menu {
        width: 100%;
        height: 25vh;
    }
</style>
@endpush

@push('js')
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-85622565-1', 'auto');
    ga('send', 'pageview');
</script>
@endpush
