<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(StockCategorySeeder::class);
        $this->call(DurationSeeder::class);
        $this->call(licenseApplicationLicenseGenerationTypeSeeder::class);
        $this->call(licenseApplicationMaterialSeeder::class);
        $this->call(LicenseApplicationCategorySeeder::class);
        $this->call(ApprovalLetterApplicationMaterialSeeder::class);
        $this->call(licenseApplicationCancellationSeeder::class);
        $this->call(licenseApplicationReplacementSeeder::class);
        $this->call(licenseApplicationChangeTypeSeeder::class);
        $this->call(LicenseApplicationBuyPaddyConditionSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ZoneSeeder::class);
        $this->call(RiceGradeSeeder::class);
        $this->call(StateSeeder::class);
        $this->call(DistrictSeeder::class);
        $this->call(ParlimentSeeder::class);
        $this->call(DunSeeder::class);
        $this->call(BranchSeeder::class);
        $this->call(AreaCoverageSeeder::class);
        $this->call(BuildingTypeSeeder::class);
        $this->call(BusinessTypeSeeder::class);
        $this->call(CompanyTypeSeeder::class);
        $this->call(PremiseTypeSeeder::class);
        $this->call(RaceTypeSeeder::class);
        $this->call(RaceSeeder::class);
        $this->call(OwnershipTypeSeeder::class);
        $this->call(StockStatementPeriodSeeder::class);
        $this->call(StoreOwnershipTypeSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(SpinTypeSeeder::class);
        $this->call(TotalGradeSeeder::class);
        $this->call(StockTypeSeeder::class);
        $this->call(ApprovalLetterStatusSeeder::class);
        $this->call(SettingSeeder::class);
        $this->call(LandTypeSeeder::class);
        $this->call(FactoryTypeSeeder::class);
        $this->call(StoreTypeSeeder::class);
        $this->call(LicenseTypeSeeder::class);
        $this->call(LandStatusSeeder::class);
        $this->call(RiceCategorySeeder::class);
        $this->call(PartnerTypeSeeder::class);
        $this->call(PermitTypeSeeder::class);
        $this->call(LicenseApplicationTypeSeeder::class);
        $this->call(ZoneSeeder::class);
        $this->call(UserProfileSeeder::class);

        //portal
        $this->call(PortalFaqSeeder::class);
        $this->call(PortalAnnouncementSeeder::class);
        $this->call(PortalLicenseSeeder::class);
        $this->call(PortalSettingSeeder::class);
    }
}
