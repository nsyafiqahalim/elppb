<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Portal\FaqDataObject;

class FaqController extends Controller
{
    public function index()
    {
        return view('portal.faq.index');
    }

    public function add()
    {
        return view('portal.faq.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        $faqDAO = FaqDataObject::findFaqById($decryptedId);

        return view('portal.faq.edit', compact('faqDAO'));
    }
}
