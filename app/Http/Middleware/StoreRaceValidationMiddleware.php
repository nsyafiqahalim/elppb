<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreRaceValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'code' => ['required', 'string', 'max:6'],
            'description' => ['required', 'string']
        ], [
            'name.required' => ['Medan Nama diperlukan'],
            'code.required' => ['Medan Kod diperlukan'],
            'description.required' => ['Medan Keterangan diperlukan'],
        ]);

        return $next($request);
    }
}
