<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RoleDataObject;

class RoleController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRole(Request $request)
    {
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $role = RoleDataObject::addRole($param);

        return response()->json([
            'message'   =>  'Peranan berjaya ditambah',
            'data'      =>  $role,
            'success'   =>  true,
            'route'     =>  route('admin.role.index')
        ], 200);
    }

    public function updateRole(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $role = RoleDataObject::updateRole($param, $decryptedId);

        return response()->json([
            'message'   =>  'Peranan berjaya dikemaskini',
            'data'      =>  $role,
            'success'   =>  true,
            'route'     =>  route('admin.role.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RoleDataObject::findAllRoleUsingDatatableFormat($param);
    }
}
