@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Senarai Liputan Kawasan</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Senarai Liputan Kawasan</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'>Carian</b>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <div class="alert alert-warning">
                        Pilihan carian ini tidak wajib
                    </div>
                    <form class="form" id="filter-role">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' class="form-control" placeholder="Nama" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' class="form-control" placeholder="Kod" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="icon-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                <div class="m-t-40">
                    <a href="{{ route('admin.area-coverage.add') }}">
                        <button type="submit" class="btn btn-success float-right">
                            <i class="icon-plus"></i> Tambah
                        </button>
                    </a>
                    <table id="role-datatable" class="table display table-bordered table-striped table-responsived" style='text-align:center'>
                        <thead>
                            <tr style="text-align:center">
                                <th style="vertical-align:middle">Bil.</th>
                                <th style="vertical-align:middle">Nama</th>
                                <th style="vertical-align:middle">Kod</th>
                                <th style="vertical-align:middle">Status</th>
                                <th style="vertical-align:middle" width="300">Tindakan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables-rowReorder.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables-responsive.css') }}">
@endpush

@push('js')
<script src="{{ asset('DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('DataTables/datatables-rowReorder.min.js') }}"></script>
<script src="{{ asset('DataTables/datatables-responsive.min.js') }}"></script>
<script>
    $("#filter-role").submit(function(event) {
        event.preventDefault();
        form = $('#filter-role').serializeArray();
        formdata = {}
        for (x = 0; x < form.length; x++) {
            formdata[form[x].name] = form[x].value;
        }
        $('#role-datatable').DataTable().destroy();
        $('#role-datatable').DataTable(
            loadConfig(formdata)
        );
    });

    $('#role-datatable').DataTable(
        loadConfig()
    );

    function loadConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.admin.area-coverage.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'Nama'
                },
                {
                    data: 'code',
                    name: 'Kod'
                },
                {
                    data: 'status',
                    name: 'Status'
                },
                {
                    data: 'action',
                    name: 'Tindakan'
                },
            ]

        };

        return json;
    }
</script>
@endpush
