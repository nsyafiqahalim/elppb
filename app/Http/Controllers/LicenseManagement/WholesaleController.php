<?php

namespace App\Http\Controllers\LicenseManagement;

use App\Models\LicenseApplication;
use App\Models\LicenseActivity;
use App\Models\LicenseRemark;
use App\Models\LicenseReport;
use App\Models\LicenseAttachment;
use App\Models\UserProfile;
use App\Models\Admin\Status;
use App\Models\LicenseApplication\CompanyStoreIncludeLicenseItem;
use App\Http\Controllers\Controller;
use Auth;
use PDF\PDF;
use Response;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class WholesaleController extends Controller
{
    // view for maklumat pengurusan Lesen// tugasan saya dashboard
    public function showWholesale($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_reports = LicenseReport::where('license_application_id', $decryptedId)->get()->toArray();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        if (Auth::user()->can("Semakan Siasatan Permohonan Lesen Borong (Cawangan)")) {
              return view('license-management.wholesale.new_2-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
        }

        return view('license-management.wholesale.new-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items ]);
    }

    public function showWholesaleRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_reports = LicenseReport::where('license_application_id', $decryptedId)->get()->toArray();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        if (Auth::user()->can("Semakan Siasatan Permohonan Lesen Borong (Cawangan)")) {
              return view('license-management.wholesale.renew_2-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
        }

        return view('license-management.wholesale.renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
    }

    public function showWholesaleChange($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_reports = LicenseReport::where('license_application_id', $decryptedId)->get()->toArray();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        if (Auth::user()->can("Semakan Siasatan Permohonan Lesen Borong (Cawangan)")) {
              return view('license-management.wholesale.change_2-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
        }

        return view('license-management.wholesale.change-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
    }

    public function showWholesaleReplacement($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        return view('license-management.wholesale.replacement-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_company_store' => $license_company_store_include_items]);
    }

    public function showWholesaleCancel($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        return view('license-management.wholesale.cancel-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_company_store' => $license_company_store_include_items]);
    }

    public function showWholesaleChangeRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_reports = LicenseReport::where('license_application_id', $decryptedId)->get()->toArray();
        $license_company_store_include_items = CompanyStoreIncludeLicenseItem::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        if (Auth::user()->can("Semakan Siasatan Permohonan Lesen Borong (Cawangan)")) {
              return view('license-management.wholesale.new-renew_2-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
        }

        return view('license-management.wholesale.new-renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_reports' => $license_reports, 'pegawai_siasatan' => $pegawai_siasatan, 'license_company_store' => $license_company_store_include_items]);
    }

    // edit for maklumat pengurusan Lesen// tugasan saya dashboard
    public function editWholesale(Request $request, $id)
    {
        $decryptedId = decrypt($id);

        $license = LicenseApplication::find($decryptedId);
        $status = Status::where('code', $request->status)->get('id');
        $license->status_id = $status[0]->id;
        if (!is_null($request->investigation_officer)) {
          $license->license_investigation_officer_id = $request->investigation_officer;
        }
        $license->save();

        // licence application remark
        $license_remark = new LicenseRemark();
        $license_remark->status_id = $status[0]->id;
        $license_remark->remarks = $request->remark;
        $license_remark->user_id = Auth::user()->id;
        $license_remark->license_application_id = $license->id;
        $license_remark->save();

        // licence application activities
        if ($request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BORONG_BAHARU' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BORONG_PEMBAHARUAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BORONG_PERUBAHAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Pemilihan Pegawai Siasatan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_SIASATAN_BORONG_BAHARU' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BORONG_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BORONG_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_BORONG_PEMBATALAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_BORONG_PENGGANTIAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit Cawangan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BORONG_BAHARU' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BORONG_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BORONG_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit HQ";
        } elseif ($request->status == 'KELULUSAN_PERMOHONAN_BORONG_BAHARU' || $request->status == 'KELULUSAN_PERMOHONAN_BORONG_PEMBAHARUAN' || $request->status == 'KELULUSAN_PERMOHONAN_BORONG_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_BORONG_PENGGANTIAN' || $request->status == 'KELULUSAN_PERMOHONAN_BORONG_PEMBATALAN' ) {
          $activity = "Hantar Semakan Ke Ketua Pengarah";
        } elseif ($request->status == 'JANAAN_LESEN_BORONG_BAHARU' || $request->status == 'JANAAN_LESEN_BORONG_PEMBAHARUAN' || $request->status == 'JANAAN_LESEN_BORONG_PERUBAHAN' || $request->status == 'JANAAN_LESEN_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'JANAAN_LESEN_BORONG_PENGGANTIAN' || $request->status == 'JANAAN_LESEN_BORONG_PEMBATALAN' ) {
          $activity = "Permohonan Dilulus";
        } elseif ($request->status == 'TOLAK_BORONG_BAHARU' || $request->status == 'TOLAK_BORONG_PEMBAHARUAN' || $request->status == 'TOLAK_BORONG_PERUBAHAN' || $request->status == 'TOLAK_BORONG_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'TOLAK_BORONG_PENGGANTIAN' || $request->status == 'TOLAK_BORONG_PEMBATALAN' ) {
          $activity = "Permohonan Ditolak";
        }

        $license_activity = new LicenseActivity();
        $license_activity->status_id = $status[0]->id;
        $license_activity->activity = $activity;
        $license_activity->user_id = Auth::user()->id;
        $license_activity->license_application_id = $license->id;
        $license_activity->save();

        return response()->json([
            'message'   =>  'Permohonan berjaya dikemaskini',
            'success'   =>  true,
            'route'     => route('management.mytask-index')
        ], 200);
      }

      public function investigationWholesale(Request $request, $id)
      {
          $decryptedId = decrypt($id);

          $license = LicenseApplication::find($decryptedId);
          $status = Status::where('code', $request->status)->get('id');
          $license->status_id = $status[0]->id;
          $license->save();

          // licence application remark
          $license_remark = new LicenseRemark();
          $license_remark->status_id = $status[0]->id;
          $license_remark->remarks = "Laporan Dilakukan";
          $license_remark->user_id = Auth::user()->id;
          $license_remark->license_application_id = $license->id;
          $license_remark->save();

          $license_activity = new LicenseActivity();
          $license_activity->status_id = $status[0]->id;
          $license_activity->activity = "Laporan Dilakukan";
          $license_activity->user_id = Auth::user()->id;
          $license_activity->license_application_id = $license->id;
          $license_activity->save();

          $report = LicenseReport::where('license_application_id', $license->id)->get()->toArray();

          if (count($report) > 0) {
              $license_report = LicenseReport::find($report[0]['id']);
              $license_report->report = $request->report;
              $license_report->user_id = Auth::user()->id;
              $license_report->license_application_id = $license->id;
              $license_report->save();
          } else {
              $license_report = new LicenseReport();
              $license_report->report = $request->report;
              $license_report->user_id = Auth::user()->id;
              $license_report->license_application_id = $license->id;
              $license_report->save();
          }

          return response()->json([
              'message'   =>  'Laporan berjaya dihantar',
              'success'   =>  true,
              'route'     => route('management.mytask-index')
          ], 200);
        }

        public function downloadWholesaleLicense(Request $request, $id)
        {
            $decryptedId = decrypt($id);
            $license = LicenseApplication::find($decryptedId);
            $status = Status::where('code', $request->status)->get('id');

            if ($license->status_id != $status[0]->id) {
                $license->status_id = $status[0]->id;
                $license->save();

                $license_remark = new LicenseRemark();
                $license_remark->status_id = $status[0]->id;
                $license_remark->remarks = 'Janaan Lesen';
                $license_remark->user_id = Auth::user()->id;
                $license_remark->license_application_id = $license->id;
                $license_remark->save();

                $license_activity = new LicenseActivity();
                $license_activity->status_id = $status[0]->id;
                $license_activity->activity = 'Janaan Lesen';
                $license_activity->user_id = Auth::user()->id;
                $license_activity->license_application_id = $license->id;
                $license_activity->save();
            }

            $pdf = \PDF::loadView('license-management.generate-license.wholesale-license', ['license' => $license]);
            return $pdf->download('Janaan-Lesen-Borong.pdf');
        }

        public function showSSM($id)
        {
            $decryptedId = decrypt($id);
            $license_attachement_ssm = LicenseAttachment::where('code', 'SSM')->where('license_application_id', $decryptedId)->get();

            $file_name = $license_attachement_ssm[0]->file_path;
            $path = env('FILE_URL').'/'.$file_name;

            $file_type = mime_content_type($path);

            return Response::make(file_get_contents($path), 200, [
                  'Content-Type' => $file_type,
                  'Content-Disposition' => 'inline; filename="'.$file_name.'"'
            ]);
        }

        public function showLesenPerniagaan($id)
        {
            $decryptedId = decrypt($id);
            $license_attachement_ssm = LicenseAttachment::where('code', 'MINUTE_MEETING')->where('license_application_id', $decryptedId)->get();

            $file_name = $license_attachement_ssm[0]->file_path;
            $path = env('FILE_URL').'/'.$file_name;

            $file_type = mime_content_type($path);

            return Response::make(file_get_contents($path), 200, [
                  'Content-Type' => $file_type,
                  'Content-Disposition' => 'inline; filename="'.$file_name.'"'
            ]);
        }
}
