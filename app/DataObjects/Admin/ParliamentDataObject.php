<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Parliament;

class ParliamentDataObject
{
    public static function addParliament($param)
    {
        $parliament = null;

        DB::transaction(function () use ($param, &$parliament) {
            $parliament = Parliament::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'state_id'     =>  $param['state_id'],
                'description'     =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $parliament;
    }

    public static function updateParliament($param, $id)
    {
        $parliament = null;

        DB::transaction(function () use ($param, $id,  &$parliament) {
            $parliament = Parliament::find($id);

            $parliament->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'state_id'     =>  $param['state_id'],
                'description'     =>  $param['description'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $parliament;
    }

    public static function findAllParliamentUsingDatatableFormat($param)
    {
        $parliaments = Parliament::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($parliaments)
        ->addIndexColumn()
        ->addColumn('action', function ($parliament) {
            return view('admin.parliament.partials.datatable-button', compact('parliament'))->render();
        })
        ->addColumn('status', function ($parliament) {
            $status= ($parliament->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.parliament.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findParliamentById($id)
    {
        return Parliament::find($id);
    }

    public static function findAllActiveParliaments()
    {
        return Parliament::activeOnly()->get();
    }
}
