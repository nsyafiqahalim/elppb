@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-cogs"></i> Penetapan Sistem</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('admin.index') }}">Pentadbiran Sistem</a></li>
            <li class="breadcrumb-item active"><i class="fas fa-cogs"></i> Penetapan</li>
            <li class="breadcrumb-item active">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Penetapan</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form url-encoded-submission-without-clear-field" method="post" action="{{ route('api.admin.setting.update',[encrypt($settingDomain)]) }}">
                        @method('PATCH')
                        <div class="form-body">
                            <div class="row">
                                @foreach($settings as $setting)
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="{{ $setting->code }}_label">{{ $setting->label }} <small>[ Format: {{ $setting->metric }} ]</small></label>
                                        @if($setting->class == 'time')
                                            <div class="input-group {{ $setting->class }}" data-placement="bottom" data-align="top" data-autoclose="true">
                                                <input type="text" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}" name="{{ $setting->code }}" value="{{ $setting->value }}"
                                                  @if($setting->readonly == "true") readonly @endif>
                                                    <span class="input-group-addon">
                                                        <span class="icon-clock"></span>
                                                    </span>
                                            </div>
                                        @elseif(isset($setting->table))
                                        <select class="form-control" name="{{ $setting->code }}">
                                            @foreach($models[$setting->table] as $item)
                                            <option
                                                @if($item->value == $setting->value)
                                                    selected
                                                @endif
                                            value="{{ $item->id }}" >{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        @else
                                            <input type="text" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}" name="{{ $setting->code }}" value="{{ $setting->value }}" <blade
                                              @if($setting->readonly == "true") readonly @endif>
                                        @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Hantar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
