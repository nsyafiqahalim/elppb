<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8">
    <!-- <title>Invoice Report - </title> -->

    <style type="text/css">
        @page {
            margin: 2cm;
        }
        header {
            position: fixed;
            top: -80px;
            left: 0px;
            right: 0px;
            height: 30px;
        }
        footer {
            position: fixed;
            bottom: 0px;
            left: 0px;
            right: 0px;
            height: 10px;
            /* text-align: left; */
            /* line-height: normal; */
        }
        body {
               font-size:10;
               font-family: Calibri, sans-serif;
        }
    </style>

</head>
<body>
      <header>
          <table style="width: 100%;">
                <tr>
                    <td width="10%" valign="top"><h4>PENDUA</h4></td>
                    <td width="10%" align="center"><h4>BORANG B</h4>
                      <h4>(Peraturan 4)</h4></td>
                    <td width="10%"> </td>
                </tr>
          </table>
      </header>

      <h4 align="right">No. Siri:</h4>

      <table>
            <tr>
                <td></td>
                <td width="60%">
                    <center>
                        <h4>AKTA KAWALAN PADI DAN BERAS 1994 PERATURAN-PERATURAN KAWALAN PADI DAN BERAS (PELESENAN PEMBORONG DAN PERUNCIT) 1996</h4>
                        <h3>LESEN RUNCIT</h3>
                    </center>
                </td>
                <td></td>
            </tr>
      </table>

      <table style="width: 100%;">
            <tr>
                <td width="70%"></td>
                <td width="15%"><b>No. Lesen:</b></td>
                <td width="15%"></td>
            </tr>
            <tr>
                <td width="70%"></td>
                <td width="15%"><b>No. Rujukan:</b></td>
                <td width="15%"></td>
            </tr>
      </table>
      <br/>

      <p>Lesen dikeluarkan kepada: </p>
      <p>Nama Perniagaan: <b>{{ $license->company_name }}</b></p>
      <br/>
      <p>Alamat premis perniagaan: <b>{{ $license->companyAddress->address_1 }}, {{ $license->companyAddress->address_2}}, {{ $license->companyAddress->address_3 }}, {{ $license->companyAddress->postcode }}, {{ $license->companyAddress->state_name }} </b></p>

      <br/><br/>
      <table>
            <tr>
                <td width="70%">Untuk menjual beras secara <b>{{ strtoupper($license->licenseType->name) }}</b> di premis perniagaan di atas sahaja</td>
                <td></td>
            </tr>
            <tr>
                <td width="70%">Pemegang lesen runcit tidak boleh mempunyai dalam stoknya beras lebih daripada <b>{{ $license->apply_load }}</b>
                  pada satu-satu masa.</td>
                <td></td>
            </tr>
      </table>

      <p>Lesen ini sah dari <b>29-Apr-2019</b> hingga <b>28-Apr-2020</b> </p>
      <p>Pejabat yang mengeluarkan : <b>SEKSYEN KAWAL SELIA PADI DAN BERAS</b></p>
      <br/>
      <p>Tarikh dikeluarkan: <b>29-Apr-2019</b></p>
      <p>No. resit:</p>
      <br/><br/><br/><br/><br/><br/><br/><br/>


      <table style="width: 100%;">
            <tr>
                <td width="35%">(Tandatangan Pemegang Lesen)</td>
                <td width="30%"></td>
                <td width="35%"><center>Ketua Pengarah<br/><b>SHAMSUDDIN BIN ISMAIL</b></center> </td>
            </tr>
      </table>

      <br/>

      <p>(Cop atau Meterai Rasmi)</p>

      <footer>
            <div>
              <table>
                    <tr>
                        <td valign="top">Peringatan:</td>
                        <td width="90%" align="left"><b>Suatu permohonan untuk membaharui lesen hendaklah dibuat tidak kurang daripada tiga puluh hari sebelum tarikh tamat tempoh lesen itu. </b></td>
                    </tr>
              </table>
            </div>
      </footer>
</body>
</html>
