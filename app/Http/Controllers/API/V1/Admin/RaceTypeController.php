<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RaceTypeDataObject;

class RaceTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRaceType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $raceType = RaceTypeDataObject::addRaceType($param);

        return response()->json([
            'message'   =>  'Jenis Bangsa berjaya ditambah',
            'data'      =>  $raceType,
            'success'   =>  true,
            'route'     =>  route('admin.race-type.index')
        ], 200);
    }

    public function updateRaceType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $raceType = RaceTypeDataObject::updateRaceType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Bangsa berjaya dikemaskini',
            'data'      =>  $raceType,
            'success'   =>  true,
            'route'     =>  route('admin.race-type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RaceTypeDataObject::findAllRaceTypeUsingDatatableFormat($param);
    }
}
