<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\DunDataObject;
use App\DataObjects\Admin\ParliamentDataObject;

class DunController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.dun.index');
    }

    public function add()
    {
        $parliaments  = ParliamentDataObject::findAllActiveParliaments();

        return view('admin.dun.add', compact('parliaments'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $dun = DunDataObject::findDunById($decryptedId);
        $parliaments  = ParliamentDataObject::findAllActiveParliaments();

        return view('admin.dun.edit', compact('dun', 'parliaments'));
    }
}
