<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\OwnershipType;

class OwnershipTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Pemilik',
            'Pengarah',
            'Pemegang Saham',
        ];

        foreach ($data as $item) {
            OwnershipType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
