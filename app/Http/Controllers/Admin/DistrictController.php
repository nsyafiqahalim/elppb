<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\DistrictDataObject;
use App\DataObjects\Admin\StateDataObject;

class DistrictController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.district.index');
    }

    public function add()
    {
        $states  = StateDataObject::findAllActiveStates();

        return view('admin.district.add', compact('states'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $district = DistrictDataObject::findDistrictById($decryptedId);
        $states  = StateDataObject::findAllActiveStates();
        
        return view('admin.district.edit', compact('district', 'states'));
    }
}
