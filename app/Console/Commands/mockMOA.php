<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\DataObjects\Admin\BranchDataObject;

class mockMOA extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:moa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $branches = BranchDataObject::findAllActiveBranches();
        
        $roles= [
            'Ketua Unit Cawangan',
            'Ketua Unit HQ',
            'Pegawai Siasatan',
            'Penjana Lesen',
            'Pemohon Lesen',
            'Pegawai Kaunter',
            'Ketua Pengarah',
        ];

        $size = sizeOf($roles);

        $this->info("Creating mock MOA Staff...");
        foreach($branches as $branch){
            foreach($roles as $role){
                factory(\App\User::class, (int) 1)->create()->each(function ($u) use($branch,$role) {
                    $u->assignRole($role);
                    $u->update([
                        'type'  =>  'MOA'  
                    ]);
                    $u->profile()->create([
                        'branch_id' => $branch->id
                    ]);
                });
            }
        }
        
        $this->info("Successfully created mock MOA Staff!");
    }
}
