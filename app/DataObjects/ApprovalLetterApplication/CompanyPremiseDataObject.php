<?php

namespace App\DataObjects\ApprovalLetterApplication;

use DB;
use Auth;

use App\Models\ApprovalLetterApplication\CompanyPremise;

class CompanyPremiseDataObject
{
    public static function addNewCompanyPremise($param)
    {
        return CompanyPremise::create([
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'business_type_name'       =>  $param['business_type_name'],
            'building_type_id'       =>  $param['building_type_id'],
            'building_type_name'       =>  $param['building_type_name'],
            'dun_id'                =>  $param['dun_id'],
            'dun_name'                =>  $param['dun_name'],
            'district_id'                =>  $param['district_id'],
            'district_name'                =>  $param['district_name'],
            'parliament_id'         =>  $param['parliament_id'],
            'company_premise_id'         =>  $param['company_premise_id'],
            'parliament_name'         =>  $param['parliament_name'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'state_name'                  =>  $param['state_name'],
            'phone_number'                  =>  $param['phone_number'],
            'email'                  =>  $param['email'],
            'approval_letter_application_id'                  =>  $param['approval_letter_application_id'],
            'company_id'               =>  $param['company_id'],
        ]);
    }

    public static function findAllActiveCompanyPremises()
    {
        $Companys = CompanyPremise::activeOnly()->get();

        return $Companys;
    }

    public static function findAllCompanyPremisesUsingDatatableFormat($param)
    {
        $companies =  CompanyPremise::distinct()
                    ->when(isset($param['user_id']), function ($company) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
        })->addColumn('business_type', function ($company) {
            return $company->businessType->name ?? '-';
        })->addColumn('building_type', function ($company) {
            return $company->buildingType->name ?? '-';
        })
        ->addColumn('dun', function ($company) {
            return $company->dun->name ?? '-';
        })
        ->addColumn('parliament', function ($company) {
            return $company->parliament->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'    =>  encrypt($company->id)
            ]
            )->render();
        })
        ->addColumn('name', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
