<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseApplicationReplacementType;

class licenseApplicationReplacementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LicenseApplicationReplacementType::create([
            'name'  =>  'Ganti',
            'code'  =>  'GANTI',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Ganti'
        ]);

        LicenseApplicationReplacementType::create([
            'name'  =>  'Hilang',
            'code'  =>  'HILANG',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis Ganti'
        ]);
    }
}
