<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PremiseType extends Model
{
    public $guarded = ['id'];
}
