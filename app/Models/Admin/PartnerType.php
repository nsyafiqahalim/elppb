<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PartnerType extends Model
{
    public $guarded = ['id'];
}
