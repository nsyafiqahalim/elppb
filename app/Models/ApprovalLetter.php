<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication;
use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\ApprovalLetterApplication\CompanyPartner;
use App\Models\ApprovalLetterApplication\CompanyPremise;
use App\Models\Admin\Status;

class ApprovalLetter extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letters';

    private const ACTIVE = 1;

    public function approvalLetterApplication()
    {
        return $this->belongsTo(ApprovalLetterApplication::class, 'approval_letter_application_id');
    }
}
