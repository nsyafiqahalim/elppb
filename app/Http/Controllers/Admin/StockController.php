<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\StockDataObject;

class StockController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.stock.index');
    }

    public function add()
    {
        return view('admin.stock.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $stock = StockDataObject::findStockById($decryptedId);

        return view('admin.stock.edit', compact('stock'));
    }
}
