<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\PermitType;

class PermitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Pemindahan Padi Antara Negeri',
            'Pemindahan Beras Antara Negeri',
            'Pemindahan Hasil Sampingan Antara Negeri',
        ];

        foreach ($data as $item) {
            PermitType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
