<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\PartnerTypeDataObject;

class PartnerTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addPartnerType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $partnerType = PartnerTypeDataObject::addPartnerType($param);

        return response()->json([
            'message'   =>  'Jenis Rakan Kongsi berjaya ditambah',
            'data'      =>  $partnerType,
            'success'   =>  true,
            'route'     =>  route('admin.partner-type.index')
        ], 200);
    }

    public function updatePartnerType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $partnerType = PartnerTypeDataObject::updatePartnerType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Rakan Kongsi berjaya dikemaskini',
            'data'      =>  $partnerType,
            'success'   =>  true,
            'route'     =>  route('admin.partner-type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return PartnerTypeDataObject::findAllPartnerTypeUsingDatatableFormat($param);
    }
}
