<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('license_management.index') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Laman Utama</a></li>
        <!---
        <!---
        <li class="sidebarnav">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Maklumat Syarikat</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_application.company.index') }}" aria-expanded="false">Syarikat</a></li>
                <li><a href="{{ route('license_application.import_export') }}" aria-expanded="false">Lesen</a></li>
            </ul>
        </li>
        --->
        &nbsp;&nbsp;
        <li class="sidebarnav>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Baharu</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('permit_application.rice') }}" aria-expanded="false">Permit Beras</a></li>
                <li><a href="{{ route('permit_application.rice') }}" aria-expanded="false">Permit Hasil Sampingan</a></li>
                <li><a href="{{ route('permit_application.paddy') }}" aria-expanded="false">Permit Padi</a></li>
            </ul>
        </li>
        <li><a href="{{ route('license_application.license') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu">Pengurusan Permit</a></li>
        <!---
        <li class="sidebarnav>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Permohonan Permit</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_application.retail') }}" aria-expanded="false">Permit Beras/Hasil Sampingan</a></li>
                <li><a href="{{ route('license_application.wholesale') }}" aria-expanded="false">Permit Padi</a></li>
                <li><a href="{{ route('license_application.wholesale') }}" aria-expanded="false">Senarai Permit</a></li>
            </ul>
        </li>
        <li class="sidebarnav>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Status Permohonan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_application.list_of_application') }}" aria-expanded="false">Lesen</a></li>
                <li><a href="{{ route('license_application.list_of_permit') }}" aria-expanded="false">Permit</a></li>
            </ul>
        </li>
        --->
    </ul>
</nav>