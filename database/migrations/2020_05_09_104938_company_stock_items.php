<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyStockItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_stock_statement_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stock_type_id')->index()->nullable();
            $table->unsignedBigInteger('stock_category_id')->nullable()->index();
            $table->unsignedBigInteger('spin_type_id')->nullable()->index();
            $table->unsignedBigInteger('total_grade_id')->nullable()->index();
            $table->unsignedBigInteger('license_type_id')->nullable()->index();
            $table->unsignedBigInteger('rice_grade_id')->nullable()->index();
            $table->unsignedBigInteger('stock_statement_period_id')->nullable()->index();
            $table->unsignedBigInteger('company_stock_statement_account_id')->nullable()->index('css_cssa_idx');
            
            $table->bigInteger('initial_stock')->nullable();
            $table->float('total_buy', 8, 2)->nullable();
            $table->float('buying_price', 8, 2)->nullable();
            $table->float('total_spin', 8, 2)->nullable();
            $table->float('stock_balance', 8, 2)->nullable();
            $table->float('spin_result', 8, 2)->nullable();
            $table->float('total_sell', 8, 2)->nullable();
            $table->float('selling_price', 8, 2)->nullable();

            $table->foreign('stock_type_id')->references('id')->on('stock_types');
            $table->foreign('rice_grade_id')->references('id')->on('rice_grades');
            $table->foreign('stock_category_id')->references('id')->on('stock_categories');
            $table->foreign('license_type_id')->references('id')->on('license_types');
            $table->foreign('spin_type_id')->references('id')->on('spin_types');
            $table->foreign('total_grade_id')->references('id')->on('total_grades');
            
            $table->foreign('stock_statement_period_id')->references('id')->on('stock_statement_periods');
            $table->foreign('company_stock_statement_account_id','fk_css_cssa')->references('id')->on('company_stock_statement_accounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
