<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\StoreType;

class StoreTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          'Gudang',
          'Stor',
      ];

      foreach ($data as $item) {
          StoreType::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
      }
    }
}
