<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\CompanyAddress;
use App\Models\Admin\CompanyType;
use App\User;

class Material extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_materials';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

}
