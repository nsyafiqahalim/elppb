<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\RiceType;

class RiceTypeDataObject
{
    public static function addRiceType($param)
    {
        $riceType = null;

        DB::transaction(function () use ($param, &$riceType) {
            $riceType = RiceType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $riceType;
    }

    public static function updateRiceType($param, $id)
    {
        $riceType = null;

        DB::transaction(function () use ($param, $id,  &$riceType) {
            $riceType = RiceType::find($id);

            $riceType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $riceType;
    }

    public static function findAllRiceTypeUsingDatatableFormat($param)
    {
        $riceType = RiceType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($riceType)
        ->addIndexColumn()
        ->addColumn('action', function ($riceType) {
            return view('admin.rice-type.partials.datatable-button', compact('riceType'))->render();
        })
        ->addColumn('status', function ($riceType) {
            $status= ($riceType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.rice-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findRiceTypeById($id)
    {
        return RiceType::find($id);
    }
}
