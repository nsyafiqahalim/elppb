
@if($status == 'ACTIVE')
<span class="label label-success">Aktif</span>
@endif

@if($status == 'DEACTIVATED')
<span class="label label-danger">Tidak Aktif</span>
@endif

@if($status == 'LOCKED')
<span class="label label-warning">Dikunci</span>
@endif

@if($status == 'WAITING_FOR_VERIFICATION')
<span class="label label-info">Pengesahan Akaun</span>
@endif
