<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Portal\GalleryDataObject;

class GalleryController extends Controller
{
    public function index()
    {
        $galleryDAOs = GalleryDataObject::findImageWithPagination(8);

        return view('portal.gallery.index', compact('galleryDAOs'));
    }

    public function add()
    {
        return view('portal.gallery.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        $gallerymentDAO = GalleryDataObject::findGalleryById($decryptedId);

        return view('portal.gallery.edit', compact('gallerymentDAO'));
    }
}
