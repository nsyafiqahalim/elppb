<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Portal\GalleryDataObject;

class GalleryController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addNewImageToGallery(Request $request)
    {
        $param = $request->all();
        $decryptedUserId = decrypt($param['user_id']);

        $param['created_by'] = $decryptedUserId;
        $param['updated_by'] = $decryptedUserId;

        $galleryDAO = GalleryDataObject::addNewImageToGallery($param);

        return response()->json([
            'message'   =>  'Infografik berjaya ditambah',
            'data'      =>  $galleryDAO,
            'success'   =>  true,
            'route'     =>  route('portal.gallery.index')
        ], 200);
    }

    public function deleteGallery($id)
    {
        $decryptedId = decrypt($id);

        $galleryDAO = GalleryDataObject::findAndDeleteImageFromGallery($decryptedId);

        return response()->json([
            'message'   =>  'Infografik berjaya dipadam',
            'data'      =>  $galleryDAO,
            'success'   =>  true,
            'route'     =>  route('portal.gallery.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        return GalleryDataObject::findAllGalleryUsingDatatableFormat();
    }
}
