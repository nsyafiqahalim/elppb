<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Models\ReferenceData\Company;
use App\Models\Admin\Status;
use App\Models\Admin\ApprovalLetterType;
use App\Models\Admin\LicenseApplicationBuyPaddyCondition;

use App\Models\ApprovalLetter;
use App\Models\Admin\ApprovalLetterApplicationType;
use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;

use App\DataObjects\ApprovalLetterApplication\AttachmentDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyAddressDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPartnerDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyStoreDataObject;
use App\DataObjects\ApprovalLetterApplication\CompanyPremiseDataObject;
use App\Models\ApprovalLetterApplication\BuyPaddy;

class ApprovalLetterApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:approvalLetterApplication {--belipadi} {--kilangpadi} {--suratkebenaran} {--bersekali}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mock:approvalLetterApplication {--belipadi} {--kilangpadi} {--suratkebenaran} {--bersekali}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('belipadi')) {
            $statuses = Status::where('domain', 'like', 'SURAT_KELULUSAN_BELI_PADI_BAHARU')->get();
            $approvalLetterType = ApprovalLetterType::where('name', 'Membeli Padi')->first();
        } elseif ($this->option('kilangpadi')) {
            $statuses = Status::where('domain', 'like', 'SURAT_KELULUSAN_KILANG_PADI_KOMERSIAL_BAHARU')->get();
            $approvalLetterType = ApprovalLetterType::where('name', 'Kilang Padi Komersil')->first();
        } else {
            $statuses = Status::all();
            $approvalLetterType = ApprovalLetterType::all();
        }
    

        $companies = Company::all();
        $approvalLetterApplicationTypes = ApprovalLetterApplicationType::all();
        $this->info("Creating {$companies->count()}  mock approval_letter applications...");
        foreach ($companies as $company) {
            foreach ($statuses as $status) {
                foreach ($approvalLetterApplicationTypes as $approvalLetterApplicationType) {
                    factory(\App\Models\ApprovalLetterApplication::class, 1)->create([
                    'company_name'   =>  $company->name,
                    'applicant_id'   =>  $company->user_id,
                    'status_id'   =>  $status->id,
                    'branch_id'     =>  1,
                    'approval_letter_type_id'   =>  $approvalLetterType->id,
                    'approval_letter_application_type_id'   => $approvalLetterApplicationType->id
                ])
                ->each(function ($approvalLetterApplication) use ($company, $status, $approvalLetterType, $approvalLetterApplicationType) {
                    $param = [];

                    $premises = $company->premises;
                    $stores = $company->stores;

                    $param['original_company'] = $company;
                    $param['original_premise'] = $premises->random();
                    $param['original_store'] = $stores->random();
                    $param['original_partners'] = OriginalCompanyPartnerDataObject::findCompanyParnerByCompanyId($param['original_company']->id);
                    $param['company_name'] = $param['original_company']->name;
                    $param['original_address'] = $param['original_company']->address;

                    $companyAddressParam = $param['original_address']->toArray();
                    $companyAddressParam['state_name'] = $param['original_address']->state->name;
                    $companyAddressParam['approval_letter_application_id'] = $approvalLetterApplication->id;
                    $companyAddressParam['company_id'] = $param['original_company']->id;
                    unset($param['original_address']['id']);

                    $companyDAOParam = $param['original_company']->toArray();
                    $companyDAOParam['company_type_name'] = $param['original_company']->companyType->name;
                    $companyDAOParam['company_id'] = $param['original_company']->id;
                    $companyDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
                    unset($companyDAOParam['id']);

                    $companyPremiseDAOParam = $param['original_premise']->toArray();
                    $companyPremiseDAOParam['business_type_name'] = $param['original_premise']->businessType->name;
                    $companyPremiseDAOParam['building_type_name'] = $param['original_premise']->buildingType->name;
                    $companyPremiseDAOParam['state_name'] = $param['original_premise']->state->name;
                    $companyPremiseDAOParam['parliament_name'] = $param['original_premise']->parliament->name;
                    $companyPremiseDAOParam['dun_name'] = $param['original_premise']->dun->name;
                    $companyPremiseDAOParam['district_name'] = $param['original_premise']->district->name;
                    $companyPremiseDAOParam['company_premise_id'] = $param['original_premise']->id;
                    $companyPremiseDAOParam['phone_number'] = '0129101561';
                    $companyPremiseDAOParam['email'] = 'con@con.toh';

                    $companyPremiseDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
                    unset($companyPremiseDAOParam['id']);

                        $companyStoreDAOParam = $param['original_store']->toArray();
                        $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
                        $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
                        $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
                        $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
                        $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;

                        $companyStoreDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
                        unset($companyStoreDAOParam['id']);

                        $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);
                    
                    $company = CompanyDataObject::createNewCompany($companyDAOParam);
                    $addess = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
                    $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);

                    foreach ($param['original_partners'] as $item) {
                        $companyPartnerDAOParam = $item->toArray();
                        //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
                        $companyPartnerDAOParam['race_name'] = $item->race->name;
                        $companyPartnerDAOParam['race_type_name'] = $item->raceType->name;
                        $companyPartnerDAOParam['is_citizen'] = $item->is_citizen;
                        $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
                        $companyPartnerDAOParam['state_name'] = $item->state->name;

                        $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
                        $companyPartnerDAOParam['company_partner_id'] = $companyPartnerDAOParam['id'];
                        $companyPartnerDAOParam['approval_letter_application_id'] = $approvalLetterApplication->id;
                        unset($companyPartnerDAOParam['id']);

                        CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
                    }

                    if ($status->name ==  'Tamat' && $this->option('suratkebenaran')) {
                        ApprovalLetter::create([
                            'company_id'                    =>  $param['original_company']->id,
                            'applicant_id'                       =>  $company->user_id,
                            'status_id'                     =>  369,
                            'approval_letter_type_id'               =>  $approvalLetterType->id,
                            'approval_letter_application_id'        =>  $approvalLetterApplication->id,
                            'approval_letter_application_type_id'   =>  $approvalLetterApplicationType->id,
                            'expiry_date'                   =>  Carbon::now()->addMonths(mt_rand(12, 26)),
                            'approval_letter_number'                =>  'test'
                        ]);
                    }

                    if ($this->option('belipadi')) {
                        BuyPaddy::create([
                            'approval_letter_application_id'                =>  $approvalLetterApplication->id,
                            'license_application_buy_paddy_condition_id'    =>  mt_rand(1, 3),
                        ]);
                    }
                });
                }
            }
        }
        

        $this->info("Successfully created {$companies->count()} mock applications!");
    }
}
