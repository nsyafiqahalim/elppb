<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationStoreTemporaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_appliclicense_application_store_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('license_application_id')->index('laast_la_idx');
            $table->unsignedBigInteger('building_type_id')->index('laast_bt_idx');
            $table->unsignedBigInteger('store_ownership_type_id')->index('laast_sot_idx');
            $table->unsignedBigInteger('state_id')->index('laast_st_idx');
            $table->unsignedBigInteger('district_id')->index('laast_dt_idx');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email')->nullable();
            $table->enum('type', ['MAILING'])->default('MAILING');

            $table->foreign('state_id','fk_laast_st')->references('id')->on('states');
            $table->foreign('license_application_id','fk_laast_la')->references('id')->on('license_applications');
            $table->foreign('district_id','fk_laast_dt')->references('id')->on('districts');
            $table->foreign('store_ownership_type_id','fk_laast_sot')->references('id')->on('store_ownership_types');
            $table->foreign('building_type_id','fk_laast_bt')->references('id')->on('building_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
