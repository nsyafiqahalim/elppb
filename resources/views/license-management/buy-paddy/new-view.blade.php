@extends('layouts.license-management-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-bookmark"></i> Maklumat Permohonan</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('management.index') }}">Pengurusan Lesen</a></li>
            <li class="breadcrumb-item active">Maklumat Permohonan Lesen ( {{ $license->licenseType->name }} )</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content">
                  <form id="submission-form" class="tab-wizard wizard-circle">
                    <!-- Step 1 -->
                    <h6>Syarikat</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Syarikat</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Syarikat</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $license->company_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Syarikat</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fas fa-building"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->company->company_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No Pendaftaran / Trading License / IRD</label>
                                        <input type="text" class="form-control" value="{{ $license->company->registration_number }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Tarikh Mansuh</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-calendar-question"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ \Carbon\Carbon::parse($license->company->expiry_date)->format('d/m/Y') }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Modal Berbayar</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">RM</span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->company->paidup_capital }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Company Address -->
                            <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 1</label>
                                        <input type="text" value="{{ $license->companyAddress->address_1 }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 2</label>
                                        <input type="text" value="{{ $license->companyAddress->address_2}}" disabled class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 3</label>
                                        <input type="text" value="{{ $license->companyAddress->address_3 }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" value="{{ $license->companyAddress->postcode }}" disabled class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" value="{{ $license->companyAddress->state_name }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-cellphone"></i>
                                                </span>
                                            </div>
                                            <input type="text" value="{{ $license->companyAddress->phone_number }}" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-fax"></i>
                                                </span>
                                            </div>
                                            <input type="text" disabled value="039553333" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-email"></i>
                                                </span>
                                            </div>
                                            <input type="text" value="{{ $license->companyAddress->email }}" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>Rakan Kongsi</h6>
                    <section style="padding-top:30px;">
                        <h3 class="card-title">Maklumat Rakan Kongsi</h3>
                        @if (count($license->companyPartners) > 0)
                        @foreach($license->companyPartners as $licensePartner)
                        <div class="form-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Pemilik / Rakan Kongsi / Ahli Lembaga</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $licensePartner->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status Kewarganegaraan</label>
                                        <input type="text" class="form-control" value="{{ $licensePartner->citizen_name }}" disabled>
                                    </div>
                                </div>
                            </div> <!-- End Row 1 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No Kad Pengenalan</label>
                                        <input type="text" class="form-control" value="{{ $licensePartner->nric }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Passport (Bukan Bumiputera)</label>
                                        <input type="text" class="form-control" value="{{ is_null($licensePartner->passport) ? '-' : $licensePartner->passport}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status Pemilikan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-account-key"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="Pemegang Saham">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 2 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Syer</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">RM</span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $licensePartner->total_share }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Peratusan Syer</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" value="{{ $licensePartner->share_percentage }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $licensePartner->phone_number }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 3 -->
                        </div>
                        @endforeach
                        @endif
                    </section>
                    <!-- Step 3 -->
                    <h6>Premis</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Premis Perniagaan</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Perniagaan</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $license->companyPremise->business_type_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 1</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 2</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 3</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_3 }}" disabled>
                                    </div>
                                </div>
                            </div> <!-- End Row 1 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->postcode }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->state_name }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Parlimen</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->parliament_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Dun</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->dun_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Bangunan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-building"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->companyPremise->building_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Perniagaan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-briefcase"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->companyPremise->business_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722587">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-fax"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722555">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-email"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="shn123@yahoo.com">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 2 -->
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h6>Stor</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Stor</h3>
                            @if (count($license->companyStores) > 0)
                            @foreach($license->companyStores as $licenseStore)
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Syarikat</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $license->company_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Bangunan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-building"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled value="{{ $licenseStore->building_type_name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Hak Milik</label>
                                        <input type="text" class="form-control" disabled value="{{ $licenseStore->store_ownership_type_name }}">
                                    </div>
                                </div>
                            </div> <!-- End Row 1 -->
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 1</label>
                                        <input type="text" class="form-control" disabled value="{{ $licenseStore->address_1 }}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 2</label>
                                        <input type="text" class="form-control" disabled value="{{ $licenseStore->address_2 }}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 3</label>
                                        <input type="text" class="form-control" disabled value="{{ $licenseStore->address_3 }}">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" disabled value="{{ $licenseStore->postcode }}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" disabled value="{{ $licenseStore->state_name }}"class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled value="{{ $licenseStore->phone_number }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-fax"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled value="{{ $licenseStore->fax_number }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-email"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled value="{{ $licenseStore->email }}">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 2 -->
                            @endforeach
                            @endif
                        </div>
                    </section>
                    <!-- Step 5 -->
                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th colspan="2">Fail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                Salinan Sijil Pendaftaran SSM
                                            </td>
                                            <td> <button class="btn btn-warning ssmButton">Lihat Fail</button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>
                                                Salinan Lesen Perniagaan (Sabah/Sarawak)
                                            </td>
                                            <td> <button class="btn btn-warning lesenPerniagaanButton">Lihat Fail</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- Step 6 -->
                    <h6>Log Aktiviti</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th>Aktiviti</th>
                                            <th>Tarikh</th>
                                            <th>Pelaku</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @if (count($license_activities) > 0)
                                        @foreach($license_activities as $license_activity)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $license_activity->activity }}</td>
                                            <td>{{ \Carbon\Carbon::parse($license_activity->created_at)->format('d/m/Y g:i A') }}</td>
                                            <td>{{ $license_activity->user->name }} </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- Step 7 -->
                    <h6>Status</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th>Pelaku</th>
                                            <!-- <th>Jawatan</th> -->
                                            <th>Tarikh</th>
                                            <th>Ulasan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @if (count($license_remarks) > 0)
                                        @foreach($license_remarks as $license_remark)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $license_remark->user->name }} </td>
                                            <!-- <td> </td> -->
                                            <td>{{ \Carbon\Carbon::parse($license_remark->created_at)->format('d/m/Y g:i A') }}</td>
                                            <td>{{ $license_remark->remarks }}</td>

                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    <!-- Step Pegawai Siasatan -->
                    @if (Auth::user()->can("Siasatan Permohonan Lesen Beli Padi (Cawangan)"))
                    <h6>Laporan</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Laporan Siasatan</h3>
                            <hr>
                            <div class="form-group row">
                                <label for="example-url-input" class="col-2 col-form-label">Laporan</label>
                                <div class="col-6">
                                    <textarea class="form-control" rows="10" type="url"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>
                    @endif

                    <!-- Step Ketua Cawangan -->
                    @if (Auth::user()->can("Peraku Cawangan Permohonan Lesen Beli Padi (Cawangan)"))
                    <h6>Laporan</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Laporan Siasatan</h3>
                            <hr>
                            @if (!is_null($license->license_investigation_officer_id))
                              <div class="form-group row">
                                 <label class="col-2 col-form-label">Pengawai Siasatan</label>
                                 <div class="col-6">
                                     <input type="text" class="col-md-12 col-form-label" value="{{ $license->investigationOfficer->name }} " disabled>
                                 </div>
                             </div>
                             <div class="form-group row">
                                <label for="example-url-input" class="col-2 col-form-label">Laporan</label>
                                <div class="col-6">
                                    <textarea class="form-control" rows="10" type="url" disabled>Laporan siasatan berjalan lancar</textarea>
                                </div>
                            </div>
                            @else
                            <div class="form-group row">
                               <label class="col-12 col-form-label">Tiada Laporan Siasatan Dijalankan</label>
                           </div>
                            @endif
                        </div>
                    </section>
                    @endif

                    <!-- Final Step -->
                    <h6>Tindakan</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Tindakan Seterusnya</h3>
                            <hr>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Tempoh Permohonan</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_duration }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Had Muatan (TAN)</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_load }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-2 col-form-label">Status Permohonan Sekarang</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->status->name }} ( {{ $license->licenseApplicationType->code }} )" disabled>
                                </div>
                            </div>
                            @if (!Auth::user()->can("Jana Lesen"))
                            <div class="form-group row">
                                <label for="example-url-input" class="col-2 col-form-label">Ulasan</label>
                                <div class="col-6">
                                    <textarea class="form-control" rows="10" type="url" id="remark" name="remark" required></textarea>
                                </div>
                            </div>
                            @endif
                            @if (Auth::user()->can("Semakan Siasatan Permohonan Lesen Beli Padi (Cawangan)"))
                              @if (is_null($license->license_investigation_officer_id))
                                 <div class="form-group row">
                                    <label class="col-2 col-form-label">Pilih Pengawai Siasatan</label>
                                    <div class="col-6">
                                        <select class="form-control select2" id="pegawai_siasatan" name="pegawai_siasatan" required>
                                          <option value=""></option>
                                        @if(count($pegawai_siasatan) > 0)
                                            @foreach ($pegawai_siasatan as $pegawai)
                                                <option value="{{ $pegawai->user->id }}"> {{$pegawai->user->name}} </option>
                                            @endforeach
                                        @endif
                                        </select>
                                    </div>
                                </div>
                              @endif
                            @endif
                        </div>
                    </section>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script type='text/javascript'>
    $(document).ready(function(){
        $('#datatable').DataTable({
            responsive: true,
            searching: false,
            ordering: false,
        });

        $(document).on('click','.ssmButton',function(){
            var url = "{{  route('management.buy_paddy.new.ssm', [encrypt($license->id)]) }}";
            window.open(url);
        })

        $(document).on('click','.lesenPerniagaanButton',function(){
            var url = "{{  route('management.buy_paddy.new.lesen-perniagaan', [encrypt($license->id)]) }}";
            window.open(url);
        })

        $(document).on('click','.submitButton',function(){
            var remark = $('#remark').val();
            var status = $('#status').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.buy_paddy.new.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })

        $(document).on('click','.submitButton_1',function(){
            var remark = $('#remark').val();
            var status = $('#status_1').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.buy_paddy.new.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })

        $(document).on('click','.submitButton_3',function(){
            var remark = $('#remark').val();
            var status = $('#status_3').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.buy_paddy.new.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })

        $(document).on('click','.submitButton_5',function(){
            var status = $('#status').val();

            $('#submission-form').attr('action', "{{ route('management.buy_paddy.new.download', [encrypt($license->id)]) }}?status="+status);
        })

        $(document).on('click','.rejectButton',function(){
            var remark = $('#remark').val();
            var status = $('#status_2').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.buy_paddy.new.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })

    });

</script>
@endpush

@push('wizard')
<script type='text/javascript'>
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "TOLAK_BELI_PADI_BAHARU",
            previous: "Kembali",
            next: "Seterusnya"
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            if(currentIndex == 7) {
                $('ul[aria-label=Pagination] a[href="#finish"]').remove();

                if ("{{ $license->status->name }}" == "Permohonan Baharu") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU" name="status" id="status">Semakan Ketua Unit Cawangan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_BELI_PADI_BAHARU" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Beli Padi (Cawangan)") && $license->status->name == "Peraku Semakan Ibu Pejabat" }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU" name="status" id="status">Semakan Semula Ketua Unit Cawangan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_1" value="KELULUSAN_PERMOHONAN_BELI_PADI_BAHARU" name="status_1" id="status_1">Semakan Ketua Pengarah</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_BELI_PADI_BAHARU" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Kelulusan Permohonan Lesen Beli Padi (Cawangan)") && $license->status->name == "Kelulusan Permohonan" }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU" name="status" id="status">Semakan Semula Ketua Unit Cawangan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_1" value="PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_BAHARU" name="status_1" id="status_1">Semakan Semula Ketua Unit HQ</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_3" value="JANAAN_LESEN_BELI_PADI_BAHARU" name="status_3" id="status_3">Lulus Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_BELI_PADI_BAHARU" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Jana Lesen") }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_5" value="TAMAT_BELI_PADI_BAHARU" name="status" id="status">Jana Lesen</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                }
            } else if(currentIndex == 8) {
                $('ul[aria-label=Pagination] a[href="#finish"]').remove();

                if ("{{ Auth::user()->can("Semakan Siasatan Permohonan Lesen Beli Padi (Cawangan)") && is_null($license->license_investigation_officer_id) }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU" name="status" id="status">Lakukan Siasatan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_BELI_PADI_BAHARU" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Semakan Siasatan Permohonan Lesen Beli Padi (Cawangan)") && ($license->license_investigation_officer_id != null) }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_1" value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU" name="status_1" id="status_1">Semakan Semula Siasatan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_3" value="PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_BAHARU" name="status_3" id="status_3">Semakan Ketua Unit HQ</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_BELI_PADI_BAHARU" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Siasatan Permohonan Lesen Beli Padi (Cawangan)") }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PERAKU_SEMAKAN_SIASATAN_BELI_PADI_BAHARU" name="status" id="status">Hantar Laporan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                }
            } else {
                $('ul[aria-label=Pagination] button[value="PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="PERAKU_SEMAKAN_SIASATAN_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="KELULUSAN_PERMOHONAN_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="JANAAN_LESEN_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="TAMAT_BELI_PADI_BAHARU"]').remove();
                $('ul[aria-label=Pagination] button[value="TOLAK_BELI_PADI_BAHARU"]').remove();
            }
        }
    });
</script>
@endpush
