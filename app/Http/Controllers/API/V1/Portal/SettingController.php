<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;


use App\DataObjects\Portal\SettingDataObject;

class SettingController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['auth', 'verified']);
    }

    public function update(Request $request, $settingDomain)
    {
        $decryptedDomain = decrypt($settingDomain);
        (new SettingDataObject)->updateSetting($request, $decryptedDomain);
        
        return response()->json([
            'success'   =>  true,
            'route'     =>  Route('portal.setting.index', [encrypt($decryptedDomain)]),
            'message'   =>  'Penetapan Portal berjaya dikemaskini',
            "id"        =>  null,
        ], 200);
    }
}
