<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('license_management.index') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Laman Utama</a></li>
        <li><a href="{{ route('license_management.application.retail_license.new_application.index') }}" aria-expanded="false"><i class="fa fa-inbox"></i><span class="hide-menu">Tugasan Kami</a></li>
        <li><a href="{{ route('license_management.license.generate') }}" aria-expanded="false"><i class="fa fa-file"></i><span class="hide-menu">Jana Lesen</a></li>
        <li class="three-column">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Laporan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="ui-tooltip-stylish.html">Permohonan Lesen</a></li>
            </ul>
        </li>
        <li><a href="{{ route('license_management.license.search') }}" aria-expanded="false"><i class="fa fa-search"></i><span class="hide-menu">Carian Lesen</a></li>
       <!---
        <li>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Runcit<span class="label label-rounded label-info">5</span></span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_management.application.retail_license.new_application.index') }}">Baharu <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.retail_license.change_request_application.index') }}">Perubahan <span class="label label-rounded label-warning">3</span></a></li>
                <li><a href="{{ route('license_management.application.retail_license.renew_application.index') }}">Pembaharuan <span class="label label-rounded label-warning">2</span></a></li>
                <li><a href="{{ route('license_management.application.retail_license.replacement_application.index') }}">Gantian <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.retail_license.cancel_application.index') }}">Pembatalan <span class="label label-rounded label-warning">0</span></a></li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Borong<span class="label label-rounded label-info">5</span></span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_management.application.wholesale_license.new_application.index') }}">Baharu <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.wholesale_license.change_request_application.index') }}">Perubahan <span class="label label-rounded label-warning">3</span></a></li>
                <li><a href="{{ route('license_management.application.wholesale_license.renew_application.index') }}">Pembaharuan <span class="label label-rounded label-warning">2</span></a></li>
                <li><a href="{{ route('license_management.application.wholesale_license.replacement_application.index') }}">Gantian <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.wholesale_license.cancel_application.index') }}">Pembatalan <span class="label label-rounded label-warning">0</span></a></li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Import/Eksport<span class="label label-rounded label-info">5</span></span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_management.application.import_export_license.new_application.index') }}">Baharu <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.import_export_license.change_request_application.index') }}">Perubahan <span class="label label-rounded label-warning">3</span></a></li>
                <li><a href="{{ route('license_management.application.import_export_license.renew_application.index') }}">Pembaharuan <span class="label label-rounded label-warning">2</span></a></li>
                <li><a href="{{ route('license_management.application.import_export_license.replacement_application.index') }}">Gantian <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.import_export_license.cancel_application.index') }}">Pembatalan <span class="label label-rounded label-warning">0</span></a></li>
            </ul>
        </li>
        <li>
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Beli Padi<span class="label label-rounded label-info">5</span></span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_management.application.buy_paddy_license.new_application.index') }}">Baharu <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.buy_paddy_license.change_request_application.index') }}">Perubahan <span class="label label-rounded label-warning">3</span></a></li>
                <li><a href="{{ route('license_management.application.buy_paddy_license.renew_application.index') }}">Pembaharuan <span class="label label-rounded label-warning">2</span></a></li>
                <li><a href="{{ route('license_management.application.buy_paddy_license.replacement_application.index') }}">Gantian <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.buy_paddy_license.cancel_application.index') }}">Pembatalan <span class="label label-rounded label-warning">0</span></a></li>
            </ul>
        </li>
        <li>
        <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Kilang<span class="label label-rounded label-info">5</span></span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('license_management.application.factory_license.new_application.index') }}">Baharu <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.factory_license.change_request_application.index') }}">Perubahan <span class="label label-rounded label-warning">3</span></a></li>
                <li><a href="{{ route('license_management.application.factory_license.renew_application.index') }}">Pembaharuan <span class="label label-rounded label-warning">2</span></a></li>
                <li><a href="{{ route('license_management.application.factory_license.replacement_application.index') }}">Gantian <span class="label label-rounded label-warning">1</span></a></li>
                <li><a href="{{ route('license_management.application.factory_license.cancel_application.index') }}">Pembatalan <span class="label label-rounded label-warning">0</span></a></li>
            </ul>
        </li>
        --->
        
    </ul>
</nav>