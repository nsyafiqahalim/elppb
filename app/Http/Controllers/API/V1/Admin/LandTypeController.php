<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\LandTypeDataObject;

class LandTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addLandType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $landType = LandTypeDataObject::addLandType($param);

        return response()->json([
            'message'   =>  'Jenis tanah berjaya ditambah',
            'data'      =>  $landType,
            'success'   =>  true,
            'route'     =>  route('admin.land_type.index')
        ], 200);
    }

    public function updateLandType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $landType = LandTypeDataObject::updateLandType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis tanah berjaya dikemaskini',
            'data'      =>  $landType,
            'success'   =>  true,
            'route'     =>  route('admin.land_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return LandTypeDataObject::findAllLandTypeUsingDatatableFormat($param);
    }
}
