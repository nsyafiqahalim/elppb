<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\PremiseType;

class PremiseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Kedai Kampung',
            'Rumah Kedai',
            'Rumah Kediaman',
            'Gudang',
            'Lain - Lain',
        ];

        foreach ($data as $item) {
            PremiseType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
