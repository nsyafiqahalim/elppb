<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_applications_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('license_application_id')->index('lac_la_idx');
            $table->foreign('license_application_id','fk_lac_la')->references('id')->on('license_applications');
            $table->string('apply_load')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
