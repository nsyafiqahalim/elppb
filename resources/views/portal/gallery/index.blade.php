@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Pengumuman</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Laman Utama</li>
          <li class="breadcrumb-item"><a href="{{ route('portal.index') }}">Pengurusan Portal</a></li>
          <li class="breadcrumb-item">Pengumuman</a></li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <a href="{{ route('portal.gallery.add') }}">
            <button type="submit" class="btn btn-success float-right">
                <i class="icon-plus"></i> Tambah Infografik
            </button>
        </a>
    </div>
</div>

<div class="row el-element-overlay">
    @foreach($galleryDAOs as $galleryDAO)
    <div class="col-lg-3 col-md-6">
        <div class="card">
            <div class="el-card-item">
                <div class="el-card-avatar el-overlay-1"> <img src="{{ asset('file/'.$galleryDAO->file_path) }}" style='display: block;width: 100%;height: 200px;' alt="user" />
                    <div class="el-overlay">
                        <ul class="el-info">
                            <li><a class="btn default btn-outline" href="{{ asset('file/'.$galleryDAO->file_path) }}"><i class="icon-eye"></i></a></li>
                            <li><a class="btn danger btn-outline deletion" href="{{ route('api.portal.gallery.delete',[encrypt($galleryDAO->id)]) }}"><i class="icon-trash"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="el-card-content">
                    <h4 class="box-title">
                        {{ $galleryDAO->file_original_name }}
                    </h4>
                    <br/>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>

<div class="row el-element-overlay">
    <div class="col-lg-3 col-md-6">
        {{ $galleryDAOs->links() }}
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}">
@endpush

@push('js')

@endpush
