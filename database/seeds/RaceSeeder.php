<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Race;

class RaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Melayu',
            'Cina',
            'India',
            'Bumiputera Sarawak',
            'Bumiputera Sabah',
            'Orang Asal',
            'Lain - Lain',
        ];

        $raceType = [
            1,
            2,
            2,
            1,
            1,
            1,
            2
        ];


        foreach ($data as $index=> $item) {
            Race::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'race_type_id'  =>   $raceType[$index],
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
