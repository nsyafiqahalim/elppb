<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\LandStatusDataObject;

class LandStatusController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addLandStatus(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $landStatus = LandStatusDataObject::addLandStatus($param);

        return response()->json([
            'message'   =>  'Status Tanah berjaya ditambah',
            'data'      =>  $landStatus,
            'success'   =>  true,
            'route'     =>  route('admin.land-status.index')
        ], 200);
    }

    public function updateLandStatus(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $landStatus = LandStatusDataObject::updateLandStatus($param, $decryptedId);

        return response()->json([
            'message'   =>  'Status Tanah berjaya dikemaskini',
            'data'      =>  $landStatus,
            'success'   =>  true,
            'route'     =>  route('admin.land-status.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return LandStatusDataObject::findAllLandStatusUsingDatatableFormat($param);
    }
}
