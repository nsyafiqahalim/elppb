<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Race;

class RaceDataObject
{
    public static function addRace($param)
    {
        $race = null;

        DB::transaction(function () use ($param, &$race) {
            $race = Race::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $race;
    }

    public static function updateRace($param, $id)
    {
        $race = null;

        DB::transaction(function () use ($param, $id,  &$race) {
            $race = Race::find($id);

            $race->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $race;
    }

    public static function findAllRaceUsingDatatableFormat($param)
    {
        $race = Race::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($race)
        ->addIndexColumn()
        ->addColumn('action', function ($race) {
            return view('admin.race.partials.datatable-button', compact('race'))->render();
        })
        ->addColumn('status', function ($race) {
            $status= ($race->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.race.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findRaceById($id)
    {
        return Race::find($id);
    }
}
