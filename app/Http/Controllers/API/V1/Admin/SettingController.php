<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;


use App\DataObjects\Admin\SettingDataObject;

class SettingController extends Controller
{
    public function __construct()
    {
        //$this->middleware(['auth', 'verified']);
    }

    public function update(Request $request, $settingDomain)
    {
        $decryptedDomain = decrypt($settingDomain);
        (new SettingDataObject)->updateSetting($request, $decryptedDomain);
        
        return response()->json([
            'success'   =>  true,
            'route'     =>  Route('admin.setting.index', [encrypt($decryptedDomain)]),
            'message'   =>  'Penetapan berjaya dikemaskini',
            "id"        =>  null,
        ], 200);
    }
}
