<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\UserDataObject;

class UserController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addUser(Request $request)
    {
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;
        $param['type'] = UserDataObject::USER_TYPE_MOA;

        $user = UserDataObject::addUser($param);

        return response()->json([
            'message'   =>  'Pengguna berjaya ditambah',
            'data'      =>  $user,
            'success'   =>  true,
            'route'     =>  route('admin.user.index')
        ], 200);
    }

    public function updateUser(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $user = UserDataObject::updateUser($param, $decryptedId);

        return response()->json([
            'message'   =>  'Pengguna berjaya dikemaskini',
            'data'      =>  $user,
            'success'   =>  true,
            'route'     =>  route('admin.user.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return UserDataObject::findAllUserUsingDatatableFormat($param);
    }
}
