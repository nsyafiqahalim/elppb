<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\PremiseType;

class PremiseTypeDataObject
{
    public static function addPremiseType($param)
    {
        $premiseType = null;

        DB::transaction(function () use ($param, &$premiseType) {
            $premiseType = PremiseType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'          =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $premiseType;
    }

    public static function updatePremiseType($param, $id)
    {
        $premiseType = null;

        DB::transaction(function () use ($param, $id,  &$premiseType) {
            $premiseType = PremiseType::find($id);

            $premiseType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'          =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $premiseType;
    }

    public static function findAllPremiseTypeUsingDatatableFormat($param)
    {
        $premiseType = PremiseType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($premiseType)
        ->addIndexColumn()
        ->addColumn('action', function ($premiseType) {
            return view('admin.premise-type.partials.datatable-button', compact('premiseType'))->render();
        })
        ->addColumn('status', function ($premiseType) {
            $status= ($premiseType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.premise-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findPremiseTypeById($id)
    {
        return PremiseType::find($id);
    }
}
