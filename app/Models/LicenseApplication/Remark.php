<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\admin\Race;
use App\Models\admin\OwnershipType;
use App\User;

class Remark extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_remarks';

    private const ACTIVE = 1;

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class, 'ownership_type_id');
    }

    public function user()
    {
        return $this->belongsTo(Race::class, 'race_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
