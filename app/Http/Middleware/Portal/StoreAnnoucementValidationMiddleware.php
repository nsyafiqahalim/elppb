<?php

namespace App\Http\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreAnnoucementValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'description' => ['required', 'string']
        ], [
            'description.required' => ['Medan Keterangan diperlukan'],
        ]);

        return $next($request);
    }
}
