<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
// });

Route::group(['prefix' => '/v1'], function () {
    Route::prefix('penetapan-sistem')->group(
        function () {
            Route::patch('/update/{id}', 'API\V1\Admin\SettingController@update')->name('api.admin.setting.update');
        }
    );

    Route::prefix('pengguna-dan-peranan')->group(
        function () {
            Route::prefix('peranan')->group(function () {
                Route::post('/store', 'API\V1\Admin\RoleController@addRole')
            ->middleware('StoreRoleValidation')
            ->name('api.admin.role.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RoleController@updateRole')->name('api.admin.role.update');
                Route::get('/datatable', 'API\V1\Admin\RoleController@datatable')->name('api.admin.role.datatable');
            });

            Route::prefix('pengguna')->group(function () {
                Route::post('/store', 'API\V1\Admin\UserController@addUser')
            ->middleware('StoreUserValidationMiddleware')
            ->name('api.admin.user.store');
                Route::patch('/update/{id}', 'API\V1\Admin\UserController@updateUser')
                ->middleware('UpdateUserValidationMiddleware')
                ->name('api.admin.user.update');
                Route::get('/datatable', 'API\V1\Admin\UserController@datatable')->name('api.admin.user.datatable');
            });
        }
    );

    Route::prefix('pentadbiran-sistem')->group(
        function () {
            Route::prefix('jenis-syarikat')->group(function () {
                Route::post('/store', 'API\V1\Admin\CompanyTypeController@addCompanyType')
          ->middleware('StoreCompanyTypeValidationMiddleware')
          ->name('api.admin.company_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\CompanyTypeController@updateCompanyType')->name('api.admin.company_type.update');
                Route::get('/datatable', 'API\V1\Admin\CompanyTypeController@datatable')->name('api.admin.company_type.datatable');
            });

            Route::prefix('jenis-bangunan')->group(function () {
                Route::post('/store', 'API\V1\Admin\BuildingTypeController@addBuildingType')
            ->middleware('StoreBuildingTypeValidationMiddleware')
            ->name('api.admin.building_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\BuildingTypeController@updateBuildingType')->name('api.admin.building_type.update');
                Route::get('/datatable', 'API\V1\Admin\BuildingTypeController@datatable')->name('api.admin.building_type.datatable');
            });

            Route::prefix('jenis-premis')->group(function () {
                Route::post('/store', 'API\V1\Admin\PremiseTypeController@addPremiseType')
            ->middleware('StorePremiseTypeValidationMiddleware')
            ->name('api.admin.premise_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\PremiseTypeController@updatePremiseType')->name('api.admin.premise_type.update');
                Route::get('/datatable', 'API\V1\Admin\PremiseTypeController@datatable')->name('api.admin.premise_type.datatable');
            });

            Route::prefix('jenis-perniagaan')->group(function () {
                Route::post('/store', 'API\V1\Admin\BusinessTypeController@addBusinessType')
            ->middleware('StoreBusinessTypeValidationMiddleware')
            ->name('api.admin.business_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\BusinessTypeController@updateBusinessType')->name('api.admin.business_type.update');
                Route::get('/datatable', 'API\V1\Admin\BusinessTypeController@datatable')->name('api.admin.business_type.datatable');
            });

            Route::prefix('jenis-tanah')->group(function () {
                Route::post('/store', 'API\V1\Admin\LandTypeController@addLandType')
            ->middleware('StoreLandTypeValidationMiddleware')
            ->name('api.admin.land_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\LandTypeController@updateLandType')->name('api.admin.land_type.update');
                Route::get('/datatable', 'API\V1\Admin\LandTypeController@datatable')->name('api.admin.land_type.datatable');
            });

            Route::prefix('jenis-permit')->group(function () {
                Route::post('/store', 'API\V1\Admin\PermitTypeController@addPermitType')
            ->middleware('StorePermitTypeValidationMiddleware')
            ->name('api.admin.permit_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\PermitTypeController@updatePermitType')->name('api.admin.permit_type.update');
                Route::get('/datatable', 'API\V1\Admin\PermitTypeController@datatable')->name('api.admin.permit_type.datatable');
            });

            Route::prefix('jenis-gudang')->group(function () {
                Route::post('/store', 'API\V1\Admin\StoreTypeController@addStoreType')
            ->middleware('StoreStoreTypeValidationMiddleware')
            ->name('api.admin.store_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\StoreTypeController@updateStoreType')->name('api.admin.store_type.update');
                Route::get('/datatable', 'API\V1\Admin\StoreTypeController@datatable')->name('api.admin.store_type.datatable');
            });

            Route::prefix('jenis-beras')->group(function () {
                Route::post('/store', 'API\V1\Admin\RiceTypeController@addRiceType')
            ->middleware('StoreRiceTypeValidationMiddleware')
            ->name('api.admin.rice-type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RiceTypeController@updateRiceType')->name('api.admin.rice-type.update');
                Route::get('/datatable', 'API\V1\Admin\RiceTypeController@datatable')->name('api.admin.rice-type.datatable');
            });

            Route::prefix('jenis-kilang')->group(function () {
                Route::post('/store', 'API\V1\Admin\FactoryTypeController@addFactoryType')
            ->middleware('StoreFactoryTypeValidationMiddleware')
            ->name('api.admin.factory_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\FactoryTypeController@updateFactoryType')->name('api.admin.factory_type.update');
                Route::get('/datatable', 'API\V1\Admin\FactoryTypeController@datatable')->name('api.admin.factory_type.datatable');
            });

            Route::prefix('jenis-lesen')->group(function () {
                Route::post('/store', 'API\V1\Admin\LicenseTypeController@addLicenseType')
            ->middleware('StoreLicenseTypeValidationMiddleware')
            ->name('api.admin.license_type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\LicenseTypeController@updateLicenseType')->name('api.admin.license_type.update');
                Route::get('/datatable', 'API\V1\Admin\LicenseTypeController@datatable')->name('api.admin.license_type.datatable');
            });

            Route::prefix('status-tanah')->group(function () {
                Route::post('/store', 'API\V1\Admin\LandStatusController@addLandStatus')
            ->middleware('StoreLandStatusValidationMiddleware')
            ->name('api.admin.land-status.store');
                Route::patch('/update/{id}', 'API\V1\Admin\LandStatusController@updateLandStatus')->name('api.admin.land-status.update');
                Route::get('/datatable', 'API\V1\Admin\LandStatusController@datatable')->name('api.admin.land-status.datatable');
            });

            Route::prefix('kategori-beras')->group(function () {
                Route::post('/store', 'API\V1\Admin\RiceCategoryController@addRiceCategory')
            ->middleware('StoreRiceCategoryValidationMiddleware')
            ->name('api.admin.rice-category.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RiceCategoryController@updateRiceCategory')->name('api.admin.rice-category.update');
                Route::get('/datatable', 'API\V1\Admin\RiceCategoryController@datatable')->name('api.admin.rice-category.datatable');
            });

            Route::prefix('dun')->group(function () {
                Route::post('/store', 'API\V1\Admin\DunController@addDun')
            ->middleware('StoreDunValidationMiddleware')
            ->name('api.admin.dun.store');
                Route::patch('/update/{id}', 'API\V1\Admin\DunController@updateDun')->name('api.admin.dun.update');
                Route::get('/datatable', 'API\V1\Admin\DunController@datatable')->name('api.admin.dun.datatable');
            });

            Route::prefix('zon')->group(function () {
                Route::post('/store', 'API\V1\Admin\ZoneController@addZone')
            ->middleware('StoreZoneValidationMiddleware')
            ->name('api.admin.zone.store');
                Route::patch('/update/{id}', 'API\V1\Admin\ZoneController@updateZone')->name('api.admin.zone.update');
                Route::get('/datatable', 'API\V1\Admin\ZoneController@datatable')->name('api.admin.zone.datatable');
            });

            Route::prefix('daerah')->group(function () {
                Route::post('/store', 'API\V1\Admin\DistrictController@addDistrict')
            ->middleware('StoreDistrictValidationMiddleware')
            ->name('api.admin.district.store');
                Route::patch('/update/{id}', 'API\V1\Admin\DistrictController@updateDistrict')->name('api.admin.district.update');
                Route::get('/datatable', 'API\V1\Admin\DistrictController@datatable')->name('api.admin.district.datatable');
            });

            Route::prefix('liputan-kawasan')->group(function () {
                Route::post('/store', 'API\V1\Admin\AreaCoverageController@addAreaCoverage')
            ->middleware('StoreAreaCoverageValidationMiddleware')
            ->name('api.admin.area-coverage.store');
                Route::patch('/update/{id}', 'API\V1\Admin\AreaCoverageController@updateAreaCoverage')->name('api.admin.area-coverage.update');
                Route::get('/datatable', 'API\V1\Admin\AreaCoverageController@datatable')->name('api.admin.area-coverage.datatable');
            });

            Route::prefix('negeri')->group(function () {
                Route::post('/store', 'API\V1\Admin\StateController@addState')
            ->middleware('StoreStateValidationMiddleware')
            ->name('api.admin.state.store');
                Route::patch('/update/{id}', 'API\V1\Admin\StateController@updateState')->name('api.admin.state.update');
                Route::get('/datatable', 'API\V1\Admin\StateController@datatable')->name('api.admin.state.datatable');
            });

            Route::prefix('cawangan')->group(function () {
                Route::post('/store', 'API\V1\Admin\BranchController@addBranch')
            ->middleware('StoreBranchValidationMiddleware')
            ->name('api.admin.branch.store');
                Route::patch('/update/{id}', 'API\V1\Admin\BranchController@updateBranch')->name('api.admin.branch.update');
                Route::get('/datatable', 'API\V1\Admin\BranchController@datatable')->name('api.admin.branch.datatable');
            });

            Route::prefix('parlimen')->group(function () {
                Route::post('/store', 'API\V1\Admin\ParliamentController@addParliament')
            ->middleware('StoreParliamentValidationMiddleware')
            ->name('api.admin.parliament.store');
                Route::patch('/update/{id}', 'API\V1\Admin\ParliamentController@updateParliament')->name('api.admin.parliament.update');
                Route::get('/datatable', 'API\V1\Admin\ParliamentController@datatable')->name('api.admin.parliament.datatable');
            });

            Route::prefix('status')->group(function () {
                Route::post('/store', 'API\V1\Admin\StatusController@addStatus')
            ->middleware('StoreStatusValidationMiddleware')
            ->name('api.admin.status.store');
                Route::patch('/update/{id}', 'API\V1\Admin\StatusController@updateStatus')->name('api.admin.status.update');
                Route::get('/datatable', 'API\V1\Admin\StatusController@datatable')->name('api.admin.status.datatable');
            });

            Route::prefix('rakan-kongsi')->group(function () {
                Route::post('/store', 'API\V1\Admin\PartnerTypeController@addPartnerType')
            ->middleware('StorePartnerTypeValidationMiddleware')
            ->name('api.admin.partner-type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\PartnerTypeController@updatePartnerType')->name('api.admin.partner-type.update');
                Route::get('/datatable', 'API\V1\Admin\PartnerTypeController@datatable')->name('api.admin.partner-type.datatable');
            });

            Route::prefix('gred-beras')->group(function () {
                Route::post('/store', 'API\V1\Admin\RiceGradeController@addRiceGrade')
            ->middleware('StoreRiceGradeValidationMiddleware')
            ->name('api.admin.rice-grade.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RiceGradeController@updateRiceGrade')->name('api.admin.rice-grade.update');
                Route::get('/datatable', 'API\V1\Admin\RiceGradeController@datatable')->name('api.admin.rice-grade.datatable');
            });

            Route::prefix('bangsa')->group(function () {
                Route::post('/store', 'API\V1\Admin\RaceController@addRace')
            ->middleware('StoreRaceValidationMiddleware')
            ->name('api.admin.race.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RaceController@updateRace')->name('api.admin.race.update');
                Route::get('/datatable', 'API\V1\Admin\RaceController@datatable')->name('api.admin.race.datatable');
            });

            Route::prefix('stok')->group(function () {
                Route::post('/store', 'API\V1\Admin\StockController@addStock')
            ->middleware('StoreStockValidationMiddleware')
            ->name('api.admin.stock.store');
                Route::patch('/update/{id}', 'API\V1\Admin\StockController@updateStock')->name('api.admin.stock.update');
                Route::get('/datatable', 'API\V1\Admin\StockController@datatable')->name('api.admin.stock.datatable');
            });

            Route::prefix('status-pemilikan')->group(function () {
                Route::post('/store', 'API\V1\Admin\OwnershipStatusController@addOwnershipStatus')
            ->middleware('StoreOwnershipStatusValidationMiddleware')
            ->name('api.admin.ownership-status.store');
                Route::patch('/update/{id}', 'API\V1\Admin\OwnershipStatusController@updateOwnershipStatus')->name('api.admin.ownership-status.update');
                Route::get('/datatable', 'API\V1\Admin\OwnershipStatusController@datatable')->name('api.admin.ownership-status.datatable');
            });

            Route::prefix('bayaran')->group(function () {
                Route::post('/store', 'API\V1\Admin\PaymentTypeController@addPaymentType')
            ->middleware('StorePaymentTypeValidationMiddleware')
            ->name('api.admin.payment-type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\PaymentTypeController@updatePaymentType')->name('api.admin.payment-type.update');
                Route::get('/datatable', 'API\V1\Admin\PaymentTypeController@datatable')->name('api.admin.payment-type.datatable');
            });

            Route::prefix('pemilikan-stor')->group(function () {
                Route::post('/store', 'API\V1\Admin\StoreOwnershipController@addStoreOwnership')
            ->middleware('StoreOwnershipValidationMiddleware')
            ->name('api.admin.store-ownership.store');
                Route::patch('/update/{id}', 'API\V1\Admin\StoreOwnershipController@updateStoreOwnership')->name('api.admin.store-ownership.update');
                Route::get('/datatable', 'API\V1\Admin\StoreOwnershipController@datatable')->name('api.admin.store-ownership.datatable');
            });

            Route::prefix('pemilikan-kilang')->group(function () {
                Route::post('/store', 'API\V1\Admin\FactoryOwnershipTypeController@addFactoryOwnership')
            ->middleware('StoreFactoryOwnershipValidationMiddleware')
            ->name('api.admin.factory-ownership.store');
                Route::patch('/update/{id}', 'API\V1\Admin\FactoryOwnershipTypeController@updateFactoryOwnership')->name('api.admin.factory-ownership.update');
                Route::get('/datatable', 'API\V1\Admin\FactoryOwnershipTypeController@datatable')->name('api.admin.factory-ownership.datatable');
            });

            Route::prefix('jenis-bangsa')->group(function () {
                Route::post('/store', 'API\V1\Admin\RaceTypeController@addRaceType')
            ->middleware('StoreRaceTypeValidationMiddleware')
            ->name('api.admin.race-type.store');
                Route::patch('/update/{id}', 'API\V1\Admin\RaceTypeController@updateRaceType')->name('api.admin.race-type.update');
                Route::get('/datatable', 'API\V1\Admin\RaceTypeController@datatable')->name('api.admin.race-type.datatable');
            });
        }
    );
});
