<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SetRaceAndRaceTypeInParterToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_partners', function (Blueprint $table) {
            $table->bigInteger('race_type_id')->nullable()->unsigned()->change();
            $table->bigInteger('race_id')->nullable()->unsigned()->change();
        });

        Schema::table('license_application_company_partners', function (Blueprint $table) {
            $table->bigInteger('race_type_id')->nullable()->unsigned()->change();
            $table->bigInteger('race_id')->nullable()->unsigned()->change();
        });

        Schema::table('approval_letter_application_company_partners', function (Blueprint $table) {
            $table->bigInteger('race_type_id')->nullable()->unsigned()->change();
            $table->bigInteger('race_id')->nullable()->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parter_to_nullable', function (Blueprint $table) {
            //
        });
    }
}
