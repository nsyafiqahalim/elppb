<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
    public const ACTIVE = 1;
    
    public $guarded = ['id'];
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
