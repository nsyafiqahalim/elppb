<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OwnershipStatus extends Model
{
    public $guarded = ['id'];
}
