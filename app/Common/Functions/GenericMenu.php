<?php

namespace App\Common\Functions;
use App\Models\Menu\Batch;
use App\Models\Menu\Group;
use Auth;
use Session;
/**
 *
 * This class is for generating menu in in SPWTD easily
 *
 * Class GenericMenu
 * @package App\Common\Functions
 */

class GenericMenu {

	/**
	 *
	Author
	Name : Mohamamd Khairi Bin Ali
	Email : mohammadkhairibinali@gmail.com
	Phone Number : 0129101561
	 *
	The menu can be generated if only menu batch is exist. to do this, you'll need to add menu batch code parameter to extract the menu.
	 * only string
	 *
	 * @param $data must be array so that it is easy to be manage in the future
	 * @return json
	 */
	public function getMenu($data) {
		$menu = Batch::distinct()->activeMenu();
		if (isset($data["code"])) {
			$menu = $menu->where('code', $data["code"]);
		}
		if (isset($data["workflow_status_id"]) || isset($data["workflow_code"])) {

			$statusid = null;
			$workflow_code = null;
			if (isset($data["workflow_status_id"])) {
				$statusid = $data["workflow_status_id"];
			}
			if (isset($data["workflow_code"])) {
				$workflow_code = $data["workflow_code"];
			}
			$menu = $menu->WorkflowItems($statusid, $workflow_code);
		}
		if (isset($data["menu_group_id"])) {
			$menu = $menu->where("menu_group_id", trim($data["menu_group_id"]));
		}
		if (isset($data["with_permission"])) {
			$menu = $menu->whereHas("menus", function ($menus) {
				//$menus->withPermission();
			});
		}

		$menu = $menu->get();
		return $menu;
	}

	public function getMenuGroup() {

		$menugroups = Group::distinct()
			->where('is_active', 1)
			->whereHas("permission",function($p){
				$p->whereHas("rolePermission",function($rp){
					$rp->whereHas("role",function($r){
						$r->whereHas("userRole",function($ur){
							$ur->whereHas("user",function($u){
								$u->where("id",Auth::user()->id);
							});
						});
					});
				});
			});

		$menugroups = $menugroups->orderby("id", "asc")->get();
		return $menugroups;
	}

	public function generateMenuSideBar($data) {
		$data["with_permission"] = true;

		if(isset($data)){
			$menu = $this->getMenu($data)->first();
			//$menu = ;
			if (isset($menu)) {
				$menu = $menu->menus;
				return view("applicationcomponent.menu.sidebar", compact("menu"))->render();
			} else {
				return null;
			}
		}
		else{
			return null;
		}

	}

	public function generateMenuButton($data) {
		$menu = $this->getMenu($data)->first();
		return (isset($menu)) ? $menu->menus : null;
	}

	public function generateMenuHeader($data) {
		$menu = $this->getMenu($data)->first()->menus;
		return view("applicationcomponent.menu.header", compact("menu"))->render();
	}

	public function getMenuDropdown($data) {
		$menu = $this->getMenu($data)->first();
		return (isset($menu)) ? $menu->menus : null;
	}

	public function generateMenuGroup($data = null) {
		$menugroup = Session::get("menugroups");
		
		return view("applicationcomponent.menu.menugroup", compact("menugroup"))->render();

	}

}