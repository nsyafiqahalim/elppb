<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Portal\PermitDataObject;

class PermitController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addPermit(Request $request)
    {
        $param = $request->all();
        $decryptedUserId = decrypt($param['user_id']);

        $param['created_by'] = $decryptedUserId;
        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $permitDAO = PermitDataObject::storeNewPermit($param);

        return response()->json([
            'message'   =>  'Maklumat Permit berjaya ditambah',
            'data'      =>  $permitDAO,
            'success'   =>  true,
            'route'     =>  route('portal.permit.index')
        ], 200);
    }

    public function updatePermit(Request $request, $id)
    {
        $param = $request->all();

        $decryptedId = decrypt($id);
        $decryptedUserId = decrypt($param['user_id']);

        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $permitDAO = PermitDataObject::updatePermit($param, $decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Permit berjaya dikemaskini',
            'data'      =>  $permitDAO,
            'success'   =>  true,
            'route'     =>  route('portal.permit.index')
        ], 200);
    }

    public function deletePermit($id)
    {
        $decryptedId = decrypt($id);

        $permitDAO = PermitDataObject::deletePermit($decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Permit berjaya dipadam',
            'data'      =>  $permitDAO,
            'success'   =>  true,
            'route'     =>  route('portal.permit.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return PermitDataObject::findAllPermitUsingDatatableFormat($param);
    }
}
