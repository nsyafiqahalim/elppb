<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RoleDataObject;
use App\DataObjects\Admin\PermissionDataObject;

class RoleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.role.index');
    }

    public function add()
    {
        $permissions = PermissionDataObject::findAllPermissions();

        return view('admin.role.add', compact('permissions'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $role = RoleDataObject::findRoleById($decryptedId);
        $permissions = PermissionDataObject::findAllPermissions();

        return view('admin.role.edit', compact('permissions', 'role'));
    }
}
