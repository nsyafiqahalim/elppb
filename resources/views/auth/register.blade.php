@extends('layouts.external-layout')

@section('content')
<form class="form-horizontal form-material" id="loginform" method="post" action="{{ route('register') }}">
    @csrf
    <h3 class="box-title mb-3" style="text-align:center">Sistem Lesen Permit Padi Dan Beras 2.0</h3>
    <div class="form-group ">
        <div class="col-xs-12">
            <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" placeholder='Nama' value="{{ old('name') }}" required autocomplete="name" autofocus>
            @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group ">
        <div class="col-xs-12">
            <input id="kad_pengenalan" type="text" class="form-control @error('kad_pengenalan') is-invalid @enderror" name="kad_pengenalan" placeholder='Kad Pengenalan' value="{{ old('kad_pengenalan') }}" required autocomplete="kad_pengenalan" autofocus>
            @error('kad_pengenalan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group ">
        <div class="col-xs-12">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" placeholder='E-mel' value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder='Kata Laluan' required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
     <div class="form-group">
        <div class="col-xs-12">
            <input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" placeholder='Pengesahan Kata Laluan' required autocomplete="current-password_confirmation">
            @error('password_confirmation')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    
    <div class="form-group text-center mt-3">
        <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Daftar Masuk</button>
        </div>
    </div>

    <div class="form-group mb-0">
        <div class="col-sm-12 text-center">
            <p>Mahu Log Masuk? <a href="{{ route('login') }}" class="text-info ml-1"><b>Log Masuk</b></a></p>
        </div>
    </div>
    </form>
@endsection
