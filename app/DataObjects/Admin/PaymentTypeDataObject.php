<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\PaymentType;

class PaymentTypeDataObject
{
    public static function addPaymentType($param)
    {
        $paymentType = null;

        DB::transaction(function () use ($param, &$paymentType) {
            $paymentType = PaymentType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $paymentType;
    }

    public static function updatePaymentType($param, $id)
    {
        $paymentType = null;

        DB::transaction(function () use ($param, $id,  &$paymentType) {
            $paymentType = PaymentType::find($id);

            $paymentType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $paymentType;
    }

    public static function findAllPaymentTypeUsingDatatableFormat($param)
    {
        $paymentType = PaymentType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($paymentType)
        ->addIndexColumn()
        ->addColumn('action', function ($paymentType) {
            return view('admin.payment-type.partials.datatable-button', compact('paymentType'))->render();
        })
        ->addColumn('status', function ($paymentType) {
            $status= ($paymentType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.payment-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findPaymentTypeById($id)
    {
        return PaymentType::find($id);
    }
}
