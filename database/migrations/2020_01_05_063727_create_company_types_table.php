<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('old_id')->nullable();
            $table->string('code', 30);
            $table->string('name', 255);
            $table->text('description');
            $table->tinyInteger('is_active')->default(0);
            $table->tinyInteger('has_paidup_capital')->default(0);
            $table->tinyInteger('has_expiry_date')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_types');
    }
}
