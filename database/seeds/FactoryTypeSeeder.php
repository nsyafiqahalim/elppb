<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\FactoryType;

class FactoryTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Kilang Padi Beras (Loji Biji Benih)',
            'Kilang Padi Komersial',
            'Kilang Padi Upah Kisar',
            'Tiada',
        ];

        foreach ($data as $item) {
            FactoryType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
