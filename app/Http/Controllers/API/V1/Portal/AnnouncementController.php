<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Portal\AnnouncementDataObject;

class AnnouncementController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addAnnouncement(Request $request)
    {
        $param = $request->all();
        $decryptedUserId = decrypt($param['user_id']);

        $param['created_by'] = $decryptedUserId;
        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $announcementDAO = AnnouncementDataObject::addAnnouncement($param);

        return response()->json([
            'message'   =>  'Pengumuman berjaya ditambah',
            'data'      =>  $announcementDAO,
            'success'   =>  true,
            'route'     =>  route('portal.announcement.index')
        ], 200);
    }

    public function updateAnnouncement(Request $request, $id)
    {
        $param = $request->all();

        $decryptedId = decrypt($id);
        $decryptedUserId = decrypt($param['user_id']);

        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $announcementDAO = AnnouncementDataObject::updateAnnouncement($param, $decryptedId);

        return response()->json([
            'message'   =>  'Pengumuman berjaya dikemaskini',
            'data'      =>  $announcementDAO,
            'success'   =>  true,
            'route'     =>  route('portal.announcement.index')
        ], 200);
    }

    public function deleteAnnouncement($id)
    {
        $decryptedId = decrypt($id);

        $announcementDAO = AnnouncementDataObject::deleteAnnouncement($decryptedId);

        return response()->json([
            'message'   =>  'Pengumuman berjaya dipadam',
            'data'      =>  $announcementDAO,
            'success'   =>  true,
            'route'     =>  route('portal.announcement.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return AnnouncementDataObject::findAllAnnouncementUsingDatatableFormat($param);
    }
}
