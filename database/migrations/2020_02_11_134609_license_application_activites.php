<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationActivites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status_id');
            $table->text('activity');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('license_application_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('license_application_id')->references('id')->on('license_applications');
            $table->timestamps();
        });

        Schema::create('approval_letter_application_activities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status_id');
            $table->text('activity');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('approval_letter_application_id')->index('alaa_alp_idx');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('approval_letter_application_id', 'fk_alaa_alpa')->references('id')->on('approval_letter_applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
