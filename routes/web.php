<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::get('/pengesahan-akaun-dalaman/{id}', 'Auth\LoginController@loginViaUrl')->name('auth.login_via_url');


Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
