<?php

namespace App\DataObjects;

use DB;
use Carbon\Carbon;
use App\Models\LicenseApplication;

class LicenseApplicationDataObject
{
    public static function createNewLicenseApplication($param)
    {
        return LicenseApplication::create([
            'reference_number'          =>  $param['reference_number'],
            'status_id'                 =>  $param['status_id'],
            'branch_id'                 =>  $param['branch_id'],
            'apply_duration'            =>  $param['apply_duration'],
            'apply_load'                =>  $param['apply_load'],
            'apply_date'                =>  Carbon::now()->format('Y-m-d')

        ]);
    }

    public static function findAllLicenseApplication()
    {
        return LicenseApplication::all();
    }

    public static function findLicenseApplicationById($id)
    {
        return LicenseApplication::find($id);
    }

    public static function updateLicenseStatusByStatusId($id, $status)
    {
        return LicenseApplication::find($id)->update([
            'status_id' =>  $status->id
        ]);
    }
}
