<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\Info;

class InfoDataObject
{
    public static function storeNewInfo($param)
    {
        return Info::create([
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'phone_number'              =>  $param['phone_number'],
            'email'                     =>  $param['email'],
            'state'                     =>  $param['state'],
            'license_application_id'    =>  $param['license_application_id'],
        ]);
    }
}
