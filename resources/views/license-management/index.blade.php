@extends('layouts.license-management-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-bookmark"></i> Pengurusan Lesen</h3>
        <ol class="breadcrumb">
        </ol>
    </div>
</div>
@endsection
