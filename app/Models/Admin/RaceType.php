<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class RaceType extends Model
{
    public $guarded = ['id'];
}
