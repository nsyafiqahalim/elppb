<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\FactoryTypeDataObject;

class FactoryTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.factory-type.index');
    }

    public function add()
    {
        return view('admin.factory-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $factoryType = FactoryTypeDataObject::findFactoryTypeById($decryptedId);

        return view('admin.factory-type.edit', compact('factoryType'));
    }
}
