<?php

namespace App\Http\Controllers\Auth;

use Activity;
use App\Http\Controllers\Controller;
use Auth;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Session;
use App\User;
use Lang;

class LoginController extends Controller
{
    /*
            |--------------------------------------------------------------------------
            | Login Controller
            |--------------------------------------------------------------------------
            |
            | This controller handles authenticating users for the application and
            | redirecting them to your home screen. The controller uses a trait
            | to conveniently provide its functionality to your applications.
            |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.

        $user = User::where('login_id', $request->login_id)->first();

        if ($user && $user->status != 'ACTIVE') {
            return $this->sendLockedAccountResponse($request);
        } else {
            if ($user && $user->is_active == 1) {
                if ($this->hasTooManyLoginAttempts($request)) {
                    // block account
                    $this->deactivateUser($request);
                    return $this->sendLockedAccountResponse($request);
                }
            } else {
                if ($this->hasTooManyLoginAttempts($request)) {
                    // block for browser
                    $this->fireLockoutEvent($request);
                    return $this->sendLockoutResponse($request);
                }
            }

            if ($this->attemptLogin($request)) {
                // check case sensitive
                if (Auth::user()->login_id !== $request->login_id) {
                    Session::flush();
                    $this->incrementLoginAttempts($request);

                    return $this->sendFailedLoginResponse($request);
                }

                // added to log user login activity
                return $this->sendLoginResponse($request);
            }

            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        }
    }

    public function loginViaUrl($id)
    {
        $decryptedId = decrypt($id);
        Auth::loginUsingId($decryptedId);
        
        return redirect()->route("home");
    }

    public function authenticated(Request $request, $user)
    {
        return redirect()->route("home");
    }

    /**
     *
     * Authenticate using login_id instead of email
     *
     * @return string
     */
    public function username()
    {
        return 'login_id';
    }

    /**
     * Get the locked account response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLockedAccountResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors([
                $this->username() => $this->getLockedAccountMessage(),
            ]);
    }

    /**
     * Get the locked account message.
     *
     * @return string
     */
    protected function getLockedAccountMessage()
    {
        return Lang::has('auth.locked')
                ? Lang::get('auth.locked')
                : 'Akaun anda tidak aktif. Sila hubungi Pentadbir Sistem untuk bantuan.';
    }

    public function logUserLogin()
    {
    }

    /**
     * Log the user out of the application.
     * Overrides default logout
     *
     * @return \Illuminate\Http\Response
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        $this->guard()->logout();

        return redirect('http://178.128.19.132:81');
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request),
            $this->maxAttempts()
        );
    }

    public function maxAttempts()
    {
        return property_exists($this, 'maxAttempts') ? $this->maxAttempts : 3;
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    public function deactivateUser(Request $request)
    {
        $user = User::where('login_id', $request->login_id)->firstOrFail();
        $user->is_active = 0;
        $user->save();

        $this->clearLoginAttempts($request);
    }

    public function updateLastLogin()
    {
        $user_id = Auth::user()->id;
        $last_login=null;
        $last_logout=null;

        $user = User::find($user_id);
        if ($user->last_login != null) {
            $last_logout = $user->last_login;
            $last_login = Carbon::now();
        } else {
            $last_login = Carbon::now();
        }

        $user->last_login = $last_login;
        $user->last_logout = $last_logout;
        $user->save();
    }
}
