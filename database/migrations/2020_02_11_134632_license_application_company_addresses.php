<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->string('state_name');
            $table->string('phone_number');
            $table->string('email');
            $table->enum('type', ['MAILING'])->default('MAILING');
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('license_application_id')->index('laca_la_idx');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('license_application_id', 'fk_laca_la')->references('id')->on('license_applications');

            $table->timestamps();
        });

        Schema::create('approval_letter_application_company_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->string('state_name');
            $table->string('phone_number');
            $table->string('email');
            $table->enum('type', ['MAILING'])->default('MAILING');
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('approval_letter_application_id')->index('alca_la_idx');
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('approval_letter_application_id', 'fk_alca_la')->references('id')->on('approval_letter_applications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
