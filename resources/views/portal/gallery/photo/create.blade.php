@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
      <h2 class="text-themecolor mb-0 mt-0">Muat Naik Gambar</h2>
      <ol class="breadcrumb">
          <li class="breadcrumb-item">Album</li>
          <li class="breadcrumb-item"><a href="{{ route('portal.gallery.album.index') }}">Gambar</a></li>
          <li class="breadcrumb-item">Muat Naik Gambar</li>
      </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample" method="post" action="{{ route('photo-store') }}" enctype="multipart/form-data">
                        @csrf
                        <input type='hidden' name='album-id' value="{{ $albumId }}" />
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="gambar">Gambar <span class='required'>*</span>
                                            <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="" data-original-title="Contoh: .jpeg, .jpg, .png, .gif, .bmp, .svg"><i
                                                  class="fas fa-info-circle"></i></button>
                                        </label>
                                        <input type='file' class='form-control' name='gambar' id='gambar' multiple />
                                    </div>
                                </div>
                            </div> <!-- End Row -->
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                <i class="fas fa-upload"></i> Muat Naik
                            </button>
                            <a href="{{ route('portal.gallery.album.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
