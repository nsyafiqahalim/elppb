<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * MULA
         * SELAIN PERMOHONAN PENGGANTIAN DAN PEMBATALAN, status permohonan
         */
        //RUNCIT
        $types = [
            'BAHARU',
            'PEMBAHARUAN',
            'PERUBAHAN',
            'PEMBAHARUAN DAN PERUBAHAN',
        ];

        $data = [
            'Draf',
            'Permohonan Baharu',
            'Peraku Semakan Cawangan',
            'Kelulusan Permohonan',
            'Janaan Lesen',
            'Tamat',
            'Tolak',
        ];

        $displayData = [
            'Draf',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Membuat Bayaran',
            'Tamat',
            'Ditolak',
        ];


        foreach ($types as $type) {
            foreach ($data as $index => $item) {
                Status::create([
                    'name'  =>  $item,
                    'display_name'  =>  $displayData[$index],
                    'code'  =>  strtoupper(str_replace(' ', '_', $item)).'_RUNCIT'.'_'.str_replace(' ', '_', $type),
                    'domain'    => 'RUNCIT_'.str_replace(' ', '_', $type),
                    'description'   =>  'Data ini merujuk kepada data jenis '.$item
                ]);
            }
        }

        ///BUKAN RUNCIT

        $types = [
            'BAHARU',
            'PEMBAHARUAN',
            'PERUBAHAN',
            'PEMBAHARUAN DAN PERUBAHAN',
        ];

        $licenses = [
            'BORONG',
            'IMPORT',
            'EKSPORT',
            'KILANG PADI KOMERSIL',
            'BELI PADI',
            'MENGERING PADI DAN KEGUNAAN SENDIRI',
        ];
        $data = [
            'Draf',
            'Permohonan Baharu',
            'Pemilihan Pegawai Siasatan',
            'Peraku Semakan Siasatan',
            'Peraku Semakan Ibu Pejabat',
            'Kelulusan Permohonan',
            'Janaan Lesen',
            'Tamat',
            'Tolak',
        ];
        $displayData = [
            'Draf',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Membuat Bayaran',
            'Tamat',
            'DiTolak',
        ];

        foreach ($licenses as $license) {
            foreach ($types as $type) {
                foreach ($data as $index => $item) {
                    Status::create([
                        'name'  =>  $item,
                        'display_name'  =>  $displayData[$index],
                        'code'  =>  strtoupper(str_replace(' ', '_', $item)).'_'.str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'domain'    =>  str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'description'   =>  'Data ini merujuk kepada data jenis '.$item
                    ]);
                }
            }
        }

        /**
         * TAMAT
         */

        /**
         * MULA
         */

        $types = [
            'PENGGANTIAN',
            'PEMBATALAN',
        ];

        $data = [
            'Draf',
            'Permohonan Baharu',
            'Peraku Semakan Cawangan',
            'Kelulusan Permohonan',
            'Janaan Lesen',
            'Tamat',
            'Tolak',
        ];

        ///BUKAN RUNCIT
        $licenses = [
            'RUNCIT',
            'BORONG',
            'IMPORT',
            'EKSPORT',
            'KILANG PADI KOMERSIL',
            'MENGERING PADI DAN KEGUNAAN SENDIRI',
            'BELI PADI'
        ];

        $displayData = [
            'Draf',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Membuat Bayaran',
            'Tamat',
            'Ditolak',
        ];

        foreach ($licenses as $license) {
            foreach ($types as $type) {
                foreach ($data as $index => $item) {
                    Status::create([
                        'name'  =>  $item,
                        'display_name'  =>  $displayData[$index],
                        'code'  =>  strtoupper(str_replace(' ', '_', $item)).'_'.str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'domain'    =>  str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'description'   =>  'Data ini merujuk kepada data jenis '.$item
                    ]);
                }
            }
        }

        $licenses = [
            'Aktif',
            'Tamat Tempoh',
            'Permohonan Penggantian',
            'Permohonan Pembatalan',
            'Permohonan Pembaharuan',
            'Permohonan Perubahan',
            'Permohonan Pembaharuan Dan Perubahan',
            'Dibatalkan',
        ];

        foreach ($licenses as $license) {
            Status::create([
                        'name'  =>  $license,
                        'display_name'  =>  $license,
                        'code'  => 'LESEN_'.strtoupper(str_replace(' ', '_', $license)),
                        'domain'    =>  'LESEN',
                        'description'   =>  'Data ini merujuk kepada data jenis '.$license
                    ]);
        }



        ///BUKAN RUNCIT

        $types = [
            'BAHARU'
        ];

        $licenses = [
            'KILANG PADI KOMERSIL',
            'BELI PADI'
        ];
        $data = [
            'Draf',
            'Permohonan Baharu',
            'Pemilihan Pegawai Siasatan',
            'Peraku Semakan Siasatan',
            'Peraku Semakan Ibu Pejabat',
            'Kelulusan Permohonan',
            'Cetakan Surat Kelulusan Bersyarat',
            'Tamat',
            'Tolak',
        ];
        $displayData = [
            'Draf',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Ambil Surat Kelulusan Bersyarat',
            'Tamat',
            'DiTolak',
        ];

        foreach ($licenses as $license) {
            foreach ($types as $type) {
                foreach ($data as $index => $item) {
                    Status::create([
                        'name'  =>  $item,
                        'display_name'  =>  $displayData[$index],
                        'code'  =>  strtoupper(str_replace(' ', '_', $item)).'_SURAT_KELULUSAN_BERSYARAT_'.str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'domain'    =>  str_replace(' ', '_', $license).'_'.str_replace(' ', '_', $type),
                        'description'   =>  'Data ini merujuk kepada data jenis '.$item
                    ]);
                }
            }
        }

        $licenses = [
            'Aktif',
            'Tamat Tempoh',
            'Dibatalkan',
            'Telah Diganti',
            'Telah Dikemaskini'
        ];
        foreach ($licenses as $license) {
            Status::create([
                        'name'  =>  $license,
                        'display_name'  =>  $license,
                        'code'  => 'SURAT_KELULUSAN_BERSYARAT_'.strtoupper(str_replace(' ', '_', $license)),
                        'domain'    =>  'SURAT_KELULUSAN_BERSYARAT',
                        'description'   =>  'Data ini merujuk kepada data jenis '.$license
                    ]);
        }

        $licenses = [
            'Aktif',
            'Tamat Tempoh',
            'Dibatalkan',
            'Telah Diganti',
            'Telah Dikemaskini'
        ];
        foreach ($licenses as $license) {
            Status::create([
                        'name'  =>  $license,
                        'display_name'  =>  $license,
                        'code'  => 'PERMIT_'.strtoupper(str_replace(' ', '_', $license)),
                        'domain'    =>  'PERMIT',
                        'description'   =>  'Data ini merujuk kepada data jenis '.$license
                    ]);
        }

        $licenses = [
            'Sedang Proses',
            'Aktif',
            'Tamat',
        ];
        //Lesen yang bakal dijana oleh sesuatu permohonan
        
        foreach ($licenses as $license) {
            Status::create([
                        'name'  =>  $license,
                        'display_name'  =>  $license,
                        'code'  => 'KUMPULAN_LESEN_'.strtoupper(str_replace(' ', '_', $license)),
                        'domain'    =>  'KUMPULAN_LESEN',
                        'description'   =>  'Data ini merujuk kepada data jenis '.$license
                    ]);
        }

        $licenses = [
            'Perlu Dicetak',
            'Tidak Perlu Dicetak',
            'Telah Dicetak',
            'Telah Perlu Dibatalkan',
            'Perlu Dibatalkan',
            'Perlu Digantikan',
            'Telah Perlu Digantikan',
        ];

        
        foreach ($licenses as $license) {
            Status::create([
                        'name'  =>  $license,
                        'display_name'  =>  $license,
                        'code'  => 'LESEN_BAKAL_DIJANA_'.strtoupper(str_replace(' ', '_', $license)),
                        'domain'    =>  'LESEN_BAKAL_DIJANA_',
                        'description'   =>  'Data ini merujuk kepada data jenis '.$license
                    ]);
        }
    }
}
