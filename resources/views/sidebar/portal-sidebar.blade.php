<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('index') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Dashboard</a></li>
        <li><a href="{{ route('portal.gallery.index') }}" aria-expanded="false"><i class="mdi mdi-bullhorn"></i><span class="hide-menu">Pengumuman</a></li>
        <!-- <li><a href="{{ route('file-manager') }}" aria-expanded="false"><i class="mdi mdi-image"></i><span class="hide-menu"> Galeri</a></li> --->
        <li><a href="{{ route('portal.gallery.album.index') }}" aria-expanded="false"><i class="mdi mdi-image"></i><span class="hide-menu">Galeri</a></li>
        <li class="dropright-submenu"><i class="mdi mdi-settings"></i> Info</a>
            <ul class="dropright-menu">
                <li><a href="{{ route('portal.setting.index',[encrypt('director')]) }}">Perutusan Ketua Pengarah</a></li>
                <li><a href="{{ route('portal.setting.index',[encrypt('info')]) }}">Sejarah Penubuhan Seksyen Kawalselia Padi dan Beras</a></li>
                <li><a href="{{ route('portal.setting.index',[encrypt('SKPB')]) }}">Peranan atau Fungsi Seksyen Kawalselia Padi dan Beras</a></li>
                <li><a href="{{ route('portal.setting.index',[encrypt('info2')]) }}">Akta dan Peraturan Yang Berkuatkuasa</a></li>
            </ul>
        </li>
        <li><a href="{{ route('portal.license.index') }}" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu"> Lesen</a></li>
        <li><a href="{{ route('portal.permit.index') }}" aria-expanded="false"><i class="mdi mdi-file-document-box"></i><span class="hide-menu"> Permit</a></li>
        <li><a href="{{ route('portal.faq.index') }}" aria-expanded="false"><i class="mdi mdi-comment-question-outline"></i><span class="hide-menu"> Soalan Lazim</a></li>
    </ul>
</nav>
