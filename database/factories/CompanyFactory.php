<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\Company;
use App\Models\Admin\CompanyType;

$companyType = \App\Models\Admin\CompanyType::all();

$factory->define(Company::class, function (Faker $faker) use ($companyType) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));

    $compType = $companyType->random();
    return [
       'name'                  =>  $faker->companyName,
        'company_type_id'       =>  $compType->id,
        'registration_number'   =>  $faker->numberBetween(100000000, 999999999) . '-' . strtoupper(str_random(1)),
        'expiry_date'           =>  Carbon::now(),
        'paidup_capital'        =>  mt_rand(100, 100000)
    ];
});
