<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Dun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('duns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code', 30);
            $table->string('name', 255);
            $table->text('description')->nullable();
            $table->bigInteger('parliament_id')->nullable()->unsigned()->index();
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();

            //foreign key
            $table->foreign('parliament_id')->references('id')->on('parliaments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
