<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('file_path');
            $table->string('code');
            $table->text('file_original_name');
            $table->unsignedBigInteger('license_application_id')->index('laa_la_idx');
            $table->timestamps();
            $table->foreign('license_application_id', 'fk_laa_la')->references('id')->on('license_applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
