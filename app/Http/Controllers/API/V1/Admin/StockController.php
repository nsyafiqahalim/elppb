<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\StockDataObject;

class StockController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addStock(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $stock = StockDataObject::addStock($param);

        return response()->json([
            'message'   =>  'Jenis Stok berjaya ditambah',
            'data'      =>  $stock,
            'success'   =>  true,
            'route'     =>  route('admin.stock.index')
        ], 200);
    }

    public function updateStock(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $stock = StockDataObject::updateStock($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Stok berjaya dikemaskini',
            'data'      =>  $stock,
            'success'   =>  true,
            'route'     =>  route('admin.stock.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        return StockDataObject::findAllStockUsingDatatableFormat($param);
    }
}
