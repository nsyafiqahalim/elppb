<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LandStatus extends Model
{
    public $guarded = ['id'];
}
