<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\BranchDataObject;
use App\DataObjects\Admin\DistrictDataObject;

class BranchController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.branch.index');
    }

    public function add()
    {
        $districts  = DistrictDataObject::findAllActiveDistricts();

        return view('admin.branch.add', compact('districts'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $branch = BranchDataObject::findBranchById($decryptedId);
        $districts  = DistrictDataObject::findAllActiveDistricts();

        return view('admin.branch.edit', compact('branch', 'districts'));
    }
}
