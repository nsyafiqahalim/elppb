<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Duration;

class DurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '1',
            '2',
            '3'
        ];

        foreach ($data as $item) {
            Duration::create([
                'name'  =>  $item.' Tahun',
                'code'  =>  $item.'_TAHUN',
                'value' =>  $item,
                'is_active' => 1,
                'description'   =>  'Data ini merujuk kepada data jenis '.$item.' Tahun',
            ]);
        }
    }
}
