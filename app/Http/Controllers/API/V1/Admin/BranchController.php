<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\BranchDataObject;

class BranchController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addBranch(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $branch = BranchDataObject::addBranch($param);

        return response()->json([
            'message'   =>  'Jenis cawangan berjaya ditambah',
            'data'      =>  $branch,
            'success'   =>  true,
            'route'     =>  route('admin.branch.index')
        ], 200);
    }

    public function updateBranch(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $branch = BranchDataObject::updateBranch($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis cawangan berjaya dikemaskini',
            'data'      =>  $branch,
            'success'   =>  true,
            'route'     =>  route('admin.branch.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        return BranchDataObject::findAllBranchUsingDatatableFormat($param);
    }
}
