@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Galeri</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Laman Utama</li>
            <li class="breadcrumb-item"><a href="{{ route('portal.index') }}">Pengurusan Portal</a></li>
            <li class="breadcrumb-item">Galeri</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="input-group">
            <iframe src="{{ url('/filemanager?type=file') }}" style="width: 100%; height: 70vh; overflow: hidden; border: none;"></iframe>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$('#lfm').filemanager('file');
</script>
@endsection
