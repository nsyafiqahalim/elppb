<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenseApplicationPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status_payment');
            $table->float('total_payment');
            $table->unsignedBigInteger('license_application_id')->index();
            $table->foreign('license_application_id')->references('id')->on('license_applications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('license_application_payments');
    }
}
