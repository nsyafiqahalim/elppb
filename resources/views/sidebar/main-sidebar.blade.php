<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="nav-small-cap">Menu Utama</li>
        <li>
            <a href="{{ route('admin.index') }}" aria-expanded="false"><i class="mdi mdi-settings"></i><span
                    class="hide-menu">Pentadbiran Sistem</a></li>
        <li class="dropdown">
            <a href="{{ route('management.index') }}" class="has-arrow" aria-expanded="false"><i class="mdi mdi-book"></i><span class="hide-menu"> Pengurusan Lesen</a>
        </li>
        <li>
            <a href="{{ route('portal.index') }}" aria-expanded="false"><i class="mdi mdi-web"></i><span class="hide-menu">Pengurusan Portal</a>
        </li>
        <li>
            <a href="#" aria-expanded="false"><i class="mdi mdi-ticket"></i><span class="hide-menu"> Meja Bantuan</a>
        </li>
    </ul>
</nav>
