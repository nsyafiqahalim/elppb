<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreBranchValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'short_code' => ['required', 'string','max:6'],
            'is_active' => ['required', 'string'],
        ], [
            'name.required' => ['Medan Nama diperlukan'],
            'short_code.required' => ['Medan Kod diperlukan'],
            'is_active.required' => ['Medan Status diperlukan'],
        ]);

        return $next($request);
    }
}
