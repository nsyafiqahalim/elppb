<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Branch;

class BranchDataObject
{
    public static function addBranch($param)
    {
        $branch = null;

        DB::transaction(function () use ($param, &$branch) {
            $branch = Branch::create([
                'name'          =>  $param['name'],
                'short_code'    =>  $param['short_code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        foreach ($param['districts'] as $district) {
            $branch->areaCoverages()->create([
                'district_id'   => decrypt($district)
            ]);
        }

        return $branch;
    }

    public static function updateBranch($param, $id)
    {
        $branch = null;

        DB::transaction(function () use ($param, $id,  &$branch) {
            $branch = Branch::find($id);

            $branch->update([
                'name'          =>  $param['name'],
                'short_code'    =>  $param['short_code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
            ]);

            $branch->areaCoverages()->delete();

            foreach ($param['districts'] as $district) {
                $branch->areaCoverages()->create([
                'district_id'   => decrypt($district)
            ]);
            }
        });
        return $branch;
    }

    public static function findAllBranchUsingDatatableFormat($param)
    {
        $branches = Branch::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('short_code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($branches)
        ->addIndexColumn()
        ->addColumn('action', function ($branch) {
            return view('admin.branch.partials.datatable-button', compact('branch'))->render();
        })
        ->addColumn('status', function ($branch) {
            $status= ($branch->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.branch.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findBranchById($id)
    {
        return Branch::find($id);
    }

    public static function findAllActiveBranches(){
        return Branch::activeOnly()->get();
    }
}
