@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-users"></i> Pengguna dan Peranan</h3>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="{{ route ('admin.index') }}">Pentadbiran Sistem</a></li>
          <li class="breadcrumb-item active">Peranan</li>
          <li class="breadcrumb-item active">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Peranan</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.role.update',[encrypt($role->id)]) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label">Nama Peranan <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-user"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Peranan" value="{{ (isset($role)) ? $role->name : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="description_label">Keterangan <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-pencil"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="description" name="description" placeholder="Keterangan" value="{{ (isset($role)) ? $role->description : '' }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="description_label">Kebenaran</label>
                                        <select class="select-permission form-control" id="permissions" name="permissions[]" data-placeholder="Pilih Kebenaran" multiple>
                                            @foreach($permissions as $permission)
                                            <option {{ isset($role) && $role->hasPermissionTo($permission['name']) ? 'selected' : '' }} value="{{ $permission['name'] }}">{{ $permission['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route('admin.role.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
    <link href="{{ asset('dualListbox/dist/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
@endpush

@push('js')
<script src="{{ asset('dualListbox/dist/jquery.bootstrap-duallistbox.js') }}"></script>
<script>
$('.select-permission').bootstrapDualListbox({
    nonSelectedListLabel: 'Belum Dipilih',
    selectedListLabel: 'Sudah Dipilih',
    preserveSelectionOnMove: 'Pindahkan',
    infoText: 'Tunjuk semua {0}',
    infoTextFiltered: '<span class="label label-warning">Ditapis</span> {0} dari {1}',
    filterPlaceHolder:'Carian',
    infoTextEmpty:'Senarai yang kosong',
    selectorMinimalHeight:300,
    moveOnSelect: true,
});
</script>
@endpush
