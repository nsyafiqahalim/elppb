<?php

namespace App\Models\ReferenceData;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\StockCategory;
use App\Models\Admin\SpinType;
use App\Models\Admin\TotalGrade;
use App\Models\Admin\RiceGrade;
use App\Models\Admin\StockType;

class CompanyStockStatement extends Model
{
    public $guarded = ['id'];
    public $table = 'company_stock_statements';

    private const ACTIVE = 1;

    public function stockCategory()
    {
        return $this->belongsTo(StockCategory::class, 'stock_category_id');
    }

    public function spinType()
    {
        return $this->belongsTo(SpinType::class, 'spin_type_id');
    }

    public function totalGrade()
    {
        return $this->belongsTo(TotalGrade::class, 'total_grade_id');
    }

    public function riceGrade()
    {
        return $this->belongsTo(RiceGrade::class, 'rice_grade_id');
    }

    public function stockType()
    {
        return $this->belongsTo(StockType::class, 'stock_type_id');
    }
}
