<?php

use Illuminate\Database\Seeder;

use App\Models\Portal\Announcement;

class PortalAnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Announcement::create([
            'created_by'    =>  '1',
            'updated_by'    =>  '1',
            'is_published'  =>  1,
            'description'   =>  '<p> Kepada semua pengguna sistem eLPPB sila gunakan ID dan kata laluan yang telah diemelkan kepada tuan/ puan untuk log masuk sistem. Sebarang masalah untuk log masuk sistem emelkan aduan kepada elesen_admin@moa.gov.my </p>'
        ]);

        Announcement::create([
            'created_by'    =>  '1',
            'updated_by'    =>  '1',
            'is_published'  =>  1,
            'description'   =>  '<p> Kepada semua pengguna sistem eLPPB sila gunakan ID dan kata laluan yang telah diemelkan kepada tuan/ puan untuk log masuk sistem. Sebarang masalah untuk log masuk sistem emelkan aduan kepada elesen_admin@moa.gov.my </p>'
        ]);

        Announcement::create([
            'created_by'    =>  '1',
            'updated_by'    =>  '1',
            'is_published'  =>  1,
            'description'   =>  '<p> Kepada semua pengguna sistem eLPPB sila gunakan ID dan kata laluan yang telah diemelkan kepada tuan/ puan untuk log masuk sistem. Sebarang masalah untuk log masuk sistem emelkan aduan kepada elesen_admin@moa.gov.my </p>'
        ]);
    }
}
