<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseApplicationBuyPaddyCondition;

class LicenseApplicationBuyPaddyConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LicenseApplicationBuyPaddyCondition::create([
            'name'  =>  'Bagi maksud dijual ke kilang sendiri',
            'code'  =>  'JUAL_KE_KILANG_SENDIRI',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis bagi maksud dijual ke kilang sendiri'
        ]);

        LicenseApplicationBuyPaddyCondition::create([
            'name'  =>  'Bagi maksud dijual ke kilang',
            'code'  =>  'JUAL_KE_KILANG',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis bagi maksud dijual ke kilang'
        ]);

        LicenseApplicationBuyPaddyCondition::create([
            'name'  =>  'Bagi maksud dibuat biji benih',
            'code'  =>  'BUAT_BIJI_BENIH',
            'is_active' =>  1,
            'description'   =>  'Data ini merujuk kepada data jenis bagi buat bijih benih'
        ]);
    }
}
