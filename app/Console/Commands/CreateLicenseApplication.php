<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\ReferenceData\Company;
use App\Models\Admin\Status;
use App\Models\Admin\LicenseType;
use App\Models\License;
use App\Models\Admin\LicenseApplicationType;
use App\Models\Admin\LicenseApplicationCategory;
use App\Models\Admin\StockStatementPeriod;
use App\Models\LicenseApplication\IncludeLicense;
use App\Models\LicenseApplication\IncludeLicenseItem;
use App\Models\LicenseApplication\CompanyStoreIncludeLicenseItem;

use App\Models\ReferenceData\companyStockStatementAccount;
use App\DataObjects\ReferenceData\CompanyDataObject as OriginalCompanyDataObject;
use App\DataObjects\ReferenceData\CompanyPremiseDataObject as OriginalCompanyPremiseDataObject;
use App\DataObjects\ReferenceData\CompanyPartnerDataObject as OriginalCompanyPartnerDataObject;
use App\DataObjects\ReferenceData\CompanyStatementDataObject as OriginalCompanyStockStatementDataObject;

use App\DataObjects\LicenseApplication\AttachmentDataObject;
use App\DataObjects\LicenseApplication\CompanyDataObject;
use App\DataObjects\LicenseApplication\CompanyAddressDataObject;
use App\DataObjects\LicenseApplication\CompanyPartnerDataObject;
use App\DataObjects\LicenseApplication\CompanyStoreDataObject;
use App\DataObjects\LicenseApplication\CompanyPremiseDataObject;
use App\DataObjects\LicenseApplication\CompanyStockStatementDataObject;

use Carbon\Carbon;

class CreateLicenseApplication extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:licenseApplication {--runcit} {--borong} {--import} {--eksport} {--belipadi} {--kilangpadi} {--keringpadi} {--lesen} {--penyatastok}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('runcit')) {
            $statuses = Status::where('domain', 'like', '%RUNCIT%')->get();
            $licenseType = LicenseType::where('name', 'runcit')->first();
            $licenseApplicationTypes = LicenseApplicationType::where('code','!=','PERUBAHAN_MAKLUMAT_STOR')->get();
        } elseif ($this->option('borong')) {
            $statuses = Status::where('domain', 'like', '%BORONG%')->get();
            $licenseType = LicenseType::where('name', 'borong')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        } elseif ($this->option('import')) {
            $statuses = Status::where('domain', 'like', '%IMPORT%')->get();
            $licenseType = LicenseType::where('name', 'import')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        } elseif ($this->option('eksport')) {
            $statuses = Status::where('domain', 'like', '%EKSPORT%')->get();
            $licenseType = LicenseType::where('name', 'eksport')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        } elseif ($this->option('belipadi')) {
            $statuses = Status::where('domain', 'like', 'BELI_PADI%')->get();
            $licenseType = LicenseType::where('name', 'Membeli Padi')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        } elseif ($this->option('kilangpadi')) {
            $statuses = Status::where('domain', 'like', '%KILANG_PADI_KOMERSIAL%')->get();
            $licenseType = LicenseType::where('name', 'Kilang Padi Komersil')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        }elseif ($this->option('keringpadi')) {
            $statuses = Status::where('domain', 'like', '%MENGERING_PADI%')->get();
            $licenseType = LicenseType::where('name', 'Kilang Padi Komersil')->first();
            $licenseApplicationTypes = LicenseApplicationType::all();
        } else {
            $statuses = Status::all();
            $licenseType = LicenseType::all();
            $licenseApplicationTypes = LicenseApplicationType::all();
        }
        
        $licenseApplicationCategories = LicenseApplicationCategory::all();
        $companies = Company::all();
        $stockStatements = StockStatementPeriod::limit(3)->get();
        $companyStockStatements = null;

        $this->info("Creating {$companies->count()}  mock license applications...");
        foreach ($companies as $company) {
            companyStockStatementAccount::UpdateOrCreate([
               'company_id'                    =>  $company->id,
                'license_type_id'               =>  $licenseType->id, 
            ],[
                'balance_stock' =>  0,
                'initial_stock'  =>  0
            ]);

            if ($this->option('penyatastok')) {
                foreach($stockStatements as $stockStatement){
                    factory(\App\Models\ReferenceData\CompanyStockStatement::class, 1)->create([
                        'company_id'                    =>  $company->id,
                        'license_type_id'               =>  $licenseType->id,
                        'stock_statement_period_id'    =>  $stockStatement->id
                    ])->each(function ($companyStockAccount) use($company,$licenseType,$stockStatement){
                        //$companyStockAccount->where('company_id',$company->id)->where('license_type_id',$licenseType->id)->first();
                        
                        $initalStock = $companyStockAccount->stock_balance;
                        $balanceStock = $initalStock - $companyStockAccount->total_sell + $companyStockAccount->total_buy;
                        $a = companyStockStatementAccount::where('company_id',$company->id)->where('license_type_id',$licenseType->id)->first();

                        $a->update([
                            'balance_stock' =>  $balanceStock ,
                            'initial_stock'  =>  $initalStock,
                            'current_stock_statement_period_id' =>  $stockStatement->id
                        ]);
                    });
                }
                $companyStockStatements = $company->stockStatements;
            }
            

            foreach ($statuses as $status) {
                foreach ($licenseApplicationTypes as $licenseApplicationType) {
                    $lg = $licenseApplicationCategories->random();
                    factory(\App\Models\LicenseApplication::class, 1)->create([
                    'company_name'   =>  $company->name,
                    'applicant_id'   =>  $company->user_id,
                    'status_id'   =>  $status->id,
                    'branch_id'     =>  1,
                    'license_type_id'   =>  $licenseType->id,
                    'license_application_type_id'   => $licenseApplicationType->id,
                    'license_application_category_id'   =>  $lg->id
                ])
                ->each(function ($licenseApplication) use ($company, $status, $licenseType, $licenseApplicationType, $companyStockStatements) {
                    $param = [];

                    $premises = $company->premises;
                    $stores = $company->stores;

                    $param['original_company'] = $company;
                    $param['original_premise'] = $premises->random();
                    //$param['original_store'] = $stores->random();
                    $param['original_partners'] = OriginalCompanyPartnerDataObject::findCompanyParnerByCompanyId($param['original_company']->id);
                    $param['company_name'] = $param['original_company']->name;
                    $param['original_address'] = $param['original_company']->address;

                    $companyAddressParam = $param['original_address']->toArray();
                    $companyAddressParam['state_name'] = $param['original_address']->state->name;
                    $companyAddressParam['license_application_id'] = $licenseApplication->id;
                    $companyAddressParam['company_id'] = $param['original_company']->id;
                    unset($param['original_address']['id']);

                    $companyDAOParam = $param['original_company']->toArray();
                    $companyDAOParam['company_type_name'] = $param['original_company']->companyType->name;
                    $companyDAOParam['company_id'] = $param['original_company']->id;
                    $companyDAOParam['license_application_id'] = $licenseApplication->id;
                    unset($companyDAOParam['id']);

                    $companyPremiseDAOParam = $param['original_premise']->toArray();
                    $companyPremiseDAOParam['business_type_name'] = $param['original_premise']->businessType->name;
                    $companyPremiseDAOParam['building_type_name'] = $param['original_premise']->buildingType->name;
                    $companyPremiseDAOParam['state_name'] = $param['original_premise']->state->name;
                    $companyPremiseDAOParam['parliament_name'] = $param['original_premise']->parliament->name;
                    $companyPremiseDAOParam['dun_name'] = $param['original_premise']->dun->name;
                    $companyPremiseDAOParam['district_name'] = $param['original_premise']->district->name;
                    $companyPremiseDAOParam['company_premise_id'] = $param['original_premise']->id;
                    $companyPremiseDAOParam['phone_number'] = '0129101561';
                    $companyPremiseDAOParam['email'] = 'con@con.toh';
                    if ($this->option('runcit')) {
                        $companyPremiseDAOParam['apply_load'] = mt_rand(1, 100);
                    }

                    $companyPremiseDAOParam['license_application_id'] = $licenseApplication->id;
                    unset($companyPremiseDAOParam['id']);
                    
                    $company = CompanyDataObject::createNewCompany($companyDAOParam);
                    $address = CompanyAddressDataObject::createNewCompanyAddress($companyAddressParam);
                    $premise = CompanyPremiseDataObject::addNewCompanyPremise($companyPremiseDAOParam);

                    foreach ($param['original_partners'] as $item) {
                        $companyPartnerDAOParam = $item->toArray();
                        //$companyPartnerDAOParam['ownership_type'] = $item->ownershipType->name;
                        $companyPartnerDAOParam['race_name'] = $item->race->name;
                        $companyPartnerDAOParam['race_type_name'] = $item->raceType->name;
                        $companyPartnerDAOParam['is_citizen'] = $item->is_citizen;
                        $companyPartnerDAOParam['citizen_name'] = ($item->is_citizen  == 1)? 'Warganegara' : 'Bukan Warganegara';
                        $companyPartnerDAOParam['state_name'] = $item->state->name;

                        $companyPartnerDAOParam['company_id'] = $param['original_company']->id;
                        $companyPartnerDAOParam['license_application_id'] = $licenseApplication->id;
                        $companyPartnerDAOParam['company_partner_id'] = $companyPartnerDAOParam['id'];

                        CompanyPartnerDataObject::addNewCompanyPartner($companyPartnerDAOParam);
                    }

                    if ($this->option('penyatastok')) {
                        foreach($companyStockStatements as $companyStockStatement){
                            $arrayedStockStatement = $companyStockStatement->toArray();
                            /*
                            CompanyStockStatementDataObject::addNewCompanyStockStatement([
                                'stock_category_id'                     =>  $arrayedStockStatement['stock_category_id'],
                                'stock_category'                        =>  $arrayedStockStatement['stock_category']['name'] ?? null,
                                'stock_type_id'                         =>  $arrayedStockStatement['stock_type_id'],
                                'stock_type'                            =>  $arrayedStockStatement['stock_type']['name'] ?? null,
                                'spin_type_id'                          =>  $arrayedStockStatement['spin_type_id'],
                                'spin_type'                             =>  $arrayedStockStatement['spin_type']['name'] ?? null,
                                'total_grade_id'                        =>  $arrayedStockStatement['total_grade_id'],
                                'total_grade'                           =>  $arrayedStockStatement['total_grade']['name'] ?? null,
                                'company_id'                            =>  $arrayedStockStatement['company_id'],
                                'license_type_id'                       =>  $arrayedStockStatement['license_type_id'],
                                'rice_grade_id'                         =>  $arrayedStockStatement['rice_grade_id'],
                                'rice_grade'                            =>  $arrayedStockStatement['rice_grade']['name'] ?? null,
                                'stock_statement_period_id'             =>  $arrayedStockStatement['stock_statement_period_id'],
                                'company_stock_statement_account_id'    =>  $arrayedStockStatement['company_stock_statement_account_id'],
                                'initial_stock'                         =>  $arrayedStockStatement['initial_stock'],
                                'total_buy'                             =>  $arrayedStockStatement['total_buy'],
                                'buying_price'                          =>  $arrayedStockStatement['buying_price'],
                                'total_spin'                            =>  $arrayedStockStatement['total_spin'],
                                'stock_balance'                         =>  $arrayedStockStatement['stock_balance'],
                                'spin_result'                           =>  $arrayedStockStatement['spin_result'],
                                'total_sell'                            =>  $arrayedStockStatement['total_sell'],
                                'selling_price'                         =>  $arrayedStockStatement['selling_price'],
                            ]);
                            */
                        }
                    }
                    
                    if ($status->name ==  'Tamat' && $this->option('lesen')) {
                        $licenseNumber = base64_encode(rand());
                        $license = License::create([
                            'company_id'                    =>  $param['original_company']->id,
                            'applicant_id'                       =>  $company->user_id,
                            'status_id'                     =>  343,
                            'license_type_id'               =>  $licenseType->id,
                            'license_application_id'        =>  $licenseApplication->id,
                            'license_application_type_id'   =>  $licenseApplicationType->id,
                            'expiry_date'                   =>  Carbon::now()->addMonths(mt_rand(12, 26)),
                            'license_number'                =>  $licenseNumber
                        ]);

                        $includeLicense = IncludeLicense::create([
                            'license_application_id'        =>  $licenseApplication->id,
                            'license_application_type_id'   =>  $licenseApplicationType->id,
                            'status_id'                     =>  380,
                            'license_type_id'               =>  $licenseType->id,
                        ]);
                        
                        $includeLicenseItem = IncludeLicenseItem::create([
                            'license_application_id'        =>  $licenseApplication->id,
                            'license_application_include_license_id'=>  $includeLicense->id,
                            'status_id'                     =>  383,
                            'license_type_id'               =>  $licenseType->id,
                            'current_license_number'        =>  $licenseNumber,
                            'current_license_id'            =>  $license->id
                        ]);

                        if (!$this->option('runcit')) {
                            foreach($stores as $store){
                                $param['original_store'] = $store;
                                $companyStoreDAOParam = $param['original_store']->toArray();
                                $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
                                $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
                                $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
                                $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
                                $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;
                                //$companyStoreDAOParam['apply_load'] = mt_rand(1, 100);
                                $companyStoreDAOParam['license_application_id'] = $licenseApplication->id;
                                //$companyStoreDAOParam['license_application_include_license_item_id'] = $includeLicenseItem->id;
                                unset($companyStoreDAOParam['id']);

                                $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);

                                CompanyStoreIncludeLicenseItem::create([
                                    'license_application_company_store_id'  =>  $store->id,
                                    'license_application_id'                =>  $licenseApplication->id,
                                    'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                                    'apply_load'              =>    mt_rand(1, 100)      
                                ]);
                            }
                        }
                        
                        if ($this->option('borong') ) {
                            $import = rand();
                            if(($import %2) == 0){
                                $importLicenseType = LicenseType::where('name', 'import')->first();
                                $importLicenseNumber = base64_encode(rand());

                                $wholeSalelicense = License::create([
                                    'company_id'                    =>  $param['original_company']->id,
                                    'applicant_id'                  =>  $company->user_id,
                                    'status_id'                     =>  343,
                                    'license_type_id'               =>  $importLicenseType->id,
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_type_id'   =>  $licenseApplicationType->id,
                                    'expiry_date'                   =>  Carbon::now()->addMonths(mt_rand(12, 26)),
                                    'license_number'                =>  $importLicenseNumber
                                ]);

                                $includeLicenseItem = IncludeLicenseItem::create([
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_include_license_id'=>  $includeLicense->id,
                                    'status_id'                     =>  383,
                                    'license_type_id'               =>  $importLicenseType->id,
                                    'current_license_number'        =>  $importLicenseNumber,
                                    'current_license_id'            =>  $wholeSalelicense->id
                                ]);

                                foreach($licenseApplication->companyStores as $store){
                                    // $param['original_store'] = $store;
                                    // $companyStoreDAOParam = $param['original_store']->toArray();
                                    // $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
                                    // $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
                                    // $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
                                    // $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
                                    // $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;
                                    // //$companyStoreDAOParam['apply_load'] = mt_rand(1, 100);
                                    // $companyStoreDAOParam['license_application_id'] = $licenseApplication->id;
                                    // //$companyStoreDAOParam['license_application_include_license_item_id'] = $includeLicenseItem->id;
                                    // unset($companyStoreDAOParam['id']);

                                    //$store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);

                                    CompanyStoreIncludeLicenseItem::create([
                                        'license_application_company_store_id'  =>  $store->id,
                                        'license_application_id'                =>  $licenseApplication->id,
                                        'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                                        'apply_load'              =>    mt_rand(1, 100)      
                                    ]);
                                }
                            }

                            $export = rand();
                            if(($export %2) == 0){
                                $eksportLicenseType = LicenseType::where('name', 'eksport')->first();
                                $eksportLicensNumber = base64_encode(rand());
                                $wholeSalelicense = License::create([
                                    'company_id'                    =>  $param['original_company']->id,
                                    'applicant_id'                  =>  $company->user_id,
                                    'status_id'                     =>  343,
                                    'license_type_id'               =>  $eksportLicenseType->id,
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_type_id'   =>  $licenseApplicationType->id,
                                    'expiry_date'                   =>  Carbon::now()->addMonths(mt_rand(12, 26)),
                                    'license_number'                =>  $eksportLicensNumber
                                ]);

                                $includeLicenseItem = IncludeLicenseItem::create([
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_include_license_id'=>  $includeLicense->id,
                                    'status_id'                     =>  383,
                                    'license_type_id'               =>  $eksportLicenseType->id,
                                    'current_license_number'        =>  $eksportLicensNumber,
                                    'current_license_id'            =>  $wholeSalelicense->id
                                ]);

                                foreach($licenseApplication->companyStores as $store){
                                    // $param['original_store'] = $store;
                                    // $companyStoreDAOParam = $param['original_store']->toArray();
                                    // $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
                                    // $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
                                    // $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
                                    // $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
                                    // $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;
                                    // //$companyStoreDAOParam['apply_load'] = mt_rand(1, 100);
                                    // $companyStoreDAOParam['license_application_id'] = $licenseApplication->id;
                                    // //$companyStoreDAOParam['license_application_include_license_item_id'] = $includeLicenseItem->id;
                                    // unset($companyStoreDAOParam['id']);

                                    // $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);

                                    CompanyStoreIncludeLicenseItem::create([
                                        'license_application_company_store_id'  =>  $store->id,
                                        'license_application_id'                =>  $licenseApplication->id,
                                        'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                                        'apply_load'              =>    mt_rand(1, 100)      
                                    ]);
                                }
                            }
                        }
                        if ($this->option('import') || $this->option('eksport')) {
                                $borongLicenseType = LicenseType::where('name', 'borong')->first();
                                $borongLicensNumber = base64_encode(rand());
                                $wholeSalelicense = License::create([
                                    'company_id'                    =>  $param['original_company']->id,
                                    'applicant_id'                  =>  $company->user_id,
                                    'status_id'                     =>  343,
                                    'license_type_id'               =>  $borongLicenseType->id,
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_type_id'   =>  $licenseApplicationType->id,
                                    'expiry_date'                   =>  Carbon::now()->addMonths(mt_rand(12, 26)),
                                    'license_number'                =>  $borongLicensNumber
                                ]);

                                $includeLicenseItem = IncludeLicenseItem::create([
                                    'license_application_id'        =>  $licenseApplication->id,
                                    'license_application_include_license_id'=>  $includeLicense->id,
                                    'status_id'                     =>  383,
                                    'license_type_id'               =>  $borongLicenseType->id,
                                    'current_license_number'        =>  $borongLicensNumber,
                                    'current_license_id'            =>  $wholeSalelicense->id
                                ]);

                                foreach($licenseApplication->companyStores as $store){
                                    // $param['original_store'] = $store;
                                    // $companyStoreDAOParam = $param['original_store']->toArray();
                                    // $companyStoreDAOParam['building_type_name'] = $param['original_store']->buildingType->name;
                                    // $companyStoreDAOParam['store_ownership_type_name'] = $param['original_store']->storeOwnershipType->name;
                                    // $companyStoreDAOParam['state_name'] = $param['original_store']->state->name;
                                    // $companyStoreDAOParam['district_name'] = $param['original_store']->district->name;
                                    // $companyStoreDAOParam['company_store_id'] = $param['original_store']->id;
                                    // //$companyStoreDAOParam['apply_load'] = mt_rand(1, 100);
                                    // $companyStoreDAOParam['license_application_id'] = $licenseApplication->id;
                                    // //$companyStoreDAOParam['license_application_include_license_item_id'] = $includeLicenseItem->id;
                                    // unset($companyStoreDAOParam['id']);

                                    // $store = CompanyStoreDataObject::addNewCompanyStore($companyStoreDAOParam);

                                    CompanyStoreIncludeLicenseItem::create([
                                        'license_application_company_store_id'  =>  $store->id,
                                        'license_application_id'                =>  $licenseApplication->id,
                                        'license_application_include_license_item_id'   =>  $includeLicenseItem->id,
                                        'apply_load'              =>    mt_rand(1, 100)      
                                    ]);
                                }
                        }
                        
                    }
                });
                }
            }
        }
        

        $this->info("Successfully created {$companies->count()} mock applications!");
    }
}
