<?php

namespace App\DataObjects\ApprovalLetterApplication;

use DB;
use Auth;

use App\Models\ApprovalLetterApplication\CompanyPartner;

class CompanyPartnerDataObject
{
    public static function addNewCompanyPartner($param)
    {
        return CompanyPartner::create([
            'name'                  =>  $param['name'],
            //'ownership_type_id'       =>  $param['ownership_type_id'],
            //'ownership_type'       =>  $param['ownership_type'],
            'race_id'       =>  $param['race_id'],
            'race_name'       =>  $param['race_name'],
            'race_type_id'       =>  $param['race_type_id'],
            'race_type_name'       =>  $param['race_type_name'],
            'total_share'   =>  $param['total_share'],
            'share_percentage'   =>  $param['share_percentage'],
            'citizen_name'           =>  $param['citizen_name'],
            'is_citizen'           =>  $param['is_citizen'],
            'phone_number'           =>  $param['phone_number'],
            'email'           =>  $param['email'],
            'nric'        =>  $param['nric'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'approval_letter_application_id'    =>  $param['approval_letter_application_id'],
            'state_name'                  =>  $param['state_name'],
            'company_partner_id'                  =>  $param['company_partner_id'],
            //'user_id'               =>  $param['user_id'],
        ]);
    }

    public static function findAllActiveCompanyPartners()
    {
        return CompanyPartner::get();
    }

    public static function findAllCompanyPartnersUsingDatatableFormat($param)
    {
        $companies =  CompanyPartner::distinct()
                    ->when(isset($param['user_id']), function ($company) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('ownership_type', function ($company) {
            return $company->ownershipType->name ?? '-';
        })
        ->addColumn('race', function ($company) {
            return $company->race->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
