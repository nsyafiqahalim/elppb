<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\StoreOwnershipType;

class StoreOwnershipTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Sendiri',
            'Kontrak',
            'Sewa',
        ];

        foreach ($data as $item) {
            StoreOwnershipType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
