<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyPremises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_premises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->unsignedBigInteger('business_type_id')->index();
            $table->unsignedBigInteger('building_type_id')->index();
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('dun_id')->index();
            $table->unsignedBigInteger('district_id')->index();
            $table->unsignedBigInteger('parliament_id')->index();
            $table->unsignedBigInteger('company_id')->index();
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('dun_id')->references('id')->on('duns');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('parliament_id')->references('id')->on('parliaments');
            $table->foreign('business_type_id')->references('id')->on('business_types');
            $table->foreign('building_type_id')->references('id')->on('building_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
