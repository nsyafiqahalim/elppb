<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('index') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="hide-menu">Laman Utama</a></li>
        <li class="three-column">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-database"></i><span class="hide-menu">Data Rujukan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('admin.building_type.index') }}"><i class="fas fa-building"></i> Jenis Bangunan</a></li>
                <li><a href="{{ route('admin.company_type.index') }}"><i class="fas fa-suitcase"></i> Jenis Syarikat</a></li>
                <li><a href="{{ route('admin.premise_type.index') }}"><i class="fas fa-tablet"></i> Jenis Premis</a></li>
                <li><a href="{{ route('admin.business_type.index') }}"><i class="fas fa-briefcase"></i> Jenis Perniagaan</a></li>
                <li><a href="{{ route('admin.factory_type.index') }}"><i class="fas fa-industry"></i> Jenis Kilang</a></li>
                <li><a href="{{ route('admin.store_type.index') }}"><i class="fas fa-warehouse"></i> Jenis Stor</a></li>
                <li><a href="{{ route('admin.land_type.index') }}"><i class="fas fa-newspaper"></i> Jenis Tanah</a></li>
                <li><a href="{{ route('admin.license_type.index') }}"><i class="fas fa-id-badge"></i> Jenis Lesen</a></li>
                <li><a href="{{ route('admin.permit_type.index') }}"><i class="fas fa-file"></i> Jenis Permit</a></li>
                <li><a href="{{ route('admin.payment-type.index') }}"><i class="fas fa-dollar-sign"></i> Jenis Bayaran</a></li>
                <li><a href="{{ route('admin.stock.index') }}"><i class="fas fa-cube"></i> Jenis Stok</a></li>
                <!---<li><a href="#"><i class="fas fa-box-open"></i> Jenis Pemilikan Kilang</a></li>--->
                <!---<li><a href="#"><i class="fas fa-boxes"></i> Jenis Pemilikan Stor</a></li>--->
                <li><a href="{{ route('admin.rice-type.index') }}"><i class="fas fa-clone"></i> Jenis Beras</a></li>
                <li><a href="{{ route('admin.rice-category.index') }}"><i class="fas fa-tag"></i> Kategori Beras</a></li>
                <li><a href="{{ route('admin.rice-grade.index') }}"><i class="fas fa-tags"></i> Gred Beras</a></li>
                <li><a href="{{ route('admin.partner-type.index') }}"><i class="fas fa-handshake"></i> Rakan Kongsi</a></li>
                <li><a href="{{ route('admin.race.index') }}"><i class="fas fa-users"></i> Bangsa</a></li>
                <li><a href="{{ route('admin.race-type.index') }}"><i class="fas fa-user"></i> Jenis Bangsa</a></li>
                <li><a href="{{ route('admin.land-status.index') }}"><i class="fas fa-hockey-puck"></i> Status Tanah</a></li>
                <li><a href="{{ route('admin.ownership-status.index') }}"><i class="fas fa-book"></i> Status Pemilikan</a></li>
                <li><a href="{{ route('admin.dun.index') }}"><i class="fas fa-map-marker-alt"></i> Dun</a></li>
                <li><a href="{{ route('admin.zone.index') }}"><i class="fas fa-map-marker"></i> Zon</a></li>
                <li><a href="{{ route('admin.district.index') }}"><i class="fas fa-leaf"></i> Daerah</a></li>
                <!---<li><a href="{{ route('admin.area-coverage.index') }}">Liputan Kawasan</a></li>--->
                <li><a href="{{ route('admin.state.index') }}"><i class="fas fa-map"></i> Negeri</a></li>
                <li><a href="{{ route('admin.branch.index') }}"><i class="fas fa-map-pin"></i> Cawangan</a></li>
                <li><a href="{{ route('admin.parliament.index') }}"><i class="fas fa-map-signs"></i> Parlimen</a></li>
                <!---<li><a href="{{ route('admin.status.index') }}">Status</a></li>--->
            </ul>
        </li>
        <li class="dropdown">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Penetapan Sistem</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('admin.setting.index',[encrypt('general_setting')]) }}">Awam</a></li>
                <div class="dropdown-divider"></div>
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fas fa-hdd"></i> Lesen</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('retail_license_setting')]) }}">Lesen Runcit</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('wholesale_license_setting')]) }}">Lesen Borong</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('import_license_setting')]) }}">Lesen Import</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('export_license_setting')]) }}">Lesen Eksport</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('buy_paddy_license_setting')]) }}">Lesen Beli Padi</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('factory_paddy_license_setting')]) }}">Lesen Kilang Padi</a></li>
                    </ul>
                </li>
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fas fa-paperclip"></i> Permit</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('move_paddy_permit_setting')]) }}">Permindahan Padi</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('rice_permit_setting')]) }}">Beras/Hasil Sampingan</a></li>
                    </ul>
                </li>
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false">Surat Kelulusan Bersyarat</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('approval_commercial_setting')]) }}">Lesen Kilang Padi (Komersial)</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('approval_buy_paddy_setting')]) }}">Lesen Membeli Padi</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="dropright">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Penetapan KPI</span></a>
            <ul aria-expanded="false" class="collapse">
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fas fa-hdd"></i> Lesen</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('retail_license_kpisetting')]) }}">Lesen Runcit</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('wholesale_license_kpisetting')]) }}">Lesen Borong</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('import_license_kpisetting')]) }}">Lesen Import</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('export_license_kpisetting')]) }}">Lesen Eksport</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('buy_paddy_license_kpisetting')]) }}">Lesen Beli Padi</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('factory_paddy_license_kpisetting')]) }}">Lesen Kilang Padi</a></li>
                    </ul>
                </li>
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false"><i class="fas fa-paperclip"></i> Permit</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('move_paddy_permit_kpisetting')]) }}">Permindahan Padi</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('rice_permit_kpisetting')]) }}">Beras/Hasil Sampingan</a></li>
                    </ul>
                </li>
                <li class="dropright-submenu"><a href="#" class="has-arrow" aria-expanded="false">Surat Kelulusan Bersyarat</a>
                    <ul class="dropright-menu">
                        <li><a href="{{ route('admin.setting.index',[encrypt('approval_commercial_kpisetting')]) }}">Lesen Kilang Padi (Komersial)</a></li>
                        <li><a href="{{ route('admin.setting.index',[encrypt('approval_buy_paddy_kpisetting')]) }}">Lesen Membeli Padi</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="two-column">
            <a class="has-arrow" href="#" aria-expanded="false"><i class="mdi mdi-account-settings"></i><span class="hide-menu">Pengguna Dan Peranan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="{{ route('admin.user.index') }}">Pengguna</a></li>
                <li><a href="{{ route('admin.role.index') }}">Peranan</a></li>
            </ul>
        </li>
    </ul>
</nav>
