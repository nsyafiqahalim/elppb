@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Penetapan Portal</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Laman Utama</li>
            <li class="breadcrumb-item"><a href="{{ route('portal.index') }}">Pengurusan Portal</a></li>
            <li class="breadcrumb-item">Info</a></li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Info</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form form-data-submission" method="post" action="{{ route('api.portal.setting.update',[encrypt($settingDomain)]) }}">
                        @method('PATCH')
                        <div class="form-body">
                            <div class="row">
                                @foreach($settings as $setting)
                                {{ $setting->typePP }}
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="{{ $setting->code }}_label">{{ $setting->label }}<span class='required'>&nbsp;*</span></label>
                                        @if($setting->class == 'time')
                                            <div class="input-group {{ $setting->class }}" data-placement="bottom" data-align="top" data-autoclose="true">
                                                <input type="text" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}" name="{{ $setting->code }}" value="{{ $setting->value }}"
                                                  @if($setting->readonly == "true") readonly @endif>
                                                    <span class="input-group-addon">
                                                        <span class="icon-clock"></span>
                                                    </span>
                                            </div>
                                            @elseif($setting->type == 'TEXTAREA')
                                                <textarea id="description" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}"
                                                  name="{{ $setting->code }}">{{ $setting->value }}</textarea>
                                                @elseif($setting->type == 'IMAGE')
                                                    <input type="file" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}" name="{{ $setting->code }}" value="{{ $setting->value }} "
                                                      @if($setting->readonly == "true") readonly @endif>
                                                        @else
                                                        <input type="text" class="form-control border-primary {{ $setting->class }}" placeholder="{{ $setting->label }}" id="{{ $setting->code }}" name="{{ $setting->code }}"
                                                          value="{{ $setting->value }} " @if($setting->readonly == "true") readonly @endif>
                                                            @endif
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                <i class="icon-check2"></i> Hantar
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
        selector: "textarea#description",
        theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            'image code',
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",

        image_title: true,
        automatic_uploads: true,
        file_picker_types: 'image',

        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');

            input.onchange = function() {
                var file = this.files[0];
                var reader = new FileReader();

                reader.onload = function() {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);

                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), {
                        title: file.name
                    });
                };
                reader.readAsDataURL(file);
            };
            input.click();
        }
    });
</script>
@endpush
