<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationBuyPaddyCondition extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_buy_paddy_conditions';
}
