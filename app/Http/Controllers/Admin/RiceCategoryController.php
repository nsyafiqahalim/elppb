<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RiceCategoryDataObject;

class RiceCategoryController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.rice-category.index');
    }

    public function add()
    {
        return view('admin.rice-category.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $riceCategory = RiceCategoryDataObject::findRiceCategoryById($decryptedId);

        return view('admin.rice-category.edit', compact('riceCategory'));
    }
}
