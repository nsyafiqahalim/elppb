<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RaceDataObject;

class RaceController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.race.index');
    }

    public function add()
    {
        return view('admin.race.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $race = RaceDataObject::findRaceById($decryptedId);

        return view('admin.race.edit', compact('race'));
    }
}
