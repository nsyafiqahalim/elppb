<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\Announcement;

class AnnouncementDataObject
{
    public static function addAnnouncement($param)
    {
        return Announcement::create([
            'description'          =>  $param['description'],
            'created_by'           =>  $param['created_by'],
            'updated_by'           =>  $param['updated_by'],
            'is_published'         =>  $param['is_published'],
        ]);
    }

    public static function updateAnnouncement($param, $id)
    {
        return Announcement::find($id)->update([
            'description'          =>  $param['description'],
            'updated_by'           =>  $param['updated_by'],
            'is_published'         =>  $param['is_published'],
        ]);
    }

    public static function deleteAnnouncement($id)
    {
        return Announcement::find($id)->delete();
    }

    public static function findAllAnnouncementUsingDatatableFormat($param)
    {
        $announcements = Announcement::distinct()->orderby('id', 'desc')
                        ->when(isset($param['description']) && $param['description'], function ($query) use ($param) {
                            $query->where('description', 'like', '%'.$param['description'].'%');
                        })->when(isset($param['is_published']), function ($query) use ($param) {
                            if ($param['is_published'] == 1) {
                                $query->where('is_published', 1);
                            } elseif ($param['is_published'] == 0) {
                                $query->where('is_published', '<>', 1)->whereNotNull('is_published');
                            }
                        });

        return datatables()->of($announcements)
        ->addIndexColumn()
         ->addColumn('published', function ($announcement) {
             $status = ($announcement->is_published == 1)? 'Diterbitkan' : 'Tidak Diterbitkan';

             return View('portal.announcement.partials.status', compact('status'))->render();
         })
        ->addColumn('action', function ($announcement) {
            $encryptedId = encrypt($announcement->id);

            return View('portal.announcement.partials.datatable-button', compact('encryptedId'))->render();
        })
        ->rawColumns(['published','description','action'])
       ->make(true);
    }

    public static function findAnnouncementById($id)
    {
        return Announcement::find($id);
    }
}
