<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\PremiseTypeDataObject;

class PremiseTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        return view('admin.premise-type.index');
    }

    public function add()
    {
        return view('admin.premise-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        
        $premiseType = PremiseTypeDataObject::findPremiseTypeById($decryptedId);

        return view('admin.premise-type.edit', compact('premiseType'));
    }
}
