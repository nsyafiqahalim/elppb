<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\BuildingType;
use App\Models\Admin\BusinessType;
use App\Models\Admin\StoreOwnershipType;
use App\Models\Admin\Dun;
use App\Models\Admin\Parliament;
use App\Models\Admin\District;
use App\Models\Admin\State;

class CompanyStoreIncludeApprovalLetterItem extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_store_include_approval_letter_items';

    private const ACTIVE = 1;
}
