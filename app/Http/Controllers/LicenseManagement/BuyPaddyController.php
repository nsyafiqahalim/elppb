<?php

namespace App\Http\Controllers\LicenseManagement;

use App\Models\LicenseApplication;
use App\Models\LicenseActivity;
use App\Models\LicenseRemark;
use App\Models\LicenseAttachment;
use App\Models\UserProfile;
use App\Models\Admin\Status;
use App\Http\Controllers\Controller;
use Auth;
use PDF\PDF;
use Response;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class BuyPaddyController extends Controller
{
    // view for maklumat pengurusan Lesen// tugasan saya dashboard
    public function showBuyPaddy($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.buy-paddy.new-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showBuyPaddyRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.buy-paddy.renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showBuyPaddyChange($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.buy-paddy.change-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showBuyPaddyReplacement($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        return view('license-management.buy-paddy.replacement-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities ]);
    }

    public function showBuyPaddyCancel($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        return view('license-management.buy-paddy.cancel-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities ]);
    }

    public function showBuyPaddyChangeRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.buy-paddy.new-renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    // edit for maklumat pengurusan Lesen// tugasan saya dashboard
    public function editBuyPaddy(Request $request, $id)
    {
        $decryptedId = decrypt($id);

        $license = LicenseApplication::find($decryptedId);
        $status = Status::where('code', $request->status)->get('id');
        $license->status_id = $status[0]->id;
        if (!is_null($request->investigation_officer)) {
          $license->license_investigation_officer_id = $request->investigation_officer;
        }
        $license->save();

        // licence application remark
        $license_remark = new LicenseRemark();
        $license_remark->status_id = $status[0]->id;
        $license_remark->remarks = $request->remark;
        $license_remark->user_id = Auth::user()->id;
        $license_remark->license_application_id = $license->id;
        $license_remark->save();

        // licence application activities
        if ($request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_BAHARU' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_PEMBAHARUAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_PERUBAHAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Pemilihan Pegawai Siasatan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_SIASATAN_BELI_PADI_BAHARU' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BELI_PADI_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BELI_PADI_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_BELI_PADI_PEMBATALAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_BELI_PADI_PENGGANTIAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit Cawangan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_BAHARU' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit HQ";
        } elseif ($request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_BAHARU' || $request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_PEMBAHARUAN' || $request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_PENGGANTIAN' || $request->status == 'KELULUSAN_PERMOHONAN_BELI_PADI_PEMBATALAN' ) {
          $activity = "Hantar Semakan Ke Ketua Pengarah";
        } elseif ($request->status == 'JANAAN_LESEN_BELI_PADI_BAHARU' || $request->status == 'JANAAN_LESEN_BELI_PADI_PEMBAHARUAN' || $request->status == 'JANAAN_LESEN_BELI_PADI_PERUBAHAN' || $request->status == 'JANAAN_LESEN_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'JANAAN_LESEN_BELI_PADI_PENGGANTIAN' || $request->status == 'JANAAN_LESEN_BELI_PADI_PEMBATALAN' ) {
          $activity = "Permohonan Dilulus";
        } elseif ($request->status == 'TOLAK_BELI_PADI_BAHARU' || $request->status == 'TOLAK_BELI_PADI_PEMBAHARUAN' || $request->status == 'TOLAK_BELI_PADI_PERUBAHAN' || $request->status == 'TOLAK_BELI_PADI_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'TOLAK_BELI_PADI_PENGGANTIAN' || $request->status == 'TOLAK_BELI_PADI_PEMBATALAN' ) {
          $activity = "Permohonan Ditolak";
        }

        $license_activity = new LicenseActivity();
        $license_activity->status_id = $status[0]->id;
        $license_activity->activity = $activity;
        $license_activity->user_id = Auth::user()->id;
        $license_activity->license_application_id = $license->id;
        $license_activity->save();

        return response()->json([
            'message'   =>  'Permohonan berjaya dikemaskini',
            'success'   =>  true,
            'route'     => route('management.mytask-index')
        ], 200);
      }

      public function downloadBuyPaddyLicense(Request $request, $id)
      {
          $decryptedId = decrypt($id);
          $license = LicenseApplication::find($decryptedId);
          $status = Status::where('code', $request->status)->get('id');

          if ($license->status_id != $status[0]->id) {
              $license->status_id = $status[0]->id;
              $license->save();

              $license_remark = new LicenseRemark();
              $license_remark->status_id = $status[0]->id;
              $license_remark->remarks = 'Janaan Lesen';
              $license_remark->user_id = Auth::user()->id;
              $license_remark->license_application_id = $license->id;
              $license_remark->save();

              $license_activity = new LicenseActivity();
              $license_activity->status_id = $status[0]->id;
              $license_activity->activity = 'Janaan Lesen';
              $license_activity->user_id = Auth::user()->id;
              $license_activity->license_application_id = $license->id;
              $license_activity->save();
          }

          $pdf = \PDF::loadView('license-management.generate-license.buy-paddy-license', ['license' => $license]);
          return $pdf->download('Janaan-Lesen-Beli-Padi.pdf');
      }

      public function showSSM($id)
      {
          $decryptedId = decrypt($id);
          $license_attachement_ssm = LicenseAttachment::where('code', 'SSM')->where('license_application_id', $decryptedId)->get();

          $file_name = $license_attachement_ssm[0]->file_path;
          $path = env('FILE_URL').'/'.$file_name;

          $file_type = mime_content_type($path);

          return Response::make(file_get_contents($path), 200, [
                'Content-Type' => $file_type,
                'Content-Disposition' => 'inline; filename="'.$file_name.'"'
          ]);
      }

      public function showLesenPerniagaan($id)
      {
          $decryptedId = decrypt($id);
          $license_attachement_ssm = LicenseAttachment::where('code', 'MINUTE_MEETING')->where('license_application_id', $decryptedId)->get();

          $file_name = $license_attachement_ssm[0]->file_path;
          $path = env('FILE_URL').'/'.$file_name;

          $file_type = mime_content_type($path);

          return Response::make(file_get_contents($path), 200, [
                'Content-Type' => $file_type,
                'Content-Disposition' => 'inline; filename="'.$file_name.'"'
          ]);
      }
}
