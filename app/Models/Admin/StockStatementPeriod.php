<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class StockStatementPeriod extends Model
{
    public $guarded = ['id'];

    public $table = 'stock_statement_periods';
}
