<?php

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('portal')->group(function () {
        Route::get('/', 'Portal\DashboardController@index')->name('portal.index');

        Route::prefix('pengumuman')->group(function () {
            //Route::get('/', 'Portal\AnnouncementController@index')->name('portal.announcement.index');
            //Route::get('/paparan/{id}', 'Portal\AnnouncementController@view')->name('portal.announcement.view');
            //Route::get('/tambah', 'Portal\AnnouncementController@add')->name('portal.announcement.add');
            //Route::get('/kemaskini/{id}', 'Portal\AnnouncementController@edit')->name('portal.announcement.edit');
            Route::get('/', 'Portal\GalleryController@index')->name('portal.gallery.index');
            Route::get('/paparan/{id}', 'Portal\GalleryController@view')->name('portal.gallery.view');
            Route::get('/tambah', 'Portal\GalleryController@add')->name('portal.gallery.add');
            Route::get('/kemaskini/{id}', 'Portal\GalleryController@edit')->name('portal.gallery.edit');
        });

        Route::prefix('galeri')->group(function () {
            Route::get('/album', 'Portal\AlbumController@index')->name('portal.gallery.album.index');
            Route::get('/album/tambah', 'Portal\AlbumController@create')->name('portal.gallery.album.create');
            Route::post('/album/simpan', 'Portal\AlbumController@store')->name('portal.gallery.album.store');
            Route::get('/album/{id}', 'Portal\AlbumController@show')->name('album-show');
            Route::delete('/album/{id}/hapus','Portal\AlbumController@destroy')->name('album-destroy');

            Route::get('/gambar/tambah/{albumId}', 'Portal\PhotoController@create')->name('photo-create');
            Route::post('/gambar/simpan', 'Portal\PhotoController@store')->name('photo-store');
            Route::get('/gambar/{id}', 'Portal\PhotoController@show')->name('photo-show');
            Route::get('/gambar/{id}/hapus','Portal\PhotoController@destroy')->name('photo-destroy');
        });

        Route::prefix('lesen')->group(function () {
            Route::get('/', 'Portal\LicenseController@index')->name('portal.license.index');
            Route::get('/paparan/{id}', 'Portal\LicenseController@view')->name('portal.license.view');
            Route::get('/tambah', 'Portal\LicenseController@add')->name('portal.license.add');
            Route::get('/kemaskini/{id}', 'Portal\LicenseController@edit')->name('portal.license.edit');
        });

        Route::prefix('permit')->group(function () {
            Route::get('/', 'Portal\PermitController@index')->name('portal.permit.index');
            Route::get('/paparan/{id}', 'Portal\PermitController@view')->name('portal.permit.view');
            Route::get('/tambah', 'Portal\PermitController@add')->name('portal.permit.add');
            Route::get('/kemaskini/{id}', 'Portal\PermitController@edit')->name('portal.permit.edit');
        });

        Route::prefix('faq')->group(function () {
            Route::get('/', 'Portal\FaqController@index')->name('portal.faq.index');
            Route::get('/paparan/{id}', 'Portal\FaqController@view')->name('portal.faq.view');
            Route::get('/tambah', 'Portal\FaqController@add')->name('portal.faq.add');
            Route::get('/kemaskini/{id}', 'Portal\FaqController@edit')->name('portal.faq.edit');
        });

        Route::prefix('permit')->group(function () {
            Route::get('/', 'Portal\PermitController@index')->name('portal.permit.index');
            Route::get('/paparan/{id}', 'Portal\PermitController@view')->name('portal.permit.view');
            Route::get('/tambah', 'Portal\PermitController@add')->name('portal.permit.add');
            Route::get('/kemaskini/{id}', 'Portal\PermitController@edit')->name('portal.permit.edit');
        });

        Route::prefix('penetapan-portal')->group(function () {
            Route::prefix('kemaskini')->group(function () {
                Route::get('/{setting}', 'Portal\SettingController@index')->name('portal.setting.index');
            });
        });

        Route::prefix('file-manager')->group(function () {
            Route::get('/', 'Portal\FileManagerController@index')->name('file-manager');
        });
    });
});
