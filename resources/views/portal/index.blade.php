@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="mdi mdi-desktop-mac"></i> Pengurusan Portal</h3>
        <ol class="breadcrumb">
            <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Pengurusan Portal</a></li> -->
        </ol>
    </div>
</div>
@endsection
