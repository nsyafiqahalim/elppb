<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\OwnershipStatusDataObject;

class OwnershipStatusController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.ownership-status.index');
    }

    public function add()
    {
        return view('admin.ownership-status.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $ownershipStatus = OwnershipStatusDataObject::findOwnershipStatusById($decryptedId);

        return view('admin.ownership-status.edit', compact('ownershipStatus'));
    }
}
