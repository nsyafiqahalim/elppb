$(document).ready(function () {
    $(document).on('submit', '.url-encoded-submission-without-clear-field', function (e) {
        e.preventDefault();
        $(".se-pre-con").fadeIn("fast");
        $('.error-message').remove();

        var formData = $(this).serialize();
        form = $(this);

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            success: function (data) {
                popupSuccessMessage(data);
            },
            error: function (data) {
                popupErrorMessage(data);
            }
        });
    });

    $(document).on('submit', '.url-encoded-submission', function (e) {
        e.preventDefault();

        $('.error-message').remove();
        $(".se-pre-con").fadeIn("fast");
        var formData = $(this).serialize();
        form = $(this);

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            success: function (data) {
                form[0].reset();
                popupSuccessMessage(data);
            },
            error: function (data) {
                popupErrorMessage(data);
            }
        });
    });

    $(document).on('submit', '.form-data-submission', function (e) {
        e.preventDefault();

        $('.error-message').remove();

        var formData = new FormData(this);
        form = $(this);
        $(".se-pre-con").fadeIn("fast");
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                form[0].reset();
                popupSuccessMessage(data);
            },
            error: function (data) {
                popupErrorMessage(data);
            }
        });
    });


    $(document).on('submit', '.multiple-file-submission', function (e) {
        e.preventDefault();
        $('.error-message').remove();

        var formData = new FormData(this);
        form = $(this);
        $(".se-pre-con").fadeIn("fast");
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                form[0].reset();
                toastrSuccessMessage(data);
            },
            error: function (data) {
                toastrErrorMessage(data);
            }
        });
    });

    $(document).on('submit', '.form-data-submission-with-confirmation', function (e) {
        $(".se-pre-con").fadeIn("fast");
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var text = $(this).attr('text');
        var formData = new FormData(this);
        form = $(this);

        Swal.fire({
            title: 'Pengesahan',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preconfirm: function () {
                $('.error-message').remove();
                e.preventDefault();
                $.ajax({
                    type: method,
                    url: action,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        form[0].reset();
                        popupSuccessMessage(data);
                    },
                    error: function (data) {
                        popupErrorMessage(data);
                    }
                });
            },
        }

        );
    });

    $(document).on('submit', '.url-encoded-submission-with-confirmation', function (e) {
        e.preventDefault();
        var method = $(this).attr('method');
        var action = $(this).attr('action');
        var text = $(this).attr('text');
        var formData = $(this).serialize();
        form = $(this);

        Swal.fire({
            title: 'Pengesahan',
            text: text,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preconfirm: function () {
                $('.error-message').remove();
                $(".se-pre-con").fadeIn("fast");

                $.ajax({
                    type: method,
                    url: action,
                    data: formData,
                    success: function (data) {
                        form[0].reset();
                        popupSuccessMessage(data);
                    },
                    error: function (data) {
                        popupErrorMessage(data);
                    }
                });
            },
        },

        );
    });

    $(document).on('click', '.confirmation', function (e) {
        e.preventDefault();
        var formData = {};
        formData['_method'] = "PATCH";

        var href = $(this).attr('href');

        Swal.fire({
            title: 'Pengesahan',
            text: 'Anda Pasti ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preConfirm: function () {
                $(".se-pre-con").fadeIn("fast");
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: formData,
                    success: function (data) {
                        popupSuccessMessage(data);
                    },
                    error: function (data) {
                        popupErrorMessage(data);
                    }
                });
            },
        }
        );
    });

    $(document).on('click', '.deletion', function (e) {
        e.preventDefault();

        var formData = {};
        formData['_method'] = "DELETE";
        formData['_token'] = $('input[name=_token]').val();
        console.log(formData);
        var href = $(this).attr('href');

        Swal.fire({
            title: 'Padam',
            text: 'Anda Pasti ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: "Ya",
            cancelButtonText: "Batal",
            preConfirm: function () {
                $(".se-pre-con").fadeIn("fast");
                $.ajax({
                    type: 'POST',
                    url: href,
                    data: formData,
                    success: function (data) {
                        popupSuccessMessage(data);
                    },
                    error: function (data) {
                        popupErrorMessage(data);
                    }
                });
            },
        })
    });

    async function popupSuccessMessage(data) {
        $(".se-pre-con").fadeOut("slow");
        if (data['success'] == true) {
            type = "success";
            title = "Berjaya";
        }
        else {
            type = "error"
            title = "Amaran";
        }
        Swal.fire({
            title: title,
            text: data['message'],
            type: type,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Ya",
            preConfirm: function (isConfirm) {
                $('.modal').modal('hide');
                if (isConfirm && data['success'] == true && data['route'] !== null) {
                    window.location = data['route'];
                }
                else if (isConfirm && data['success'] == true && data['datatable'] !== null) {
                    $('#' + data['datatable']).DataTable().ajax.reload();
                }

            }
        }
        );
    }

    async function toastrSuccessMessage(data) {
        $(".se-pre-con").fadeOut("slow");
        if (data['success'] == true) {
            type = "success";
            title = "Berjaya";
        }
        else {
            type = "error"
            title = "Amaran";
        }

        await setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass: 'toast-bottom-right',
                timeOut: 4000
            };
            toastr.success("Fail berjaya dimuat naik");
        }, 100);

        if (data['success'] == true && data['route'] !== null) {
            window.location = data['route'];
        }
        else if (data['success'] == true && data['datatable'] !== null) {
            $('#' + data['datatable']).DataTable().ajax.reload();
        }
    }

    function popupErrorMessage(data) {
        $(".se-pre-con").fadeOut("slow");
        var err = data.responseJSON;
        if (err.errors !== undefined) {
            Swal.close();
            $.each(err['errors'], function (index, value) {
                message = '<span class="error-message">' + value + '</span>';
                index = index.replace(".", "_");
                $('#' + index + '_label').parent().append(message);
            });
        }
        else if (err.message !== undefined) {
            $(".se-pre-con").fadeOut("slow");
            Swal.fire("Warning", err.message, "warning");
        }
        else {
            $(".se-pre-con").fadeOut("slow");
            Swal.fire("Warning", "Unexpected Error", "warning");
        }
    }

    async function toastrErrorMessage(data) {
        await setTimeout(function () {
            toastr.options = {
                closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                positionClass: 'toast-bottom-right',
                timeOut: 4000
            };
            toastr.error("Fail gagal dimuat naik");
        }, 100);
    }
});
