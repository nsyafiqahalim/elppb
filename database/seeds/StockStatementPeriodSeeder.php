<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\StockStatementPeriod;

use Carbon\Carbon;

class StockStatementPeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $totalDaysForCurrentMonth = Carbon::now()->daysInMonth;
        $numberPhasePerdayForCurrentMonth = $totalDaysForCurrentMonth/2;

        $firstPhaseStartDate = Carbon::now()->firstOfMonth();
        $firstPhaseEndDate   = Carbon::now()->firstOfMonth()->addDays($numberPhasePerdayForCurrentMonth-1);
        
        $secondPhaseEndDate = Carbon::now()->endOfMonth();
        $secondPhaseStartDate   = Carbon::now()->endOfMonth()->subDays($numberPhasePerdayForCurrentMonth-1);
        

        $totalDaysForCurrentMonth   = Carbon::now()->subMonths()->daysInMonth;
        $numberPhasePerdayForCurrentMonth = $totalDaysForCurrentMonth/2;

        $previousPhaseEndDate       =   Carbon::now()->subMonths()->endOfMonth();
        $previousPhaseStartDate     =   Carbon::now()->subMonths()->endOfMonth()->subDays($numberPhasePerdayForCurrentMonth-1);

        StockStatementPeriod::create([
            'label'         =>  'Penggal Kedua '.$previousPhaseStartDate->format('F').' '.$previousPhaseStartDate->format('Y'),
            'period'        =>  'Penggal Kedua',
            'month'         =>  $previousPhaseStartDate->format('F'),
            'year'          =>  $previousPhaseStartDate->format('Y'),
            'start_date'    =>  $previousPhaseStartDate,
            'end_date'      =>  $previousPhaseEndDate,
        ]);

        StockStatementPeriod::create([
            'label'         =>  'Penggal Pertama '.$firstPhaseStartDate->format('F').' '.$firstPhaseStartDate->format('Y'),
            'period'        =>  'Penggal Pertama',
            'month'         =>  $firstPhaseStartDate->format('F'),
            'year'          =>  $firstPhaseStartDate->format('Y'),
            'start_date'    =>  $firstPhaseStartDate,
            'end_date'      =>  $secondPhaseStartDate,
        ]);

        StockStatementPeriod::create([
            'label'         =>  'Penggal Kedua '.$secondPhaseStartDate->format('F').' '.$secondPhaseStartDate->format('Y'),
            'period'        =>  'Penggal Kedua',
            'month'         =>  $secondPhaseStartDate->format('F'),
            'year'          =>  $secondPhaseStartDate->format('Y'),
            'start_date'    =>  $secondPhaseStartDate,
            'end_date'      =>  $secondPhaseEndDate,
        ]);
        /*
        print_r($firstPhaseStartDate);
        print_r($firstPhaseEndDate);
        print_r('---------------------');
        print_r($secondPhaseStartDate);
        print_r($secondPhaseEndDate);
        */
        //StockStatementPeriod
    }
}
