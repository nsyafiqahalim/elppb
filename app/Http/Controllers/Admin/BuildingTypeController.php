<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\BuildingTypeDataObject;

class BuildingTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        return view('admin.building-type.index');
    }

    public function add()
    {
        return view('admin.building-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        
        $buildingType = BuildingTypeDataObject::findBuildingTypeById($decryptedId);

        return view('admin.building-type.edit', compact('buildingType'));
    }
}
