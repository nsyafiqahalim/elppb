<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\StockType;

class StockTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Jenis Padi',
          'Gred Beras',
      ];

      foreach ($data as $item) {
          StockType::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
      }
    }
}
