<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\BusinessTypeDataObject;

class BusinessTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addBusinessType(Request $request)
    {
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $businessType = BusinessTypeDataObject::addBusinessType($param);

        return response()->json([
            'message'   =>  'Jenis perniagaan berjaya ditambah',
            'data'      =>  $businessType,
            'success'   =>  true,
            'route'     =>  route('admin.business_type.index')
        ], 200);
    }

    public function updateBusinessType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $businessType = BusinessTypeDataObject::updateBusinessType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis perniagaan berjaya dikemaskini',
            'data'      =>  $businessType,
            'success'   =>  true,
            'route'     =>  route('admin.business_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return BusinessTypeDataObject::findAllBusinessTypeUsingDatatableFormat($param);
    }
}
