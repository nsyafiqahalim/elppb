<?php

return [
    'nav-back'          => 'Kembali',
    'nav-new'           => 'Folder Baharu',
    'nav-upload'        => 'Muat Naik',
    'nav-thumbnails'    => 'Gambar Kecil (Thumbnails)',
    'nav-list'          => 'Senarai',
    'nav-sort'          => 'Susun',
    'nav-sort-alphabetic'=> 'Susun Ikut Abjad',
    'nav-sort-time'     => 'Susun Ikut Masa',

    'menu-rename'       => 'Ganti Nama',
    'menu-delete'       => 'Padam',
    'menu-view'         => 'Pra-Tonton',
    'menu-download'     => 'Muat Turun',
    'menu-resize'       => 'Ubah Saiz',
    'menu-crop'         => 'Potong (Crop)',
    'menu-move'         => 'Pindah',
    'menu-multiple'     => 'Pelbagai Pilihan (Multi-Selection)',

    'title-page'        => 'Pengurus Fail',
    'title-panel'       => 'Pengurus Fail Laravel',
    'title-upload'      => 'Memuat Naik Fail - Fail',
    'title-view'        => 'Papar Fail',
    'title-user'        => 'Fail - Fail',
    'title-share'       => 'Fail - Fail Kongsian',
    'title-item'        => 'Bahan',
    'title-size'        => 'Saiz',
    'title-type'        => 'Jenis',
    'title-modified'    => 'Telah Diubah Suai',
    'title-action'      => 'Tindakan',

    'type-folder'       => 'Folder',

    'message-empty'     => 'Folder Kosong.',
    'message-choose'    => 'Sila Pilih Fail - Fail',
    'message-delete'    => 'Anda Yakin Mahu Memadam Bahan Ini?',
    'message-name'      => 'name: Folder',
    'message-rename'    => 'Diubah Nama to:',
    'message-extension_not_found' => 'Sila Memasang Sambungan (Extension) "gd" atau "imagick" Untuk Potong (crop), Ubah Saizr, Dan Membuat Gambar Kecil (Thumbnails) gambar - gambar.',
    'message-drop'      => 'Or drop files here to upload',

    'error-rename'      => 'Nama Fail Telah Digunakan!',
    'error-file-name'   => 'Fail Tidak Boleh Kosong!',
    'error-file-empty'  => 'Anda Perlu Memilih Fail!',
    'error-file-exist'  => 'Fail Yang Menggunakan Nama Ini Telah Wujud!',
    'error-file-size'   => 'Saiz Fail Melebihi Had Server! (Saiz Maksimum: :max)',
    'error-delete-folder'=> 'Anda Tidak Boleh Memadam Folder Ini Kerana Ia Mempunyai Fail!',
    'error-folder-name' => 'Nama Folder Tidak Boleh Kosong!',
    'error-folder-exist'=> 'Folder Yang Mempunyai Nama Ini Telah Wujud!',
    'error-folder-alnum'=> 'Hanya Folder Bernama Yang Mempunyai Abjad-Angka Yang Dibenarkan!',
    'error-folder-not-found'=> 'Folder Tidak Dijumpai! (:folder)',
    'error-mime'        => 'MimeType: Tidak Dibenarkan Dijumpai',
    'error-size'        => 'Sudah Melebihi Had size:',
    'error-instance'    => 'Fail Yang Dimuat Naik Mestilah Contoh "UploadedFile"',
    'error-invalid'     => 'Permintaan Muat Naik Yang Tidak Sah',
    'error-other'       => 'Ralat Telah Berlaku. occured: ',
    'error-too-large'   => 'Permintaan Muat Naik Terlampau Besar!',

    'btn-upload'        => 'Muat Naik Fail - Fail',
    'btn-uploading'     => 'Sedang Memuat Naik...',
    'btn-close'         => 'Tutup',
    'btn-crop'          => 'Potong (Crop)',
    'btn-copy-crop'     => 'Salin (Copy) & Potong (Crop) ',
    'btn-crop-free'     => 'Bebas',
    'btn-cancel'        => 'Batalkan',
    'btn-confirm'       => 'Sahkan',
    'btn-resize'        => 'Ubah Saiz',
    'btn-open'          => 'Buka Folder',

    'resize-ratio'      => 'Ratio:',
    'resize-scaled'     => 'scaled: Gambar',
    'resize-true'       => 'Ya',
    'resize-old-height' => 'Height: Asli',
    'resize-old-width'  => 'Width: Asli',
    'resize-new-height' => 'Height:',
    'resize-new-width'  => 'Width:',
];
