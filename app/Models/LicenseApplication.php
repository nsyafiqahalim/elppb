<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\Company;
use App\Models\LicenseApplication\CompanyAddress;
use App\Models\LicenseApplication\CompanyPartner;
use App\Models\LicenseApplication\CompanyPremise;
use App\Models\LicenseApplication\CompanyStore;
use App\Models\Admin\Status;
use App\Models\Admin\LicenseType;
use App\Models\Admin\LicenseApplicationType;
use App\User;

class LicenseApplication extends Model
{
    public $guarded = ['id'];
    public $table = 'license_applications';

    public $dates = ['apply_date', 'expiry_date'];

    private const ACTIVE = 1;

    public function companyAddress()
    {
        return $this->hasOne(CompanyAddress::class, 'license_application_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'license_application_id');
    }

    public function companyPartners()
    {
        return $this->hasMany(CompanyPartner::class, 'license_application_id');
    }

    public function companyStores()
    {
        return $this->hasMany(CompanyStore::class, 'license_application_id');
    }

    public function companyPremise()
    {
        return $this->hasOne(CompanyPremise::class, 'license_application_id');
    }

    public function licenseType()
    {
        return $this->belongsTo(LicenseType::class, 'license_type_id');
    }

    public function licenseApplicationType()
    {
        return $this->belongsTo(LicenseApplicationType::class, 'license_application_type_id');
    }

    public function investigationOfficer()
    {
        return $this->belongsTo(User::class, 'license_investigation_officer_id');
    }
}
