<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseType extends Model
{
    public $guarded = ['id'];
}
