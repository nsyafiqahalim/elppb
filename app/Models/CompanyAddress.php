<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\CompanyType;
use App\Models\Company;

use App\User;

class CompanyAddress extends Model
{
    public $guarded = ['id'];
    public $table = 'company_addresses';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
