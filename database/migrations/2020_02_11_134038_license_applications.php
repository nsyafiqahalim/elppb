<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('apply_duration')->nullable();
            //$table->string('apply_load')->nullable();
            $table->string('material_domain')->nullable();
            $table->date('apply_date')->nullable();
            $table->date('received_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('duration_id')->index()->nullable();
            $table->unsignedBigInteger('branch_id')->index()->nullable();
            $table->unsignedBigInteger('license_type_id')->index();
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->unsignedBigInteger('applicant_id')->nullable()->index();
            $table->unsignedBigInteger('license_application_type_id')->index();
            $table->unsignedBigInteger('license_application_category_id')->nullable()->index();
            $table->unsignedBigInteger('license_application_license_generation_type_id')->nullable()->index('la_laplgt_idx');
            $table->unsignedBigInteger('license_investigation_officer_id')->nullable();
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('license_type_id')->references('id')->on('license_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('applicant_id')->references('id')->on('users');
            $table->foreign('license_application_license_generation_type_id', 'fk_la_laplgt')->references('id')->on('license_application_license_generation_types');
            $table->foreign('license_application_type_id')->references('id')->on('license_application_types');
            $table->foreign('license_application_category_id')->references('id')->on('license_application_categories');
            $table->foreign('duration_id')->references('id')->on('durations');
            $table->foreign('license_investigation_officer_id')->references('id')->on('users');
            $table->timestamps();
        });

        Schema::create('approval_letter_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reference_number')->nullable();
            $table->string('company_name')->nullable();
            $table->date('apply_date')->nullable();
            $table->date('received_date')->nullable();
            $table->date('finished_date')->nullable();
            $table->string('material_domain')->nullable();
            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('branch_id')->index()->nullable();
            $table->unsignedBigInteger('approval_letter_type_id')->index();
            $table->unsignedBigInteger('approval_letter_application_type_id')->index('ala_alat_idx');
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->unsignedBigInteger('applicant_id')->nullable()->index();
            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('approval_letter_type_id')->references('id')->on('approval_letter_types');
            $table->foreign('approval_letter_application_type_id', 'fk_ala_alat')->references('id')->on('approval_letter_application_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('applicant_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
