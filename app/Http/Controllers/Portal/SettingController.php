<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;

use App\DataObjects\Portal\SettingDataObject;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($setting)
    {
        $settingDomain = decrypt($setting);

        $settings = (new SettingDataObject)->findSettingByDomain($settingDomain);
        return view('portal.setting.index', compact('settings', 'settingDomain'));
    }
}
