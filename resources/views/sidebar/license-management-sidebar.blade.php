<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li><a href="{{ route('index') }}" aria-expanded="false"><i class="mdi mdi-home"></i>
                <span class="hide-menu">Laman Utama</a></li>
        <li><a href="{{ route('management.mytask-index') }}" aria-expanded="false"><i class="mdi mdi-desktop-mac"></i>
                <span class="hide-menu">Tugasan Saya</a></li>
        <li><a href="#" aria-expanded="false"><i class="mdi mdi-printer-alert"></i>
                <span class="hide-menu">Jana Lesen</a></li>
        <li><a href="#" aria-expanded="false"><i class="mdi mdi-archive"></i>
                <span class="hide-menu">Laporan</a></li>
        <li><a href="#" aria-expanded="false"><i class="ti-search"></i>
                <span class="hide-menu">Carian Lesen</a></li>
    </ul>
</nav>
