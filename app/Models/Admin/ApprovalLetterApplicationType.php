<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ApprovalLetterApplicationType extends Model
{
    public $guarded = ['id'];
}
