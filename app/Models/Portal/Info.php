<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_infos';
}
