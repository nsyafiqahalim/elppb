<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\Race;
use App\Models\Admin\OwnershipType;
use App\Models\Admin\State;

class CompanyPartner extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_company_partners';

    private const ACTIVE = 1;

    public function ownershipType()
    {
        return $this->belongsTo(OwnershipType::class, 'ownership_type_id');
    }

    public function race()
    {
        return $this->belongsTo(Race::class, 'race_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
