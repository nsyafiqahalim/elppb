<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\CompanyPartner;
use App\Models\Admin\CompanyType;
use App\Models\Admin\District;
use App\Models\Admin\State;

$districts = District::all();
$states = State::all();

$factory->define(CompanyPartner::class, function (Faker $faker) use ($districts, $states) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));


    $dist = $districts->random();
    $stat = $states->random();

    return [
            'name'                      =>  $faker->name,
            //'ownership_type_id'         =>  mt_rand(1, 3),
            'race_id'                   =>  mt_rand(1, 5),
            'race_type_id'                   =>  mt_rand(1, 2),
            'total_share'               =>  mt_rand(0, 100),
            'share_percentage'          =>  mt_rand(0, 100),
            'passport'                  => strtoupper(str_random(6)),
            'phone_number'              =>  $faker->phoneNumber,
            'email'                     =>  $faker->unique()->safeEmail,
            'nric'                      =>  $faker->myKadNumber(),
            'address_1'                =>  $faker->buildingPrefix . ' ' . $faker->buildingNumber . ' ' . $faker->streetName,
            'address_2'                 =>  $faker->township,
            'address_3'                 =>  $faker->townState,
            'postcode'                  =>  $faker->numberBetween(10000, 99999),
            'state_id'                  =>  $stat->id,
            'is_citizen'   =>  mt_rand(0, 1)
    ];
});
