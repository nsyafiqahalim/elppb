<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\Faq;

class FaqDataObject
{
    public static function storeNewFaq($param)
    {
        return Faq::create([
            'title'                 =>  $param['title'],
            'description'           =>  $param['description'],
            'is_published'          =>  $param['is_published'],
            'created_by'            =>  $param['created_by'],
            'updated_by'            =>  $param['updated_by'],
        ]);
    }

    public static function updateFaq($param, $id)
    {
        return Faq::find($id)->update([
            'title'                 =>  $param['title'],
            'description'           =>  $param['description'],
            'is_published'          =>  $param['is_published'],
            'updated_by'            =>  $param['updated_by'],
        ]);
    }

    public static function deleteFaq($id)
    {
        return Faq::find($id)->delete();
    }

    public static function findAllFaqUsingDatatableFormat($param)
    {
        $faqs = Faq::distinct()->orderby('id', 'desc')
                        ->when(isset($param['title']) && $param['title'], function ($query) use ($param) {
                            $query->where('title', 'like', '%'.$param['title'].'%');
                        })->when(isset($param['is_published']), function ($query) use ($param) {
                            if ($param['is_published'] == 1) {
                                $query->where('is_published', 1);
                            } elseif ($param['is_published'] == 0) {
                                $query->where('is_published', '<>', 1)->whereNotNull('is_published');
                            }
                        });

        return datatables()->of($faqs)
        ->addIndexColumn()
         ->addColumn('published', function ($faq) {
             $status = ($faq->is_published == 1)? 'Diterbitkan' : 'Tidak Diterbitkan';

             return View('portal.faq.partials.status', compact('status'))->render();
         })
        ->addColumn('action', function ($faq) {
            $encryptedId = encrypt($faq->id);

            return View('portal.faq.partials.datatable-button', compact('encryptedId'))->render();
        })
        ->rawColumns(['published','action'])
       ->make(true);
    }

    public static function findFaqById($id)
    {
        return Faq::find($id);
    }
}
