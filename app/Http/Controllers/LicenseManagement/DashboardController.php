<?php

namespace App\Http\Controllers\LicenseManagement;

use App\Models\LicenseApplication;
use App\Models\LicenseActivity;
use App\Models\LicenseRemark;
use App\Models\LicenseAttachment;
use App\Models\LicensePayment;
use App\Models\UserProfile;
use App\Models\Admin\Status;
use App\Http\Controllers\Controller;
use Auth;
use PDF\PDF;
use Response;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class DashboardController extends Controller
{
    // pengurusan lesen dashboard
    public function index()
    {
        return view('license-management.index');
    }

    // tugasan saya dashboard
    public function myTask()
    {
        if (Auth::user()->can("Jana Lesen")) { // Roles : Penjana Lesen
            $status = Status::whereIn('name', ['Janaan Lesen'])->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        } elseif (Auth::user()->can("Peraku Cawangan Permohonan Lesen Runcit (Cawangan)") || Auth::user()->can("Semakan Siasatan Permohonan Lesen Borong (Cawangan)") || Auth::user()->can("Semakan Siasatan Permohonan Lesen Import (Cawangan)") || Auth::user()->can("Semakan Siasatan Permohonan Lesen Export (Cawangan)") || Auth::user()->can("Semakan Siasatan Permohonan Lesen Beli Padi (Cawangan)") || Auth::user()->can("Semakan Siasatan Permohonan Lesen Kilang Padi Komersial (Cawangan)")) { // Roles : Ketua unit cawangan
            $status = Status::whereIn('name', ['Peraku Semakan Cawangan', 'Pemilihan Pegawai Siasatan', 'Peraku Semakan Siasatan'])->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        } elseif (Auth::user()->can("Kelulusan Permohonan Lesen Runcit (Cawangan)") || Auth::user()->can("Kelulusan Permohonan Lesen Borong (Cawangan)") || Auth::user()->can("Kelulusan Permohonan Lesen Import (Cawangan)") || Auth::user()->can("Kelulusan Permohonan Lesen Export (Cawangan)") || Auth::user()->can("Kelulusan Permohonan Lesen Beli Padi (Cawangan)") || Auth::user()->can("Kelulusan Permohonan Lesen Kilang Padi Komersial (Cawangan)")) { // Roles : Ketua Pengarah
            $status = Status::whereIn('name', ['Kelulusan Permohonan'])->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        } elseif (Auth::user()->can("Siasatan Permohonan Lesen Borong (Cawangan)") || Auth::user()->can("Siasatan Permohonan Lesen Import (Cawangan)") || Auth::user()->can("Siasatan Permohonan Lesen Export (Cawangan)") || Auth::user()->can("Siasatan Permohonan Lesen Beli Padi (Cawangan)") || Auth::user()->can("Siasatan Permohonan Lesen Kilang Padi Komersial (Cawangan)")) { // Roles : pegawai siasatan
            $status = Status::whereIn('name', ['Pemilihan Pegawai Siasatan'])->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        } elseif (Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Borong (Ibu Pejabat)") || Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Import (Ibu Pejabat)") || Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Export (Ibu Pejabat)") || Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Beli Padi (Ibu Pejabat)") || Auth::user()->can("Peraku Ibu Pejabat Permohonan Lesen Kilang Padi Komersial (Ibu Pejabat)")) { // Roles : ketua HQ
            $status = Status::whereIn('name', ['Peraku Semakan Ibu Pejabat'])->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        } else { // roles: pegawai kaunter
            $status = Status::where('name', 'Permohonan Baharu')->pluck('id');
            $licenses = LicenseApplication::whereIn('status_id', $status)->orderBy('created_at', 'desc')->get();
        }

        return view('license-management.mytask-index')->with(['licenses' => $licenses]);
    }

    // view for maklumat pengurusan Lesen// tugasan saya dashboard
    public function showRetail($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.new-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments ]);
    }

    public function showRetailRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments]);
    }

    public function showRetailChange($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.change-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments]);
    }

    public function showRetailReplacement($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.replacement-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments]);
    }

    public function showRetailCancel($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.cancel-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments ]);
    }

    public function showRetailChangeRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();
        $license_payments = LicensePayment::where('license_application_id', $decryptedId)->get()->toArray();

        return view('license-management.retail.new-renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'license_payments' => $license_payments]);
    }

    // edit for maklumat pengurusan Lesen// tugasan saya dashboard
    public function editRetail(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $status = Status::where('code', $request->status)->get('id');
        $license->status_id = $status[0]->id;
        if (!is_null($request->investigation_officer)) {
          $license->license_investigation_officer_id = $request->investigation_officer;
        }
        $license->save();

        // licence application remark
        $license_remark = new LicenseRemark();
        $license_remark->status_id = $status[0]->id;
        $license_remark->remarks = $request->remark;
        $license_remark->user_id = Auth::user()->id;
        $license_remark->license_application_id = $license->id;
        $license_remark->save();

        // licence application activities
        if ($request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_BAHARU' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PENGGANTIAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PEMBATALAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Hantar Semakan ke Ketua Cawangan";
        } elseif ($request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_BAHARU' || $request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_PEMBAHARUAN' || $request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_PENGGANTIAN' || $request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_PEMBATALAN' || $request->status == 'KELULUSAN_PERMOHONAN_RUNCIT_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Hantar Semakan ke Ketua Pengarah";
        } elseif ($request->status == 'JANAAN_LESEN_RUNCIT_BAHARU' || $request->status == 'JANAAN_LESEN_RUNCIT_PEMBAHARUAN' || $request->status == 'JANAAN_LESEN_RUNCIT_PERUBAHAN' || $request->status == 'JANAAN_LESEN_RUNCIT_PENGGANTIAN' || $request->status == 'JANAAN_LESEN_RUNCIT_PEMBATALAN' || $request->status == 'JANAAN_LESEN_RUNCIT_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Permohonan Dilulus";
        } elseif ($request->status == 'TOLAK_RUNCIT_BAHARU' || $request->status == 'TOLAK_RUNCIT_PEMBAHARUAN' || $request->status == 'TOLAK_RUNCIT_PERUBAHAN' || $request->status == 'TOLAK_RUNCIT_PENGGANTIAN' || $request->status == 'TOLAK_RUNCIT_PEMBATALAN' || $request->status == 'TOLAK_RUNCIT_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Permohonan Ditolak";
        }

        $license_activity = new LicenseActivity();
        $license_activity->status_id = $status[0]->id;
        $license_activity->activity = $activity;
        $license_activity->user_id = Auth::user()->id;
        $license_activity->license_application_id = $license->id;
        $license_activity->save();

        if ($activity == "Permohonan Dilulus") {
            $total_payment = (intval($license->apply_load) + 10) * intval($license->apply_duration);

            $license_payment = new LicensePayment();
            $license_payment->status_payment = 'unpaid';
            $license_payment->total_payment = $total_payment;
            $license_payment->license_application_id = $license->id;
            $license_payment->save();
        }

        return response()->json([
            'message'   =>  'Permohonan berjaya dikemaskini',
            'success'   =>  true,
            'route'     => route('management.mytask-index')
        ], 200);
      }

      public function paymentRetailLicense(Request $request, $id)
      {
          $decryptedId = decrypt($id);
          $license = LicenseApplication::find($decryptedId);
          $status = Status::where('display_name', $request->status)->get('id');
          $license_payment = LicensePayment::where('license_application_id', $license->id)->get()->toArray();

          if ($license->status_id == $status[0]->id) {
              $license_remark = new LicenseRemark();
              $license_remark->status_id = $status[0]->id;
              $license_remark->remarks = 'Melakukan Pembayaran';
              $license_remark->user_id = Auth::user()->id;
              $license_remark->license_application_id = $license->id;
              $license_remark->save();

              $license_activity = new LicenseActivity();
              $license_activity->status_id = $status[0]->id;
              $license_activity->activity = 'Melakukan Pembayaran';
              $license_activity->user_id = Auth::user()->id;
              $license_activity->license_application_id = $license->id;
              $license_activity->save();

              $license_payment = LicensePayment::find($license_payment[0]['id']);
              $license_payment->status_payment = 'paid';
              $license_payment->save();
          }

          return response()->json([
              'message'   =>  'Permohonan berjaya dibayar',
              'success'   =>  true,
              'route'     => route('management.mytask-index')
          ], 200);
      }

      public function downloadRetailLicense(Request $request, $id)
      {
          $decryptedId = decrypt($id);
          $license = LicenseApplication::find($decryptedId);
          $status = Status::where('code', $request->status)->get('id');

          if ($license->status_id != $status[0]->id) {
              $license->status_id = $status[0]->id;
              $license->save();

              $license_remark = new LicenseRemark();
              $license_remark->status_id = $status[0]->id;
              $license_remark->remarks = 'Janaan Lesen';
              $license_remark->user_id = Auth::user()->id;
              $license_remark->license_application_id = $license->id;
              $license_remark->save();

              $license_activity = new LicenseActivity();
              $license_activity->status_id = $status[0]->id;
              $license_activity->activity = 'Janaan Lesen';
              $license_activity->user_id = Auth::user()->id;
              $license_activity->license_application_id = $license->id;
              $license_activity->save();
          }

          $pdf = \PDF::loadView('license-management.generate-license.retail-license', ['license' => $license]);
          return $pdf->download('Janaan-Lesen-Runcit.pdf');
      }

    public function showSSM($id)
    {
        $decryptedId = decrypt($id);
        $license_attachement_ssm = LicenseAttachment::where('code', 'SSM')->where('license_application_id', $decryptedId)->get();

        $file_name = $license_attachement_ssm[0]->file_path;
        $path = env('FILE_URL').'/'.$file_name;

        $file_type = mime_content_type($path);

        return Response::make(file_get_contents($path), 200, [
              'Content-Type' => $file_type,
              'Content-Disposition' => 'inline; filename="'.$file_name.'"'
        ]);
    }

    public function showLesenPerniagaan($id)
    {
        $decryptedId = decrypt($id);
        $license_attachement_ssm = LicenseAttachment::where('code', 'MINUTE_MEETING')->where('license_application_id', $decryptedId)->get();

        $file_name = $license_attachement_ssm[0]->file_path;
        $path = env('FILE_URL').'/'.$file_name;

        $file_type = mime_content_type($path);

        return Response::make(file_get_contents($path), 200, [
              'Content-Type' => $file_type,
              'Content-Disposition' => 'inline; filename="'.$file_name.'"'
        ]);
    }
}
