<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\StateDataObject;

class StateController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addState(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $state = StateDataObject::addState($param);

        return response()->json([
            'message'   =>  'Negeri berjaya ditambah',
            'data'      =>  $state,
            'success'   =>  true,
            'route'     =>  route('admin.state.index')
        ], 200);
    }

    public function updateState(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $state = StateDataObject::updateState($param, $decryptedId);

        return response()->json([
            'message'   =>  'Negeri berjaya dikemaskini',
            'data'      =>  $state,
            'success'   =>  true,
            'route'     =>  route('admin.state.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return StateDataObject::findAllStateUsingDatatableFormat($param);
    }
}
