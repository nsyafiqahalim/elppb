<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PermitType extends Model
{
    public $guarded = ['id'];
}
