<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\ParliamentDataObject;

class ParliamentController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addParliament(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;
        $param['state_id'] = decrypt($param['state_id']);


        $parliament = ParliamentDataObject::addParliament($param);

        return response()->json([
            'message'   =>  'Parlimen berjaya ditambah',
            'data'      =>  $parliament,
            'success'   =>  true,
            'route'     =>  route('admin.parliament.index')
        ], 200);
    }

    public function updateParliament(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;
        $param['state_id'] = decrypt($param['state_id']);


        $parliament = ParliamentDataObject::updateParliament($param, $decryptedId);

        return response()->json([
            'message'   =>  'Parlimen berjaya dikemaskini',
            'data'      =>  $parliament,
            'success'   =>  true,
            'route'     =>  route('admin.parliament.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return ParliamentDataObject::findAllParliamentUsingDatatableFormat($param);
    }
}
