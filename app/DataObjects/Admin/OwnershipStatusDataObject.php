<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\OwnershipStatus;

class OwnershipStatusDataObject
{
    public static function addOwnershipStatus($param)
    {
        $ownershipStatus = null;

        DB::transaction(function () use ($param, &$ownershipStatus) {
            $ownershipStatus = OwnershipStatus::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $ownershipStatus;
    }

    public static function updateOwnershipStatus($param, $id)
    {
        $ownershipStatus = null;

        DB::transaction(function () use ($param, $id,  &$ownershipStatus) {
            $ownershipStatus = OwnershipStatus::find($id);

            $ownershipStatus->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $ownershipStatus;
    }

    public static function findAllOwnershipStatusUsingDatatableFormat($param)
    {
        $ownershipStatus = OwnershipStatus::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($ownershipStatus)
        ->addIndexColumn()
        ->addColumn('action', function ($ownershipStatus) {
            return view('admin.ownership-status.partials.datatable-button', compact('ownershipStatus'))->render();
        })
        ->addColumn('status', function ($ownershipStatus) {
            $status= ($ownershipStatus->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.ownership-status.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findOwnershipStatusById($id)
    {
        return OwnershipStatus::find($id);
    }
}
