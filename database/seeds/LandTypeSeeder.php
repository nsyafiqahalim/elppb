<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LandType;

class LandTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'kekal',
            'Pajak',
            'Sendiri',
            'Berhad',
            'Kontrak',
            'Sewa',
            'Lain-lain'
        ];

        foreach ($data as $item) {
            LandType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
