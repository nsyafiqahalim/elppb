<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Parliament extends Model
{
    public const ACTIVE = 1;
    
    public $guarded = ['id'];
    
    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
