<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\LicenseApplication;
use App\Models\Status;

$factory->define(LicenseApplication::class, function (Faker $faker) {
    return [
            'apply_duration'   =>  mt_rand(1, 5)." TAHUN"
    ];
});
