<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\BuildingType;
use App\Models\Admin\BusinessType;
use App\Models\Admin\Dun;
use App\Models\Admin\Parliament;
use App\Models\Admin\District;
use App\Models\Admin\State;

class CompanyPremise extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_company_premises';

    private const ACTIVE = 1;

    public function buildingType()
    {
        return $this->belongsTo(BuildingType::class, 'building_type_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public function dun()
    {
        return $this->belongsTo(Dun::class, 'dun_id');
    }

    public function parliament()
    {
        return $this->belongsTo(Parliament::class, 'parliament_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }
}
