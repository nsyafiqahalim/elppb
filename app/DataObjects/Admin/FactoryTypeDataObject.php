<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\FactoryType;

class FactoryTypeDataObject
{
    public static function addFactoryType($param)
    {
        $factoryType = null;

        DB::transaction(function () use ($param, &$factoryType) {
            $factoryType = FactoryType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $factoryType;
    }

    public static function updateFactoryType($param, $id)
    {
        $factoryType = null;

        DB::transaction(function () use ($param, $id,  &$factoryType) {
            $factoryType = FactoryType::find($id);

            $factoryType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $factoryType;
    }

    public static function findAllFactoryTypeUsingDatatableFormat($param)
    {
        $factoryType = FactoryType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($factoryType)
        ->addIndexColumn()
        ->addColumn('action', function ($factoryType) {
            return view('admin.factory-type.partials.datatable-button', compact('factoryType'))->render();
        })
        ->addColumn('status', function ($factoryType) {
            $status= ($factoryType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.factory-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findFactoryTypeById($id)
    {
        return FactoryType::find($id);
    }
}
