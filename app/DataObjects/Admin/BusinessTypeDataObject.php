<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\BusinessType;

class BusinessTypeDataObject
{
    public static function addBusinessType($param)
    {
        $businessType = null;

        DB::transaction(function () use ($param, &$businessType) {
            $businessType = BusinessType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $businessType;
    }

    public static function updateBusinessType($param, $id)
    {
        $businessType = null;

        DB::transaction(function () use ($param, $id,  &$businessType) {
            $businessType = BusinessType::find($id);

            $businessType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $businessType;
    }

    public static function findAllBusinessTypeUsingDatatableFormat($param)
    {
        $businessType = BusinessType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($businessType)
        ->addIndexColumn()
        ->addColumn('action', function ($businessType) {
            return view('admin.business-type.partials.datatable-button', compact('businessType'))->render();
        })
        ->addColumn('status', function ($businessType) {
            $status= ($businessType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.business-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findBusinessTypeById($id)
    {
        return BusinessType::find($id);
    }
}
