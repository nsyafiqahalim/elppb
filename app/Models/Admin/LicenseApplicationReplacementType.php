<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationReplacementType extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_replacement_types';
}
