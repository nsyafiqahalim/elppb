<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Portal\Album;

class AlbumController extends Controller
{
    public function index()
    {
      $albums = Album::get();

      return view('portal.gallery.album.index')->with('albums', $albums);
    }

    public function create()
    {
      return view('portal.gallery.album.create');
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'nama_album'        => 'required|max:50',
        'keterangan_album'  => 'required|max:50',
        //'kulit_album'       => 'required|image',
      ]);

      // $filenameWithExtension = $request->file('kulit_album')->getClientOriginalName();
      // $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);
      // $extension = $request->file('kulit_album')->getClientOriginalExtension();
      // $filenameToStore = $filename . '_' . time() . '.' . $extension;

      // //save image to machine
      // $request->file('kulit_album')->storeAs('public/album_covers', $filenameToStore);
      
      //$filenameToStore = Storage::disk('fileURL')->put('albums', $request->file('kulit_album'));

      $album = new Album();
      $album->name = $request->input('nama_album');
      $album->description = $request->input('keterangan_album');
      //$album->cover_image = $filenameToStore;
      $album->save();

      return redirect()->route('portal.gallery.album.index')->with('success', 'Album anda telah berjaya ditambah.');
    }

    public function show($id)
    {
      $album = Album::with('photos')->find(decrypt($id));

      return view('portal.gallery.album.show')->with('album', $album);
    }

    public function destroy($id)
    {
        $album = Album::find(decrypt($id));

         $album->delete();

        return response()->json([
            'message'   =>  'Album berjaya dipadam',
            'data'      =>  $album,
            'success'   =>  true,
            'route'     =>  route('portal.gallery.album.index')
        ], 200);
    }
}
