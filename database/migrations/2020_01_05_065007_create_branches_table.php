<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('old_id')->nullable();
            $table->bigInteger('state_id')->nullable()->unsigned()->index();
            $table->bigInteger('district_id')->nullable()->unsigned()->index();
            $table->bigInteger('zone_id')->nullable()->unsigned()->index();
            $table->string('short_code', 100);
            $table->string('name', 255);
            $table->text('description')->nullable();
            // null temporary

            $table->tinyInteger('is_active')->default(0);
            $table->string('type', 30)->nullable();
            $table->string('office_name', 100)->nullable();
            $table->string('ministry_name', 100)->nullable();
            $table->string('address_1', 255)->nullable();
            $table->string('address_2', 255)->nullable();
            $table->string('address_3', 255)->nullable();
            $table->string('postcode', 10)->nullable();
            $table->timestamps();

            //foreign key
            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('zone_id')->references('id')->on('zones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
