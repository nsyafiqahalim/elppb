<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('nric');
            $table->string('passport')->nullable();

            $table->string('total_share');
            $table->string('share_percentage');
  
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->tinyInteger('is_citizen');

            $table->string('state_name');
            $table->string('race_name')->nullable();
            $table->string('race_type_name')->nullable();
            $table->string('citizen_name');
            //$table->string('ownership_type');

            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('race_id')->index()->nullable();
            $table->unsignedBigInteger('race_type_id')->index()->nullable();
            $table->unsignedBigInteger('company_partner_id')->index('lcp_cp_idx');
            // $table->unsignedBigInteger('ownership_type_id')->index();
            $table->unsignedBigInteger('license_application_id')->index('lcp_la_idx');

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('race_id')->references('id')->on('races');
            $table->foreign('race_type_id')->references('id')->on('race_types');
            // $table->foreign('ownership_type_id')->references('id')->on('ownership_types');
            $table->foreign('license_application_id', 'fk_lcp_la')->references('id')->on('license_applications');
            $table->foreign('company_partner_id', 'fk_lcp_cp')->references('id')->on('company_partners');
            $table->timestamps();
        });

        Schema::create('approval_letter_application_company_partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('nric');
            $table->string('passport')->nullable();

            $table->string('total_share');
            $table->string('share_percentage');
  
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->tinyInteger('is_citizen');

            $table->string('state_name');
            $table->string('race_name')->nullable();
            $table->string('race_type_name')->nullable();
            $table->string('citizen_name');
            //$table->string('ownership_type');

            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('race_id')->index()->nullable();
            $table->unsignedBigInteger('race_type_id')->index()->nullable();
            $table->unsignedBigInteger('company_partner_id')->index('al_cp_idx');
            // $table->unsignedBigInteger('ownership_type_id')->index();
            $table->unsignedBigInteger('approval_letter_application_id')->index('al_la_idx');

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('race_id')->references('id')->on('races');
            $table->foreign('race_type_id', 'fk_alp_rt')->references('id')->on('race_types');
            // $table->foreign('ownership_type_id')->references('id')->on('ownership_types');
            $table->foreign('approval_letter_application_id', 'fk_alp_la')->references('id')->on('approval_letter_applications');
            $table->foreign('company_partner_id', 'fk_alcp_cp')->references('id')->on('company_partners');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
