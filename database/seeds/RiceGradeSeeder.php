<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\RiceGrade;

class RiceGradeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'SUPER TEMPATAN 15%',
          'SUPER SPECIAL TEMPATAN 5%',
          'SUPER SPECIAL TEMPATAN 10%',
          'PREMIUM',
          'STANDARD',
          'REBUS',
          'HANCUR',
          'BERAS BARIO',
          'WANGI TEMPATAN',
          'BERAS NASIONAL SABAH/SARAWAK(S15%)',
          'BERAS PERANG',
          'PULUT',
          'SUPER IMPORT',
          'WANGI IMPORT',
          'THAI WHITE RICE',
          'PAKISTAN WHITE RICE',
          'VIETNAM WHITE RICE',
          'COMBODIA RICE',
          'BERAS CINA',
          'TSS',
          'BASMATHI (B)',
          'BASMATHI (S)',
          'PONNIES',
          'CARLROSE',
          'PULUT THAILAND',
          'PULUT VIETNAM',
          'DEDAK',
          'TEMUKUT',
          'TEPUNG BERAS',
          'TEPUNG PULUT',
          'BIHUN',
          'PADI BENIH MR 219',
          'PADI BENIH MR 220',
          'PADI BENIH MR 232',
          'PADI BENIH MR 84',
          'PADI BIASA',
          'PADI BIASA SUBSIDI',
          'HAMPA BERAT'
      ];

        foreach ($data as $item) {
            RiceGrade::create([
              'name'  =>  ucwords(strtolower($item)),
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'is_active' => 1,
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
          ]);
        }
    }
}
