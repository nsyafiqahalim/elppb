<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationBuyPaddies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('license_application_buy_paddies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('license_application_buy_paddy_condition_id')->nullable()->index('labp_labpc_idx');
            $table->unsignedBigInteger('license_application_id')->index('labp_ala_idx');
            $table->foreign('license_application_id', 'fk_labp_ala')->references('id')->on('license_applications');
            $table->foreign('license_application_buy_paddy_condition_id', 'fk_labp_labpc')->references('id')->on('license_application_buy_paddy_conditions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
