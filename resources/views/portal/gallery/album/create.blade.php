@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Galeri</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Pengurusan Portal</li>
            <li class="breadcrumb-item"><i class="fas fa-file-image"></i><a href="{{ route('portal.gallery.album.index') }}"> Galeri</a></li>
            <li class="breadcrumb-item">Tambah</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="fas fa-plus-circle"></i> Tambah album</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample" method="post" action="{{ route('portal.gallery.album.store') }}" enctype="multipart/form-data">
                        @csrf
                        <input type='hidden' name='user_id' value="{{ encrypt(Auth::user()->id) }}" />
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="nama_album">Nama Album <span class='required'>*</span></label>
                                        <input type='text' class='form-control' name='nama_album' id='nama_album' />
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="keterangan_album">Keterangan Album <span class='required'>*</span></label>
                                        <textarea id="keterangan_album" class="form-control border-primary" rows="5" name="keterangan_album"></textarea>
                                    </div>
                                </div>
                                <!--
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="kulit_album">Kulit Album <span class='required'>*</span>
                                          <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="" data-original-title="Contoh: .jpeg, .jpg, .png, .gif, .bmp, .svg"><i class="fas fa-info-circle"></i></button>
                                        </label>
                                        <input type='file' class='form-control' name='kulit_album' id='kulit_album' />
                                    </div>
                                </div>
                                --->
                            </div> <!-- End Row -->
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route ('portal.gallery.album.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
