<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\PartnerType;

class PartnerTypeDataObject
{
    public static function addPartnerType($param)
    {
        $partnerType = null;

        DB::transaction(function () use ($param, &$partnerType) {
            $partnerType = PartnerType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $partnerType;
    }

    public static function updatePartnerType($param, $id)
    {
        $partnerType = null;

        DB::transaction(function () use ($param, $id,  &$partnerType) {
            $partnerType = PartnerType::find($id);

            $partnerType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $partnerType;
    }

    public static function findAllPartnerTypeUsingDatatableFormat($param)
    {
        $partnerType = PartnerType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($partnerType)
        ->addIndexColumn()
        ->addColumn('action', function ($partnerType) {
            return view('admin.partner-type.partials.datatable-button', compact('partnerType'))->render();
        })
        ->addColumn('status', function ($partnerType) {
            $status= ($partnerType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.partner-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findPartnerTypeById($id)
    {
        return PartnerType::find($id);
    }
}
