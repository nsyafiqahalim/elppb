<?php

namespace App\DataObjects\Portal;

use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\User;

use Illuminate\Support\Facades\Cache;

use App\Models\Portal\Setting;

class SettingDataObject
{
    public function findSettingByDomain($domain)
    {
        if (Cache::get($domain) !== null) {
            return Cache::get($domain);
        }
        //if not cache instances
        else {
            $settings = Cache::rememberForever($domain, function () use ($domain) {
                return  Setting::where('domain', $domain)->get();
            });
            return $settings;
        }
    }

    public function updateSetting($request, $domain)
    {
        $settings = null;
        DB::transaction(function () use ($request,$domain, &$settings) {
            $settings  = Setting::where('domain', $domain)->where('configurable', 1)->get();
            foreach ($settings as $setting) {
                if ($setting->type == 'IMAGE') {
                    $currentFilePath = public_path().'//file/'.$setting->value;

                    if (isset($setting->value) && strlen($setting->value) > 0 && file_exists($currentFilePath)) {
                        unlink($currentFilePath);
                    }
                    if ($request->file($setting->code) !== null) {
                        $path = Storage::disk('portal')->put('setting', $request->file($setting->code));

                        Setting::where('code', $setting->code)->update([
                            'value' => $path
                         ]);
                    }
                } else {
                    Setting::where('code', $setting->code)->update([
                        'value' => $request->input($setting->code)
                    ]);
                }
            }

            Cache::forget($domain);
            $settings = $this->findSettingByDomain($domain);
        });

        return $settings;
    }
}
