<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Permit extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_permits';
}
