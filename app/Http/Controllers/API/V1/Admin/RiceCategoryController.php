<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RiceCategoryDataObject;

class RiceCategoryController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRiceCategory(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $riceCategory = RiceCategoryDataObject::addRiceCategory($param);

        return response()->json([
            'message'   =>  'Kategori beras berjaya ditambah',
            'data'      =>  $riceCategory,
            'success'   =>  true,
            'route'     =>  route('admin.rice-category.index')
        ], 200);
    }

    public function updateRiceCategory(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $riceCategory = RiceCategoryDataObject::updateRiceCategory($param, $decryptedId);

        return response()->json([
            'message'   =>  'Kategori beras berjaya dikemaskini',
            'data'      =>  $riceCategory,
            'success'   =>  true,
            'route'     =>  route('admin.rice-category.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RiceCategoryDataObject::findAllRiceCategoryUsingDatatableFormat($param);
    }
}
