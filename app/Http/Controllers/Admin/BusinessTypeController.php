<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\BusinessTypeDataObject;

class BusinessTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        return view('admin.business-type.index');
    }

    public function add()
    {
        return view('admin.business-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        
        $businessType = BusinessTypeDataObject::findBusinessTypeById($decryptedId);

        return view('admin.business-type.edit', compact('businessType'));
    }
}
