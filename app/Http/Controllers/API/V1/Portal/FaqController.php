<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Portal\FaqDataObject;

class FaqController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addFaq(Request $request)
    {
        $param = $request->all();
        $decryptedUserId = decrypt($param['user_id']);

        $param['created_by'] = $decryptedUserId;
        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $faqDAO = FaqDataObject::storeNewFaq($param);

        return response()->json([
            'message'   =>  'Maklumat Soalan Lazim berjaya ditambah',
            'data'      =>  $faqDAO,
            'success'   =>  true,
            'route'     =>  route('portal.faq.index')
        ], 200);
    }

    public function updateFaq(Request $request, $id)
    {
        $param = $request->all();

        $decryptedId = decrypt($id);
        $decryptedUserId = decrypt($param['user_id']);

        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $faqDAO = FaqDataObject::updateFaq($param, $decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Soalan Lazim berjaya dikemaskini',
            'data'      =>  $faqDAO,
            'success'   =>  true,
            'route'     =>  route('portal.faq.index')
        ], 200);
    }

    public function deleteFaq($id)
    {
        $decryptedId = decrypt($id);

        $faqDAO = FaqDataObject::deleteFaq($decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Soalan Lazim berjaya dipadam',
            'data'      =>  $faqDAO,
            'success'   =>  true,
            'route'     =>  route('portal.faq.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return FaqDataObject::findAllFaqUsingDatatableFormat($param);
    }
}
