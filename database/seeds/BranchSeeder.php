<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\Models\Admin\Branch;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Branch::create([
            'state_id'      => '16',
            'district_id'   => '126',
            'zone_id'       => '2',
            'short_code'    => "PJY",
            'name'          => 'PUTRAJAYA',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "SEKSYEN KAWALSELIA PADI DAN BERAS",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "ARAS 2, BLOK MENARA, LOT 4G1, WISMA TANI",
            'address_2'     => "NO. 28, PERSIARAN PERDANA",
            'postcode'      => "62624",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '10',
            'district_id'   => '74',
            'zone_id'       => '2',
            'short_code'    => "SEL",
            'name'          => 'SELANGOR',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI SELANGOR",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "BLOK B, IADA BARAT LAUT SELANGOR",
            'address_2'     => "KOMPLEKS PEJABAT IADA BARAT LAUT SELANGOR",
            'postcode'      => "45000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '5',
            'district_id'   => '41',
            'zone_id'       => '2',
            'short_code'    => "NSB",
            'name'          => 'NEGERI SEMBILAN',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI SEMBILAN",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "TINGKAT 1,  PT 2171, BATU 4 Â½,",
            'address_2'     => "JALAN SEREMBAN-TAMPIN",
            'postcode'      => "70450",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '4',
            'district_id'   => '34',
            'zone_id'       => '3',
            'short_code'    => "MLK",
            'name'          => 'MELAKA',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI MELAKA",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. G19 & 01-19, BLOK BEGONIA,",
            'address_2'     => "SAUJANA PURI, BUKIT KATIL",
            'postcode'      => "75450",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '1',
            'district_id'   => '3',
            'zone_id'       => '3',
            'short_code'    => "KLG",
            'name'          => 'KLUANG',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN KLUANG",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => 'NO. 8, JALAN INTAN 2/1,',
            'address_2'     => 'TAMAN INTAN',
            'postcode'      => "86000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'  	=> '1',
            'district_id'   => '2',
            'zone_id'    	=> '3',
            'short_code'    => "JBR",
            'name'  		=> 'JOHOR BAHRU',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'   	=> "1",
            'type'   		=> "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN JOHOR BAHRU",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. 34,JALAN GARUDA 1",
            'address_2'     => "LARKIN",
            'postcode'      => "80350",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'  	=> '2',
            'district_id'   => '11',
            'zone_id'    	=> '1',
            'short_code'    => "KDH",
            'name'  		=> 'KEDAH',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'   	=> "1",
            'type'   		=> "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI KEDAH",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. 233 & 234, JALAN SHAHAB 2",
            'address_2'     => "SHAHAB PERDANA",
            'postcode'      => "05150",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '9',
            'district_id'   => '71',
            'zone_id'       => '2',
            'short_code'    => "PLI",
            'name'          => 'PERLIS',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI PERLIS",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. 16, JALAN SENA INDAH SATU",
            'address_2'     => "TAMAN SENA INDAH",
            'postcode'      => "01000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '8',
            'district_id'   => '55',
            'zone_id'       => '1',
            'short_code'    => "PPG",
            'name'          => 'PULAU PINANG',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI PULAU PINANG",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. 10, JALAN TODAK 5",
            'address_2'     => "PUSAT BANDAR SEBERANG JAYA",
            'postcode'      => "13700",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '8',
            'district_id'   => '62',
            'zone_id'       => '2',
            'short_code'    => "PRK",
            'name'          => 'PERAK',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI PERAK",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. 186, TAMAN ANDA",
            'address_2'     => "JALAN SULTAN AZLAN SHAH UTARA",
            'postcode'      => "31400",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '6',
            'district_id'   => '47',
            'zone_id'       => '4',
            'short_code'    => "KTN",
            'name'          => 'KUANTAN',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN KUANTAN",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "NO. B24, JALAN IM 3/10, BIM POINT,",
            'address_2'     => "BANDAR INDERA MAHKOTA",
            'postcode'      => "25200",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '6',
            'district_id'   => '51',
            'zone_id'       => '4',
            'short_code'    => "TLH",
            'name'          => 'TEMERLOH',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN TEMERLOH",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => 'LOT 1208, KAWASAN PERINDUSTRIAN SONGSANG',
            'address_2'     => 'JALAN JERANTUT',
            'postcode'      => "28000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '11',
            'district_id'   => '84',
            'zone_id'       => '4',
            'short_code'    => "TLH",
            'name'          => 'TERENGGANU',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI TERENGGANU",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => 'PT2347K, JALAN RAJAWALI 5',
            'address_2'     => 'KAWASAN PERINDUSTRIAN CHENDERING',
            'postcode'      => "21080",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);


        Branch::create([
            'state_id'  	=> '3',
            'district_id'   => '24',
            'zone_id'    	=> '4',
            'short_code'    => "KLN",
            'name'  		=> 'KELANTAN',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'   	=> "1",
            'type'   		=> "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS NEGERI KELANTAN",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "LOT 2052/2053,",
            'address_2'     => "JALAN DATO LUNDANG",
            'postcode'      => "15720",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '13',
            'district_id'   => '113',
            'zone_id'       => '5',
            'short_code'    => "KUC",
            'name'          => 'KUCHING',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "PENGARAH",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN KUCHING",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => 'AG 101 & AG 102 BLOK G PLOT 4,',
            'address_2'     => 'JALAN BATU KAWAH,BATU KAWAH NEW TOWNSHIP',
            'postcode'      => "93250",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '13',
            'district_id'   => '116',
            'zone_id'       => '5',
            'short_code'    => "MRI",
            'name'          => 'MIRI',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN MIRI",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "LOT 2303 TAMAN BULATAN,COMMERCIAL CENTRE,",
            'address_2'     => "COMMERCIAL CENTRE,JALAN DATO MUIP",
            'postcode'      => "98000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '13',
            'district_id'   => '115',
            'zone_id'       => '5',
            'short_code'    => "SBU",
            'name'          => 'SIBU',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN SIBU",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => 'NO. 4C, TINGKAT 1, LOT 1157 BLOK 6',
            'address_2'     => 'SIBU TOWN DISTRICT, JALAN HUA KIEW',
            'postcode'      => "45000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'      => '12',
            'district_id'   => '94',
            'zone_id'       => '5',
            'short_code'    => "SDN",
            'name'          => 'SANDAKAN',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'     => "1",
            'type'          => "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN SANDAKAN",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "LOT 6, BLOK 16, W.D.T. NO. 599,TINGKAT BAWAH, BAND",
            'address_2'     => "BATU 4, JALAN LABUK",
            'postcode'      => "90009",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        Branch::create([
            'state_id'  	=> '12',
            'district_id'   => '88',
            'zone_id'    	=> '5',
            'short_code'    => "KKB",
            'name'  		=> 'KOTA KINABALU',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'   	=> "1",
            'type'   		=> "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN KOTA KINABALU",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1'     => "TINGKAT 5, BLOK D,",
            'address_2'     => "BANGUNAN POS BESAR",
            'postcode'      => "88827",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

           Branch::create([
            'state_id'  	=> '12',
            'district_id'   => '97',
            'zone_id'    	=> '5',
            'short_code'    => "TWU",
            'name'  		=> 'TAWAU',
            'description'   => "Data ini merujuk kepada Cawangan",
            'is_active'   	=> "1",
            'type'   		=> "KETUA CAWANGAN",
            'office_name'   => "PEJABAT KAWALSELIA PADI DAN BERAS CAWANGAN TAWAU",
            'ministry_name' => "KEMENTERIAN PERTANIAN DAN INDUSTRI ASAS TANI",
            'address_1' 	=> 'TB 8279, PERDANA SQUARE, BT.3',
            'address_2' 	=> 'JALAN APAS',
            'postcode'      => "91000",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

       
    }
}
