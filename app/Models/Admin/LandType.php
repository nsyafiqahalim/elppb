<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LandType extends Model
{
    public $guarded = ['id'];
}
