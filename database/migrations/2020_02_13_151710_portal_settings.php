<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PortalSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('label');
            $table->string('code');
            $table->string('domain');
            $table->string('metric');
            $table->string('readonly');
            $table->tinyInteger('configurable')->default(1);

            $table->string('class')->nullable();
            $table->enum('type', ['TEXTAREA','TEXT','IMAGE']);
            $table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
