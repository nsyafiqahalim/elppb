<?php

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('permohonan-lesen')->group(function () {
        Route::get('/', 'LicenseApplication\DashboardController@index')->name('license_application.index');

        Route::prefix('syarikat')->group(function () {
            Route::get('/', 'LicenseApplication\CompanyController@index')->name('license_application.company.index');
            Route::get('/papar', 'LicenseApplication\CompanyController@view')->name('license_application.company.view');
            Route::get('/kemaskini', 'LicenseApplication\CompanyController@edit')->name('license_application.company.edit');

            Route::get('/tambah', 'LicenseApplication\CompanyController@add')->name('license_application.company.add');
        });

        Route::prefix('runcit')->group(function () {
            Route::prefix('pembaharuan')->group(function () {
                Route::get('/', 'LicenseApplication\Retail\RenewController@add')->name('license_application.retail.renew.add');
                Route::get('/kemaskini', 'LicenseApplication\Retail\RenewController@edit')->name('license_application.retail.renew.edit');
            });

            Route::prefix('ganti')->group(function () {
                Route::get('/', 'LicenseApplication\Retail\ReplacementController@add')->name('license_application.retail.replacement.add');
                Route::get('/ganti/kemaskini', 'LicenseApplication\Retail\ReplacementController@edit')->name('license_application.retail.replacement.edit');
            });

            Route::prefix('perubahan')->group(function () {
                Route::get('/', 'LicenseApplication\Retail\UpdateController@add')->name('license_application.retail.update.add');
                Route::get('/perubahan/kemaskini', 'LicenseApplication\Retail\UpdateController@edit')->name('license_application.retail.update.edit');
            });

            Route::prefix('pembatalan')->group(function () {
                Route::get('/', 'LicenseApplication\Retail\CancelController@add')->name('license_application.retail.cancel.add');
                Route::get('/kemaskini', 'LicenseApplication\Retail\CancelController@edit')->name('license_application.retail.cancel.edit');
            });
        });

        Route::prefix('borong')->group(function () {
            Route::prefix('pembaharuan')->group(function () {
                Route::get('/', 'LicenseApplication\Wholesale\RenewController@add')->name('license_application.wholesale.renew.add');
                Route::get('/kemaskini', 'LicenseApplication\Wholesale\RenewController@edit')->name('license_application.wholesale.renew.edit');
            });

            Route::prefix('ganti')->group(function () {
                Route::get('/', 'LicenseApplication\Wholesale\ReplacementController@add')->name('license_application.wholesale.replacement.add');
                Route::get('/ganti/kemaskini', 'LicenseApplication\Wholesale\ReplacementController@edit')->name('license_application.wholesale.replacement.edit');
            });

            Route::prefix('perubahan')->group(function () {
                Route::get('/', 'LicenseApplication\Wholesale\UpdateController@add')->name('license_application.wholesale.update.add');
                Route::get('/perubahan/kemaskini', 'LicenseApplication\Wholesale\UpdateController@edit')->name('license_application.wholesale.update.edit');
            });

            Route::prefix('pembatalan')->group(function () {
                Route::get('/', 'LicenseApplication\Wholesale\CancelController@add')->name('license_application.wholesale.cancel.add');
                Route::get('/kemaskini', 'LicenseApplication\Wholesale\CancelController@edit')->name('license_application.wholesale.cancel.edit');
            });
        });

        Route::prefix('import')->group(function () {
            Route::prefix('pembaharuan')->group(function () {
                Route::get('/', 'LicenseApplication\Import\RenewController@add')->name('license_application.import.renew.add');
                Route::get('/kemaskini', 'LicenseApplication\Import\RenewController@edit')->name('license_application.import.renew.edit');
            });

            Route::prefix('ganti')->group(function () {
                Route::get('/', 'LicenseApplication\Import\ReplacementController@add')->name('license_application.import.replacement.add');
                Route::get('/ganti/kemaskini', 'LicenseApplication\Import\ReplacementController@edit')->name('license_application.import.replacement.edit');
            });

            Route::prefix('perubahan')->group(function () {
                Route::get('/', 'LicenseApplication\Import\UpdateController@add')->name('license_application.import.update.add');
                Route::get('/perubahan/kemaskini', 'LicenseApplication\Import\UpdateController@edit')->name('license_application.import.update.edit');
            });

            Route::prefix('pembatalan')->group(function () {
                Route::get('/', 'LicenseApplication\Import\CancelController@add')->name('license_application.import.cancel.add');
                Route::get('/kemaskini', 'LicenseApplication\Import\CancelController@edit')->name('license_application.import.cancel.edit');
            });
        });

        Route::prefix('export')->group(function () {
            Route::prefix('pembaharuan')->group(function () {
                Route::get('/', 'LicenseApplication\Export\RenewController@add')->name('license_application.export.renew.add');
                Route::get('/kemaskini', 'LicenseApplication\Export\RenewController@edit')->name('license_application.export.renew.edit');
            });

            Route::prefix('ganti')->group(function () {
                Route::get('/', 'LicenseApplication\Export\ReplacementController@add')->name('license_application.export.replacement.add');
                Route::get('/ganti/kemaskini', 'LicenseApplication\Export\ReplacementController@edit')->name('license_application.export.replacement.edit');
            });

            Route::prefix('perubahan')->group(function () {
                Route::get('/', 'LicenseApplication\Export\UpdateController@add')->name('license_application.export.update.add');
                Route::get('/perubahan/kemaskini', 'LicenseApplication\Export\UpdateController@edit')->name('license_application.export.update.edit');
            });

            Route::prefix('pembatalan')->group(function () {
                Route::get('/', 'LicenseApplication\Export\CancelController@add')->name('license_application.export.cancel.add');
                Route::get('/kemaskini', 'LicenseApplication\Export\CancelController@edit')->name('license_application.export.cancel.edit');
            });
        });

        Route::get('/runcit', 'LicenseApplication\LicenseController@retail')->name('license_application.retail');
        Route::get('/borong', 'LicenseApplication\LicenseController@wholesale')->name('license_application.wholesale');
        Route::get('/import-eksport', 'LicenseApplication\LicenseController@importExport')->name('license_application.import_export');
        Route::get('/beli-padi', 'LicenseApplication\LicenseController@buyPaddy')->name('license_application.buy_paddy');
        Route::get('/kilang', 'LicenseApplication\LicenseController@factory')->name('license_application.factory');

        Route::get('/senarai-permohonan-lesen', 'LicenseApplication\LicenseController@listOfApplication')->name('license_application.list_of_application');
        Route::get('/senarai-permohonan-permit', 'LicenseApplication\LicenseController@listOfPermit')->name('license_application.list_of_permit');
        Route::get('/senarai-lesen', 'LicenseApplication\LicenseController@license')->name('license_application.license');
    });
});
