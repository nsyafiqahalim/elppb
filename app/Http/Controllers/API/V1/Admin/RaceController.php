<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RaceDataObject;

class RaceController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRace(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $race = RaceDataObject::addRace($param);

        return response()->json([
            'message'   =>  'Bangsa berjaya ditambah',
            'data'      =>  $race,
            'success'   =>  true,
            'route'     =>  route('admin.race.index')
        ], 200);
    }

    public function updateRace(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $race = RaceDataObject::updateRace($param, $decryptedId);

        return response()->json([
            'message'   =>  'Bangsa berjaya dikemaskini',
            'data'      =>  $race,
            'success'   =>  true,
            'route'     =>  route('admin.race.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RaceDataObject::findAllRaceUsingDatatableFormat($param);
    }
}
