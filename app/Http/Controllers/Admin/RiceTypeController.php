<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RiceTypeDataObject;

class RiceTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.rice-type.index');
    }

    public function add()
    {
        return view('admin.rice-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $riceType = RiceTypeDataObject::findRiceTypeById($decryptedId);

        return view('admin.rice-type.edit', compact('riceType'));
    }
}
