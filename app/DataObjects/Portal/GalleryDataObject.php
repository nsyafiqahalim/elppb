<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\Gallery;
use Illuminate\Support\Facades\Storage;

class GalleryDataObject
{
    public static function addNewImageToGallery($param)
    {
        $path = Storage::disk('fileURL')->put('gallery', $param['file']);

        Gallery::create([
            'file_path'          =>  $path,
            'file_original_name' =>  $param['file']->getClientOriginalName(),
            'created_by'         =>  $param['created_by']
        ]);
    }

    public static function findAndDeleteImageFromGallery($id)
    {
        $gallery = Gallery::find($id);

        unlink(public_path().'//file/'.$gallery->file_path);

        $gallery->delete();
    }

    public static function findImageWithPagination($paginationSize)
    {
        return Gallery::paginate($paginationSize);
    }
}
