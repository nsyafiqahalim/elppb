<?php

namespace App\Models\ApprovalLetterApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\Admin\CompanyType;
use App\User;

class Material extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_application_materials';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

}
