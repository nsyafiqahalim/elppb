<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_licenses';
}
