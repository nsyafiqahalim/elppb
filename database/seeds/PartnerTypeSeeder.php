<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\PartnerType;

class PartnerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Pemilik',
          'Pengarah',
          'Pemegang Saham',
      ];

        foreach ($data as $item) {
            PartnerType::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'is_active' => 1,
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
          ]);
        }
    }
}
