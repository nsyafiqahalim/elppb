<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Models\Admin\District;
use App\Models\Admin\AreaCoverage;

class Branch extends Model
{
    public $guarded = ['id'];
    public const ACTIVE = 1;
    
    public function areaCoverages()
    {
        return $this->hasMany(AreaCoverage::class, 'branch_id');
    }

    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
