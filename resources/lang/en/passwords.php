<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Kata laluan anda telah ditukar!',
    'sent' => 'Kami telah menghantar pautan tukar kata laluan ke e-mel anda!',
    'token' => 'Token tukar kata laluan ini tidak sah.',
    'user' => "Pengguna dengan alamat e-mel tersebut tiada dalam data.",

];
