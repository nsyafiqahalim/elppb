<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\BuildingType;

class BuildingTypeDataObject
{
    public static function addBuildingType($param)
    {
        $buildingType = null;

        DB::transaction(function () use ($param, &$buildingType) {
            $buildingType = BuildingType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $buildingType;
    }

    public static function updateBuildingType($param, $id)
    {
        $buildingType = null;

        DB::transaction(function () use ($param, $id,  &$buildingType) {
            $buildingType = BuildingType::find($id);

            $buildingType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $buildingType;
    }

    public static function findAllBuildingTypeUsingDatatableFormat($param)
    {
        $buildingType = BuildingType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($buildingType)
        ->addIndexColumn()
        ->addColumn('action', function ($buildingType) {
            return view('admin.building-type.partials.datatable-button', compact('buildingType'))->render();
        })
        ->addColumn('status', function ($buildingType) {
            $status= ($buildingType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.building-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findBuildingTypeById($id)
    {
        return BuildingType::find($id);
    }
}
