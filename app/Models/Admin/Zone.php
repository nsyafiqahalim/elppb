<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    public $guarded = ['id'];
}
