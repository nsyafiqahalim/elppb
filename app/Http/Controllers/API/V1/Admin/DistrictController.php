<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\DistrictDataObject;

class DistrictController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addDistrict(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at']    = $currentDateTime;
        $param['updated_at']    = $currentDateTime;
        $param['state_id']      = decrypt($param['state_id']);

        $district = DistrictDataObject::addDistrict($param);
        

        return response()->json([
            'message'   =>  'Daerah berjaya ditambah',
            'data'      =>  $district,
            'success'   =>  true,
            'route'     =>  route('admin.district.index')
        ], 200);
    }

    public function updateDistrict(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at']    = $currentDateTime;
        $param['state_id']      = decrypt($param['state_id']);

        $district = DistrictDataObject::updateDistrict($param, $decryptedId);

        return response()->json([
            'message'   =>  'Daerah berjaya dikemaskini',
            'data'      =>  $district,
            'success'   =>  true,
            'route'     =>  route('admin.district.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return DistrictDataObject::findAllDistrictUsingDatatableFormat($param);
    }
}
