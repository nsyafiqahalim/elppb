@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-12 col-12 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0"><i class="fas fa-list"></i> Senarai Pengguna</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('admin.index') }}">Pentadbiran Sistem</a></li>
            <li class="breadcrumb-item">Peranan Dan Pengguna</li>
            <li class="breadcrumb-item">Senarai Pengguna</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <a style='color:white; font-weight: 400;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Carian</a>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <form class="form" id="filter-user">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='name' class="form-control" placeholder="Nama" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='login_id' class="form-control" placeholder="ID Pengguna" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <select name='status' class="form-control select2">
                                            <option value=''> Semua Status</option>
                                            <option value='ACTIVE'>Aktif</option>
                                            <option value='DEACTIVATED'>Tidak Aktif</option>
                                            <option value='LOCKED'>Dikunci</option>
                                            <option value='WAITING_FOR_VERIFICATION'>Menunggu Pengesahan Akaun</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="icon-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                <div class="m-t-40">
                    <a href="{{ route('admin.user.add') }}">
                        <button type="submit" class="btn btn-success float-right">
                            <i class="icon-plus"></i> Tambah
                        </button>
                    </a>
                    <table id="user-datatable" class="table display table-bordered table-striped table-responsived" style='text-align:center'>
                        <thead>
                            <tr style="text-align:center">
                                <th style="vertical-align:middle">Bil.</th>
                                <th style="vertical-align:middle">Nama</th>
                                <th style="vertical-align:middle">ID Pengguna</th>
                                <th style="vertical-align:middle">Status</th>
                                <th style="vertical-align:middle" width="300">Tindakan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}">
    @endpush

    @push('js')
    <script src="{{ asset('DataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('DataTables/datatables-rowReorder.min.js') }}"></script>
    <script src="{{ asset('DataTables/datatables-responsive.min.js') }}"></script>
    <script>
        $("#filter-user").submit(function(event) {
            event.preventDefault();
            form = $('#filter-user').serializeArray();
            formdata = {}
            for (x = 0; x < form.length; x++) {
                formdata[form[x].name] = form[x].value;
            }
            $('#user-datatable').DataTable().destroy();
            $('#user-datatable').DataTable(
                loadConfig(formdata)
            );
        });

        $('#user-datatable').DataTable(
            loadConfig()
        );

        function loadConfig(form) {
            if (form == null) {
                form = {};
            }
            form["src"] = "datatable";
            var json = {
                "language": {
                    "url": "{{ asset('DataTables/lang/malay.json') }}"
                },
                "lengthMenu": [20, 50, 75, 100],
                "rowReorder": {
                    selector: 'td:nth-child(2)'
                },
                "responsive": true,
                "rowReorder": {
                    selector: 'td:nth-child(2)'
                },
                "responsive": true,
                "processing": true,
                "serverSide": true,
                "bPaginate": true,
                "searching": false,
                "ordering": false,
                "ajax": {
                    "url": "{{ route('api.admin.user.datatable') }}",
                    "type": "GET",
                    "dataType": 'json',
                    "data": form,
                    "dataSrc": "data"
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'login_id',
                        name: 'login_id'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'action',
                        name: 'Tindakan'
                    },
                ]

            };

            return json;
        }
    </script>
    @endpush
