<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\PermitType;

class PermitTypeDataObject
{
    public static function addPermitType($param)
    {
        $permitType = null;

        DB::transaction(function () use ($param, &$permitType) {
            $permitType = PermitType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $permitType;
    }

    public static function updatePermitType($param, $id)
    {
        $permitType = null;

        DB::transaction(function () use ($param, $id,  &$permitType) {
            $permitType = PermitType::find($id);

            $permitType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $permitType;
    }

    public static function findAllPermitTypeUsingDatatableFormat($param)
    {
        $permitType = PermitType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($permitType)
        ->addIndexColumn()
        ->addColumn('action', function ($permitType) {
            return view('admin.permit-type.partials.datatable-button', compact('permitType'))->render();
        })
        ->addColumn('status', function ($permitType) {
            $status= ($permitType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.permit-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findPermitTypeById($id)
    {
        return PermitType::find($id);
    }
}
