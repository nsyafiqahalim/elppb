<?php

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('pengurusan-lesen')->group(function () {
      Route::get('/', 'LicenseManagement\DashboardController@index')->name('management.index');

      Route::prefix('tugasan-saya')->group(function () {
        Route::get('/', 'LicenseManagement\DashboardController@myTask')->name('management.mytask-index');

          Route::prefix('runcit')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetail')->name('management.retail.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.new.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.new.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetailRenew')->name('management.retail.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.renew.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.renew.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetailChange')->name('management.retail.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.change.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.change.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetailReplacement')->name('management.retail.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.replacement.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.replacement.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetailCancel')->name('management.retail.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.cancel.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.cancel.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.cancel.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.cancel.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\DashboardController@showRetailChangeRenew')->name('management.retail.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\DashboardController@editRetail')->name('management.retail.renew_dan_change.edit');
              Route::get('/bayaran-lesen/{id}', 'LicenseManagement\DashboardController@paymentRetailLicense')->name('management.retail.renew_dan_change.payment');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\DashboardController@downloadRetailLicense')->name('management.retail.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\DashboardController@showSSM')->name('management.retail.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\DashboardController@showLesenPerniagaan')->name('management.retail.renew_dan_change.lesen-perniagaan');
            });
          });

          Route::prefix('borong')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesale')->name('management.wholesale.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.new.edit');
              Route::get('/laporan-siasatan/{id}', 'LicenseManagement\WholesaleController@investigationWholesale')->name('management.wholesale.new.investigation');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\WholesaleController@downloadWholesaleLicense')->name('management.wholesale.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesaleRenew')->name('management.wholesale.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.renew.edit');
              Route::get('/laporan-siasatan/{id}', 'LicenseManagement\WholesaleController@investigationWholesale')->name('management.wholesale.renew.investigation');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\WholesaleController@downloadWholesaleLicense')->name('management.wholesale.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesaleChange')->name('management.wholesale.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.change.edit');
              Route::get('/laporan-siasatan/{id}', 'LicenseManagement\WholesaleController@investigationWholesale')->name('management.wholesale.change.investigation');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\WholesaleController@downloadWholesaleLicense')->name('management.wholesale.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesaleReplacement')->name('management.wholesale.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.replacement.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\WholesaleController@downloadWholesaleLicense')->name('management.wholesale.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesaleCancel')->name('management.wholesale.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.cancel.edit');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.cancel.ssm');
              Route::get('/laporan-siasatan/{id}', 'LicenseManagement\WholesaleController@investigationWholesale')->name('management.wholesale.new.investigation');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\WholesaleController@showWholesaleChangeRenew')->name('management.wholesale.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\WholesaleController@editWholesale')->name('management.wholesale.renew_dan_change.edit');
              Route::get('/laporan-siasatan/{id}', 'LicenseManagement\WholesaleController@investigationWholesale')->name('management.wholesale.renew_dan_change.investigation');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\WholesaleController@downloadWholesaleLicense')->name('management.wholesale.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\WholesaleController@showSSM')->name('management.wholesale.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\WholesaleController@showLesenPerniagaan')->name('management.wholesale.renew_dan_change.lesen-perniagaan');
            });
          });

          Route::prefix('import')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImport')->name('management.import.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.new.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImportRenew')->name('management.import.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.renew.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImportChange')->name('management.import.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImportReplacement')->name('management.import.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.replacement.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImportCancel')->name('management.import.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.cancel.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.cancel.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.cancel.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ImportController@showImportChangeRenew')->name('management.import.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ImportController@editImport')->name('management.import.renew_dan_change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ImportController@downloadImportLicense')->name('management.import.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ImportController@showSSM')->name('management.import.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ImportController@showLesenPerniagaan')->name('management.import.renew_dan_change.lesen-perniagaan');
            });
          });

          Route::prefix('eksport')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExport')->name('management.export.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.new.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExportRenew')->name('management.export.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.renew.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExportChange')->name('management.export.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExportReplacement')->name('management.export.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.replacement.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExportCancel')->name('management.export.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.cancel.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.cancel.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.cancel.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\ExportController@showExportChangeRenew')->name('management.export.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\ExportController@editExport')->name('management.export.renew_dan_change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\ExportController@downloadExportLicense')->name('management.export.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\ExportController@showSSM')->name('management.export.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\ExportController@showLesenPerniagaan')->name('management.export.renew_dan_change.lesen-perniagaan');
            });
          });

          Route::prefix('kilang-padi')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddy')->name('management.factory_paddy.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.new.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddyRenew')->name('management.factory_paddy.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.renew.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddyChange')->name('management.factory_paddy.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddyReplacement')->name('management.factory_paddy.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.replacement.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddyCancel')->name('management.factory_paddy.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.cancel.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.cancel.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.cancel.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\FactoryPaddyController@showFactoryPaddyChangeRenew')->name('management.factory_paddy.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\FactoryPaddyController@editFactoryPaddy')->name('management.factory_paddy.renew_dan_change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\FactoryPaddyController@downloadFactoryPaddyLicense')->name('management.factory_paddy.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\FactoryPaddyController@showSSM')->name('management.factory_paddy.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\FactoryPaddyController@showLesenPerniagaan')->name('management.factory_paddy.renew_dan_change.lesen-perniagaan');
            });
          });

          Route::prefix('beli-padi')->group(function () {
            Route::prefix('baharu')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddy')->name('management.buy_paddy.new.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.new.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.new.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.new.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.new.lesen-perniagaan');
            });

            Route::prefix('pembaharuan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddyRenew')->name('management.buy_paddy.renew.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.renew.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.renew.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.renew.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.renew.lesen-perniagaan');
            });

            Route::prefix('perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddyChange')->name('management.buy_paddy.change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.change.lesen-perniagaan');
            });

            Route::prefix('penggantian')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddyReplacement')->name('management.buy_paddy.replacement.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.replacement.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.replacement.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.replacement.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.replacement.lesen-perniagaan');
            });

            Route::prefix('pembatalan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddyCancel')->name('management.buy_paddy.cancel.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.cancel.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.cancel.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.cancel.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.cancel.lesen-perniagaan');
            });

            Route::prefix('pembaharuan dan perubahan')->group(function () {
              Route::get('/papar/{id}', 'LicenseManagement\BuyPaddyController@showBuyPaddyChangeRenew')->name('management.buy_paddy.renew_dan_change.show');
              Route::get('/kemaskini/{id}', 'LicenseManagement\BuyPaddyController@editBuyPaddy')->name('management.buy_paddy.renew_dan_change.edit');
              Route::get('/janaan-lesen/{id}', 'LicenseManagement\BuyPaddyController@downloadBuyPaddyLicense')->name('management.buy_paddy.renew_dan_change.download');
              Route::get('/papar/ssm-fail/{id}', 'LicenseManagement\BuyPaddyController@showSSM')->name('management.buy_paddy.renew_dan_change.ssm');
              Route::get('/papar/lesen-perniagaan-fail/{id}', 'LicenseManagement\BuyPaddyController@showLesenPerniagaan')->name('management.buy_paddy.renew_dan_change.lesen-perniagaan');
            });
          });

      });
    });
});
