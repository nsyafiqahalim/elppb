<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Portal\AnnouncementDataObject;

class AnnouncementController extends Controller
{
    public function index()
    {
        return view('portal.announcement.index');
    }

    public function add()
    {
        return view('portal.announcement.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        $announcementDAO = AnnouncementDataObject::findAnnouncementById($decryptedId);

        return view('portal.announcement.edit', compact('announcementDAO'));
    }
}
