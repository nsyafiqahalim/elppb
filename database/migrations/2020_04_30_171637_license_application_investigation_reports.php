<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationInvestigationReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('license_application_reports', function (Blueprint $table) {
             $table->bigIncrements('id');
             $table->string('report_type')->nullable();
             $table->string('report_category')->nullable();
             $table->text('report');
             $table->date('report_date')->nullable();
             $table->unsignedBigInteger('user_id')->index();
             $table->unsignedBigInteger('license_application_id')->index();
             $table->foreign('user_id')->references('id')->on('users');
             $table->foreign('license_application_id')->references('id')->on('license_applications');
             $table->timestamps();
         });

     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         //
     }
 }
