<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseGenerationType;

class licenseApplicationLicenseGenerationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            MULA BORONG
        */
        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu sahaja',
            'code'  =>  'NEW_WHOLESALE',
            'domain'    => "NEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu dan Lesen Import Baharu',
            'code'  =>  'NEW_WHOLESALE_NEW_IMPORT',
            'domain'    => "NEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu dan Lesen Eksport Baharu',
            'code'  =>  'NEW_WHOLESALE_NEW_EXPORT',
            'domain'    => "NEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu, Lesen Import Baharu dan Lesen Eksport Baharu',
            'code'  =>  'NEW_WHOLESALE_NEW_IMPORT_NEW_EXPORT',
            'domain'    => "NEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Lesen Borong Baharu sahaja',
        //     'code'  =>  'NEW_WHOLESALE',
        //     'domain'    => "NEW_WHOLESALE",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Lesen Borong Baharu dan Lesen Import Baharu',
        //     'code'  =>  'NEW_WHOLESALE_NEW_IMPORT',
        //     'domain'    => "NEW_IMPORT_NEW_WHOLESALE",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Lesen Borong Baharu sahaja',
        //     'code'  =>  'NEW_WHOLESALE',
        //     'domain'    => "NEW_EXPORT",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Lesen Borong Baharu dan Lesen Import Baharu',
        //     'code'  =>  'NEW_EXPORT_NEW_WHOLESALE',
        //     'domain'    => "EXPORT_WHOLESALE",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

        //Jika Jumpa Lesen Borong Sahaja
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong, Lesen Import Baharu dan Lesen Eksport Baharu',
            'code'  =>  'RENEW_WHOLESALE_NEW_IMPORT_NEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Lesen Import Baharu',
            'code'  =>  'RENEW_WHOLESALE_NEW_IMPORT',
            'domain'    => "RENEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Lesen Eksport Baharu',
            'code'  =>  'RENEW_WHOLESALE_NEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        //Jika Jumpa Lesen Borong dan Lesen Import
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Dan Pembaharuan Lesen Import',
            'code'  =>  'RENEW_WHOLESALE_RENEW_IMPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong, Pembaharuan Lesen Import dan Permohonan Lesen Eksport Baharu',
            'code'  =>  'RENEW_WHOLESALE_RENEW_IMPORT_NEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        
        //Jika Jumpa Lesen Borong dan Lesen Eksport

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_WHOLESALE_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Dan Pembaharuan Lesen Eksport',
            'code'  =>  'RENEW_WHOLESALE_RENEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);


        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong, Pembaharuan Lesen Eksport dan Permohonan Lesen Import Baharu',
            'code'  =>  'RENEW_WHOLESALE_RENEW_EXPORT_NEW_IMPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        //Jika Jumpa Lesen Borong  Lesen Eksport, dan Lesen Eksport
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Import',
            'code'  =>  'RENEW_WHOLESALE_RENEW_IMPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Ekpsort',
            'code'  =>  'RENEW_WHOLESALE_RENEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong, Pembaharuan Lesen Import dan Pembaharuan Lesen Eksport',
            'code'  =>  'RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT',
            'domain'    => "RENEW_WHOLESALE_RENEW_IMPORT_RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        //Jika Jumpa Lesen Borong  Lesen Eksport, dan Lesen Eksport
        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Borong Sahaja',
            'code'  =>  'CANCEL_WHOLESALE',
            'domain'    => "CANCEL_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Import dan Lesen Export',
            'code'  =>  'CANCEL_WHOLESALE_IMPORT_EXPORT',
            'domain'    => "CANCEL_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Borong, Lesen Import, dan Lesen Export',
            'code'  =>  'CANCEL_WHOLESALE_ALL',
            'domain'    => "CANCEL_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Import',
            'code'  =>  'CANCEL_WHOLESALE_IMPORT',
            'domain'    => "CANCEL_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Eksport',
            'code'  =>  'CANCEL_WHOLESALE_EXPORT',
            'domain'    => "CANCEL_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Borong dan Lesen Import',
            'code'  =>  'CANCEL_WHOLESALE_IMPORT',
            'domain'    => "CANCEL_WHOLESALE_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Import Sahaja',
            'code'  =>  'CANCEL_IMPORT',
            'domain'    => "CANCEL_WHOLESALE_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);


        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Borong dan Lesen Eksport',
            'code'  =>  'CANCEL_WHOLESALE_EXPORT',
            'domain'    => "CANCEL_WHOLESALE_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        LicenseGenerationType::create([
            'name'  =>  'Pembatalan Lesen Eksport Sahaja',
            'code'  =>  'CANCEL_EXPORT',
            'domain'    => "CANCEL_WHOLESALE_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        //Jika Jumpa Lesen Borong  Lesen Eksport, dan Lesen Eksport PENGGANTIAN
        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Borong Sahaja',
            'code'  =>  'REPLACEMENT_WHOLESALE',
            'domain'    => "REPLACEMENT_WHOLESALE_ONLY",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Borong, Lesen Import, dan Lesen Export',
            'code'  =>  'REPLACEMENT_WHOLESALE_IMPORT_EXPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Borong, Lesen Import, dan Lesen Export',
            'code'  =>  'REPLACEMENT_WHOLESALE_ALL',
            'domain'    => "REPLACEMENT_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);


        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Import dan Lesen Eksport',
            'code'  =>  'REPLACEMENT_WHOLESALE_IMPORT_EXPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);


        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Import',
            'code'  =>  'REPLACEMENT_WHOLESALE_IMPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Eksport',
            'code'  =>  'REPLACEMENT_WHOLESALE_EXPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_ALL",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Import Sahaja',
            'code'  =>  'REPLACEMENT_IMPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Borong dan Lesen Eksport',
            'code'  =>  'REPLACEMENT_WHOLESALE_EXPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        LicenseGenerationType::create([
            'name'  =>  'Penggantian Lesen Eksport Sahaja',
            'code'  =>  'REPLACEMENT_EXPORT',
            'domain'    => "REPLACEMENT_WHOLESALE_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        /*
           TAMAT BORONG
        */
        
        /*
            MULA IMPORT
        */
        // kalau ada existing lesen borong
        LicenseGenerationType::create([
            'name'  =>  'Lesen Import Baharu sahaja',
            'code'  =>  'NEW_IMPORT',
            'domain'    => "NEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Lesen Import Baharu dan Lesen Eksport Baharu',
            'code'  =>  'NEW_IMPORT_EXPORT',
            'domain'    => "NEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        // kalau ada takde existing lesen borong
        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu dan Lesen Import Baharu',
            'code'  =>  'NEW_IMPORT_WHOLESALE',
            'domain'    => "NEW_IMPORT_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
           'name'  =>  'Lesen Borong Baharu, Lesen Import, dan Lesen Eksport Baharu',
            'code'  =>  'NEW_IMPORT_WHOLESALE_EXPORT',
            'domain'    => "NEW_IMPORT_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

         // kalau ada Existing Lesen Borong Dan Import
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Import',
            'code'  =>  'RENEW_IMPORT_WHOLESALE',
            'domain'    => "RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>   'Pembaharuan Lesen Borong, Pembaharuan Lesen Import dan Lesen Eksport Baharu',
            'code'  =>  'RENEW_WHOLESALE_IMPORT_EXPORT',
            'domain'    => "RENEW_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Pembaharuan Lesen Borong Sahaja ',
        //     'code'  =>  'RENEW_WHOLESALE_EXPORT',
        //     'domain'    => "RENEW_IMPORT",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

         // kalau ada Existing Lesen Borong, Lesen Import, Dan Lesen Export
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_IMPORT_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Import',
            'code'  =>  'RENEW_IMPORT_WHOLESALE',
            'domain'    => "RENEW_IMPORT_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>   'Pembaharuan Lesen Borong, Pembaharuan Lesen Import dan Pembaharuan Lesen Eksport',
            'code'  =>  'RENEW_WHOLESALE_IMPORT_EXPORT',
            'domain'    => "RENEW_IMPORT_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        /*
           TAMAT IMPORT
        */

        /*
            MULA EKSPORT
        */
        // kalau ada existing lesen borong
        LicenseGenerationType::create([
            'name'  =>  'Lesen Eksport Baharu sahaja',
            'code'  =>  'NEW_EXPORT',
            'domain'    => "NEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Lesen Eksport Baharu dan Lesen Import Baharu',
            'code'  =>  'NEW_EXPORT_IMPORT',
            'domain'    => "NEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        // kalau ada takde existing lesen borong
        LicenseGenerationType::create([
            'name'  =>  'Lesen Borong Baharu dan Lesen Eksport Baharu',
            'code'  =>  'NEW_EXPORT_WHOLESALE',
            'domain'    => "NEW_EXPORT_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
           'name'  =>  'Lesen Borong Baharu, Lesen Eksport, dan Lesen Import Baharu',
            'code'  =>  'NEW_EXPORT_WHOLESALE_IMPORT',
            'domain'    => "NEW_EXPORT_WHOLESALE",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

         // kalau ada Existing Lesen Borong Dan Import
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Eksport',
            'code'  =>  'RENEW_EXPORT_WHOLESALE',
            'domain'    => "RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>   'Pembaharuan Lesen Borong, Pembaharuan Lesen Eksport dan Lesen Import Baharu',
            'code'  =>  'RENEW_WHOLESALE_EXPORT_IMPORT',
            'domain'    => "RENEW_EXPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        // LicenseGenerationType::create([
        //     'name'  =>  'Pembaharuan Lesen Borong Sahaja ',
        //     'code'  =>  'RENEW_WHOLESALE_EXPORT',
        //     'domain'    => "RENEW_EXPORT",
        //     'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        // ]);

         // kalau ada Existing Lesen Borong, Lesen Import, Dan Lesen Export
        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong Sahaja',
            'code'  =>  'RENEW_WHOLESALE',
            'domain'    => "RENEW_EXPORT_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>  'Pembaharuan Lesen Borong dan Pembaharuan Lesen Eksport',
            'code'  =>  'RENEW_EXPORT_WHOLESALE',
            'domain'    => "RENEW_EXPORT_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);

        LicenseGenerationType::create([
            'name'  =>   'Pembaharuan Lesen Borong, Pembaharuan Lesen Eksport dan Pembaharuan Lesen Import',
            'code'  =>  'RENEW_WHOLESALE_EXPORT_IMPORT',
            'domain'    => "RENEW_EXPORT_IMPORT",
            'description'   =>  'Data ini merujuk kepada data jenis Borong Sahaja'
        ]);
        
        /*
           TAMAT EKSPORT
        */
    }
}
