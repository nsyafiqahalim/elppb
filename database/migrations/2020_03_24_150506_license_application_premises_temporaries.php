<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationPremisesTemporaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_premise_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->unsignedBigInteger('business_type_id')->index('lacpt_bt_idx');
            $table->unsignedBigInteger('building_type_id')->index('lacpt_bt2_idx');
            $table->unsignedBigInteger('state_id')->index('lacpt_st_idx');
            $table->unsignedBigInteger('dun_id')->index('lacpt_dn_idx');
            $table->unsignedBigInteger('district_id')->index('lacpt_dt_idx');
            $table->unsignedBigInteger('parliament_id')->index('lacpt_p_idx');
            $table->unsignedBigInteger('license_application_id')->index('lacpt_la_idx');
            $table->timestamps();

            $table->foreign('state_id','fk_lacpt_bt')->references('id')->on('states');
            $table->foreign('license_application_id','fk_lacpt_la')->references('id')->on('license_applications');
            $table->foreign('dun_id','fk_lacpt_dn')->references('id')->on('duns');
            $table->foreign('district_id','fk_lacpt_dt')->references('id')->on('districts');
            $table->foreign('parliament_id','fk_lacpt_p')->references('id')->on('parliaments');
            $table->foreign('business_type_id','fk_bt_dn')->references('id')->on('business_types');
            $table->foreign('building_type_id', 'fk_lacpt_bt2')->references('id')->on('building_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
