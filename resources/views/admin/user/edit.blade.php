@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-users"></i> Pengguna dan Peranan</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route ('admin.index') }}">Pentadbiran Sistem</a></li>
            <li class="breadcrumb-item active">Pengguna</li>
            <li class="breadcrumb-item active">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Pengguna</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.user.update',[encrypt($userDAO->id)]) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label">Nama Pengguna <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-user"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Nama Pengguna" value="{{ $userDAO->name  }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="login_id_label">ID Pengguna <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-lock"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="login_id" name="login_id" placeholder="ID Pengguna" value="{{ $userDAO->login_id  }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="email_label">E-mel <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-email"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" id="email" name="email" placeholder="contoh@contoh.com" value="{{ $userDAO->email  }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="status_label">Status <span style="color: red;">*</span></label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="ti-info"></i>
                                                </span>
                                            </div>
                                            <select class="form-control" id="roles" name="status" id='status'>
                                                <option value=''>Sila Pilih</option>
                                                <option value='ACTIVE' @if($userDAO['status'] == 'ACTIVE') selected @endif>Aktif</option>
                                                    <option value='DEACTIVATED' @if($userDAO['status'] == 'DEACTIVATED') selected @endif>Tidak Aktif</option>
                                                        <option value='LOCKED' @if($userDAO['status'] == 'LOCKED') selected @endif>Kunci Akaun</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label for="role_label"> Peranan</label>
                                        <select class="select-role form-control" id="roles" name="roles[]" data-placeholder="Pilih Peranan" multiple>
                                            @foreach($roles as $role)
                                            <option {{ isset($userDAO) && $userDAO->hasRole($role['name']) ? 'selected' : '' }} value="{{ $role['name'] }}">{{ $role['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route('admin.user.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('css')
<link href="{{ asset('dualListbox/dist/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
@endpush

@push('js')
<script src="{{ asset('dualListbox/dist/jquery.bootstrap-duallistbox.js') }}"></script>
<script>
    $('.select-role').bootstrapDualListbox({
        nonSelectedListLabel: 'Belum Dipilih',
        selectedListLabel: 'Sudah Dipilih',
        preserveSelectionOnMove: 'Pindahkan',
        infoText: 'Tunjuk semua {0}',
        infoTextFiltered: '<span class="label label-warning">Ditapis</span> {0} dari {1}',
        filterPlaceHolder: 'Carian',
        infoTextEmpty: 'Senarai yang kosong',
        selectorMinimalHeight: 300,
        moveOnSelect: true,
    });
</script>
@endpush
