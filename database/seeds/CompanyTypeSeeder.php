<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\CompanyType;

class CompanyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Perkongsian',
            'Persendirian',
            'Sendirian Berhad',
            'Berhad',
            'Kerjasama',
            'Koperasi',
            'Pertubuhan Peladang'
        ];

        $paidupCaptial = [
            '0',
            '0',
            '1',
            '1',
            '0',
            '0',
            '0'
        ];

        $expiryDate = [
            '1',
            '1',
            '0',
            '0',
            '0',
            '0',
            '0'
        ];



        foreach ($data as $index => $item) {
            CompanyType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'has_paidup_capital'    => $paidupCaptial[$index],
                'has_expiry_date'    => $expiryDate[$index],
                'is_active' => 1
            ]);
        }
    }
}
