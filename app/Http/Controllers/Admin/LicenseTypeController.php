<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\LicenseTypeDataObject;

class LicenseTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.license-type.index');
    }

    public function add()
    {
        return view('admin.license-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $licenseType = LicenseTypeDataObject::findLicenseTypeById($decryptedId);

        return view('admin.license-type.edit', compact('licenseType'));
    }
}
