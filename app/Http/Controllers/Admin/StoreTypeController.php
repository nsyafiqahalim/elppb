<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\StoreTypeDataObject;

class StoreTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.store-type.index');
    }

    public function add()
    {
        return view('admin.store-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $storeType = StoreTypeDataObject::findStoreTypeById($decryptedId);

        return view('admin.store-type.edit', compact('storeType'));
    }
}
