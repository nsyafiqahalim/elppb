<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\BusinessType;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Supermarket',
            'Kedai Runcit',
            'Pasar Mini',
            'Hypermarket',
            'Stesen Minyak',
            'Lain - Lain'
        ];

        foreach ($data as $item) {
            BusinessType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
