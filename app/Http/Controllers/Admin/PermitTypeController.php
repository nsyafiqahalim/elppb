<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\PermitTypeDataObject;

class PermitTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.permit-type.index');
    }

    public function add()
    {
        return view('admin.permit-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $permitType = PermitTypeDataObject::findPermitTypeById($decryptedId);

        return view('admin.permit-type.edit', compact('permitType'));
    }
}
