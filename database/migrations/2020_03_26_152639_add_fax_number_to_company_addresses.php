<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFaxNumberToCompanyAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('license_application_company_addresses', function (Blueprint $table) {
            $table->string('fax_number')->nullable();
        });

        Schema::table('approval_letter_application_company_addresses', function (Blueprint $table) {
            $table->string('fax_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('license_application_company_addresses', function (Blueprint $table) {
            //
        });
    }
}
