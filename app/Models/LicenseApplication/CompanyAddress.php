<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\Company;
use App\Models\Admin\State;

class CompanyAddress extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_company_addresses';

    private const ACTIVE = 1;

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }
}
