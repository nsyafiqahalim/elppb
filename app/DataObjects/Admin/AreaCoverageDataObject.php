<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\AreaCoverage;

class AreaCoverageDataObject
{
    public static function addAreaCoverage($param)
    {
        $areaCoverage = null;

        DB::transaction(function () use ($param, &$areaCoverage) {
            $areaCoverage = AreaCoverage::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $areaCoverage;
    }

    public static function updateAreaCoverage($param, $id)
    {
        $areaCoverage = null;

        DB::transaction(function () use ($param, $id,  &$areaCoverage) {
            $areaCoverage = AreaCoverage::find($id);

            $areaCoverage->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $areaCoverage;
    }

    public static function findAllAreaCoverageUsingDatatableFormat()
    {
        return datatables()->of($areaCoverage)
        ->addColumn('action', function ($areaCoverage) {
            return view('admin.area-coverage.partials.datatable-button', compact('areaCoverage'))->render();
        })
        ->addColumn('status', function ($areaCoverage) {
            $status= ($areaCoverage->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.area-coverage.partials.status', compact('status'))->render();
        })->make(true);
    }

    public static function findAreaCoverageById($id)
    {
        return AreaCoverage::find($id);
    }

    public static function findAreaCoverageByDistrictId($districtId)
    {
        return AreaCoverage::where('district_id',$districtId)->first();
    }
}
