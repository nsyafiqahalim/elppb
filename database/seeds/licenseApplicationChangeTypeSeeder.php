<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LicenseApplicationChangeType;

class licenseApplicationChangeTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LicenseApplicationChangeType::create([
            'name'  =>  'maklumat Syarikat',
            'code'  =>  'COMPANY_INFORMATION',
            'description'   =>  'Data ini merujuk kepada data jenis Nama Syarikat'
        ]);

        LicenseApplicationChangeType::create([
            'name'  =>  'Had Muatan',
            'code'  =>  'LICENSE_APPLICATION_INFORMATION',
            'description'   =>  'Data ini merujuk kepada data jenis Had Muatan'
        ]);

        LicenseApplicationChangeType::create([
            'name'  =>  'Alamat Premis',
            'code'  =>  'PREMISE_INFORMATION',
            'description'   =>  'Data ini merujuk kepada data alamat premis'
        ]);

        LicenseApplicationChangeType::create([
            'name'  =>  'Alamat Stor',
            'code'  =>  'STORE_INFORMATION',
            'description'   =>  'Data ini merujuk kepada data alamat stor'
        ]);

        LicenseApplicationChangeType::create([
            'name'  =>  'Perubahan Dan Pembaharuan Bersekali',
            'code'  =>  'CHANGE_RENEW',
            'description'   =>  'Data ini merujuk kepada data perubahan dan pembaharuan'
        ]);


    }
}
