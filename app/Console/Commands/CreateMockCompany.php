<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

class CreateMockCompany extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mock:company';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('type', 'APPLICANT')->get();
        $time = 2;
        $userSize = $user->count() * $time;
        $this->info("Creating {$userSize}  mock companies...");

        foreach ($user as $item) {
            factory(\App\Models\Company::class, $time)->create([
                'user_id'   =>  $item->id
            ])
            ->each(function ($company) {
                factory(\App\Models\CompanyAddress::class, 1)->create([
                    'company_id'   =>  $company->id
                ]);

                factory(\App\Models\CompanyPartner::class, 4)->create([
                    'company_id'   =>  $company->id
                ]);

                factory(\App\Models\CompanyPremise::class, 4)->create([
                    'company_id'   =>  $company->id
                ]);

                factory(\App\Models\CompanyStore::class, 4)->create([
                    'company_id'   =>  $company->id
                ]);
            });
        }
        

        $this->info("Successfully created {$userSize} mock companies!");
    }
}
