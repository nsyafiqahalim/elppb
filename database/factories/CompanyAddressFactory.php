<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\CompanyAddress;
use App\Models\Admin\CompanyType;
use App\Models\Admin\District;
use App\Models\Admin\State;

$districts = District::all();
$states = State::all();

$factory->define(CompanyAddress::class, function (Faker $faker) use ($districts, $states) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));


    $dist = $districts->random();
    $stat = $states->random();

    return [
            'address_1'                 => $faker->buildingPrefix . ' ' . $faker->buildingNumber . ' ' . $faker->streetName,
            'address_2'                 => $faker->township,
            'address_3'                 => $faker->townState,
            'postcode'                  =>  $faker->numberBetween(10000, 99999),
            'state_id'                  =>  $stat->id,
            'phone_number'              =>  $faker->phoneNumber,
            'email'                     =>  $faker->unique()->safeEmail,
    ];
});
