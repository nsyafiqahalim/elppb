<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseApplicationCategory extends Model
{
    public $guarded = ['id'];
}
