@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Album</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Pengurusan Portal</li>
            <li class="breadcrumb-item"><a href="{{ route('portal.gallery.album.index') }}">Galeri</a></li>
            <li class="breadcrumb-item">Album</li>
        </ol>
    </div>
</div>

<div class="container">
    <div class="row page-titles">
        <div class="col-md-6 col-8">
            <h4 class="card-title">Nama Album:&nbsp;{{ $album->name }}</h4>
            <h6 class="card-subtitle mb-3 text-muted">Keterangan Album:&nbsp;{{ $album->description}}</h6>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <a href="{{ route('photo-create', $album->id) }}" class="btn btn-info float-right">
                <i class="fas fa-upload"></i>&nbsp;Muat Naik Gambar
            </a>
        </div>
    </div>
</div>

@if (count($album->photos) > 0)
<div class="container">
    <div class="row">
        @foreach ($album->photos as $photo)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <img src="/storage/albums/{{ $album->id }}/{{ $photo->photo }}" alt="{{ $photo->photo}}" height="200px">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="{{ route('photo-show', $photo->id) }}" class="btn btn-sm btn-outline-primary" style="margin-right:15px;"><i class="fas fa-eye"></i></a>
                        </div>
                        <small class="text-muted">{{ $photo->size }}</small>
                    </div>
                </div>
            </div>
        </div> <!-- End col-md-4 -->
        @endforeach
    </div> <!-- End Row -->
    @else
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center;">Tiada gambar</h3>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
