<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class LicenseGenerationType extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_license_generation_types';
}
