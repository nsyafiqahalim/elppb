<?php

namespace App\Models\ReferenceData;

use Illuminate\Database\Eloquent\Model;

use App\Models\ReferenceData\CompanyAddress;
use App\Models\ReferenceData\CompanyPremise;
use App\Models\ReferenceData\CompanyStore;
use App\Models\ReferenceData\CompanyStockStatement;
use App\Models\Admin\CompanyType;

class Company extends Model
{
    public $guarded = ['id'];
    
    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function address()
    {
        return $this->hasOne(CompanyAddress::class, 'company_id');
    }

    public function premises()
    {
        return $this->hasMany(CompanyPremise::class, 'company_id');
    }

    public function stores()
    {
        return $this->hasMany(CompanyStore::class, 'company_id');
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }

    public function stockStatements()
    {
        return $this->hasMany(CompanyStockStatement::class, 'company_id')->with(['stockCategory','stockType','totalGrade','riceGrade','spinType']);
    }
}
