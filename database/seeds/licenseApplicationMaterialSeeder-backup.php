<?php

use Illuminate\Database\Seeder;
use App\Models\LicenseApplication\Material;

class licenseApplicationMaterialSeederBackup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //RUNCIT BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran  SSM / Salinan lesen perniagaan (Sabah/Sarawak) / I.R.D No 7',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu',
            'code'      =>  'minute_meeting',
            'domain'    =>  'RETAIL_NEW',
            'order'     =>  2
        ]);

        Material::create([
            
            'label'     =>  'Surat Kelulusan / Pengesahan Pemilikan Entiti Asing daripada KPDNHEP',
            'code'      =>  'alien_entity',
            'domain'    =>  'RETAIL_NEW',
            'order'     =>  3
        ]);

        //BORORNG BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM - Rakan Kongsi (Jika berkaitan)',
            'code'      =>  'ssm_partner',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi dan Pertubuhan',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'registration_certificate_copy',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Borong Baharu',
            'code'      =>  'minute_meeting',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'WHOLESALE_NEW',
            'order'     =>  7
        ]);

        //IMPORT BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM - Rakan Kongsi (Jika berkaitan)',
            'code'      =>  'ssm_partner',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi dan Pertubuhan',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'registration_certificate_copy',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Borong Baharu',
            'code'      =>  'minute_meeting',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'IMPORT_NEW',
            'order'     =>  7
        ]);

        //EXPORT BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM - Rakan Kongsi (Jika berkaitan)',
            'code'      =>  'ssm_partner',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi dan Pertubuhan',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran',
            'code'      =>  'registration_certificate_copy',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Borong Baharu',
            'code'      =>  'minute_meeting',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'EXPORT_NEW',
            'order'     =>  7
        ]);

        //BELI PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Beli Padi Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran menggunakan tapak',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Surat Kelulusan Khas Jabatan Pertanian/ Surat Lantikan Pengeluar Benih Padi Sah (bagi maksud dibuat biji benih) daripada industri Padi Beras',
            'code'      =>  'special_approval_letter',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  5
        ]);

        //KILANG PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Kilang Padi Komersial Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli / sewaan stor dan premis/<br/> salinan pemilikan kilang/ salinan kebenaran menggunakan tapak kilang',
            'code'      =>  'sale_agreement',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan Jabatan Alam Sekitar',
            'code'      =>  'nature_department_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  5
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan ubah syarat tapak kilang kepada industri / <br/> kilang padi dari Pejabat Tanah atau salinan <br/> geran atau salinan menggunakan tapak',
            'code'      =>  'change_land_status_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan mendirikan kilang padi daripada pihak berkuasa tempatan',
            'code'      =>  'pbt_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  7
        ]);

        Material::create([
            'label'     =>  'Salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia',
            'code'      =>  'fire_department_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  8
        ]);

        Material::create([
            'label'     =>  'Surat kebenaran khas membeli padi bagi tujuan ujian mengisar (Test Milling)',
            'code'      =>  'test_milling_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  8
        ]);


        //RUNCIT PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'RETAIL_RENEW',
            'order'     =>  2
        ]);


        //BORORNG PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'WHOLESALE_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Borong Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_RENEW',
            'order'     =>  3
        ]);

        //IMPORT PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'IMPORT_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Import Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_RENEW',
            'order'     =>  3
        ]);

        //EKPSORT PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'EXPORT_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Beli Padi Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_RENEW',
            'order'     =>  3
        ]);

        //BELI PADI PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan penyata belian/jualan padi 3 musim terkini',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_RENEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Beli Padi Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_RENEW',
            'order'     =>  4
        ]);

        //KILANG PADI PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran<br/> menggunakan tapak stor',
            'code'      =>  'sale_agreement',
            'domain'    =>  'FACTORY_PADDY_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Kilang Padi Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_RENEW',
            'order'     =>  3
        ]);

        //RUNCIT PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'RETAIL_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        //BORONG PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Borong Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_CHANGE_COMPANY',
            'order'     =>  3
        ]);

        //IMPORT PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'IMPORT_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Import Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_CHANGE_COMPANY',
            'order'     =>  3
        ]);
        
        //EKSPORT PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'EXPORT_CHANGE_COMPANY',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        //BUY PADDY PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        //FACTORY PADDY PERUBAHAN NAMA SYARIKAT
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_CHANGE_COMPANY',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_COMPANY',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_CHANGE_COMPANY',
            'order'     =>  2
        ]);


        //RUNCIT PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'RETAIL_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        //BORONG PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        //IMPORT PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'IMPORT_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_CHANGE_PREMISE',
            'order'     =>  2
        ]);
        
        //EKSPORT PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'EXPORT_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        //BUY PADDY PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        //FACTORY PADDY PERUBAHAN ALAMAT PREMIS
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_CHANGE_PREMISE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_CHANGE_PREMISE',
            'order'     =>  2
        ]);

        //BORONG PERUBAHAN ALAMAT STOR
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_CHANGE_STORE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_STORE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_CHANGE_STORE',
            'order'     =>  2
        ]);

        //IMPORT PERUBAHAN ALAMAT STOR
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_CHANGE_STORE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'IMPORT_CHANGE_STORE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_CHANGE_STORE',
            'order'     =>  2
        ]);
        
        //EKSPORT PERUBAHAN ALAMAT STOR
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_CHANGE_STORE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'EXPORT_CHANGE_STORE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_CHANGE_STORE',
            'order'     =>  2
        ]);

        //BUY PADDY PERUBAHAN ALAMAT STOR
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_CHANGE_STORE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_STORE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_CHANGE_STORE',
            'order'     =>  2
        ]);

        //FACTORY PADDY PERUBAHAN ALAMAT STOR
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_CHANGE_STORE',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_STORE',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_CHANGE_STORE',
            'order'     =>  2
        ]);

        //RUNCIT PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'RETAIL_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'RETAIL_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        //BORONG PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_CHANGE_LICENSE_APPLICATION',
            'order'     =>  3
        ]);

        //IMPORT PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'IMPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  3
        ]);

        //EKSPORT PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'EXPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_CHANGE_LICENSE_APPLICATION',
            'order'     =>  3
        ]);

        //BELI PADI PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  3
        ]);

        //FACTORY PADI PERUBAHAN MUATAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_PADDY_CHANGE_LICENSE_APPLICATION',
            'order'     =>  3
        ]);

        //RUNCIT PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'RETAIL_CHANGE_RENEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'RETAIL_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //BORONG PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_RENEW',
            'order'     =>  2
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'WHOLESALE_CHANGE_RENEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'WHOLESALE_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //IMPORT PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'IMPORT_CHANGE_RENEW',
            'order'     =>  2
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'IMPORT_CHANGE_RENEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'IMPORT_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //EKSPORT PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'EXPORT_CHANGE_RENEW',
            'order'     =>  2
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'EXPORT_CHANGE_RENEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'EXPORT_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //BELI PADI PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_RENEW',
            'order'     =>  2
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_CHANGE_RENEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //KILANG PADI PERUBAHAN PEMBAHARUAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_CHANGE_RENEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli/sewa premis',
            'code'      =>  'premise_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_RENEW',
            'order'     =>  2
        ]);

         Material::create([
            'label'     =>  'Salinan penyata stok belian/jualan',
            'code'      =>  'sale_agreement',
            'domain'    =>  'FACTORY_PADDY_CHANGE_RENEW',
            'order'     =>  6
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_CHANGE_RENEW',
            'order'     =>  2
        ]);

        //RUNCIT PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'RETAIL_REPLACEMENT',
            'order'     =>  1
        ]);

        //BORONG PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'WHOLESALE_REPLACEMENT',
            'order'     =>  1
        ]);

        //IMPORT PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'IMPORT_REPLACEMENT',
            'order'     =>  1
        ]);

        //EXPORT PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'EXPORT_REPLACEMENT',
            'order'     =>  1
        ]);

        //KILANG PADI PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_REPLACEMENT',
            'order'     =>  1
        ]);

         //BELI PADI PENGGANTIAN
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)/ Salinan Sijil Pendaftaran Koperasi atau Pertubuhan',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_REPLACEMENT',
            'order'     =>  1
        ]);

        //RUNCIT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'RETAIL_CANCELLATION',
            'order'     =>  1
        ]);

        //BORONG PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'WHOLESALE_CANCELLATION',
            'order'     =>  1
        ]);

        //IMPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'IMPORT_CANCELLATION',
            'order'     =>  1
        ]);

        //EXPORT PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'EXPORT_CANCELLATION',
            'order'     =>  1
        ]);

        //KILANG PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'FACTORY_PADDY_CANCELLATION',
            'order'     =>  1
        ]);

         //BELI PADI PEMBATALAN
        Material::create([
            'label'     =>  'Surat persetujuan pembatalan dari rakan kongsi syarikat',
            'code'      =>  'cancellation',
            'domain'    =>  'BUY_PADDY_CANCELLATION',
            'order'     =>  1
        ]);
    }
}
