<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\RaceType;

class RaceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Bumiputera',
            'Bukan Bumiputera',
        ];

        foreach ($data as $item) {
            RaceType::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
