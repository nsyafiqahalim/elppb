<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionDataObject
{
    public static function findAllPermissions()
    {
        return Permission::all()->toArray();
    }
}
