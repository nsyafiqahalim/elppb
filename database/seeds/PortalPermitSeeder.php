<?php

use Illuminate\Database\Seeder;

use App\Models\Portal\Permit;

class PortalPermitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      License::create([
          'title'   =>  'Peraturan Permit',
          'created_by'    =>  '1',
          'is_published'   =>1,
          'updated_by'    =>  '1',
          'description'   =>  '
            <p>Permit adalah suatu peraturan yang telah ditetapkan dibawah Akta 522. Ianya terbahagi kepada tiga (3) bahagian iaitu:</p>
              <p>a) Permit Pemindahan Beras dan Hasil Sampingan Antara Negeri</p>
              <p style="padding-left: 30px;">1. Individu atau orang perseorangan</p>
              <p style="padding-left: 30px;">2. Syarikat yang mempunyai lesen samada runcit, borong dan kilang padi komersial</p>
              <p>b) Permit Pemindahan Padi Antara Negeri</p>
              <p style="padding-left: 30px;">1. Individu atau orang perseorangan iaitu pengusaha sawah padi</p>
              <p style="padding-left: 30px;">2. Syarikat iaitu yang mempunyai lesen membeli padi</p>
              <p>c) Permit Pemindahan Antara Wilayah (Sabah dan Sarawak)</p>
              <p style="padding-left: 30px;">1. Syarikat yang mempunyai lesen samada runcit, borong dan kilang padi komersial</p>
              <p>&nbsp;</p>
              <p>Peraturan ini dinamakan Peraturan-Peraturan Kawalan Padi dan Beras (Sekatan Pemindahan Antara Negeri) 1997.</p>
              <p>Dimana tiada seorang pun boleh memindahkan padi atau beras atau hasil sampingan padi atau beras dari suatu negeri ke suatu negeri yang lain dalam Malaysia Barat melainkan kebenaran secara bertulis terlebih dahulu daripada Ketua Pengarah atau mana-mana Pegawai yang diberi kuasa yang mana tidak melebihi 10 kilogram bagi setiap orang yang membawa dan bukan untuk jualan.</p>'
      ]);
    }
}
