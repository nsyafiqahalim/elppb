<a href="{{ route('portal.permit.edit',[$encryptedId]) }}">
    <button class="btn btn-sm btn-info">
        <i class="fa fa-edit"></i> Kemaskini
    </button>
</a>


<a class='deletion' href="{{ route('api.portal.permit.delete',[$encryptedId]) }}">
    <button class="btn btn-sm btn-danger">
        <i class="fa fa-trash"></i> Padam
    </button>
</a>
