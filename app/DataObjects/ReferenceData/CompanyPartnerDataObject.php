<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\CompanyPartner;

class CompanyPartnerDataObject
{
    public static function addNewCompanyPartner($param)
    {
        return CompanyPartner::create([
            'name'                  =>  $param['name'],
            'race_type_id'       =>  $param['race_type_id'],
            'race_id'       =>  $param['race_id'],
            'is_citizen'       =>  $param['is_citizen'],
            'total_share'   =>  $param['total_share'],
            'share_percentage'   =>  $param['share_percentage'],
            'phone_number'           =>  $param['phone_number'],
            'email'           =>  $param['email'],
            'nric'        =>  $param['ic'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'               =>  $param['company_id'],
        ]);
    }

    public static function findAllActiveCompanyPartners()
    {
        $Companys = CompanyPartner::get();

        return $Companys;
    }

    public static function findCompanyParnerByCompanyId($companyId)
    {
        $Companys = CompanyPartner::where('company_id', $companyId)->get();

        return $Companys;
    }

    public static function findAllCompanyPartnersUsingDatatableFormat($param)
    {
        $companies =  CompanyPartner::distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('ownership_type', function ($company) {
            return $company->ownershipType->name ?? '-';
        })
        ->addColumn('race', function ($company) {
            return $company->race->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
