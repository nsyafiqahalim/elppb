<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication;

class LicensePayment extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_payments';

    private const ACTIVE = 1;

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }
}
