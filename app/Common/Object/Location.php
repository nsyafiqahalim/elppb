<?php

namespace App\Common\Object;


class Location {
    private $StateCode;
    private $id1;
    private $id2;
    private $SubDistrict1;
    private $SubDistrict2;
    private $distance;
    private $duration;

    public function setId1($id1){
    	$this->id1 = $id1;
    }
    public function setId2($id2){
    	$this->id2 = $id2;	
    }
    public function setSubDistrict1($SubDistrict1){
    	$this->SubDistrict1 = $SubDistrict1;
    }
    public function setSubDistrict2($SubDistrict2){
    	$this->SubDistrict2 = $SubDistrict2;
    }
    public function setStateCode($StateCode){
    	$this->StateCode = $StateCode;
    }
    public function distance($distance){
    	$this->distance = $distance;
    }
    public function duration($duration){
    	$this->duration = $duration;
    }

    public function getId1(){
    	return $this->id1;
    }
    public function getId2(){
    	return $this->id2;
    }
    public function getStateCode(){
    	return $this->StateCode;
    }
    public function getSubDistrict1(){
    	return $this->SubDistrict1;
    }
    public function getSubDistrict2(){
    	return $this->SubDistrict2;
    }
    public function getDistance(){
    	return $this->distance;
    }
    public function getDuration(){
    	return $this->duration;
    }
}
