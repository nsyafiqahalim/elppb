@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Pengumuman</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Pengurusan Portal</li>
          <li class="breadcrumb-item"><a href="{{ route('portal.gallery.index') }}">Infografik</a></li>
          <li class="breadcrumb-item">Tambah</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="fas fa-plus-circle"></i> Tambah Infografik</b>
            </div>
            <div class="card-body " id="filter">
                <h5>Gambar boleh dimuat naik secara pukal. Gambar akan dimuat naik secara automatik selepas dipilih</h5>
                <div class="card-block">
                    <form class="form-sample multiple-file-submission dropzone" method="post" action="{{ route('api.portal.gallery.store') }}">
                        @csrf
                        <input type='hidden'  name='user_id'  value="{{ encrypt(Auth::user()->id) }}" />
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                         <div class="fallback">
                                            <input name="file" type="file" multiple />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="form-actions float-right">
                    <br/>
                    <a href="{{ route ('portal.gallery.index') }}" class="btn btn-primary m-t-n-xs">Kembali</a>
                </div>
                <div class="form-actions float-left">
                    <br/>
                    <a class="mytooltip" href="javascript:void(0)"> * Perhatian<span class="tooltip-content5"><span class="tooltip-text3"><span class="tooltip-inner2">Format imej adalah dalam bentuk .png atau .jpg.<br> Saiz resolusi bagi imej adalah 630 * 350 pixel.</span></span></span></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link href="{{ asset('assets/plugins/dropzone-master/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
@endpush
@push('js')
<script src="{{ asset('assets/plugins/dropzone-master/dist/dropzone.js') }}"></script>
@endpush
