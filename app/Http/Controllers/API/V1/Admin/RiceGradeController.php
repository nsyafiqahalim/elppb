<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RiceGradeDataObject;

class RiceGradeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRiceGrade(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $riceGrade = RiceGradeDataObject::addRiceGrade($param);

        return response()->json([
            'message'   =>  'Gred Beras berjaya ditambah',
            'data'      =>  $riceGrade,
            'success'   =>  true,
            'route'     =>  route('admin.rice-grade.index')
        ], 200);
    }

    public function updateRiceGrade(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $riceGrade = RiceGradeDataObject::updateRiceGrade($param, $decryptedId);

        return response()->json([
            'message'   =>  'Gred Beras berjaya dikemaskini',
            'data'      =>  $riceGrade,
            'success'   =>  true,
            'route'     =>  route('admin.rice-grade.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RiceGradeDataObject::findAllRiceGradeUsingDatatableFormat($param);
    }
}
