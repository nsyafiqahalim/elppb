@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Pentadbiran Sistem</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Data Rujukan</li>
            <li class="breadcrumb-item"><i class="fas fa-leaf"></i> <a href="{{ route ('admin.district.index') }}">Senarai Daerah</a></li>
            <li class="breadcrumb-item active">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Daerah</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.district.update',encrypt($district->id)) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label">Nama <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value='{{ $district->name ?? "" }}'>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="code_label">Kod <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Kod" value='{{ $district->code ?? "" }}'>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="state_id_label">Negeri <span class='required'>*</span></label>
                                        <select class="form-control border-primary" id="state_id" name="state_id">
                                            <option value=''>
                                                Sila Pilih
                                            </option>
                                            @foreach($states as $state)
                                                <option
                                                @if($district->state_id == $state->id)
                                                selected
                                                @endif

                                                value="{{ encrypt($state->id) }}" >{{ $state->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label id="is_active_label">Status <span class='required'>*</span></label>
                                        <div class="input-group">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                              <i class="fas fa-info"></i>
                                            </span>
                                          </div>
                                          <select class="form-control border-primary" id="is_active" name="is_active">
                                              <option value=''>
                                                  Sila Pilih
                                              </option>
                                              <option @if($district->is_active == 1)
                                                  selected
                                                  @endif
                                                  value='1'>
                                                  Aktif
                                              </option>
                                              <option @if($district->is_active == 0)
                                                  selected
                                                  @endif
                                                  value='0'>
                                                  Tidak Aktif
                                              </option>
                                          </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route ('admin.district.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
