<?php

use Illuminate\Database\Seeder;


class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('states')->delete();
        
        \DB::table('states')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => 'Johor',
                'name' => 'Johor',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Johor',
                'created_at' => '2020-03-18 23:03:21',
                'updated_at' => '2020-03-18 23:03:21',
            ),
            1 => 
            array (
                'id' => 2,
                'code' => 'Kedah',
                'name' => 'Kedah',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Kedah',
                'created_at' => '2020-03-18 23:03:21',
                'updated_at' => '2020-03-18 23:03:21',
            ),
            2 => 
            array (
                'id' => 3,
                'code' => 'Kelantan',
                'name' => 'Kelantan',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Kelantan',
                'created_at' => '2020-03-18 23:03:21',
                'updated_at' => '2020-03-18 23:03:21',
            ),
            3 => 
            array (
                'id' => 4,
                'code' => 'Melaka',
                'name' => 'Melaka',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Melaka',
                'created_at' => '2020-03-18 23:03:21',
                'updated_at' => '2020-03-18 23:03:21',
            ),
            4 => 
            array (
                'id' => 5,
                'code' => 'Negeri Sembilan',
                'name' => 'Negeri Sembilan',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Negeri Sembilan',
                'created_at' => '2020-03-18 23:03:21',
                'updated_at' => '2020-03-18 23:03:21',
            ),
            5 => 
            array (
                'id' => 6,
                'code' => 'Pahang',
                'name' => 'Pahang',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Pahang',
                'created_at' => '2020-03-18 23:03:22',
                'updated_at' => '2020-03-18 23:03:22',
            ),
            6 => 
            array (
                'id' => 7,
                'code' => 'Pulau Pinang',
                'name' => 'Pulau Pinang',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Pulau Pinang',
                'created_at' => '2020-03-18 23:03:22',
                'updated_at' => '2020-03-18 23:03:22',
            ),
            7 => 
            array (
                'id' => 8,
                'code' => 'Perak',
                'name' => 'Perak',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Perak',
                'created_at' => '2020-03-18 23:03:22',
                'updated_at' => '2020-03-18 23:03:22',
            ),
            8 => 
            array (
                'id' => 9,
                'code' => 'Perlis',
                'name' => 'Perlis',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Perlis',
                'created_at' => '2020-03-18 23:03:22',
                'updated_at' => '2020-03-18 23:03:22',
            ),
            9 => 
            array (
                'id' => 10,
                'code' => 'Selangor',
                'name' => 'Selangor',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Selangor',
                'created_at' => '2020-03-18 23:03:22',
                'updated_at' => '2020-03-18 23:03:22',
            ),
            10 => 
            array (
                'id' => 11,
                'code' => 'Terengganu',
                'name' => 'Terengganu',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Terengganu',
                'created_at' => '2020-03-18 23:03:23',
                'updated_at' => '2020-03-18 23:03:23',
            ),
            11 => 
            array (
                'id' => 12,
                'code' => 'Sabah',
                'name' => 'Sabah',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Sabah',
                'created_at' => '2020-03-18 23:03:23',
                'updated_at' => '2020-03-18 23:03:23',
            ),
            12 => 
            array (
                'id' => 13,
                'code' => 'Sarawak',
                'name' => 'Sarawak',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri Sarawak',
                'created_at' => '2020-03-18 23:03:23',
                'updated_at' => '2020-03-18 23:03:23',
            ),
            13 => 
            array (
                'id' => 14,
                'code' => 'WP Kuala Lumpur',
                'name' => 'WP Kuala Lumpur',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri WP Kuala Lumpur',
                'created_at' => '2020-03-18 23:03:23',
                'updated_at' => '2020-03-18 23:03:23',
            ),
            14 => 
            array (
                'id' => 15,
                'code' => 'WP Labuan',
                'name' => 'WP Labuan',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri WP Labuan',
                'created_at' => '2020-03-18 23:03:24',
                'updated_at' => '2020-03-18 23:03:24',
            ),
            15 => 
            array (
                'id' => 16,
                'code' => 'WP Putrajaya',
                'name' => 'WP Putrajaya',
                'is_active' => 1,
                'description' => 'Data ini merujuk kepada parlimen negeri WP Putrajaya',
                'created_at' => '2020-03-18 23:03:24',
                'updated_at' => '2020-03-18 23:03:24',
            ),
        ));
        
        
    }
}