<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // AWAM
        Setting::create([
            'label'     =>  'Panjang Kata Laluan (Awam)',
            'code'      =>  'PANJANG_KATALALUAN_ORANG_AWAM',
            'domain'    =>  'PENETAPAN_AWAM',
            'value'     =>  8,
            'metric'    =>  'aksara',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Panjang Kata Laluan (Kerajaan)',
            'code'      =>  'PANJANG_KATALALUAN_ORANG_KERAJAAN',
            'domain'    =>  'PENETAPAN_AWAM',
            'metric'    =>  'aksara',
            'value'     =>  12,
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Panjang Kata Laluan (Default)',
            'code'      =>  'PANJANG_KATALALUAN_ASAL',
            'domain'    =>  'PENETAPAN_AWAM',
            'value'     =>  8,
            'metric'    =>  'aksara',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pertukaran Kata Laluan (Awam)',
            'code'      =>  'TEMPOH_PERUBAHAN_KATALALUAN_ORANG_AWAM',
            'domain'    =>  'PENETAPAN_AWAM',
            'value'     =>  0,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pertukaran Kata Laluan (Kerajaan)',
            'code'      =>  'TEMPOH_PERUBAHAN_KATALALUAN_ORANG_KERAJAAN',
            'domain'    =>  'PENETAPAN_AWAM',
            'value'     =>  90,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh percubaan log masuk ',
            'code'      =>  'TEMPOH_PERCUBAAN',
            'domain'    =>  'PENETAPAN_AWAM',
            'value'     =>  5,
            'metric'    =>  'kali',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);


        /**
         * Lesen Runcit
         */
        Setting::create([
            'label'     =>  'Had Muatan',
            'code'      =>  'PERMOHONAN_HAD_MUATAN',
            'domain'    =>  'PENETAPAN_LESEN_RUNCIT',
            'value'     =>  10,
            'metric'    =>  'tan',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'table'     =>  'durations',
            'domain'    =>  'PENETAPAN_LESEN_RUNCIT',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_RUNCIT',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_RUNCIT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_RUNCIT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**
         * Lesen Borong
         */
        Setting::create([
            'label'     =>  'Had Muatan',
            'code'      =>  'PERMOHONAN_HAD_MUATAN',
            'domain'    =>  'PENETAPAN_LESEN_BORONG',
            'value'     =>  100,
            'metric'    =>  'tan',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'table'     =>  'durations',
            'domain'    =>  'PENETAPAN_LESEN_BORONG',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_BORONG',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_BORONG',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_BORONG',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**
         * Lesen Import
         */
        Setting::create([
            'label'     =>  'Had Muatan',
            'code'      =>  'PERMOHONAN_HAD_MUATAN',
            'domain'    =>  'PENETAPAN_LESEN_IMPORT',
            'value'     =>  100,
            'table'     =>  'durations',
            'metric'    =>  'tan',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_LESEN_IMPORT',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_IMPORT',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_IMPORT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_IMPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**
         * Lesen Eksport
         */
        Setting::create([
            'label'     =>  'Had Muatan',
            'code'      =>  'PERMOHONAN_HAD_MUATAN',
            'domain'    =>  'PENETAPAN_LESEN_EKSPORT',
            'value'     =>  100,
            'table'     =>  'durations',
            'metric'    =>  'tan',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_LESEN_EKSPORT',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_EKSPORT',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_EKSPORT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_EKSPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**
         * Lesen Beli Padi
         */
        Setting::create([
            'label'     =>  'Had Muatan',
            'code'      =>  'PERMOHONAN_HAD_MUATAN',
            'domain'    =>  'PENETAPAN_LESEN_BELI_PADI',
            'value'     =>  'tiada',
            'metric'    =>  'tan',
            'table'     =>  'durations',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_LESEN_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_BELI_PADI',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_BELI_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**
         * Lesen Kilang Padi
         */
        Setting::create([
            'label'     =>  'Tempoh Permohonan Baharu',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_LESEN_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'table'     =>  'durations',
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (pertama)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_PERTAMA',
            'domain'    =>  'PENETAPAN_LESEN_KILANG_PADI',
            'value'     =>  60,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (kedua)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KEDUA',
            'domain'    =>  'PENETAPAN_LESEN_KILANG_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Notifikasi Batal (ketiga)',
            'code'      =>  'NOTIFIKASI_PEMBATALAN_KETIGA',
            'domain'    =>  'PENETAPAN_LESEN_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        //PERMIT
        /**Pemindahan Padi**/
        Setting::create([
            'label'     =>  'Tempoh Membuat Permohonan Sebelum Hari Perjalanan (maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_PERMIT_PINDAH_PADI',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Sah Laku Permit',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_PERMIT_PINDAH_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Beras/Hasil Sampingan**/
        Setting::create([
            'label'     =>  'Tempoh Membuat Permohonan Sebelum Hari Perjalanan (maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_PERMIT_BERAS',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Sah Laku Permit',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_PERMIT_BERAS',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        //SURAT KELULUSAN BERSYARAT
        /**Kilang Padi Komersial**/
        Setting::create([
            'label'     =>  'Tempoh Sah Laku Surat Kelulusan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Beli Padi**/
        Setting::create([
            'label'     =>  'Tempoh Sah Laku Surat Kelulusan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'Tahun',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        //KPI
        /**Lesen Runcit**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  5,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_RUNCIT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Borong**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BORONG',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Import**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_IMPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Export**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_ESKPORT',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Kilang Padi**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Lesen Beli Padi**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_LESEN_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        //KPI
        /**Permit Pemindahan Padi**/
        Setting::create([
            'label'     =>  'Kelulusan Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_PERMIT_PEMINDAHAN_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Permit Pemindahan Beras/Hasil Sampingan**/
        Setting::create([
            'label'     =>  'Kelulusan Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_PERMIT_BERAS',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        //KPI
        /**Surat Kelulusan Bersyarat Komersial**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_KILANG_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        /**Surat Kelulusan Bersyarat Komersial**/
        Setting::create([
            'label'     =>  'Tempoh Proses Keseluruhan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  15,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Pemproses',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit Cawangan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Pegawai Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  6,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Siasatan',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Semakan Ketua Unit HQ',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  2,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kelulusan Ketua Pengarah',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  3,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Tempoh Pembayaran Dibuat (Maximum)',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  30,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Pegawai Penjana',
            'code'      =>  'PERMOHONAN_TAMAT_TEMPOH',
            'domain'    =>  'PENETAPAN_KPI_SURAT_KELULUSAN_BERSYARAT_BELI_PADI',
            'value'     =>  1,
            'metric'    =>  'hari',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);
    }
}
