<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('registration_number');
            $table->string('old_registration_number');
            $table->date('expiry_date');
            $table->tinyInteger('is_active')->default(1);
            $table->unsignedBigInteger('company_type_id');
            $table->string('paidup_capital')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('license_application_id')->index('lacc_la_idx');
            $table->foreign('license_application_id','fk_lacc_la')->references('id')->on('license_applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
