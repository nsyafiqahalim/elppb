<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\ZoneDataObject;

class ZoneController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addZone(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $landStatus = ZoneDataObject::addZone($param);

        return response()->json([
            'message'   =>  'Zon berjaya ditambah',
            'data'      =>  $landStatus,
            'success'   =>  true,
            'route'     =>  route('admin.zone.index')
        ], 200);
    }

    public function updateZone(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $landStatus = ZoneDataObject::updateZone($param, $decryptedId);

        return response()->json([
            'message'   =>  'Zon berjaya dikemaskini',
            'data'      =>  $landStatus,
            'success'   =>  true,
            'route'     =>  route('admin.zone.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return ZoneDataObject::findAllZoneUsingDatatableFormat($param);
    }
}
