@extends('layouts.license-management-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-bookmark"></i> Maklumat Permohonan</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('management.mytask-index') }}">Pengurusan Lesen</a></li>
            <li class="breadcrumb-item active">Maklumat Permohonan Lesen ( {{ $license->licenseType->name }} )</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <!-- Nav tabs -->
            <div class="card-body wizard-content">
                  <form id="submission-form" class="tab-wizard wizard-circle">
                    <!-- Step 1 -->
                    <h6>Syarikat</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Syarikat</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Syarikat</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $license->company_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Syarikat</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="fas fa-building"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->company->company_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No Pendaftaran / Trading License / IRD</label>
                                        <input type="text" class="form-control" value="{{ $license->company->registration_number }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Tarikh Mansuh</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-calendar-question"></i>
                                                </span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ \Carbon\Carbon::parse($license->company->expiry_date)->format('d/m/Y') }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Modal Berbayar</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">RM</span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->company->paidup_capital }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Company Address -->
                            <h3 class="box-title mt-5">Alamat Surat Menyurat Syarikat</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 1</label>
                                        <input type="text" value="{{ $license->companyAddress->address_1 }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 2</label>
                                        <input type="text" value="{{ $license->companyAddress->address_2}}" disabled class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Alamat 3</label>
                                        <input type="text" value="{{ $license->companyAddress->address_3 }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" value="{{ $license->companyAddress->postcode }}" disabled class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" value="{{ $license->companyAddress->state_name }}" disabled class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-cellphone"></i>
                                                </span>
                                            </div>
                                            <input type="text" value="{{ $license->companyAddress->phone_number }}" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-fax"></i>
                                                </span>
                                            </div>
                                            <input type="text" disabled value="039553333" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="mdi mdi-email"></i>
                                                </span>
                                            </div>
                                            <input type="text" value="{{ $license->companyAddress->email }}" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h6>Rakan Kongsi</h6>
                    <section style="padding-top:30px;">
                        <h3 class="card-title">Maklumat Rakan Kongsi</h3>
                        @if (count($license->companyPartners) > 0)
                        @foreach($license->companyPartners as $licensePartner)
                        <div class="form-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Pemilik / Rakan Kongsi / Ahli Lembaga</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $licensePartner->name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status Kewarganegaraan</label>
                                        <input type="text" class="form-control" value="{{ $licensePartner->citizen_name }}" disabled>
                                    </div>
                                </div>
                            </div> <!-- End Row 1 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No Kad Pengenalan</label>
                                        <input type="text" class="form-control" value="{{ $licensePartner->nric }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Passport (Bukan Bumiputera)</label>
                                        <input type="text" class="form-control" value="{{ is_null($licensePartner->passport) ? '-' : $licensePartner->passport}}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Status Pemilikan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-account-key"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="Pemegang Saham">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 2 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jumlah Syer</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">RM</span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $licensePartner->total_share }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Peratusan Syer</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" value="{{ $licensePartner->share_percentage }}" disabled>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $licensePartner->phone_number }}" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 3 -->
                        </div>
                        @endforeach
                        @endif
                    </section>
                    <!-- Step 3 -->
                    <h6>Premis</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Premis Perniagaan</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Perniagaan</label>
                                        <input type="text" id="companyName" class="form-control" value="{{ $license->companyPremise->business_type_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 1</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_1 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 2</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_2 }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 3</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->address_3 }}" disabled>
                                    </div>
                                </div>
                            </div> <!-- End Row 1 -->
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->postcode }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->state_name }}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Parlimen</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->parliament_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Dun</label>
                                        <input type="text" class="form-control" value="{{ $license->companyPremise->dun_name }}" disabled>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Bangunan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-building"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->companyPremise->building_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Perniagaan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-briefcase"></i></span>
                                            </div>
                                            <input type="text" class="form-control" value="{{ $license->companyPremise->business_type_name }}" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722587">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-fax"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722555">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-email"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="shn123@yahoo.com">
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- End Row 2 -->
                        </div>
                    </section>
                    <!-- Step 4 -->
                    <!-- <h6>Stor</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Maklumat Stor</h3>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Nama Syarikat</label>
                                        <input type="text" id="companyName" class="form-control" disabled placeholder="Agro Sdn Bhd">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Bangunan</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="fas fa-building"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="Gudang">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Jenis Hak Milik</label>
                                        <input type="text" class="form-control" disabled placeholder="Sendiri">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 1</label>
                                        <input type="text" class="form-control" disabled placeholder="Lot 1780">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 2</label>
                                        <input type="text" class="form-control" disabled placeholder="Berinchang">
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Alamat 3</label>
                                        <input type="text" class="form-control" disabled placeholder="Cameron Highlands">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Poskod</label>
                                        <input type="text" disabled value="76700" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label>Negeri</label>
                                        <input type="text" disabled value="Pahang" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Telefon</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-cellphone"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722587">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">No. Faks</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-fax"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="0668722555">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">E-mel</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i class="mdi mdi-email"></i></span>
                                            </div>
                                            <input type="text" class="form-control" disabled placeholder="shn123@yahoo.com">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <!-- Step 5 -->
                    <h6>Lampiran</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th colspan="2">Fail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>
                                                Salinan Sijil Pendaftaran SSM
                                            </td>
                                            <td> <button class="btn btn-warning ssmButton">Lihat Fail</button></td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>
                                                Salinan Lesen Perniagaan (Sabah/Sarawak)
                                            </td>
                                            <td> <button class="btn btn-warning lesenPerniagaanButton">Lihat Fail</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- Step 6 -->
                    <h6>Log Aktiviti</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th>Aktiviti</th>
                                            <th>Tarikh</th>
                                            <th>Pelaku</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @if (count($license_activities) > 0)
                                        @foreach($license_activities as $license_activity)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $license_activity->activity }}</td>
                                            <td>{{ \Carbon\Carbon::parse($license_activity->created_at)->format('d/m/Y g:i A') }}</td>
                                            <td>{{ $license_activity->user->name }} </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                    <!-- Step 7 -->
                    <h6>Status</h6>
                    <section>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table no-wrap table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th width="10">Bil.</th>
                                            <th>Pelaku</th>
                                            <th>Jawatan</th>
                                            <th>Tarikh</th>
                                            <th>Ulasan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i = 1 @endphp
                                        @if (count($license_remarks) > 0)
                                        @foreach($license_remarks as $license_remark)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $license_remark->user->name }} </td>
                                            <td> </td>
                                            <td>{{ \Carbon\Carbon::parse($license_remark->created_at)->format('d/m/Y g:i A') }}</td>
                                            <td>{{ $license_remark->remarks }}</td>

                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </section>

                    @if (Auth::user()->can("Jana Lesen") && count($license_payments) > 0 && $license_payments[0]['status_payment'] == 'unpaid')
                    <!-- Pembayaran Step -->
                    <h6>Pembayaran</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Jumlah Bayaran</h3>
                            <hr>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Tempoh Permohonan</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_duration }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Had Muatan (TAN)</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_load }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Jumlah Bayaran (RM)</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license_payments[0]['total_payment'] }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-2 col-form-label">Status Pembayaran</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label btn btn-danger" value="Belum melakukan pembayaran" disabled>
                                </div>
                            </div>
                        </div>
                    </section>
                    @else
                    <!-- Final Step -->
                    <h6>Tindakan</h6>
                    <section style="padding-top:30px;">
                        <div class="form-body">
                            <h3 class="card-title">Tindakan Seterusnya</h3>
                            <hr>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Tempoh Permohonan</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_duration }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Had Muatan (TAN)</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->apply_load }}" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-2 col-form-label">Status Permohonan Sekarang</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label" value="{{ $license->status->name }} ( {{ $license->licenseApplicationType->code }} )" disabled>
                                </div>
                            </div>
                            @if (!Auth::user()->can("Jana Lesen"))
                            <div class="form-group row">
                                <label for="example-url-input" class="col-2 col-form-label">Ulasan</label>
                                <div class="col-6">
                                    <textarea class="form-control" rows="10" type="url" id="remark" name="remark" required></textarea>
                                </div>
                            </div>
                            @endif
                            @if (Auth::user()->can("Jana Lesen") && count($license_payments) > 0)
                            <div class="form-group row">
                                <label for="example-email-input" class="col-2 col-form-label">Status Pembayaran</label>
                                <div class="col-6">
                                    <input type="text" class="col-md-12 col-form-label btn btn-success" value="Pembayaran telah dilakukan" disabled>
                                </div>
                            </div>
                            @endif
                        </div>
                    </section>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script type='text/javascript'>
    $(document).ready(function(){
        $('#datatable').DataTable({
            responsive: true,
            searching: false,
            ordering: false,
        });

        $(document).on('click','.ssmButton',function(){
            var url = "{{  route('management.retail.replacement.ssm', [encrypt($license->id)]) }}";
            window.open(url);
        })

        $(document).on('click','.lesenPerniagaanButton',function(){
            var url = "{{  route('management.retail.replacement.lesen-perniagaan', [encrypt($license->id)]) }}";
            window.open(url);
        })

        $(document).on('click','.submitButton',function(){
            var remark = $('#remark').val();
            var status = $('#status').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.retail.replacement.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })

        $(document).on('click','.submitButton_5',function(){
            var status = $('#status').val();

            $('#submission-form').attr('action', "{{ route('management.retail.replacement.download', [encrypt($license->id)]) }}?status="+status);
        })

        $(document).on('click','.submitButton_6',function(){
            var status = $('#status').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.retail.replacement.payment', [encrypt($license->id)]) }}?status="+status);
        })

        $(document).on('click','.rejectButton',function(){
            var remark = $('#remark').val();
            var status = $('#status_2').val();

            $("#submission-form").addClass("form-data-submission");
            $('#submission-form').attr('action', "{{ route('management.retail.replacement.edit', [encrypt($license->id)]) }}?status="+status+"&remark="+remark);
        })
    });

</script>
@endpush

@push('wizard')
<script type='text/javascript'>
    $(".tab-wizard").steps({
        headerTag: "h6"
        , bodyTag: "section"
        , transitionEffect: "fade"
        , titleTemplate: '<span class="step">#index#</span> #title#'
        , labels: {
            finish: "TOLAK_RUNCIT_PENGGANTIAN",
            previous: "Kembali",
            next: "Seterusnya"
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            if(currentIndex == 6) {
                $('ul[aria-label=Pagination] a[href="#finish"]').remove();

                if ("{{ $license->status->name }}" == "Permohonan Baharu") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PENGGANTIAN" name="status" id="status">Semakan Ketua Unit Cawangan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_RUNCIT_PENGGANTIAN" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Peraku Cawangan Permohonan Lesen Runcit (Cawangan)") && $license->status->name == "Peraku Semakan Cawangan" }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="KELULUSAN_PERMOHONAN_RUNCIT_PENGGANTIAN" name="status" id="status">Semakan Ketua Pengarah</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_RUNCIT_PENGGANTIAN" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Kelulusan Permohonan Lesen Runcit (Cawangan)") && $license->status->name == "Kelulusan Permohonan" }}") {
                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton" value="JANAAN_LESEN_RUNCIT_PENGGANTIAN" name="status" id="status">Lulus Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));

                    var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-danger rejectButton" value="TOLAK_RUNCIT_PENGGANTIAN" name="status_2" id="status_2">Tolak Permohonan</button></li>');
                    $input.appendTo($('ul[aria-label=Pagination]'));
                } else if ("{{ Auth::user()->can("Jana Lesen") }}") {
                    if ("{{ count($license_payments) > 0 && $license_payments[0]['status_payment'] == 'unpaid' }}") {
                      var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_6" value="Membuat Bayaran" name="status" id="status">Membuat Bayaran</button></li>');
                      $input.appendTo($('ul[aria-label=Pagination]'));
                    } else {
                      var $input = $('<li aria-hidden="false" style=""><button type="submit" class="btn btn-success submitButton_5" value="TAMAT_RUNCIT_PENGGANTIAN" name="status" id="status">Jana Lesen</button></li>');
                      $input.appendTo($('ul[aria-label=Pagination]'));
                    }
                }
            } else {
                $('ul[aria-label=Pagination] button[value="PERAKU_SEMAKAN_CAWANGAN_RUNCIT_PENGGANTIAN"]').remove();
                $('ul[aria-label=Pagination] button[value="KELULUSAN_PERMOHONAN_RUNCIT_PENGGANTIAN"]').remove();
                $('ul[aria-label=Pagination] button[value="JANAAN_LESEN_RUNCIT_PENGGANTIAN"]').remove();
                $('ul[aria-label=Pagination] button[value="TAMAT_RUNCIT_PENGGANTIAN"]').remove();
                $('ul[aria-label=Pagination] button[value="TOLAK_RUNCIT_PENGGANTIAN"]').remove();
            }
        }
    });
</script>
@endpush
