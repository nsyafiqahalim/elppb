<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Dun;

class DunDataObject
{
    public static function addDun($param)
    {
        $dun = null;

        DB::transaction(function () use ($param, &$dun) {
            $dun = Dun::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'parliament_id'   =>  $param['parliament_id'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $dun;
    }

    public static function updateDun($param, $id)
    {
        $dun = null;

        DB::transaction(function () use ($param, $id,  &$dun) {
            $dun = Dun::find($id);

            $dun->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'parliament_id'   =>  $param['parliament_id'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $dun;
    }

    public static function findAllDunUsingDatatableFormat($param)
    {
        $duns = Dun::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($duns)
        ->addIndexColumn()
        ->addColumn('action', function ($dun) {
            return view('admin.dun.partials.datatable-button', compact('dun'))->render();
        })
        ->addColumn('status', function ($dun) {
            $status= ($dun->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.dun.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findDunById($id)
    {
        return Dun::find($id);
    }
}
