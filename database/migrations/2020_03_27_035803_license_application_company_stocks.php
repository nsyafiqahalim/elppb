<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_stock_statements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('stock_type_id')->nullable()->index('lacss_st_idx');
            $table->string('stock_type')->nullable();
            $table->unsignedBigInteger('stock_category_id')->nullable()->index('lacss_sc_idx');
            $table->string('stock_category')->nullable();
            $table->unsignedBigInteger('spin_type_id')->nullable()->index('lacss_spt_idx');
            $table->string('spin_type')->nullable();
            $table->unsignedBigInteger('total_grade_id')->nullable()->index('lacss_tg_idx');
            $table->string('total_grade')->nullable();
            $table->unsignedBigInteger('company_id')->nullable()->index('lacss_cmp_idx');
            $table->unsignedBigInteger('license_type_id')->nullable()->index('lacss_lt_idx');
            $table->unsignedBigInteger('rice_grade_id')->nullable()->index('lacss_rg_idx');
            $table->string('rice_grade')->nullable();
            $table->unsignedBigInteger('stock_statement_period_id')->nullable()->index('lacss_sp_idx');
            $table->unsignedBigInteger('company_stock_statement_account_id')->nullable()->index('lacss_cssa_idx');

            $table->bigInteger('initial_stock')->nullable();
            $table->float('total_buy', 8, 2)->nullable();
            $table->float('buying_price', 8, 2)->nullable();
            $table->float('total_spin', 8, 2)->nullable();
            $table->float('stock_balance', 8, 2)->nullable();
            $table->float('spin_result', 8, 2)->nullable();
            $table->float('total_sell', 8, 2)->nullable();
            $table->float('selling_price', 8, 2)->nullable();

            $table->foreign('stock_type_id','fk_lacss_st')->references('id')->on('stock_types');
            $table->foreign('rice_grade_id','fk_lacss_rg')->references('id')->on('rice_grades');
            $table->foreign('stock_category_id','fk_lacss_sc')->references('id')->on('stock_categories');
            $table->foreign('license_type_id','fk_lacss_lt')->references('id')->on('license_types');
            $table->foreign('spin_type_id','fk_lacss_spt')->references('id')->on('spin_types');
            $table->foreign('total_grade_id','fk_lacss_tg')->references('id')->on('total_grades');
            $table->foreign('company_id','fk_lacss_cmp')->references('id')->on('companies');
            $table->foreign('company_stock_statement_account_id','fk_lacss_cssa')->references('id')->on('company_stock_statement_accounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
