<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FactoryType extends Model
{
    public $guarded = ['id'];
}
