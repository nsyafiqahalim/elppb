<?php

//Route::group(['middleware' => 'auth'], function () {
    Route::prefix('pengurusan-lesen')->group(function () {
      Route::prefix('tugasan-saya')->group(function () {

        Route::prefix('runcit')->group(function () {
          Route::prefix('baharu')->group(function () {
            Route::get('/datatable', 'API\V1\LicenseManagement\Retail\NewController@datatable')->name('api.management.new.datatable');
          });
        }); // End Runcit
      });
    });
//});
