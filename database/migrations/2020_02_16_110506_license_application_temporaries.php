<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationTemporaries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_temporaries', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->unsignedBigInteger('license_application_id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('company_premise_id')->nullable();
            $table->unsignedBigInteger('company_store_id')->nullable();
            $table->unsignedBigInteger('company_factory_id')->nullable();
            $table->unsignedBigInteger('license_application_include_license_id')->nullable();
        });

        Schema::create('approval_letter_application_temporaries', function (Blueprint $table) {
            //$table->bigIncrements('id');
            $table->unsignedBigInteger('approval_letter_application_id');
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('company_premise_id')->nullable();
            $table->unsignedBigInteger('company_store_id')->nullable();
            $table->unsignedBigInteger('company_factory_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
