@extends('layouts.portal-layout')

@section('content')
<div class="container">
    <p>{{ $photo->description }}</p>
    <a href="{{ route('album-show', $photo->album->id) }}" class="btn btn-outline-dark">kembali</a>
    <small>Size: {{ $photo->size }}</small>
    <hr>
    <img src="/storage/albums/{{ $photo->album_id }}/{{ $photo->photo }}" alt="{{ $photo->photo }}">
</div>
@endsection
