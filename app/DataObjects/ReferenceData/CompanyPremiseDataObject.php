<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\CompanyPremise;

class CompanyPremiseDataObject
{
    public static function addNewCompanyPremise($param)
    {
        return CompanyPremise::create([
            //'name'                  =>  $param['name'],
            'business_type_id'       =>  $param['business_type_id'],
            'building_type_id'       =>  $param['building_type_id'],
            'dun_id'                =>  $param['dun_id'],
            'parliament_id'         =>  $param['parliament_id'],
            'address_1'                 =>  $param['address_1'],
            'address_2'                 =>  $param['address_2'],
            'address_3'                 =>  $param['address_3'],
            'postcode'                  =>  $param['postcode'],
            'state_id'                  =>  $param['state_id'],
            'company_id'               =>  $param['company_id'],
        ]);
    }

    public static function findAllActiveCompanyPremises()
    {
        $Companys = CompanyPremise::activeOnly()->get();

        return $Companys;
    }

    public static function findAllCompanyPremisesUsingDatatableFormat($param)
    {
        $companies =  CompanyPremise::distinct()
                    ->when(isset($param['company_id']), function ($company) use ($param) {
                        $company->where('company_id', $param['company_id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
        })->addColumn('business_type', function ($company) {
            return $company->businessType->name ?? '-';
        })->addColumn('building_type', function ($company) {
            return $company->buildingType->name ?? '-';
        })
        ->addColumn('dun', function ($company) {
            return $company->dun->name ?? '-';
        })
        ->addColumn('parliament', function ($company) {
            return $company->parliament->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return view(
                'license-application.component.premise.partials.radiobutton',
                [
                'id'    =>  encrypt($company->id)
            ]
            )->render();
        })
        ->addColumn('name', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyPremiseById($id)
    {
        return CompanyPremise::find($id);
    }
}
