<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\LicenseType;

class LicenseTypeDataObject
{
    public static function addLicenseType($param)
    {
        $licenseType = null;

        DB::transaction(function () use ($param, &$licenseType) {
            $licenseType = LicenseType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $licenseType;
    }

    public static function updateLicenseType($param, $id)
    {
        $licenseType = null;

        DB::transaction(function () use ($param, $id,  &$licenseType) {
            $licenseType = LicenseType::find($id);

            $licenseType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $licenseType;
    }

    public static function findAllLicenseTypeUsingDatatableFormat($param)
    {
        $licenseType = LicenseType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($licenseType)
        ->addIndexColumn()
        ->addColumn('action', function ($licenseType) {
            return view('admin.license-type.partials.datatable-button', compact('licenseType'))->render();
        })
        ->addColumn('status', function ($licenseType) {
            $status= ($licenseType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.license-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findLicenseTypeById($id)
    {
        return LicenseType::find($id);
    }
}
