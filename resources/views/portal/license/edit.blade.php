@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Info Lesen</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Pengurusan Portal</li>
            <li class="breadcrumb-item"><a href="{{ route('portal.license.index') }}">Lesen</a></li>
            <li class="breadcrumb-item">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Lesen</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.portal.license.update',[encrypt($licenseDAO->id)]) }}">
                        @csrf
                        @method('PATCH')
                        <input type='hidden'  name='user_id'  value="{{ encrypt(Auth::user()->id) }}" />
                        <div class="form-body">
                            <div class="row">

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="title_label">Tajuk <span class='required'>*</span></label>
                                        <input type='text' class='form-control' name='title' id='title' value='{{ $licenseDAO->title }}' />
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="description_label">Keterangan <span style="color: red;">*</span></label>
                                        <textarea id="description" class="form-control border-primary" rows="10" name="description">{{ $licenseDAO->description }}</textarea>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="is_published_label">Terbitkan?</label>
                                         <input type='checkbox'
                                        @if($licenseDAO->is_published == 1)
                                            checked
                                        @endif
                                        name='is_published'  />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route ('portal.license.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>
<script>
    tinymce.init({
        selector: "textarea#description",
        theme: "modern",
        height: 300,
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            'image code',
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | code | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",

        image_title: true,
        automatic_uploads: true,
        file_picker_types: 'image',

        file_picker_callback: function(cb, value, meta) {
          var input = document.createElement('input');
          input.setAttribute('type', 'file');
          input.setAttribute('accept', 'image/*');

          input.onchange = function() {
            var file = this.files[0];
            var reader = new FileReader();

            reader.onload = function() {
              var id = 'blobid' + (new Date()).getTime();
              var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
              var base64 = reader.result.split(',')[1];
              var blobInfo = blobCache.create(id, file, base64);
              blobCache.add(blobInfo);

              // call the callback and populate the Title field with the file name
              cb(blobInfo.blobUri(), { title: file.name });
            };
            reader.readAsDataURL(file);
          };
          input.click();
        }
    });
</script>
@endpush
