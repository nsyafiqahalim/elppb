<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('company_type_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('license_application_id')->index();
            $table->string('name')->nullable();
            $table->string('registration_number');
            $table->string('company_type_name');
            $table->string('paidup_capital')->nullable();
            $table->date('expiry_date')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('company_type_id')->references('id')->on('company_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('license_application_id')->references('id')->on('license_applications');
            $table->timestamps();
        });

        Schema::create('approval_letter_application_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('company_type_id')->index();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('approval_letter_application_id')->index('alac_ala_idx');
            $table->string('name')->nullable();
            $table->string('registration_number');
            $table->string('company_type_name');
            $table->string('paidup_capital')->nullable();
            $table->date('expiry_date')->nullable();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('company_type_id')->references('id')->on('company_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('approval_letter_application_id', 'fk_alac_ala')->references('id')->on('approval_letter_applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
