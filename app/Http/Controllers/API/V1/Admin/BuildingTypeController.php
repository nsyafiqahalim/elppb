<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\BuildingTypeDataObject;

class BuildingTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addBuildingType(Request $request)
    {
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $buildingType = BuildingTypeDataObject::addBuildingType($param);

        return response()->json([
            'message'   =>  'Jenis bangunan berjaya ditambah',
            'data'      =>  $buildingType,
            'success'   =>  true,
            'route'     =>  route('admin.building_type.index')
        ], 200);
    }

    public function updateBuildingType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $buildingType = BuildingTypeDataObject::updateBuildingType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis bangunan berjaya dikemaskini',
            'data'      =>  $buildingType,
            'success'   =>  true,
            'route'     =>  route('admin.building_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        return BuildingTypeDataObject::findAllBuildingTypeUsingDatatableFormat($param);
    }
}
