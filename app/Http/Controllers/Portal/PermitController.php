<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Portal\PermitDataObject;

class PermitController extends Controller
{
    public function index()
    {
        return view('portal.permit.index');
    }

    public function add()
    {
        return view('portal.permit.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        $permitDAO = PermitDataObject::findPermitById($decryptedId);

        return view('portal.permit.edit', compact('permitDAO'));
    }
}
