<?php

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('permohonan-permit')->group(function () {
        Route::get('/', 'PermitApplication\MainController@index')->name('permit_application.index');

        Route::get('/padi', 'PermitApplication\PaddyController@index')->name('permit_application.paddy');
        Route::get('/beras-dan-hasil-sampingan', 'PermitApplication\RiceController@index')->name('permit_application.rice');
    });
});
