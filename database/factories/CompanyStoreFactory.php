<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;

use App\Models\CompanyStore;
use App\Models\Admin\BusinessType;
use App\Models\Admin\BuildingType;
use App\Models\Admin\StoreOwnershipType;
use App\Models\Admin\Dun;
use App\Models\Admin\Parliament;
use App\Models\Admin\District;
use App\Models\Admin\State;

$districts = District::all();
$states = State::all();
$businessTypes = BusinessType::all();
$buildingTypes = BuildingType::all();
$storeOwnershipType = StoreOwnershipType::all();

$factory->define(CompanyStore::class, function (Faker $faker) use ($districts, $states, $businessTypes, $buildingTypes, $storeOwnershipType) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));
    
    $dist = $districts->random();
    $stat = $states->random();
    $bt = $businessTypes->random();
    $bt2 = $buildingTypes->random();
    $soT = $storeOwnershipType->random();

    return [
            'store_ownership_type_id'          =>  $soT->id,
            'building_type_id'          =>  $bt2->id,
            'address_1'                 =>  $faker->buildingPrefix . ' ' . $faker->buildingNumber . ' ' . $faker->streetName,
            'address_2'                 =>  $faker->township,
            'address_3'                 =>  $faker->townState,
            'postcode'                  =>  $faker->numberBetween(10000, 99999),
            'state_id'                  =>  $stat->id,
            'district_id'               =>  $dist->id,
            'phone_number'              =>  $faker->phoneNumber,
            'fax_number'                =>  $faker->phoneNumber,
            'email'                     =>  $faker->unique()->safeEmail,
            'unique_id_identifier'      =>  Uuid::generate(1)
    ];
});
