<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class RiceCategory extends Model
{
    public $guarded = ['id'];
}
