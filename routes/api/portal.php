<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
// });

Route::group(['prefix' => '/v1'], function () {
    Route::prefix('pengumuman')->group(
        function () {
            //Route::post('/store', 'API\V1\Portal\AnnouncementController@addAnnouncement')
          //->middleware('StoreAnnoucementValidationMiddleware')
          //->name('api.portal.announcement.store');
            //Route::patch('/update/{id}', 'API\V1\Portal\AnnouncementController@updateAnnouncement')
            //->middleware('StoreAnnoucementValidationMiddleware')
            //->name('api.portal.announcement.update');
            //Route::delete('/delete/{id}', 'API\V1\Portal\AnnouncementController@deleteAnnouncement')->name('api.portal.announcement.delete');
            //Route::get('/datatable', 'API\V1\Portal\AnnouncementController@datatable')->name('api.portal.announcement.datatable');
            Route::post('/store', 'API\V1\Portal\GalleryController@addNewImageToGallery')
          ->name('api.portal.gallery.store');
            Route::patch('/update/{id}', 'API\V1\Portal\GalleryController@updateGallery')->name('api.portal.gallery.update');
            Route::delete('/delete/{id}', 'API\V1\Portal\GalleryController@deleteGallery')->name('api.portal.gallery.delete');
            Route::get('/datatable', 'API\V1\Portal\GalleryController@datatable')->name('api.portal.gallery.datatable');
        }
    );

    Route::prefix('lesen')->group(
        function () {
            Route::post('/store', 'API\V1\Portal\LicenseController@addLicense')
          ->middleware('StoreLicenseValidationMiddleware')
          ->name('api.portal.license.store');
            Route::patch('/update/{id}', 'API\V1\Portal\LicenseController@updateLicense')
            ->middleware('StoreLicenseValidationMiddleware')
            ->name('api.portal.license.update');
            Route::delete('/delete/{id}', 'API\V1\Portal\LicenseController@deleteLicense')->name('api.portal.license.delete');
            Route::get('/datatable', 'API\V1\Portal\LicenseController@datatable')->name('api.portal.license.datatable');
        }
    );

    Route::prefix('permit')->group(
        function () {
            Route::post('/store', 'API\V1\Portal\PermitController@addPermit')
          ->middleware('StorePermitValidationMiddleware')
          ->name('api.portal.permit.store');
            Route::patch('/update/{id}', 'API\V1\Portal\PermitController@updatePermit')
            ->middleware('StorePermitValidationMiddleware')
          ->name('api.portal.permit.update');
            Route::delete('/delete/{id}', 'API\V1\Portal\PermitController@deletePermit')->name('api.portal.permit.delete');
            Route::get('/datatable', 'API\V1\Portal\PermitController@datatable')->name('api.portal.permit.datatable');
        }
    );

    Route::prefix('faq')->group(
        function () {
            Route::post('/store', 'API\V1\Portal\FaqController@addFaq')
          ->middleware('StoreFaqValidationMiddleware')
          ->name('api.portal.faq.store');
            Route::patch('/update/{id}', 'API\V1\Portal\FaqController@updateFaq')
            ->middleware('StoreFaqValidationMiddleware')
            ->name('api.portal.faq.update');
            Route::delete('/delete/{id}', 'API\V1\Portal\FaqController@deleteFaq')->name('api.portal.faq.delete');
            Route::get('/datatable', 'API\V1\Portal\FaqController@datatable')->name('api.portal.faq.datatable');
        }
    );

    Route::prefix('galeri')->group(
        function () {
            Route::post('/store', 'API\V1\Portal\GalleryController@addNewImageToGallery')
          ->name('api.portal.gallery.store');
            Route::patch('/update/{id}', 'API\V1\Portal\GalleryController@updateGallery')->name('api.portal.gallery.update');
            Route::delete('/delete/{id}', 'API\V1\Portal\GalleryController@deleteGallery')->name('api.portal.gallery.delete');
            Route::get('/datatable', 'API\V1\Portal\GalleryController@datatable')->name('api.portal.gallery.datatable');
        }
    );

    Route::prefix('penetapan-portal')->group(
        function () {
            Route::patch('/update/{id}', 'API\V1\Portal\SettingController@update')->name('api.portal.setting.update');
        }
    );
});
