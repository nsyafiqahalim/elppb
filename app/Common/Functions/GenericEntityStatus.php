<?php

namespace App\Common\Functions;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Mockery\Exception;

/**
 *
 * This class is for changing statuses in applicable tables in SPWTD easily
 *
 * Class GenericEntityStatus
 * @package App\Common\Functions
 */

class GenericEntityStatus {

	/**
	 *
	 * Changes the status(es) of given model to the given status id. Will cause error if the given entity is not a model or status id does not exist.
	 * Can be an array/collection of model or single instance
	 *
	 * @param $entity
	 * @param $statusId
	 * @return true
	 */
	public function change($entity, $statusId) {

		if ($entity instanceof Collection) {
			foreach ($entity as $item) {
				if (!$item instanceof Model) {
					throw new Exception('This entity is not a model');
				}
			}
			foreach ($entity as $item) {
				$this->setStatus($item, $statusId);
			}
		} else {

			if ($entity instanceof Model) {
				$this->setStatus($entity, $statusId);
			} else {
				throw new Exception('Entity must be a modal!');
			}
		}

		return true;
	}

	/**
	 *
	 * Saves the status of individual entity
	 *
	 * @param $single
	 * @param $statusId
	 * @return true
	 */
	protected function setStatus($single, $statusId) {
		$single->status_id = $statusId;
		$single->save();

		return true;
	}
}