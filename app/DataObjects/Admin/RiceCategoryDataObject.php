<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\RiceCategory;

class RiceCategoryDataObject
{
    public static function addRiceCategory($param)
    {
        $riceCategory = null;

        DB::transaction(function () use ($param, &$riceCategory) {
            $riceCategory = RiceCategory::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $riceCategory;
    }

    public static function updateRiceCategory($param, $id)
    {
        $riceCategory = null;

        DB::transaction(function () use ($param, $id,  &$riceCategory) {
            $riceCategory = RiceCategory::find($id);

            $riceCategory->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $riceCategory;
    }

    public static function findAllRiceCategoryUsingDatatableFormat($param)
    {
        $riceCategory = RiceCategory::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($riceCategory)
        ->addIndexColumn()
        ->addColumn('action', function ($riceCategory) {
            return view('admin.rice-category.partials.datatable-button', compact('riceCategory'))->render();
        })
        ->addColumn('status', function ($riceCategory) {
            $status= ($riceCategory->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.rice-category.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findRiceCategoryById($id)
    {
        return RiceCategory::find($id);
    }
}
