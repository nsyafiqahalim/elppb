<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\RaceTypeDataObject;

class RaceTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.race-type.index');
    }

    public function add()
    {
        return view('admin.race-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $raceType = RaceTypeDataObject::findRaceTypeById($decryptedId);

        return view('admin.race-type.edit', compact('raceType'));
    }
}
