<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    public $guarded = ['id'];
}
