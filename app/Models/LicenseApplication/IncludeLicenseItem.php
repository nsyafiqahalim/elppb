<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\GenerateLicense;
use App\Models\LicenseApplication;

class IncludeLicenseItem extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_include_license_items';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function includeLicense()
    {
        return $this->belongsTo(IncludeLicense::class, 'license_application_include_license_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

}
