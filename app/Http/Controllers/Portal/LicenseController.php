<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Portal\LicenseDataObject;

class LicenseController extends Controller
{
    public function index()
    {
        return view('portal.license.index');
    }

    public function add()
    {
        return view('portal.license.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);
        $licenseDAO = LicenseDataObject::findLicenseById($decryptedId);

        return view('portal.license.edit', compact('licenseDAO'));
    }
}
