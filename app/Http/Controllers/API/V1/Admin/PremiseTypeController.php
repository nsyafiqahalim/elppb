<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\PremiseTypeDataObject;

class PremiseTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addPremiseType(Request $request)
    {
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $premiseType = PremiseTypeDataObject::addPremiseType($param);

        return response()->json([
            'message'   =>  'Jenis premis berjaya ditambah',
            'data'      =>  $premiseType,
            'success'   =>  true,
            'route'     =>  route('admin.premise_type.index')
        ], 200);
    }

    public function updatePremiseType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();
         
        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $premiseType = PremiseTypeDataObject::updatePremiseType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis premis berjaya dikemaskini',
            'data'      =>  $premiseType,
            'success'   =>  true,
            'route'     =>  route('admin.premise_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return PremiseTypeDataObject::findAllPremiseTypeUsingDatatableFormat($param);
    }
}
