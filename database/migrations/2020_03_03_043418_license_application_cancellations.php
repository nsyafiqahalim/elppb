<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCancellations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_cancellations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('license_application_cancellation_type_id')->nullable()->index('lar_larc_idx');
            $table->unsignedBigInteger('license_application_id')->index('larc_la_idx');
            $table->foreign('license_application_id', 'fk_larc_la')->references('id')->on('license_applications');
            $table->foreign('license_application_cancellation_type_id', 'fk_larc_lact')->references('id')->on('license_application_cancellation_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
