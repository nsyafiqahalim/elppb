<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\ParliamentDataObject;
use App\DataObjects\Admin\StateDataObject;

class ParliamentController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.parliament.index');
    }

    public function add()
    {
        $states  = StateDataObject::findAllActiveStates();

        return view('admin.parliament.add', compact('states'));
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $parliament = ParliamentDataObject::findParliamentById($decryptedId);
        $states  = StateDataObject::findAllActiveStates();

        return view('admin.parliament.edit', compact('parliament', 'states'));
    }
}
