<?php

namespace App\Http\Controllers\API\V1\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Portal\LicenseDataObject;

class LicenseController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addLicense(Request $request)
    {
        $param = $request->all();
        $decryptedUserId = decrypt($param['user_id']);

        $param['created_by'] = $decryptedUserId;
        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $licenseDAO = LicenseDataObject::storeNewLicense($param);

        return response()->json([
            'message'   =>  'Maklumat Lesen berjaya ditambah',
            'data'      =>  $licenseDAO,
            'success'   =>  true,
            'route'     =>  route('portal.license.index')
        ], 200);
    }

    public function updateLicense(Request $request, $id)
    {
        $param = $request->all();

        $decryptedId = decrypt($id);
        $decryptedUserId = decrypt($param['user_id']);

        $param['updated_by'] = $decryptedUserId;
        $param['is_published'] = (isset($param['is_published']))?1:0;

        $licenseDAO = LicenseDataObject::updateLicense($param, $decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Lesen berjaya dikemaskini',
            'data'      =>  $licenseDAO,
            'success'   =>  true,
            'route'     =>  route('portal.license.index')
        ], 200);
    }

    public function deleteLicense($id)
    {
        $decryptedId = decrypt($id);

        $licenseDAO = LicenseDataObject::deleteLicense($decryptedId);

        return response()->json([
            'message'   =>  'Maklumat Lesen berjaya dipadam',
            'data'      =>  $licenseDAO,
            'success'   =>  true,
            'route'     =>  route('portal.license.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        
        return LicenseDataObject::findAllLicenseUsingDatatableFormat($param);
    }
}
