<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
// });

Route::group(['prefix' => '/v1'], function () {
    Route::prefix('license-type')->group(
        function () {
            Route::post('/store', 'API\V1\Admin\LicenseTypeController@storeLicenseType')->name('api.license_type.store');
        }
    );
});
