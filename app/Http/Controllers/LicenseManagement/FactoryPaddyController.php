<?php

namespace App\Http\Controllers\LicenseManagement;

use App\Models\LicenseApplication;
use App\Models\LicenseActivity;
use App\Models\LicenseRemark;
use App\Models\LicenseAttachment;
use App\Models\UserProfile;
use App\Models\Admin\Status;
use App\Http\Controllers\Controller;
use Auth;
use PDF\PDF;
use Response;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class FactoryPaddyController extends Controller
{
    // view for maklumat pengurusan Lesen// tugasan saya dashboard
    public function showFactoryPaddy($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.factory-paddy.new-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showFactoryPaddyRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.factory-paddy.renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showFactoryPaddyChange($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.factory-paddy.change-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    public function showFactoryPaddyReplacement($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        return view('license-management.factory-paddy.replacement-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities ]);
    }

    public function showFactoryPaddyCancel($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        return view('license-management.factory-paddy.cancel-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities ]);
    }

    public function showFactoryPaddyChangeRenew($id)
    {
        $decryptedId = decrypt($id);
        $license = LicenseApplication::find($decryptedId);
        $license_remarks = LicenseRemark::where('license_application_id', $decryptedId)->get();
        $license_activities = LicenseActivity::where('license_application_id', $decryptedId)->get();

        if (!is_null($license->branch_id)) {
           $pegawai_siasatan = UserProfile::where('branch_id', $license->branch_id)->get();
        }

        return view('license-management.factory-paddy.new-renew-view')->with(['license' => $license, 'license_remarks' => $license_remarks, 'license_activities' => $license_activities, 'pegawai_siasatan' => $pegawai_siasatan ]);
    }

    // edit for maklumat pengurusan Lesen// tugasan saya dashboard
    public function editFactoryPaddy(Request $request, $id)
    {
        $decryptedId = decrypt($id);

        $license = LicenseApplication::find($decryptedId);
        $status = Status::where('code', $request->status)->get('id');
        $license->status_id = $status[0]->id;
        if (!is_null($request->investigation_officer)) {
          $license->license_investigation_officer_id = $request->investigation_officer;
        }
        $license->save();

        // licence application remark
        $license_remark = new LicenseRemark();
        $license_remark->status_id = $status[0]->id;
        $license_remark->remarks = $request->remark;
        $license_remark->user_id = Auth::user()->id;
        $license_remark->license_application_id = $license->id;
        $license_remark->save();

        // licence application activities
        if ($request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'PEMILIHAN_PEGAWAI_SIASATAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Pemilihan Pegawai Siasatan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_SIASATAN_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_SIASATAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_KILANG_PADI_KOMERSIL_PEMBATALAN' || $request->status == 'PERAKU_SEMAKAN_CAWANGAN_KILANG_PADI_KOMERSIL_PENGGANTIAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit Cawangan";
        } elseif ($request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'PERAKU_SEMAKAN_IBU_PEJABAT_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' ) {
          $activity = "Hantar Semakan Ke Ketua Unit HQ";
        } elseif ($request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_PENGGANTIAN' || $request->status == 'KELULUSAN_PERMOHONAN_KILANG_PADI_KOMERSIL_PEMBATALAN' ) {
          $activity = "Hantar Semakan Ke Ketua Pengarah";
        } elseif ($request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_PENGGANTIAN' || $request->status == 'JANAAN_LESEN_KILANG_PADI_KOMERSIL_PEMBATALAN' ) {
          $activity = "Permohonan Dilulus";
        } elseif ($request->status == 'TOLAK_KILANG_PADI_KOMERSIL_BAHARU' || $request->status == 'TOLAK_KILANG_PADI_KOMERSIL_PEMBAHARUAN' || $request->status == 'TOLAK_KILANG_PADI_KOMERSIL_PERUBAHAN' || $request->status == 'TOLAK_KILANG_PADI_KOMERSIL_PEMBAHARUAN_DAN_PERUBAHAN' || $request->status == 'TOLAK_KILANG_PADI_KOMERSIL_PENGGANTIAN' || $request->status == 'TOLAK_KILANG_PADI_KOMERSIL_PEMBATALAN' ) {
          $activity = "Permohonan Ditolak";
        }

        $license_activity = new LicenseActivity();
        $license_activity->status_id = $status[0]->id;
        $license_activity->activity = $activity;
        $license_activity->user_id = Auth::user()->id;
        $license_activity->license_application_id = $license->id;
        $license_activity->save();

        return response()->json([
            'message'   =>  'Permohonan berjaya dikemaskini',
            'success'   =>  true,
            'route'     => route('management.mytask-index')
        ], 200);
      }

      public function downloadFactoryPaddyLicense(Request $request, $id)
      {
          $decryptedId = decrypt($id);
          $license = LicenseApplication::find($decryptedId);
          $status = Status::where('code', $request->status)->get('id');

          if ($license->status_id != $status[0]->id) {
              $license->status_id = $status[0]->id;
              $license->save();

              $license_remark = new LicenseRemark();
              $license_remark->status_id = $status[0]->id;
              $license_remark->remarks = 'Janaan Lesen';
              $license_remark->user_id = Auth::user()->id;
              $license_remark->license_application_id = $license->id;
              $license_remark->save();

              $license_activity = new LicenseActivity();
              $license_activity->status_id = $status[0]->id;
              $license_activity->activity = 'Janaan Lesen';
              $license_activity->user_id = Auth::user()->id;
              $license_activity->license_application_id = $license->id;
              $license_activity->save();
          }

          $pdf = \PDF::loadView('license-management.generate-license.factory-paddy-license', ['license' => $license]);
          return $pdf->download('Janaan-Lesen-Kilang-Padi.pdf');
      }

      public function showSSM($id)
      {
          $decryptedId = decrypt($id);
          $license_attachement_ssm = LicenseAttachment::where('code', 'SSM')->where('license_application_id', $decryptedId)->get();

          $file_name = $license_attachement_ssm[0]->file_path;
          $path = env('FILE_URL').'/'.$file_name;

          $file_type = mime_content_type($path);

          return Response::make(file_get_contents($path), 200, [
                'Content-Type' => $file_type,
                'Content-Disposition' => 'inline; filename="'.$file_name.'"'
          ]);
      }

      public function showLesenPerniagaan($id)
      {
          $decryptedId = decrypt($id);
          $license_attachement_ssm = LicenseAttachment::where('code', 'MINUTE_MEETING')->where('license_application_id', $decryptedId)->get();

          $file_name = $license_attachement_ssm[0]->file_path;
          $path = env('FILE_URL').'/'.$file_name;

          $file_type = mime_content_type($path);

          return Response::make(file_get_contents($path), 200, [
                'Content-Type' => $file_type,
                'Content-Disposition' => 'inline; filename="'.$file_name.'"'
          ]);
      }
}
