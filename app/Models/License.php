<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication;
use App\Models\LicenseApplication\CompanyAddress;
use App\Models\LicenseApplication\CompanyPartner;
use App\Models\LicenseApplication\CompanyPremise;
use App\Models\Admin\Status;

class License extends Model
{
    public $guarded = ['id'];
    public $table = 'licenses';

    private const ACTIVE = 1;

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }
}
