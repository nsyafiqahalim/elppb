<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Carbon\Carbon;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'                  => 'Developer',
            'email'                 => 'dev@dev.com',
            'login_id'              => 'dev',
            'password'              => bcrypt('123'),
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Developer');

        $user = User::create([
            'name'                  => 'Portal Developer',
            'email'                 => 'portal@dev.com',
            'login_id'              => 'portaldev',
            'password'              => bcrypt('123'),
            'type'                  => 'APPLICANT',
            'status'                 =>  'ACTIVE',
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Developer');



        $user = User::create([
            'name'                  => 'Admin',
            'email'                 => 'admin@admin.com',
            'login_id'              => 'admin123',
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'password'              => bcrypt('admin123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Administrator');

        $user = User::create([
            'name'                  => 'Ketua Unit Cawangan',
            'email'                 => 'caw@caw.com',
            'login_id'              => 'kuchq123',
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'password'              => bcrypt('kuchq123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Ketua Unit Cawangan');

        $user = User::create([
            'name'                  => 'Ketua Unit HQ',
            'email'                 => 'hq@hq.com',
            'login_id'              => 'ku123',
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'password'              => bcrypt('123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Ketua Unit HQ');

        $user = User::create([
            'name'                  => 'Penjana Lesen',
            'email'                 => 'penjana@penjana.com',
            'login_id'              => 'penjana123',
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'password'              => bcrypt('penjana123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Penjana Lesen');

        $user = User::create([
            'name'                  => 'Pegawai Siasatan',
            'email'                 => 'penyiasat@penyiasat.com',
            'login_id'              => 'pegawai123',
            'type'                  => 'MOA',
            'status'                 =>  'ACTIVE',
            'password'              => bcrypt('pegawai123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Pegawai Siasatan');

        $user = User::create([
            'name'                  => 'Pemohon Lesen',
            'email'                 => 'pelesen@pelesen.com',
            'login_id'              => 'a006',
            'status'                 =>  'ACTIVE',
            'type'                  => 'MOA',
            'password'              => bcrypt('123'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Pemohon');

        $user = User::create([
            'name'                  => 'Pegawai Kaunter',
            'email'                 => 'pekaunter@pekaunter.com',
            'login_id'              => 'pekaunter123',
            'type'                  => 'MOA',
            'password'              => bcrypt('pekaunter123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Pegawai Kaunter');

        $user = User::create([
            'name'                  => 'Ketua Pengarah',
            'email'                 => 'pengarah@pengarah.com',
            'login_id'              => 'kp123',
            'type'                  => 'MOA',
            'status'                 =>  'LOCKED',
            'password'              => bcrypt('kp123!@#'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Ketua Pengarah');

        $user = User::create([
            'name'                  => 'Pemohon',
            'email'                 => 'pemohon@pemohon.com',
            'login_id'              => 'pemohon',
            'status'                 =>  'WAITING_FOR_VERIFICATION',
            'type'                  => 'APPLICANT',
            'password'              => bcrypt('123'),
            'email_verified_at'     => Carbon::now(),
            'created_at'            => Carbon::now(),
            'updated_at'            => Carbon::now(),
            'remember_token'        => Str::random(10),
        ]);
        $user->assignRole('Ketua Pengarah');
        
    }
}
