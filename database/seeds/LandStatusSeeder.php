<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\LandStatus;

class LandStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Pertanian',
            'Industri',
            'Komersial',
            'Tol',
        ];

        foreach ($data as $item) {
            LandStatus::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'is_active' => 1,
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
            ]);
        }
    }
}
