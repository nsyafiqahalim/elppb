<?php

namespace App\DataObjects\LicenseApplication;

use DB;
use Auth;

use App\Models\LicenseApplication\CompanyStockStatement;

class CompanyStockStatementDataObject
{
    public static function addNewCompanyStockStatement($param)
    {
        return CompanyStockStatement::create([
            'stock_category_id'                     =>  $param['stock_category_id'],
            'stock_category'                        =>  $param['stock_category'],
            'stock_type_id'                         =>  $param['stock_type_id'],
            'stock_type'                            =>  $param['stock_type'],
            'spin_type_id'                          =>  $param['spin_type_id'],
            'spin_type'                             =>  $param['spin_type'],
            'total_grade_id'                        =>  $param['total_grade_id'],
            'total_grade'                           =>  $param['total_grade'],
            'company_id'                            =>  $param['company_id'],
            'license_type_id'                       =>  $param['license_type_id'],
            'rice_grade_id'                         =>  $param['rice_grade_id'],
            'rice_grade'                            =>  $param['rice_grade'],
            'stock_statement_period_id'             =>  $param['stock_statement_period_id'],
            'company_stock_statement_account_id'    =>  $param['company_stock_statement_account_id'],
            'initial_stock'                         =>  $param['initial_stock'],
            'total_buy'                             =>  $param['total_buy'],
            'buying_price'                          =>  $param['buying_price'],
            'total_spin'                            =>  $param['total_spin'],
            'stock_balance'                         =>  $param['stock_balance'],
            'spin_result'                           =>  $param['spin_result'],
            'total_sell'                            =>  $param['total_sell'],
            'selling_price'                         =>  $param['selling_price'],
        ]);
    }

    public static function findAllActiveCompanyStockStatements()
    {
        return CompanyStockStatement::get();
    }

    public static function findAllCompanyStockStatementsUsingDatatableFormat($param)
    {
        $companies =  CompanyStockStatement::distinct()
                    ->when(isset($param['user_id']), function ($company) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address_1.','
                    .$company->address_2.','
                    .$company->address_3.','
                    .$company->postcode.','
                    .$company->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('ownership_type', function ($company) {
            return $company->ownershipType->name ?? '-';
        })
        ->addColumn('race', function ($company) {
            return $company->race->name ?? '-';
        })
        ->addColumn('action', function ($company) {
            return '-';
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
