<?php

use Illuminate\Database\Seeder;

use App\Models\UserProfile;
use App\User;
class UserProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach($users as $user){
            UserProfile::create([
                'user_id'       =>  $user->id,
                'branch_id'     =>  1,
            ]);
        }
    }
}
