<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\RiceTypeDataObject;

class RiceTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addRiceType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $riceCategory = RiceTypeDataObject::addRiceType($param);

        return response()->json([
            'message'   =>  'Jenis Beras berjaya ditambah',
            'data'      =>  $riceCategory,
            'success'   =>  true,
            'route'     =>  route('admin.rice-type.index')
        ], 200);
    }

    public function updateRiceType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $riceCategory = RiceTypeDataObject::updateRiceType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Beras berjaya dikemaskini',
            'data'      =>  $riceCategory,
            'success'   =>  true,
            'route'     =>  route('admin.rice-type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return RiceTypeDataObject::findAllRiceTypeUsingDatatableFormat($param);
    }
}
