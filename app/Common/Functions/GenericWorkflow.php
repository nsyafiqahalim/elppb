<?php

namespace App\Common\Functions;
use App\Models\Workflow;
use Mockery\Exception;

/**
 *
 * This class is for generating menu in in SPWTD easily
 *
 * Class GenericMenu
 * @package App\Common\Functions
 */

class GenericWorkflow
{

    /**
     *
     Author
     Name : Mohamamd Khairi Bin Ali
     Email : mohammadkhairibinali@gmail.com
     Phone Number : 0129101561
     * 
     The workflow can be generated if only workflow existed. to do this, you'll need to add workflow  code parameter to extract the menu.
     * only string
     *
     * @param $data must be array so that it is easy to be manage in the future
     * @return json 
     */
    public function getWorkflow($data){
        $workflow =  Workflow::distinct();
        $workflow = $workflow->activeItems();
        $workflow = (isset($data["status"])) ? $workflow->activeStatus($data["status"]) : $workflow->activeStatus(Workflow::ACTIVE);
        if(isset($data["code"])){
            $workflow = $workflow->where('code',$data["code"]); 
        }
        
        $workflow = $workflow->get();
        return $workflow;
    }

}