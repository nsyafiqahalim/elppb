<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\State;

class StateDataObject
{
    public static function addState($param)
    {
        $state = null;

        DB::transaction(function () use ($param, &$state) {
            $state = State::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $state;
    }

    public static function updateState($param, $id)
    {
        $state = null;

        DB::transaction(function () use ($param, $id,  &$state) {
            $state = State::find($id);

            $state->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $state;
    }

    public static function findAllStateUsingDatatableFormat($param)
    {
        $state = State::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($state)
         ->addIndexColumn()
        ->addColumn('action', function ($state) {
            return view('admin.state.partials.datatable-button', compact('state'))->render();
        })
        ->addColumn('status', function ($state) {
            $status= ($state->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.state.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findStateById($id)
    {
        return State::find($id);
    }

    public static function findAllActiveStates()
    {
        return State::activeOnly()->get();
    }
}
