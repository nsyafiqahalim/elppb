<?php

namespace App\Models\ReferenceData;

use Illuminate\Database\Eloquent\Model;


class companyStockStatementAccount extends Model
{
    public $guarded = ['id'];
    public $table = 'company_stock_statement_accounts';

    private const ACTIVE = 1;
}
