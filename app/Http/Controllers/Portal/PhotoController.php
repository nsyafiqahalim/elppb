<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\Portal\Photo;

class PhotoController extends Controller
{
    public function create(int $albumId)
    {
      return view('portal.gallery.photo.create')->with('albumId', $albumId);
    }

    public function store(Request $request)
    {
      $this->validate($request, [
        'gambar'       => 'required|image',
      ]);

      $filenameWithExtension = $request->file('gambar')->getClientOriginalName();
      $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);
      $extension = $request->file('gambar')->getClientOriginalExtension();
      $filenameToStore = $filename . '_' . time() . '.' . $extension;

      //save image to machine
      $request->file('gambar')->storeAs('public/albums/' . $request->input('album-id'), $filenameToStore);

      $photo = new Photo();
      $photo->photo = $filenameToStore;
      $photo->size = $request->file('gambar')->getSize();
      $photo->album_id = $request->input('album-id');
      $photo->save();

      //return redirect('/gambar/' .$request->input('album-id'))->with('success', 'Gambar anda berjaya dimuatnaik!');
      //return redirect()->route('photo-show' .$request->input('album-id')->with('success', 'Gambar anda berjaya dimuatnaik!');
      return redirect()->route('portal.gallery.album.index')->with('success', 'Gambar anda telah berjaya dimuatnaik.');
    }

    public function show($id)
    {
      $photo = Photo::find($id);
      return view('portal.gallery.photo.show')->with('photo', $photo);
    }

    public function destroy($id)
    {
        $photo = Photo::find($id);
        //delete
        $photo->delete();
        return redirect()->route('portal.gallery.album.index');
    }
}
