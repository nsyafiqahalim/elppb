<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyPremises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_premises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('business_type_name')->nullable();
            $table->string('building_type_name')->nullable();
            $table->string('state_name')->nullable();
            $table->string('dun_name')->nullable();
            $table->string('district_name')->nullable();
            $table->string('parliament_name')->nullable();
            $table->string('premise_number')->nullable();

            $table->unsignedBigInteger('license_application_id')->index('lacp_la_idx');
            $table->unsignedBigInteger('business_type_id')->nullable()->index();
            $table->unsignedBigInteger('building_type_id')->nullable()->index();
            $table->unsignedBigInteger('parliament_id')->nullable()->index();
            $table->unsignedBigInteger('state_id')->nullable()->index();
            $table->unsignedBigInteger('district_id')->nullable()->index();
            $table->unsignedBigInteger('company_premise_id')->index();
            $table->unsignedBigInteger('dun_id')->nullable()->index();
            $table->unsignedBigInteger('company_id')->index();
            $table->tinyInteger('is_premise_validate')->nullable();
            $table->string('apply_load')->nullable();

            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('license_application_id', 'fk_lacp_la')->references('id')->on('license_applications');
            $table->foreign('business_type_id')->references('id')->on('business_types');
            $table->foreign('parliament_id')->references('id')->on('parliaments');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('building_type_id')->references('id')->on('building_types');
            $table->foreign('company_premise_id')->references('id')->on('company_premises');
            $table->foreign('dun_id')->references('id')->on('duns');
            $table->foreign('state_id')->references('id')->on('states');

            $table->timestamps();
        });

        Schema::create('approval_letter_application_company_premises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('business_type_name')->nullable();
            $table->string('building_type_name')->nullable();
            $table->string('state_name')->nullable();
            $table->string('dun_name')->nullable();
            $table->string('district_name')->nullable();
            $table->string('parliament_name')->nullable();
            $table->string('premise_number')->nullable();

            $table->unsignedBigInteger('approval_letter_application_id')->nullable()->index('alcp_la_idx');
            $table->unsignedBigInteger('business_type_id')->nullable()->index('alcp_bt_idx');
            $table->unsignedBigInteger('building_type_id')->nullable()->index('alcp_bt2_idx');
            $table->unsignedBigInteger('parliament_id')->nullable()->index('alcp_p_idx');
            $table->unsignedBigInteger('state_id')->nullable()->index('alcp_s_idx');
            $table->unsignedBigInteger('district_id')->nullable()->index('alcp_d2_idx');
            $table->unsignedBigInteger('company_premise_id')->nullable()->index('alcp_cp_idx');
            $table->unsignedBigInteger('dun_id')->nullable()->index('alcp_d_idx');
            $table->unsignedBigInteger('company_id')->nullable()->index();

            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('approval_letter_application_id', 'fk_alcp_la')->references('id')->on('approval_letter_applications');
            $table->foreign('business_type_id', 'fk_alacp_bt')->references('id')->on('business_types');
            $table->foreign('parliament_id', 'fk_alacp_p')->references('id')->on('parliaments');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('building_type_id', 'fk_alacp_bt2')->references('id')->on('building_types');
            $table->foreign('company_premise_id', 'fk_alacp_cp')->references('id')->on('company_premises');
            $table->foreign('dun_id', 'fk_alacp_d')->references('id')->on('duns');
            $table->foreign('state_id', 'fk_alacp_s')->references('id')->on('states');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
