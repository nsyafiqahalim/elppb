<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyAddresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('company_id')->index();
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email');
            $table->enum('type', ['MAILING'])->default('MAILING');
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
