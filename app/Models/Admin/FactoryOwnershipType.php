<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FactoryOwnershipType extends Model
{
    public $guarded = ['id'];
}
