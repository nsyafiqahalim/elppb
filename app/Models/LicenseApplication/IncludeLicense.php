<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication\IncludeLicenseItem;
use App\Models\LicenseApplication;

class IncludeLicense extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_include_licenses';

    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function includeLicenseItem()
    {
        return $this->hasMany(IncludeLicenseItem::class, 'license_application_include_item_id');
    }

    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

}
