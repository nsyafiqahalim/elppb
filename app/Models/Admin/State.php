<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    public $guarded = ['id'];

    public const ACTIVE = 1;

    public function scopeActiveOnly($query)
    {
        return $query->where('is_active', self::ACTIVE);
    }
}
