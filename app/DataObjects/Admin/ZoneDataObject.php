<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\Zone;

class ZoneDataObject
{
    public static function addZone($param)
    {
        $zone = null;

        DB::transaction(function () use ($param, &$zone) {
            $zone = Zone::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $zone;
    }

    public static function updateZone($param, $id)
    {
        $zone = null;

        DB::transaction(function () use ($param, $id,  &$zone) {
            $zone = Zone::find($id);

            $zone->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $zone;
    }

    public static function findAllZoneUsingDatatableFormat($param)
    {
        $zone = Zone::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($zone)
        ->addIndexColumn()
        ->addColumn('action', function ($zone) {
            return view('admin.zone.partials.datatable-button', compact('zone'))->render();
        })
        ->addColumn('status', function ($zone) {
            $status= ($zone->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.zone.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findZoneById($id)
    {
        return Zone::find($id);
    }
}
