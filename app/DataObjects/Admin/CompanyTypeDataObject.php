<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\CompanyType;

class CompanyTypeDataObject
{
    public static function addCompanyType($param)
    {
        $companyType = null;

        DB::transaction(function () use ($param, &$companyType) {
            $companyType = CompanyType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        Cache::forget('activeCompanyTypes');
        Cache::rememberForever('activeCompanyTypes', function () {
            return  CompanyType::activeOnly()->get();
        });

        return $companyType;
    }

    public static function updateCompanyType($param, $id)
    {
        $companyType = null;

        DB::transaction(function () use ($param, $id,  &$companyType) {
            $companyType = CompanyType::find($id);

            $companyType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $companyType;
    }

    public static function findAllCompanyTypeUsingDatatableFormat($param)
    {
        $companyType = CompanyType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($companyType)
        ->addIndexColumn()
        ->addColumn('action', function ($companyType) {
            return view('admin.company-type.partials.datatable-button', compact('companyType'))->render();
        })
        ->addColumn('status', function ($companyType) {
            $status= ($companyType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.company-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findCompanyTypeById($id)
    {
        return CompanyType::find($id);
    }
}
