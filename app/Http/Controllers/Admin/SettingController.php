<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use DB;
use App\DataObjects\Admin\SettingDataObject;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($setting)
    {
        $settingDomain = decrypt($setting);
        $models = [];
        $settings = (new SettingDataObject)->findSettingByDomain($settingDomain);
        foreach($settings as $setting){
            if(isset($setting->table)){
                $models[$setting->table] = DB::table($setting->table)->where('is_active',1)->get();
            }
        }

        return view('admin.setting.index', compact('settings', 'settingDomain','models'));
    }
}
