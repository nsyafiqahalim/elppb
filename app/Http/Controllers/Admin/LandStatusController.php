<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\LandStatusDataObject;

class LandStatusController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.land-status.index');
    }

    public function add()
    {
        return view('admin.land-status.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $landStatus = LandStatusDataObject::findLandStatusById($decryptedId);

        return view('admin.land-status.edit', compact('landStatus'));
    }
}
