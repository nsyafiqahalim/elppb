<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\RaceType;

class RaceTypeDataObject
{
    public static function addRaceType($param)
    {
        $raceType = null;

        DB::transaction(function () use ($param, &$raceType) {
            $raceType = RaceType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $raceType;
    }

    public static function updateRaceType($param, $id)
    {
        $raceType = null;

        DB::transaction(function () use ($param, $id,  &$raceType) {
            $raceType = RaceType::find($id);

            $raceType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $raceType;
    }

    public static function findAllRaceTypeUsingDatatableFormat($param)
    {
        $raceType = RaceType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($raceType)
        ->addIndexColumn()
        ->addColumn('action', function ($raceType) {
            return view('admin.race-type.partials.datatable-button', compact('raceType'))->render();
        })
        ->addColumn('status', function ($raceType) {
            $status= ($raceType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.race-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findRaceTypeById($id)
    {
        return RaceType::find($id);
    }
}
