<?php

namespace App\Models;
use App\User;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    public $guarded = ['id'];
    public $table = 'user_profiles';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
