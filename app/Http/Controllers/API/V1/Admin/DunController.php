<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\DunDataObject;

class DunController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addDun(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;
        $param['parliament_id'] = decrypt($param['parliament_id']);

        $dun = DunDataObject::addDun($param);

        return response()->json([
            'message'   =>  'Dun berjaya ditambah',
            'data'      =>  $dun,
            'success'   =>  true,
            'route'     =>  route('admin.dun.index')
        ], 200);
    }

    public function updateDun(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;
        $param['parliament_id'] = decrypt($param['parliament_id']);

        $dun = DunDataObject::updateDun($param, $decryptedId);

        return response()->json([
            'message'   =>  'Dun berjaya dikemaskini',
            'data'      =>  $dun,
            'success'   =>  true,
            'route'     =>  route('admin.dun.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return DunDataObject::findAllDunUsingDatatableFormat($param);
    }
}
