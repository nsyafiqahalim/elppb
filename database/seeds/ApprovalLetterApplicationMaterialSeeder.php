<?php

use Illuminate\Database\Seeder;
use App\Models\ApprovalLetterApplication\Material;

class ApprovalLetterApplicationMaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //BELI PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli atau sewaan stor dan premis<br/> atau salinan geran atau salinan kebenaran menggunakan tapak',
            'code'      =>  'sale_agreement',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Surat Kelulusan Khas Jabatan Pertanian/ Surat Lantikan Pengeluar Benih Padi Sah (bagi maksud dibuat biji benih) daripada industri Padi Beras',
            'code'      =>  'special_approval_letter',
            'domain'    =>  'BUY_PADDY_NEW',
            'order'     =>  5
        ]);

        //KILANG PADI BAHARU
        Material::create([
            'label'     =>  'Salinan Sijil Pendaftaran SSM/ Salinan lesen perniagaan/ I.R.D No 7(Sabah/Sarawak)',
            'code'      =>  'ssm',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  1
        ]);

        Material::create([
            'label'     =>  'Koperasi Dan Pertubuhan
                                <ol>
                                    <li>Salinan Sijil Pendaftaran</li>
                                    <li>Salinan Cabutan Minit Mesyuarat yang menunjukkan <br/> Ahli Lembaga Pengarah atau Ahli Jawatankuasa <br/> bersetuju untuk memohon  Lesen Runcit Baharu</li>
                                </ol>',
            'code'      =>  'coperation_association',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  2
        ]);

        Material::create([
            'label'     =>  'Salinan perjanjian jual beli / sewaan stor dan premis/<br/> salinan pemilikan kilang/ salinan kebenaran menggunakan tapak kilang',
            'code'      =>  'sale_agreement',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  3
        ]);

        Material::create([
            'label'     =>  'Salinan penyata akaun bank 3 bulan terkini',
            'code'      =>  'account_statement',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  4
        ]);

        Material::create([
            'label'     =>  'Surat Kelulusan Khas Jabatan Pertanian/ Surat Lantikan Pengeluar Bijih Benih Padi Sah(bagi maksud dibuat biji benih) daripada Industri Padi Beras',
            'code'      =>  'nature_department_approval_letter',
            'domain'    =>  'FACTORY_PADDY_NEW',
            'order'     =>  5
        ]);

    
    }
}
