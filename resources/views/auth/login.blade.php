@extends('layouts.external-layout')

@section('content')
<form class="form-horizontal form-material" id="loginform" method="post" action="{{ route('login') }}">
    @csrf
    <h3 class="box-title mb-3" style="text-align:center">Sistem Lesen Permit Padi Dan Beras 2.0</h3>
    <!---
    <div class="form-group ">
        <div class="col-xs-12">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="login_id" placeholder='Log ID' value="{{ old('login_id') }}" required autocomplete="login_id" autofocus>
            @error('login_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
-->
    <div class="form-group ">
        <div class="col-xs-12">
            <input id="login_id" type="login_id" class="form-control @error('login_id') is-invalid @enderror" name="login_id" placeholder='Log ID' value="{{ old('login_id') }}" required autocomplete="login_id" autofocus>
            @error('login_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder='Kata Laluan' required autocomplete="current-password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12">
            <div class="checkbox checkbox-primary float-left pt-0">
                <input id="checkbox-signup" type="checkbox">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                <label class="form-check-label" for="remember">
                    Ingati Saya
                </label>
            </div> <a href="javascript:void(0)" id="to-recover" class="text-dark float-right"><i class="fa fa-lock mr-1"></i> Lupa Kata Laluan?</a> </div>
    </div>
    <div class="form-group text-center mt-3">
        <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log Masuk</button>
        </div>
    </div>

    <div class="form-group mb-0">
        <div class="col-sm-12 text-center">
            <p>Belum mempunyai Akaun? <a href="{{ route('register') }}" class="text-info ml-1"><b>Daftar Masuk</b></a></p>
        </div>
    </div>
    </form>
    <form class="form-horizontal" id="recoverform" action="index.html">
    <div class="form-group ">
        <div class="col-xs-12">
            <h3>Lupa Kata Laluan</h3>
            <p class="text-muted">Sila masukkan emel anda supaya arahan seterusnya dapat dihantar! </p>
        </div>
    </div>
    <div class="form-group ">
        <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="E-mel"> </div>
    </div>
    <div class="form-group text-center mt-3">
        <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
        </div>
    </div>
</form>
@endsection
