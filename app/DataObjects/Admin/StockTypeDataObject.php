<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\StockType;

class StockTypeTypeDataObject
{
    public static function addStockType($param)
    {
        $stock = null;

        DB::transaction(function () use ($param, &$stock) {
            $stock = StockType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });

        return $stock;
    }

    public static function updateStockType($param, $id)
    {
        $stock = null;

        DB::transaction(function () use ($param, $id,  &$stock) {
            $stock = StockType::find($id);

            $stock->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });

        return $stock;
    }

    public static function findAllStockTypeUsingDatatableFormat($param)
    {
        $stocks = StockType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($stocks)
        ->addIndexColumn()
        ->addColumn('action', function ($stock) {
            return view('admin.stock.partials.datatable-button', compact('stock'))->render();
        })
        ->addColumn('status', function ($stock) {
            $status= ($stock->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.stock.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findStockTypeById($id)
    {
        return StockType::find($id);
    }
}
