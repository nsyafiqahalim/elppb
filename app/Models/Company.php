<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ReferenceData\CompanyAddress;


use App\Models\Admin\CompanyType;
use App\User;

class Company extends Model
{
    public $guarded = ['id'];


    public $dates = [
        'expiry_date'
    ];

    private const ACTIVE = 1;

    public function address()
    {
        return $this->hasOne(CompanyAddress::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function companyType()
    {
        return $this->belongsTo(CompanyType::class, 'company_type_id');
    }
}
