<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public $guarded = ['id'];

    public $table = 'portal_faqs';
}
