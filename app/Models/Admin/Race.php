<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    public $guarded = ['id'];
}
