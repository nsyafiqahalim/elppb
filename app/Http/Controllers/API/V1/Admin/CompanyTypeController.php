<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\CompanyTypeDataObject;

class CompanyTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addCompanyType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $companyType = CompanyTypeDataObject::addCompanyType($param);

        return response()->json([
            'message'   =>  'Jenis syarikat berjaya ditambah',
            'data'      =>  $companyType,
            'success'   =>  true,
            'route'     =>  route('admin.company_type.index')
        ], 200);
    }

    public function updateCompanyType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $companyType = CompanyTypeDataObject::updateCompanyType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis syarikat berjaya dikemaskini',
            'data'      =>  $companyType,
            'success'   =>  true,
            'route'     =>  route('admin.company_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return CompanyTypeDataObject::findAllCompanyTypeUsingDatatableFormat($param);
    }
}
