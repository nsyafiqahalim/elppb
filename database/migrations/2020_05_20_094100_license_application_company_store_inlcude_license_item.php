<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyStoreInlcudeLicenseItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_store_include_license_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('license_application_id')->index('lacsili_la_idx');
            $table->unsignedBigInteger('license_application_company_store_id')->index('lacsili_lacs_idx');
            $table->unsignedBigInteger('license_application_include_license_item_id')->index('lacsili_laili_idx');
            $table->foreign('license_application_id','fk_lacsili_la')->references('id')->on('license_applications');
            $table->foreign('license_application_company_store_id','fk_lacsili_lacs')->references('id')->on('license_application_company_stores');
            $table->foreign('license_application_include_license_item_id','fk_lacsili_laili')->references('id')->on('license_application_include_license_items');
            $table->string('apply_load')->nullable();
            $table->timestamps();
        });

        Schema::create('approval_letter_application_store_include_approval_letter_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('approval_letter_application_id')->index('alacsili_ala_idx');
            $table->unsignedBigInteger('approval_letter_application_company_store_id')->index('alacsili_alacs_idx');
            $table->unsignedBigInteger('approval_letter_application_include_approval_letter_item_id')->index('alacsili_alaili_idx');
            $table->foreign('approval_letter_application_id','fk_alacsili_ala')->references('id')->on('approval_letter_applications');
            $table->foreign('approval_letter_application_company_store_id','fk_alacsili_alacs')->references('id')->on('approval_letter_application_company_stores');
            $table->foreign('approval_letter_application_include_approval_letter_item_id','fk_alacsili_alaili')->references('id')->on('approval_letter_application_include_approval_letter_items');
            $table->string('apply_load')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
