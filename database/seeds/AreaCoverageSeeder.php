<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

use App\Models\Admin\AreaCoverage;

class AreaCoverageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AreaCoverage::create([
            'district_id'   => '124',
            'branch_id'       => '1',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '126',
            'branch_id'       => '1',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '72',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '73',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '74',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '75',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '76',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '76',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '77',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '78',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '79',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '80',
            'branch_id'       => '2',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '37',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '38',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '39',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '40',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '41',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '42',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '43',
            'branch_id'       => '3',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '34',
            'branch_id'       => '4',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '35',
            'branch_id'       => '4',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '36',
            'branch_id'       => '4',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '1',
            'branch_id'       => '5',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '3',
            'branch_id'       => '5',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '6',
            'branch_id'       => '5',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '8',
            'branch_id'       => '5',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '10',
            'branch_id'       => '5',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '2',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '4',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '5',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '7',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '9',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '11',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '12',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '13',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '14',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '15',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '16',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '17',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '18',
            'branch_id'       => '6',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '19',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '20',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '21',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '22',
            'branch_id'       => '7',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '71',
            'branch_id'       => '8',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '55',
            'branch_id'       => '9',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '56',
            'branch_id'       => '9',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '57',
            'branch_id'       => '9',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '58',
            'branch_id'       => '9',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '59',
            'branch_id'       => '9',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '60',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '61',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '62',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '63',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '64',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '65',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '66',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '67',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '68',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '69',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '70',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '47',
            'branch_id'       => '11',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '49',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '52',
            'branch_id'       => '10',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '53',
            'branch_id'       => '11',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '44',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '45',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '46',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '48',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '50',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '51',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '54',
            'branch_id'       => '12',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '81',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '82',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '83',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '84',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '85',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '86',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '87',
            'branch_id'       => '13',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '23',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '24',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '25',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '26',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '27',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '28',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '29',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '30',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '31',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '32',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '33',
            'branch_id'       => '14',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '123',
            'branch_id'       => '15',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '122',
            'branch_id'       => '15',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '113',
            'branch_id'       => '15',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '114',
            'branch_id'       => '15',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '116',
            'branch_id'       => '16',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '121',
            'branch_id'       => '16',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '115',
            'branch_id'       => '17',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '118',
            'branch_id'       => '17',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '119',
            'branch_id'       => '17',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '96',
            'branch_id'       => '18',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '95',
            'branch_id'       => '18',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '94',
            'branch_id'       => '18',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '111',
            'branch_id'       => '18',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '104',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '100',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '90',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '88',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '108',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '105',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '92',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '89',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '107',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '102',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '109',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '112',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '93',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '101',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '103',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '91',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '125',
            'branch_id'       => '19',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '110',
            'branch_id'       => '20',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '98',
            'branch_id'       => '20',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '99',
            'branch_id'       => '20',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        AreaCoverage::create([
            'district_id'   => '97',
            'branch_id'       => '20',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
       
    }
}
