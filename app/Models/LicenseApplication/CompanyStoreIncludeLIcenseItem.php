<?php

namespace App\Models\LicenseApplication;

use Illuminate\Database\Eloquent\Model;

use App\Models\LicenseApplication;
use App\Models\LicenseApplication\IncludeLicenseItem;
use App\Models\LicenseApplication\CompanyStore;

class CompanyStoreIncludeLicenseItem extends Model
{
    public $guarded = ['id'];
    public $table = 'license_application_store_include_license_items';


    public function licenseApplication()
    {
        return $this->belongsTo(LicenseApplication::class, 'license_application_id');
    }

    public function companyStores()
    {
        return $this->belongsTo(CompanyStore::class, 'license_application_company_store_id');
    }

    public function includeLicenseItem()
    {
        return $this->belongsTo(IncludeLicenseItem::class, 'license_application_include_license_item_id');
    }
}
