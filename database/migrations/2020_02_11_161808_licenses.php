<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Licenses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('license_number')->nullable();
            $table->date('expiry_date')->nullable();

            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('branch_id')->index()->nullable();
            $table->unsignedBigInteger('license_type_id')->index();
            $table->unsignedBigInteger('applicant_id')->nullable()->index();
            $table->unsignedBigInteger('license_application_type_id')->index();
            $table->unsignedBigInteger('license_application_id')->index();

            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('license_type_id')->references('id')->on('license_types');
            $table->foreign('applicant_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('license_application_type_id')->references('id')->on('license_application_types');
            $table->foreign('license_application_id')->references('id')->on('license_applications');

            $table->timestamps();
        });

        Schema::create('approval_letters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('approval_letter_number')->nullable();
            $table->date('expiry_date')->nullable();

            $table->unsignedBigInteger('status_id')->index();
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('branch_id')->index()->nullable();
            $table->unsignedBigInteger('approval_letter_type_id')->index();
            $table->unsignedBigInteger('applicant_id')->nullable()->index();
            $table->unsignedBigInteger('approval_letter_application_type_id')->index();
            $table->unsignedBigInteger('approval_letter_application_id')->index();

            $table->foreign('status_id')->references('id')->on('statuses');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('approval_letter_type_id')->references('id')->on('approval_letter_types');
            $table->foreign('applicant_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('approval_letter_application_type_id')->references('id')->on('approval_letter_application_types');
            $table->foreign('approval_letter_application_id')->references('id')->on('approval_letter_applications');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
