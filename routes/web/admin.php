<?php

Route::group(['middleware' => 'auth'], function () {
    Route::prefix('pentadbiran-sistem')->group(function () {
        Route::get('/', 'Admin\DashboardController@index')->name('admin.index');

        Route::prefix('data-rujukan')->group(function () {
            Route::prefix('jenis-syarikat')->group(function () {
                Route::get('/tambah', 'Admin\CompanyTypeController@add')->name('admin.company_type.add');
                Route::get('/kemaskini/{id}', 'Admin\CompanyTypeController@edit')->name('admin.company_type.edit');
                Route::get('/', 'Admin\CompanyTypeController@index')->name('admin.company_type.index');
            });

            Route::prefix('jenis-bangunan')->group(function () {
                Route::get('/tambah', 'Admin\BuildingTypeController@add')->name('admin.building_type.add');
                Route::get('/kemaskini/{id}', 'Admin\BuildingTypeController@edit')->name('admin.building_type.edit');
                Route::get('/', 'Admin\BuildingTypeController@index')->name('admin.building_type.index');
            });

            Route::prefix('jenis-premis')->group(function () {
                Route::get('/tambah', 'Admin\PremiseTypeController@add')->name('admin.premise_type.add');
                Route::get('/kemaskini/{id}', 'Admin\PremiseTypeController@edit')->name('admin.premise_type.edit');
                Route::get('/', 'Admin\PremiseTypeController@index')->name('admin.premise_type.index');
            });

            Route::prefix('jenis-perniagaan')->group(function () {
                Route::get('/tambah', 'Admin\BusinessTypeController@add')->name('admin.business_type.add');
                Route::get('/kemaskini/{id}', 'Admin\BusinessTypeController@edit')->name('admin.business_type.edit');
                Route::get('/', 'Admin\BusinessTypeController@index')->name('admin.business_type.index');
            });

            Route::prefix('jenis-tanah')->group(function () {
                Route::get('/tambah', 'Admin\LandTypeController@add')->name('admin.land_type.add');
                Route::get('/kemaskini/{id}', 'Admin\LandTypeController@edit')->name('admin.land_type.edit');
                Route::get('/', 'Admin\LandTypeController@index')->name('admin.land_type.index');
            });

            Route::prefix('jenis-permit')->group(function () {
                Route::get('/tambah', 'Admin\PermitTypeController@add')->name('admin.permit_type.add');
                Route::get('/kemaskini/{id}', 'Admin\PermitTypeController@edit')->name('admin.permit_type.edit');
                Route::get('/', 'Admin\PermitTypeController@index')->name('admin.permit_type.index');
            });

            Route::prefix('jenis-gudang')->group(function () {
                Route::get('/tambah', 'Admin\StoreTypeController@add')->name('admin.store_type.add');
                Route::get('/kemaskini/{id}', 'Admin\StoreTypeController@edit')->name('admin.store_type.edit');
                Route::get('/', 'Admin\StoreTypeController@index')->name('admin.store_type.index');
            });

            Route::prefix('jenis-beras')->group(function () {
                Route::get('/tambah', 'Admin\RiceTypeController@add')->name('admin.rice-type.add');
                Route::get('/kemaskini/{id}', 'Admin\RiceTypeController@edit')->name('admin.rice-type.edit');
                Route::get('/', 'Admin\RiceTypeController@index')->name('admin.rice-type.index');
            });

            Route::prefix('jenis-kilang')->group(function () {
                Route::get('/tambah', 'Admin\FactoryTypeController@add')->name('admin.factory_type.add');
                Route::get('/kemaskini/{id}', 'Admin\FactoryTypeController@edit')->name('admin.factory_type.edit');
                Route::get('/', 'Admin\FactoryTypeController@index')->name('admin.factory_type.index');
            });

            Route::prefix('jenis-lesen')->group(function () {
                Route::get('/tambah', 'Admin\LicenseTypeController@add')->name('admin.license_type.add');
                Route::get('/kemaskini/{id}', 'Admin\LicenseTypeController@edit')->name('admin.license_type.edit');
                Route::get('/', 'Admin\LicenseTypeController@index')->name('admin.license_type.index');
            });

            Route::prefix('status-tanah')->group(function () {
                Route::get('/tambah', 'Admin\LandStatusController@add')->name('admin.land-status.add');
                Route::get('/kemaskini/{id}', 'Admin\LandStatusController@edit')->name('admin.land-status.edit');
                Route::get('/', 'Admin\LandStatusController@index')->name('admin.land-status.index');
            });

            Route::prefix('kategori-beras')->group(function () {
                Route::get('/tambah', 'Admin\RiceCategoryController@add')->name('admin.rice-category.add');
                Route::get('/kemaskini/{id}', 'Admin\RiceCategoryController@edit')->name('admin.rice-category.edit');
                Route::get('/', 'Admin\RiceCategoryController@index')->name('admin.rice-category.index');
            });

            Route::prefix('dun')->group(function () {
                Route::get('/tambah', 'Admin\DunController@add')->name('admin.dun.add');
                Route::get('/kemaskini/{id}', 'Admin\DunController@edit')->name('admin.dun.edit');
                Route::get('/', 'Admin\DunController@index')->name('admin.dun.index');
            });

            Route::prefix('zon')->group(function () {
                Route::get('/tambah', 'Admin\ZoneController@add')->name('admin.zone.add');
                Route::get('/kemaskini/{id}', 'Admin\ZoneController@edit')->name('admin.zone.edit');
                Route::get('/', 'Admin\ZoneController@index')->name('admin.zone.index');
            });

            Route::prefix('daerah')->group(function () {
                Route::get('/tambah', 'Admin\DistrictController@add')->name('admin.district.add');
                Route::get('/kemaskini/{id}', 'Admin\DistrictController@edit')->name('admin.district.edit');
                Route::get('/', 'Admin\DistrictController@index')->name('admin.district.index');
            });

            Route::prefix('liputan-kawasan')->group(function () {
                Route::get('/tambah', 'Admin\AreaCoverageController@add')->name('admin.area-coverage.add');
                Route::get('/kemaskini/{id}', 'Admin\AreaCoverageController@edit')->name('admin.area-coverage.edit');
                Route::get('/', 'Admin\AreaCoverageController@index')->name('admin.area-coverage.index');
            });

            Route::prefix('negeri')->group(function () {
                Route::get('/tambah', 'Admin\StateController@add')->name('admin.state.add');
                Route::get('/kemaskini/{id}', 'Admin\StateController@edit')->name('admin.state.edit');
                Route::get('/', 'Admin\StateController@index')->name('admin.state.index');
            });

            Route::prefix('cawangan')->group(function () {
                Route::get('/tambah', 'Admin\BranchController@add')->name('admin.branch.add');
                Route::get('/kemaskini/{id}', 'Admin\BranchController@edit')->name('admin.branch.edit');
                Route::get('/', 'Admin\BranchController@index')->name('admin.branch.index');
            });

            Route::prefix('parlimen')->group(function () {
                Route::get('/tambah', 'Admin\ParliamentController@add')->name('admin.parliament.add');
                Route::get('/kemaskini/{id}', 'Admin\ParliamentController@edit')->name('admin.parliament.edit');
                Route::get('/', 'Admin\ParliamentController@index')->name('admin.parliament.index');
            });

            Route::prefix('status')->group(function () {
                Route::get('/tambah', 'Admin\StatusController@add')->name('admin.status.add');
                Route::get('/kemaskini/{id}', 'Admin\StatusController@edit')->name('admin.status.edit');
                Route::get('/', 'Admin\StatusController@index')->name('admin.status.index');
            });

            Route::prefix('rakan-kongsi')->group(function () {
                Route::get('/tambah', 'Admin\PartnerTypeController@add')->name('admin.partner-type.add');
                Route::get('/kemaskini/{id}', 'Admin\PartnerTypeController@edit')->name('admin.partner-type.edit');
                Route::get('/', 'Admin\PartnerTypeController@index')->name('admin.partner-type.index');
            });

            Route::prefix('gred-beras')->group(function () {
                Route::get('/tambah', 'Admin\RiceGradeController@add')->name('admin.rice-grade.add');
                Route::get('/kemaskini/{id}', 'Admin\RiceGradeController@edit')->name('admin.rice-grade.edit');
                Route::get('/', 'Admin\RiceGradeController@index')->name('admin.rice-grade.index');
            });

            Route::prefix('bangsa')->group(function () {
                Route::get('/tambah', 'Admin\RaceController@add')->name('admin.race.add');
                Route::get('/kemaskini/{id}', 'Admin\RaceController@edit')->name('admin.race.edit');
                Route::get('/', 'Admin\RaceController@index')->name('admin.race.index');
            });

            Route::prefix('jenis-stok')->group(function () {
                Route::get('/tambah', 'Admin\StockController@add')->name('admin.stock.add');
                Route::get('/kemaskini/{id}', 'Admin\StockController@edit')->name('admin.stock.edit');
                Route::get('/', 'Admin\StockController@index')->name('admin.stock.index');
            });

            Route::prefix('status-pemilikan')->group(function () {
                Route::get('/tambah', 'Admin\OwnershipStatusController@add')->name('admin.ownership-status.add');
                Route::get('/kemaskini/{id}', 'Admin\OwnershipStatusController@edit')->name('admin.ownership-status.edit');
                Route::get('/', 'Admin\OwnershipStatusController@index')->name('admin.ownership-status.index');
            });

            Route::prefix('bayaran')->group(function () {
                Route::get('/tambah', 'Admin\PaymentTypeController@add')->name('admin.payment-type.add');
                Route::get('/kemaskini/{id}', 'Admin\PaymentTypeController@edit')->name('admin.payment-type.edit');
                Route::get('/', 'Admin\PaymentTypeController@index')->name('admin.payment-type.index');
            });

            Route::prefix('jenis-bangsa')->group(function () {
                Route::get('/tambah', 'Admin\RaceTypeController@add')->name('admin.race-type.add');
                Route::get('/kemaskini/{id}', 'Admin\RaceTypeController@edit')->name('admin.race-type.edit');
                Route::get('/', 'Admin\RaceTypeController@index')->name('admin.race-type.index');
            });
        });

        Route::prefix('pengguna-dan-peranan')->group(function () {
            Route::prefix('pengguna')->group(function () {
                Route::get('/tambah', 'Admin\UserController@add')->name('admin.user.add');
                Route::get('/kemaskini/{id}', 'Admin\UserController@edit')->name('admin.user.edit');
                Route::get('/', 'Admin\UserController@index')->name('admin.user.index');
            });

            Route::prefix('peranan')->group(function () {
                Route::get('/tambah', 'Admin\RoleController@add')->name('admin.role.add');
                Route::get('/kemaskini/{id}', 'Admin\RoleController@edit')->name('admin.role.edit');
                Route::get('/', 'Admin\RoleController@index')->name('admin.role.index');
            });
        });

        Route::prefix('penetapan-sistem')->group(function () {
            Route::prefix('kemaskini')->group(function () {
                Route::get('/{setting}', 'Admin\SettingController@index')->name('admin.setting.index');
            });
        });
    });
});
