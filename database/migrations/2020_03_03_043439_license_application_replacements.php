<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationReplacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_replacements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('license_application_replacement_type_id')->nullable()->index('lar_lart_idx');
            $table->unsignedBigInteger('license_application_id')->index('lar_la_idx');
            $table->foreign('license_application_id', 'fk_lar_la')->references('id')->on('license_applications');
            $table->foreign('license_application_replacement_type_id', 'fk_larc_lart')->references('id')->on('license_application_replacement_types');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
