<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('license_appliclicense_application_store_changes');
        Schema::dropIfExists('license_application_company_premise_changes');
        Schema::dropIfExists('license_application_company_changes');
        Schema::dropIfExists('license_applications_changes');

        Schema::create('license_application_changes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('original_load')->nullable();
            $table->string('company_name')->nullable();
            $table->unsignedBigInteger('license_application_company_id')->index('lac_lac_idx')->nullable();
            $table->foreign('license_application_company_id','fk_lac_lac')->references('id')->on('license_application_companies');
            $table->unsignedBigInteger('license_application_company_store_id')->index('lac_lacs_idx')->nullable();
            $table->foreign('license_application_company_store_id','fk_lac_lacs')->references('id')->on('license_application_company_stores');
            $table->unsignedBigInteger('license_application_company_premise_id')->index('lac_lacp_idx')->nullable();
            $table->foreign('license_application_company_premise_id','fk_lac_lacp')->references('id')->on('license_application_company_premises');

            $table->unsignedBigInteger('license_application_id')->index('lacc_la_idx');
            $table->foreign('license_application_id','fk_lacc_la')->references('id')->on('license_applications');
            $table->unsignedBigInteger('license_application_change_type_id')->index('lacc_lact_idx');
            $table->foreign('license_application_change_type_id','fk_lacc_lact')->references('id')->on('license_application_change_types');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
