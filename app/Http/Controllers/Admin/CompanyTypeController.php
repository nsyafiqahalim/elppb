<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\CompanyTypeDataObject;

class CompanyTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.company-type.index');
    }

    public function add()
    {
        return view('admin.company-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $companyType = CompanyTypeDataObject::findCompanyTypeById($decryptedId);

        return view('admin.company-type.edit', compact('companyType'));
    }
}
