<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyFactories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_factories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('factory_type_id');
            $table->unsignedBigInteger('land_status_id');
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('state_id');
            $table->unsignedBigInteger('factory_ownership_type_id');

            $table->string('factory_type');
            $table->string('land_status');
            $table->string('company');
            $table->string('state');
            $table->string('factory_ownership_type');

            $table->string('name', 255);
            $table->float('land_width', 8, 2);
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->string('phone_number');
            $table->string('fax_number')->nullable();
            $table->string('email');
            $table->enum('type', ['MAILING'])->default('MAILING');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
