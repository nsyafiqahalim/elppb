<?php

namespace App\Http\Middleware\Admin;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreUserValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'login_id' => ['required', 'string', 'max:255', 'unique:users,login_id'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], [
            'name.required' => ['Medan Nama diperlukan'],
            'email.unique' => ['Emel sudah direkokdkan didalam sistem'],
            'email.required' => ['Medan Emel diperlukan'],
            'login_id.unique' => ['ID Pengguna sudah direkokdkan didalam sistem'],
            'login_id.required' => ['Medan ID Pengguna diperlukan'],
            'password.required' => ['Medan Kata laluan diperlukan'],
            'password.confirmed' => ['Kata laluan dan pengesahan kata laluan tidak sama'],
        ]);

        return $next($request);
    }
}
