<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\RiceCategory;

class RiceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Beras Tempatan',
          'Beras Import',
      ];

        foreach ($data as $item) {
            RiceCategory::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'is_active' => 1,
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
          ]);
        }
    }
}
