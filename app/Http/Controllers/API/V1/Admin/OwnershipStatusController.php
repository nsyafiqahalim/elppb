<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\OwnershipStatusDataObject;

class OwnershipStatusController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addOwnershipStatus(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $ownershipStatus = OwnershipStatusDataObject::addOwnershipStatus($param);

        return response()->json([
            'message'   =>  'Status Pemilikan berjaya ditambah',
            'data'      =>  $ownershipStatus,
            'success'   =>  true,
            'route'     =>  route('admin.ownership-status.index')
        ], 200);
    }

    public function updateOwnershipStatus(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $ownershipStatus = OwnershipStatusDataObject::updateOwnershipStatus($param, $decryptedId);

        return response()->json([
            'message'   =>  'Status Pemilikan berjaya dikemaskini',
            'data'      =>  $ownershipStatus,
            'success'   =>  true,
            'route'     =>  route('admin.ownership-status.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return OwnershipStatusDataObject::findAllOwnershipStatusUsingDatatableFormat($param);
    }
}
