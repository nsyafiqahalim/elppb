<?php

use Illuminate\Database\Seeder;

use App\Models\Portal\Setting;

class PortalSettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      // Ketua Pengarah
        Setting::create([
          'label'     =>  'Gambar Ketua Pengarah',
          'code'      =>  'director_images',
          'domain'    =>  'DIRECTOR',
          'type'      =>  'IMAGE',
          'value'     =>  'img/team/ketua-pengarah-baru.png',
          'metric'    =>  'Format Gambar',
          'readonly'  =>  "false",
          'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Kata Aluan Ketua Pengarah',
            'code'      =>  'director_remarks',
            'domain'    =>  'DIRECTOR',
            'type'      =>  'TEXTAREA',
            'value'     =>  '
                <p>
                    Pertama sekali saya ingin mengucapkan Selamat Datang dan Terima Kasih kerana melayari Laman Web Sistem Lesen Padi Permit dan Beras (eLPPB), Kawalselia Padi dan Beras (KPB), Kementerian Pertanian dan Industri Asas Tani.
                </p>
                <p>
                    KPB adalah merupakan sebuah bahagian yang menguatkuasakan Akta Kawalan Padi dan Beras 1994 (Akta 522) dan peraturan-peraturan di bawahnya meliputi tugas Pelesenan, Penguatkuasaan dan Pendakwaan.
                </p>
                <p>
                    Laman web ini adalah salah satu saluran untuk memperkenalkan Kawalselia Padi dan Beras kepada masyarakat umum dan merupakan medium utama bagi maklumat berkaitan lesen yang dikawal di bawah Kawalselia Padi dan Beras. Semoga maklumat yang dimuatkan dalam laman ini dapat memenuhi keperluan dan membantu anda semua.
                </p>
                <p>
                    Akhir kata, saya mengalu-alukan pengunjung mengemukakan sebarang cadangan bagi menambahbaik penampilan dan mempertingkatkan lagi keberkesanan laman web ini. Sebarang maklumbalas, pandangan dan saranan boleh diajukan supaya laman web ini terus menjadi medium yang berkesan dan bermanfaat.
                </p>',
            'metric'    =>  'Teks Bebas',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

          Setting::create([
              'label'     =>  'Nama Ketua Pengarah',
              'code'      =>  'director_name',
              'domain'    =>  'DIRECTOR',
              'value'     =>  'Azman Bin Mahmood',
              'metric'    =>  'Teks Bebas',
              'type'      =>  'TEXT',
              'readonly'  =>  "false",
              'class'     =>  null,
          ]);

            Setting::create([
                'label'     =>  'Pingat',
                'code'      =>  'director_medal',
                'domain'    =>  'DIRECTOR',
                'value'     =>  'KMW, PPC',
                'metric'    =>  'Teks Bebas',
                'type'      =>  'TEXT',
                'readonly'  =>  "false",
                'class'     =>  null,
            ]);

        // Sejarah Penubuhan
        Setting::create([
            'label'     =>  'Gambar',
            'code'      =>  'info_1_image',
            'domain'    =>  'INFO',
            'type'      =>  'IMAGE',
            'value'     =>  'img/about/tangan-menadah-beras.png',
            'metric'    =>  'Format Gambar',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        // Akta & Peraturan
        Setting::create([
            'label'     =>  'Gambar',
            'code'      =>  'info_2_image',
            'domain'    =>  'INFO2',
            'type'      =>  'IMAGE',
            'value'     =>  '/img/about/about.png',
            'metric'    =>  'Format Gambar',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        // Peranan & Fungsi
        Setting::create([
            'label'     =>  'Peranan atau Fungsi Seksyen Kawalselia Padi',
            'code'      =>  'SKPB_description',
            'domain'    =>  'SKPB',
            'type'      =>  'TEXTAREA',
            'value'     =>  '<p>
                Peranan atau fungsi KPB adalah termaktub di dalam Akta Kawalan Padi dan Beras 1994 (Akta 522) yang merangkumi tugas-tugas pelesenan, Penguatkuasaan dan pendakwaan. KPB terlibat secara langsung dalam menguatkuasakan Akta Kawalan Padi dan Beras 1994 (Akta 522) dan peraturan-peraturan di bawahnya.</p>
                <p>Seterusnya mengawal pengedaran padi, beras dan produk-produk sampingan padi dan beras melalui pengeluaran lesen, permit dan peraturan-peraturan.
                </p>
                <p>Selain itu, fungsi KPB adalah untuk memastikan dan menguatkuasakan harga beras yang berpatutan dan stabil bagi pengguna-pengguna serta membendung penyeludupan beras dengan menjalankan aktiviti pencegahan penyeludupan padi dan beras.
                </p>
                <p>Akhir sekali, Kawalselia Padi Beras bertanggungjawab dalam menyimpan dan menyenggara suatu bekalan padi dan beras yang mencukupi.
                </p>',
            'metric'    =>  'Teks Bebas',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);

        Setting::create([
            'label'     =>  'Gambar',
            'code'      =>  'SKPB_image',
            'domain'    =>  'SKPB',
            'type'      =>  'IMAGE',
            'value'     =>  'img/case/kes-6.jpg',
            'metric'    =>  'Format Gambar',
            'readonly'  =>  "false",
            'class'     =>  null,
        ]);
    }
}
