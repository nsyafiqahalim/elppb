<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\RiceGrade;

class RiceGradeDataObject
{
    public static function addRiceGrade($param)
    {
        $riceGrade = null;

        DB::transaction(function () use ($param, &$riceGrade) {
            $riceGrade = RiceGrade::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $riceGrade;
    }

    public static function updateRiceGrade($param, $id)
    {
        $riceGrade = null;

        DB::transaction(function () use ($param, $id,  &$riceGrade) {
            $riceGrade = RiceGrade::find($id);

            $riceGrade->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $riceGrade;
    }

    public static function findAllRiceGradeUsingDatatableFormat($param)
    {
        $riceGrade = RiceGrade::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($riceGrade)
        ->addIndexColumn()
        ->addColumn('action', function ($riceGrade) {
            return view('admin.rice-grade.partials.datatable-button', compact('riceGrade'))->render();
        })
        ->addColumn('status', function ($riceGrade) {
            $status= ($riceGrade->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.rice-grade.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findRiceGradeById($id)
    {
        return RiceGrade::find($id);
    }
}
