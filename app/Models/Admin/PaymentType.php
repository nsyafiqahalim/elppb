<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    public $guarded = ['id'];
}
