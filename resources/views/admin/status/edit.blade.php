@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Pentadbiran Sistem</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Data Rujukan</li>
            <li class="breadcrumb-item"><a href="{{ route ('admin.status.index') }}">Status</a></li>
            <li class="breadcrumb-item active">Kemaskini</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="ti-pencil-alt"></i> Kemaskini Status</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.status.update',encrypt($status->id)) }}">
                        @method('PATCH')
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label"><span style="color: red;">*</span> Nama</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value='{{ $status->name ?? "" }}'>
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="code_label"><span style="color: red;">*</span> Kod</label>
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Kod" value='{{ $status->code ?? "" }}'>
                                    </div>
                                </div>

                                <div class="col-md-12 col-lg-12 col-xs-12">
                                    <div class="form-group">
                                        <label id="description_label"><span style="color: red;">*</span> Keterangan</label>
                                        <textarea id="description" class="form-control border-primary" rows="10" name="description">{{ $status->description ?? "" }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route ('admin.status.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
