<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyStockStatementAccount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_stock_statement_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('initial_stock')->default(0);
            $table->float('balance_stock')->default(0);
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('current_stock_statement_period_id')->nullable()->index('fcsta_stp_idx');
            $table->unsignedBigInteger('license_type_id')->nullable()->index('fcsta_lt_idx');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('current_stock_statement_period_id','fk_csta_stp')->references('id')->on('stock_statement_periods');
            $table->foreign('license_type_id','fk_fcsta_lt')->references('id')->on('license_types');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
