<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\License;

class LicenseDataObject
{
    public static function storeNewLicense($param)
    {
        return License::create([
            'title'            =>  $param['title'],
            'description'      =>  $param['description'],
            'is_published'     =>  $param['is_published'],
            'created_by'       =>  $param['created_by'],
            'updated_by'       =>  $param['updated_by'],
        ]);
    }

    public static function updateLicense($param, $id)
    {
        return License::find($id)->update([
            'title'            =>  $param['title'],
            'description'      =>  $param['description'],
            'is_published'     =>  $param['is_published'],
            'updated_by'       =>  $param['updated_by'],
        ]);
    }

    public static function deleteLicense($id)
    {
        return License::find($id)->delete();
    }

    public static function findAllLicenseUsingDatatableFormat($param)
    {
        $licenses = License::distinct()->orderby('id', 'desc')
                        ->when(isset($param['title']) && $param['title'], function ($query) use ($param) {
                            $query->where('title', 'like', '%'.$param['title'].'%');
                        })->when(isset($param['is_published']), function ($query) use ($param) {
                            if ($param['is_published'] == 1) {
                                $query->where('is_published', 1);
                            } elseif ($param['is_published'] == 0) {
                                $query->where('is_published', '<>', 1)->whereNotNull('is_published');
                            }
                        });

        return datatables()->of($licenses)
        ->addIndexColumn()
         ->addColumn('published', function ($license) {
             $status = ($license->is_published == 1)? 'Diterbitkan' : 'Tidak Diterbitkan';

             return View('portal.license.partials.status', compact('status'))->render();
         })
        ->addColumn('action', function ($license) {
            $encryptedId = encrypt($license->id);

            return View('portal.license.partials.datatable-button', compact('encryptedId'))->render();
        })
        ->rawColumns(['published','action'])
       ->make(true);
    }

    public static function findLicenseById($id)
    {
        return License::find($id);
    }
}
