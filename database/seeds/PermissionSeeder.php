<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branchLevels = [
            'Ibu Pejabat',
            'Cawangan',
        ];

        $modules = [
            'Jenis Premis',
            'Jenis Bangunan',
            'Jenis Lesen',
            'Cawangan',
            'Daerah',
            'Negeri',
            'Kawasan Jagaan',
        ];

        foreach ($modules as $module) {
            Permission::insert([
                    //kes
                    ['name' => 'Papar '.$module, 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Tambah '.$module, 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Kemaskini '.$module, 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Lihat '.$module, 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Hapus '.$module, 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ]);
        }

        $modules = [
            'Lesen Runcit'
        ];

        foreach ($branchLevels as $branchLevel) {
            foreach ($modules as $module) {
                Permission::insert([
                    ['name' => 'Permohonan Baharu '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Siasatan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Pemilihan Pegawai Siasatan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Peraku Cawangan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Kelulusan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ]);
            }
        }

        $modules = [
            'Lesen Borong',
            'Lesen Import',
            'Lesen Export',
            'Lesen Beli Padi',
            'Lesen Kilang Padi Komersial'
        ];

        foreach ($branchLevels as $branchLevel) {
            foreach ($modules as $module) {
                Permission::insert([
                    ['name' => 'Permohonan Baharu '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Pemilihan Pegawai Siasatan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    //['name' => 'Peraku Cawangan Permohonan'.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Siasatan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Semakan Siasatan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Peraku Ibu Pejabat Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                    ['name' => 'Kelulusan Permohonan '.$module.' ('.$branchLevel.')', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
                ]);
            }
        }

        Permission::insert([
                ['name' => 'Jana Lesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ]);
    }
}
