<?php

namespace App\DataObjects\Portal;

use DB;

use App\Models\Portal\Permit;

class PermitDataObject
{
    public static function storeNewPermit($param)
    {
        return Permit::create([
            'title'            =>  $param['title'],
            'description'      =>  $param['description'],
            'is_published'     =>  $param['is_published'],
            'created_by'       =>  $param['created_by'],
            'updated_by'       =>  $param['updated_by'],
        ]);
    }

    public static function updatePermit($param, $id)
    {
        return Permit::find($id)->update([
            'title'            =>  $param['title'],
            'description'      =>  $param['description'],
            'is_published'     =>  $param['is_published'],
            'updated_by'       =>  $param['updated_by'],
        ]);
    }

    public static function deletePermit($id)
    {
        return Permit::find($id)->delete();
    }

    public static function findAllPermitUsingDatatableFormat($param)
    {
        $permits = Permit::distinct()->orderby('id', 'desc')
                        ->when(isset($param['title']) && $param['title'], function ($query) use ($param) {
                            $query->where('title', 'like', '%'.$param['title'].'%');
                        })->when(isset($param['is_published']), function ($query) use ($param) {
                            if ($param['is_published'] == 1) {
                                $query->where('is_published', 1);
                            } elseif ($param['is_published'] == 0) {
                                $query->where('is_published', '<>', 1)->whereNotNull('is_published');
                            }
                        });

        return datatables()->of($permits)
        ->addIndexColumn()
         ->addColumn('published', function ($permit) {
             $status = ($permit->is_published == 1)? 'Diterbitkan' : 'Tidak Diterbitkan';

             return View('portal.permit.partials.status', compact('status'))->render();
         })
        ->addColumn('action', function ($permit) {
            $encryptedId = encrypt($permit->id);

            return View('portal.permit.partials.datatable-button', compact('encryptedId'))->render();
        })
        ->rawColumns(['published','action'])
       ->make(true);
    }

    public static function findpermitById($id)
    {
        return Permit::find($id);
    }
}
