@extends('layouts.no-menu-layout')

@section('content')
<div class="container">
  <h3 class="box-title mb-3" style="text-align:center">Sistem Lesen Permit Padi Dan Beras 2.0</h3>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-warning">Sila sahkan e-mel anda</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Emel verifikasi yang baru telah berjaya dihantar semula.') }}
                        </div>
                    @endif

                    {{ __('Sila periksa email anda untuk e-mel verifikasi yang telah dihantar.') }}
                    {{ __('Jika anda masih belum mendapat email yang verifikasi yang dihantar') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Sila klik disini untuk penghantaran semula') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
