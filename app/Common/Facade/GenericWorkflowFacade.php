<?php

namespace App\Common\Facade;

use Illuminate\Support\Facades\Facade;

class GenericWorkflowFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'App\Common\Functions\GenericWorkflow';
    }
}