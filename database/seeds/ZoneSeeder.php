<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Zone;

class ZoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Zon Utara',
            'Zon Tengah',
            'Zon Selatan',
            'Zon Timur',
            'Zon SQL',
        ];

        foreach ($data as $item) {
            Zone::create([
                'name'  =>  $item,
                'code'  =>  strtoupper(str_replace(' ', '_', $item)),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item,
                'is_active' => 1
            ]);
        }
    }
}
