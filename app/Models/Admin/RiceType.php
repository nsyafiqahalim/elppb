<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class RiceType extends Model
{
    public $guarded = ['id'];
}
