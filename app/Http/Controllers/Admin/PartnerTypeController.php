<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\PartnerTypeDataObject;

class PartnerTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.partner-type.index');
    }

    public function add()
    {
        return view('admin.partner-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $partnerType = PartnerTypeDataObject::findPartnerTypeById($decryptedId);

        return view('admin.partner-type.edit', compact('partnerType'));
    }
}
