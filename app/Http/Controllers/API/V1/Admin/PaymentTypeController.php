<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\PaymentTypeDataObject;

class PaymentTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addPaymentType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $paymentType = PaymentTypeDataObject::addPaymentType($param);

        return response()->json([
            'message'   =>  'Jenis Bayaran berjaya ditambah',
            'data'      =>  $paymentType,
            'success'   =>  true,
            'route'     =>  route('admin.payment-type.index')
        ], 200);
    }

    public function updatePaymentType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $paymentType = PaymentTypeDataObject::updatePaymentType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Bayaran berjaya dikemaskini',
            'data'      =>  $paymentType,
            'success'   =>  true,
            'route'     =>  route('admin.payment-type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return PaymentTypeDataObject::findAllPaymentTypeUsingDatatableFormat($param);
    }
}
