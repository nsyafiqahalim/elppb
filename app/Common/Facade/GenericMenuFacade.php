<?php

namespace App\Common\Facade;

use Illuminate\Support\Facades\Facade;

class GenericMenuFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'App\Common\Functions\GenericMenu';
    }
}