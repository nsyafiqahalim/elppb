<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use Carbon\Carbon;

use App\Models\CompanyPremise;
use App\Models\Admin\BusinessType;
use App\Models\Admin\BuildingType;
use App\Models\Admin\Dun;
use App\Models\Admin\Parliament;
use App\Models\Admin\District;
use App\Models\Admin\State;

$districts = District::all();
$states = State::all();
$businessTypes = BusinessType::all();
$buildingTypes = BuildingType::all();
$duns = Dun::all();
$parliaments = Parliament::all();

$factory->define(CompanyPremise::class, function (Faker $faker) use ($districts, $states, $businessTypes, $buildingTypes, $duns, $parliaments) {
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Address($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Company($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Miscellaneous($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Payment($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\Person($faker));
    $faker->addProvider(new App\Common\FakerProvider\ms_MY\PhoneNumber($faker));

    $dist = $districts->random();
    $stat = $states->random();
    $bt = $businessTypes->random();
    $bt2 = $buildingTypes->random();
    $d = $duns->random();
    $p = $parliaments->random();

    return [
            'business_type_id'          =>  $bt->id,
            'building_type_id'          =>  $bt2->id,
            'dun_id'                    =>  $d->id,
            'parliament_id'             =>  $p->id,
            'address_1'                 =>  $faker->buildingPrefix . ' ' . $faker->buildingNumber . ' ' . $faker->streetName,
            'address_2'                 =>  $faker->township,
            'address_3'                 =>  $faker->townState,
            'district_id'               =>  $dist->id,
            'postcode'                  =>  $faker->numberBetween(10000, 99999),
            'state_id'                  =>  $stat->id,
            'phone_number'              =>  $faker->phoneNumber,
            'email'                     =>  $faker->unique()->safeEmail,
    ];
});
