<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationCompanyStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_company_stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('building_type_id')->nullable()->index();
            $table->unsignedBigInteger('store_ownership_type_id')->index('fk_larc_la');
            $table->unsignedBigInteger('state_id')->nullable()->index();
            $table->unsignedBigInteger('district_id')->nullable()->index();
            $table->unsignedBigInteger('company_store_id')->nullable()->index();
            $table->unsignedBigInteger('license_application_id')->index('larc_la_idx');
            //$table->unsignedBigInteger('license_application_include_license_item_id')->index('larc_laili_idx');
            $table->string('store_ownership_type_name')->nullable();
            $table->string('building_type_name')->nullable();
            $table->string('state_name')->nullable();
            $table->string('district_name')->nullable();
            $table->tinyInteger('is_store_validate')->nullable();

            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('fax_number')->nullable();
            $table->string('email')->nullable();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('company_store_id')->references('id')->on('company_stores');
            //$table->foreign('license_application_include_license_item_id','fk_larc_laili')->references('id')->on('license_application_include_license_items');
            

            $table->foreign('store_ownership_type_id', 'fk_lacr_sot')->references('id')->on('store_ownership_types');
            $table->foreign('building_type_id')->references('id')->on('building_types');
            $table->foreign('license_application_id', 'fk_lacr_la')->references('id')->on('license_applications');
            $table->timestamps();
        });

        Schema::create('approval_letter_application_company_stores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('company_id')->index();
            $table->unsignedBigInteger('building_type_id')->nullable()->index('larc_bt2_idx');
            $table->unsignedBigInteger('store_ownership_type_id')->nullable()->index('larc_sot_idx');
            $table->unsignedBigInteger('state_id')->nullable()->index('larc_s_idx');
            $table->unsignedBigInteger('district_id')->nullable()->index('larc_d2_idx');
            $table->unsignedBigInteger('company_store_id')->index('larc_cs_idx');
            $table->unsignedBigInteger('approval_letter_application_id')->nullable()->index('larc_la_idx');
            $table->string('store_ownership_type_name')->nullable();
            $table->string('building_type_name')->nullable();
            $table->string('state_name')->nullable();
            $table->string('district_name')->nullable();

            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('fax_number')->nullable();
            $table->string('email')->nullable();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('district_id', 'fk_lacr_d')->references('id')->on('districts');
            $table->foreign('company_store_id', 'fk_lacr_cs')->references('id')->on('company_stores');

            $table->foreign('store_ownership_type_id', 'fk_lacr_sot2')->references('id')->on('store_ownership_types');
            $table->foreign('building_type_id', 'fk_bt_sot')->references('id')->on('building_types');
            $table->foreign('approval_letter_application_id', 'fk_lacr_alp2')->references('id')->on('approval_letter_applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
