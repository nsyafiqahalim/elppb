<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class OwnershipType extends Model
{
    public $guarded = ['id'];
}
