<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    public $guarded = ['id'];
}
