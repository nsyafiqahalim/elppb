<!DOCTYPE html>
<html><head>
    <meta charset="UTF-8">
    <!-- <title>Invoice Report - </title> -->

    <style type="text/css">
        @page {
            margin: 2cm;
        }
        header {
            position: fixed;
            top: -80px;
            left: 0px;
            right: 0px;
            height: 30px;
        }
        footer {
            position: fixed;
            bottom: 0px;
            left: 0px;
            right: 0px;
            height: 10px;
            /* text-align: left; */
            /* line-height: normal; */
        }
        body {
               font-size:10;
               font-family: Calibri, sans-serif;
        }
    </style>

</head>
<body>
      <header>
          <table style="width: 100%;">
                <tr>
                    <td width="10%" valign="top"><h4>PENDUA</h4></td>
                    <td width="10%" align="center"><h4>BORANG A</h4>
                      <h4>MALAYSIA</h4></td>
                    <td width="10%"> </td>
                </tr>
          </table>
      </header>

      <h4 align="right">No. Siri:</h4>

      <table>
            <tr>
                <td></td>
                <td width="60%">
                    <center>
                        <h4>AKTA KAWALAN PADI DAN BERAS 1994 PERATURAN-PERATURAN KAWALAN PADI DAN BERAS (PELESENAN PENGIMPORT DAN PENGEKSPORT) 1994</h4>
                        <h3>(Peraturan 5)</h3>
                        <h3>LESEN EKSPORT</h3>
                    </center>
                </td>
                <td></td>
            </tr>
      </table>

      <table style="width: 100%;">
            <tr>
                <td width="70%"></td>
                <td width="15%"><b>No. Lesen:</b></td>
                <td width="15%"></td>
            </tr>
            <tr>
                <td width="70%"></td>
                <td width="15%"><b>No. Rujukan:</b></td>
                <td width="15%"></td>
            </tr>
      </table>
      <br/>

      <p>Lesen dikeluarkan kepada: </p>
      <p>Nama Perniagaan: <b>{{ $license->company_name }}</b></p>
      <p>Alamat premis perniagaan: <b>{{ $license->companyAddress->address_1 }}, {{ $license->companyAddress->address_2}}, {{ $license->companyAddress->address_3 }}, {{ $license->companyAddress->postcode }}, {{ $license->companyAddress->state_name }} </b></p>

      <br/><br/>
      <p>ialah pemborong beras, untuk mengeksport beras ke luar Malaysia dan untuk menyimpan beras di stor yang berikut sahaja:  </p>
      <table border="1px" cellspacing="0" cellpadding="3" style="width: 100%;">
            <tr>
                <td width="70%" align="center">Alamat Stor</td>
                <td width="30%" align="center">Had Maksimum Simpanan yang Dibenarkan <br/>(dalam tan metrik)</td>
            </tr>
            <tr>
                <td height="50" align="center"><b>{{ $license->companyAddress->address_1 }}, {{ $license->companyAddress->address_2}}, {{ $license->companyAddress->address_3 }}, {{ $license->companyAddress->postcode }}, {{ $license->companyAddress->state_name }} </b></td>
                <td height="50" align="center"><b>{{ $license->apply_load }}</b></td>
            </tr>
      </table>

      <p>Lesen ini sah dari <b>29-Apr-2019</b> hingga <b>28-Apr-2020</b> </p>
      <p>Pejabat yang mengeluarkan : <b>SEKSYEN KAWAL SELIA PADI DAN BERAS</b></p>
      <p>Tarikh dikeluarkan: <b>29-Apr-2019</b></p>
      <p>No. resit:</p>
      <br/><br/><br/><br/>


      <table style="width: 100%;">
            <tr>
                <td width="35%">(Tandatangan Pemegang Lesen)</td>
                <td width="30%"></td>
                <td width="35%"><center>Ketua Pengarah<br/><b>SHAMSUDDIN BIN ISMAIL</b></center> </td>
            </tr>
      </table>

      <br/>

      <p>(Cop atau Meterai Rasmi)</p>

      <footer>
            <div>
              <table>
                    <tr>
                        <td valign="top">Peringatan:</td>
                        <td width="90%" align="left"><b>Suatu permohonan untuk membaharui lesen hendaklah dibuat sebelum tarikh tamat tempoh lesen itu. </b></td>
                    </tr>
              </table>
            </div>
      </footer>
</body>
</html>
