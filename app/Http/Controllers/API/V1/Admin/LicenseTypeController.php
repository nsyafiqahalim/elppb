<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\LicenseTypeDataObject;

class LicenseTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addLicenseType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $licenseType = LicenseTypeDataObject::addLicenseType($param);

        return response()->json([
            'message'   =>  'Jenis lesen berjaya ditambah',
            'data'      =>  $licenseType,
            'success'   =>  true,
            'route'     =>  route('admin.license_type.index')
        ], 200);
    }

    public function updateLicenseType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $licenseType = LicenseTypeDataObject::updateLicenseType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis lesen berjaya dikemaskini',
            'data'      =>  $licenseType,
            'success'   =>  true,
            'route'     =>  route('admin.license_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();

        return LicenseTypeDataObject::findAllLicenseTypeUsingDatatableFormat($param);
    }
}
