<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\ZoneDataObject;

class ZoneController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.zone.index');
    }

    public function add()
    {
        return view('admin.zone.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $zone = ZoneDataObject::findZoneById($decryptedId);

        return view('admin.zone.edit', compact('zone'));
    }
}
