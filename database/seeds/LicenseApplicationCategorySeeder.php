<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\LicenseApplicationCategory;

class LicenseApplicationCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'INDIVIDUAL',
            'BESEKALI'
        ];

        foreach ($data as $item) {
            LicenseApplicationCategory::create([
                'name'  =>  $item,
                'code'  =>  str_replace(' ', '_', $item),
                'description'   =>  'Data ini merujuk kepada data jenis '.$item
            ]);
        }

    }
}
