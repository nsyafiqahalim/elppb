<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LicenseApplicationTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('license_application_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('old_id')->nullable();
            $table->string('code', 100);
            $table->string('name', 255);
            $table->text('description');
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
        });

        Schema::create('approval_letter_application_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('old_id')->nullable();
            $table->string('code', 100);
            $table->string('name', 255);
            $table->text('description');
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('license_application_types');
    }
}
