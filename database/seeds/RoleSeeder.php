<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //create role
        $developer = Role::create(
            ['name' => 'Developer', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $superAdministrator = Role::create(
            ['name' => 'Super Administrator', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );


        $administrator = Role::create(
            ['name' => 'Administrator', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $caw = Role::create(
            ['name' => 'Ketua Unit Cawangan', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $hq = Role::create(
            ['name' => 'Ketua Unit HQ', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $siasatan = Role::create(
            ['name' => 'Pegawai Siasatan', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $penjana = Role::create(
            ['name' => 'Penjana Lesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $pemohon = Role::create(
            ['name' => 'Pemohon Lesen', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $pemproses = Role::create(
            ['name' => 'Pegawai Kaunter', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $kp = Role::create(
            ['name' => 'Ketua Pengarah', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        $applicant = Role::create(
            ['name' => 'Pemohon', 'guard_name' => 'web', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        );

        //assign permission
        //1. Assign all permission to developer and super administrator
        $allPermissions = Permission::get()->pluck('name')->toArray();

        $developer->givePermissionTo(
            $allPermissions
        );

        $branchLevels = [
            'Ibu Pejabat',
            'Cawangan',
        ];


        $modules = [
            'Lesen Runcit'
        ];

        foreach ($modules as $module) {
            $pemproses->givePermissionTo(
                'Permohonan Baharu '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $siasatan->givePermissionTo(
                'Siasatan Permohonan '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $caw->givePermissionTo(
                'Pemilihan Pegawai Siasatan Permohonan '.$module.' (Cawangan)',
                'Peraku Cawangan Permohonan '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $kp->givePermissionTo(
                'Kelulusan Permohonan '.$module.' (Ibu Pejabat)'
            );
        }

        $penjana->givePermissionTo(
            'Jana Lesen'
        );


        $modules = [
            'Lesen Borong',
            'Lesen Import',
            'Lesen Export',
            'Lesen Beli Padi',
            'Lesen Kilang Padi Komersial'
        ];

        foreach ($modules as $module) {
            $pemohon->givePermissionTo(
                'Permohonan Baharu '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $caw->givePermissionTo(
                'Pemilihan Pegawai Siasatan Permohonan '.$module.' (Cawangan)',
                'Semakan Siasatan Permohonan '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $siasatan->givePermissionTo(
                'Siasatan Permohonan '.$module.' (Cawangan)'
            );
        }

        foreach ($modules as $module) {
            $hq->givePermissionTo(
                'Peraku Ibu Pejabat Permohonan '.$module.' (Ibu Pejabat)'
            );
        }

        foreach ($modules as $module) {
            $kp->givePermissionTo(
                'Kelulusan Permohonan '.$module.' (Ibu Pejabat)'
            );
        }
    }
}
