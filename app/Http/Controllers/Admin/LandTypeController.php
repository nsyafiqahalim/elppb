<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\LandTypeDataObject;

class LandTypeController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.land-type.index');
    }

    public function add()
    {
        return view('admin.land-type.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $landType = LandTypeDataObject::findLandTypeById($decryptedId);

        return view('admin.land-type.edit', compact('landType'));
    }
}
