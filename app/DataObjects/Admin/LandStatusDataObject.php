<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\LandStatus;

class LandStatusDataObject
{
    public static function addLandStatus($param)
    {
        $landStatus = null;

        DB::transaction(function () use ($param, &$landStatus) {
            $landStatus = LandStatus::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $landStatus;
    }

    public static function updateLandStatus($param, $id)
    {
        $landStatus = null;

        DB::transaction(function () use ($param, $id,  &$landStatus) {
            $landStatus = LandStatus::find($id);

            $landStatus->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $landStatus;
    }

    public static function findAllLandStatusUsingDatatableFormat($param)
    {
        $landStatus = LandStatus::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($landStatus)
        ->addIndexColumn()
        ->addColumn('action', function ($landStatus) {
            return view('admin.land-status.partials.datatable-button', compact('landStatus'))->render();
        })
        ->addColumn('status', function ($landStatus) {
            $status= ($landStatus->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.land-status.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findLandStatusById($id)
    {
        return LandStatus::find($id);
    }
}
