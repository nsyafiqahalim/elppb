<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\Status;

class ApprovalLetterStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ///BUKAN RUNCIT

        $types = [
            'BAHARU'
        ];

        $approvalLetters = [
            'SURAT KELULUSAN KILANG PADI KOMERSIAL',
            'SURAT KELULUSAN BELI PADI'
        ];

        $data = [
            'Draf',
            'Permohonan Baharu',
            'Pemilihan Pegawai Siasatan',
            'Peraku Semakan Siasatan',
            'Peraku Semakan Ibu Pejabat',
            'Kelulusan Permohonan',
            'Janaan Lesen',
            'Tamat',
            'Tolak',
        ];

        $displayData = [
            'Draf',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Sedang Proses',
            'Membuat Bayaran',
            'Tamat',
            'DiTolak',
        ];

        foreach ($approvalLetters as $approvalLetter) {
            foreach ($types as $type) {
                foreach ($data as $index => $item) {
                    Status::create([
                        'name'  =>  $item,
                        'display_name'  =>  $displayData[$index],
                        'code'  =>  strtoupper(str_replace(' ', '_', $item)).'_'.str_replace(' ', '_', $approvalLetter).'_'.str_replace(' ', '_', $type),
                        'domain'    =>  str_replace(' ', '_', $approvalLetter).'_'.str_replace(' ', '_', $type),
                        'description'   =>  'Data ini merujuk kepada data jenis '.$item
                    ]);
                }
            }
        }

        /**
         * TAMAT
         */
    }
}
