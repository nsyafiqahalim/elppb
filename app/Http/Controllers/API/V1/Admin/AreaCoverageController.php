<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\AreaCoverageDataObject;

class AreaCoverageController extends Controller
{
    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addAreaCoverage(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $areaCoverage = AreaCoverageDataObject::addAreaCoverage($param);

        return response()->json([
            'message'   =>  'Liputan Kawasan berjaya ditambah',
            'data'      =>  $areaCoverage,
            'success'   =>  true,
            'route'     =>  route('admin.area-coverage.index')
        ], 200);
    }

    public function updateAreaCoverage(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $areaCoverage = AreaCoverageDataObject::updateAreaCoverage($param, $decryptedId);

        return response()->json([
            'message'   =>  'Liputan Kawasan berjaya dikemaskini',
            'data'      =>  $areaCoverage,
            'success'   =>  true,
            'route'     =>  route('admin.area-coverage.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        return AreaCoverageDataObject::findAllAreaCoverageUsingDatatableFormat();
    }
}
