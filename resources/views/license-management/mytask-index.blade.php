@extends('layouts.license-management-layout')

@section('content')
@if (session('success'))
  <div class="alert alert-success alert-dismissable custom-success-box" style="margin: 15px;">
     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
     <strong> {{ session('success') }} </strong>
  </div>
@endif
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0"><i class="fas fa-bookmark"></i> Senarai Permohonan Lesen</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('management.index') }}">Pengurusan Lesen</a></li>
            <li class="breadcrumb-item active">Tugasan Saya</li>
            <li class="breadcrumb-item active">Senarai Permohonan Lesen</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <a style='color:white; font-weight: 400;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Carian</a>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <form class="form" id="filter-role">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='name' class="form-control" placeholder="Nama Syarikat" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='reference_number' class="form-control" placeholder="No Rujukan" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='license_type' class="form-control" placeholder="Jenis Lesen" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="icon-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                <div class="m-t-40">
                    <!--<a href="#">
                        <button type="submit" class="btn btn-success float-right">
                            <i class="icon-plus"></i> Tambah
                        </button>
                    </a>-->
                    @php $i = 1 @endphp
                    <table id="datatable" class="table display table-bordered table-striped table-responsived" style='text-align:center'>
                        <thead>
                            <tr style="text-align:center">
                                <th style="vertical-align:middle">Bil.</th>
                                <th style="vertical-align:middle">No.Rujukan</th>
                                <th style="vertical-align:middle">Nama Syarikat</th>
                                <th style="vertical-align:middle">Jenis Lesen</th>
                                <th style="vertical-align:middle">Jenis Permohonan</th>
                                <th style="vertical-align:middle">Status</th>
                                <!-- <th style="vertical-align:middle">Muatan (TAN)</th> -->
                                <th style="vertical-align:middle" width="250">Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                              @foreach($licenses as $license)
                              <tr>
                                  <td align="center">{{ $i++ }}</td>
                                  <td align="center">{{ $license->reference_number }}</td>
                                  <td align="center">{{ $license->company_name }}</td>
                                  <td align="center">{{ $license->licenseType->name }}</td>
                                  <td align="center">{{ $license->licenseApplicationType->name }}</td>
                                  <td align="center">{{ $license->status->display_name }}</td>
                                  <!-- <td align="center">{{ $license->apply_load }}</td> -->
                                  <td align="center">

                                    @if ($license->licenseType->name == 'Import')
                                        @php $license_type = 'import' @endphp
                                    @elseif ($license->licenseType->name == 'Eksport')
                                        @php $license_type = 'export' @endphp
                                    @elseif ($license->licenseType->name == 'Borong')
                                        @php $license_type = 'wholesale' @endphp
                                    @elseif ($license->licenseType->name == 'Runcit')
                                        @php $license_type = 'retail' @endphp
                                    @elseif ($license->licenseType->name == 'Membeli Padi')
                                        @php $license_type = 'buy_paddy' @endphp
                                    @elseif ($license->licenseType->name == 'Kilang Padi Komersil')
                                        @php $license_type = 'factory_paddy' @endphp
                                    @endif

                                    @if ($license->licenseApplicationType->code == 'BAHARU')
                                        @php $license_application_type = 'new' @endphp
                                    @elseif ($license->licenseApplicationType->code == 'PEMBAHARUAN')
                                        @php $license_application_type = 'renew' @endphp
                                    @elseif ($license->licenseApplicationType->code == 'PERUBAHAN_MAKLUMAT_SYARIKAT' || $license->licenseApplicationType->code == 'PERUBAHAN_MAKLUMAT_PREMIS' || $license->licenseApplicationType->code == 'PERUBAHAN_MAKLUMAT_STOR' || $license->licenseApplicationType->code == 'PERUBAHAN_MAKLUMAT_PERMOHONAN_LESEN' )
                                        @php $license_application_type = 'change' @endphp
                                    @elseif ($license->licenseApplicationType->code == 'PENGGANTIAN')
                                        @php $license_application_type = 'replacement' @endphp
                                    @elseif ($license->licenseApplicationType->code == 'PEMBATALAN')
                                        @php $license_application_type = 'cancel' @endphp
                                    @elseif ($license->licenseApplicationType->code == 'PEMBAHARUAN_DAN_PERUBAHAN')
                                        @php $license_application_type = 'renew_dan_change' @endphp
                                    @endif

                                    <a href="{{ route('management.'.$license_type.'.'.$license_application_type.'.show', [encrypt($license->id)]) }}">

                                        <button class="btn btn-sm btn-info">
                                            <i class="fa fa-eye"></i> Papar
                                        </button>
                                    </a>
                                  </td>
                              </tr>
                              @endforeach
                          </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/dataTables.bootstrap4.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/datatables.net-bs4/css/responsive.dataTables.min.css') }}">
@endpush

@push('js')
<script src="{{ asset('assets/plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables.net-bs4/js/dataTables.responsive.min.js') }}"></script>
<script>
    $('#datatable').DataTable(
        loadGenerationTypeConfig()
    );

    function loadGenerationTypeConfig() {

        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "responsive": true,
            "processing": true,
            "searching": false,
            "ordering": false,

        };

        return json;
    }
</script>
@endpush
