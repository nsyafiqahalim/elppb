<?php

namespace App\Http\Middleware\Portal;

use Closure;
use Illuminate\Support\Facades\Auth;

class StoreLicenseValidationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->validate([
            'description' => ['required', 'string'],
            'title' => ['required', 'string'],
            'user_id' => ['required', 'string']
        ], [
            'description.required' => ['Medan Keterangan diperlukan'],
            'title.required' => ['Medan Tajuk diperlukan'],
            'user_id.required' => ['Medan ID Pengguna diperlukan'],
        ]);

        return $next($request);
    }
}
