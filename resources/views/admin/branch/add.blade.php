@extends('layouts.admin-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor mb-0 mt-0">Pentadbiran Sistem</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item active">Data Rujukan</li>
            <li class="breadcrumb-item"><i class="fas fa-map-pin"></i> <a href="{{ route ('admin.branch.index') }}">Senarai Cawangan</a></li>
            <li class="breadcrumb-item active">Tambah</li>
        </ol>
    </div>
</div>

<div class="row page-titles">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <b style='color:white;'><i class="fas fa-plus-circle"></i> Tambah Cawangan</b>
            </div>
            <div class="card-body " id="filter">
                <div class="card-block">
                    <form class="form-sample url-encoded-submission" method="post" action="{{ route('api.admin.branch.store') }}">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="name_label">Nama <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Nama">
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="code_label">Kod <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="short_code" name="short_code" placeholder="Kod">
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-4 col-xs-12">
                                    <div class="form-group">
                                        <label id="is_active_label">Status <span class='required'>*</span></label>
                                        <div class="input-group">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                              <i class="fas fa-info"></i>
                                            </span>
                                          </div>
                                          <select class="form-control border-primary" id="is_active" name="is_active">
                                              <option value=''>
                                                  Sila Pilih
                                              </option>
                                              <option value='1'>
                                                  Aktif
                                              </option>
                                              <option value='0'>
                                                  Tidak Aktif
                                              </option>
                                          </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-lg-12 col-xs-12">
                            <div class="form-group">
                                <label for="daerah_label">Daerah</label>
                                <select class="select-district form-control" id="districts" name="districts[]" data-placeholder="Pilih Kebenaran" multiple>
                                    @foreach($districts as $district)
                                    <option value="{{ encrypt($district->id) }}">{{ $district->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-success">
                                Hantar
                            </button>
                            <a href="{{ route ('admin.branch.index') }}" class="btn btn-default m-t-n-xs">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
    <link href="{{ asset('dualListbox/dist/bootstrap-duallistbox.min.css') }}" rel="stylesheet">
@endpush

@push('js')
<script src="{{ asset('dualListbox/dist/jquery.bootstrap-duallistbox.js') }}"></script>
<script>
$('.select-district').bootstrapDualListbox({
    nonSelectedListLabel: 'Belum Dipilih',
    selectedListLabel: 'Sudah Dipilih',
    preserveSelectionOnMove: 'Pindahkan',
    infoText: 'Tunjuk semua {0}',
    infoTextFiltered: '<span class="label label-warning">Ditapis</span> {0} dari {1}',
    filterPlaceHolder:'Carian',
    infoTextEmpty:'Senarai yang kosong',
    selectorMinimalHeight:300,
    moveOnSelect: true,
});
</script>
@endpush
