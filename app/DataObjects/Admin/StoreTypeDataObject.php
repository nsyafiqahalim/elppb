<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\StoreType;

class StoreTypeDataObject
{
    public static function addStoreType($param)
    {
        $storeType = null;

        DB::transaction(function () use ($param, &$storeType) {
            $storeType = StoreType::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $storeType;
    }

    public static function updateStoreType($param, $id)
    {
        $storeType = null;

        DB::transaction(function () use ($param, $id,  &$storeType) {
            $storeType = StoreType::find($id);

            $storeType->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $storeType;
    }

    public static function findAllStoreTypeUsingDatatableFormat($param)
    {
        $storeType = StoreType::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($storeType)
        ->addIndexColumn()
        ->addColumn('action', function ($storeType) {
            return view('admin.store-type.partials.datatable-button', compact('storeType'))->render();
        })
        ->addColumn('status', function ($storeType) {
            $status= ($storeType->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.store-type.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findStoreTypeById($id)
    {
        return StoreType::find($id);
    }
}
