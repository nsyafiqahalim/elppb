<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\SpinType;

class SpinTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Beras',
          'Hancur',
          'Sekam',
          'Temukut',
          'Dedak'
      ];

      foreach ($data as $item) {
          SpinType::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
      }
    }
}
