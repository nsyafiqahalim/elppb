<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\StateDataObject;

class StateController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.state.index');
    }

    public function add()
    {
        return view('admin.state.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $state = StateDataObject::findStateById($decryptedId);

        return view('admin.state.edit', compact('state'));
    }
}
