<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleDataObject
{
    public static function addRole($param)
    {
        $role = null;

        DB::transaction(function () use ($param, &$role) {
            $role = Role::create([
                'name'          =>  $param['name'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);

            foreach ($param['permissions'] as $permission) {
                $role->givePermissionTo($permission);
            }
        });

        return $role;
    }

    public static function updateRole($param, $id)
    {
        $role = null;

        DB::transaction(function () use ($param, $id,  &$role) {
            $role = Role::find($id);

            $role->update([
                'name'          =>  $param['name'],
                'description'   =>  $param['description'],
                'updated_at'    =>  $param['updated_at'],
            ]);

            if (isset($param['permissions'])) {
                $role->syncPermissions([]);

                foreach ($param['permissions'] as $permission) {
                    $role->givePermissionTo($permission);
                }
            };
        });

        return $role;
    }

    public static function findAllRoleUsingDatatableFormat($param)
    {
        $role = Role::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });
        
        return datatables()->of($role)
        ->addIndexColumn()
        ->addColumn('action', function ($role) {
            return view('admin.role.partials.datatable-button', compact('role'))->render();
        })->rawColumns(['status','action'])->make(true);
    }

    public static function findAllRoles()
    {
        return Role::whereNotIn('name', ['Developer', 'Pelesen'])->get()->toArray();
    }

    public static function findRoleById($id)
    {
        return Role::find($id);
    }
}
