<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\AreaCoverageDataObject;

class AreaCoverageController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.area-coverage.index');
    }

    public function add()
    {
        return view('admin.area-coverage.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $areaCoverage = AreaCoverageDataObject::findAreaCoverageById($decryptedId);

        return view('admin.area-coverage.edit', compact('areaCoverage'));
    }
}
