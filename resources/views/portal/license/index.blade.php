@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Lesen</h2>
        <ol class="breadcrumb">
          <li class="breadcrumb-item">Laman Utama</li>
          <li class="breadcrumb-item"><a href="{{ route('portal.index') }}">Pengurusan Portal</a></li>
          <li class="breadcrumb-item">Lesen</a></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card card-outline-info">
            <div class="card-header" style='color:white;'>
                <a style='color:white; font-weight: 400;' data-toggle="collapse" href="#filter" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Carian</a>
                <ul class="nav float-right panel_toolbox">
                    <li><a style='color:white;' data-toggle="collapse" href="#filter" license="button" aria-expanded="false" aria-controls="multiCollapseExample1"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>
            <div class="card-body collapse multi-collapse" id="filter">
                <div class="card-block">
                    <form class="form" id="filter-license">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <input type='text' name='title' class="form-control" placeholder="Perkara" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-lg-6 col-xs-12">
                                    <div class="form-group">
                                        <select class="form-control border-primary" id="is_published" name="is_published">
                                            <option value=''>
                                                Pilih Semua Status
                                            </option>
                                            <option value='1'>
                                                Diterbitkan
                                            </option>
                                            <option value='0'>
                                                Tidak Diterbitkan
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions float-right">
                            <button type="submit" class="btn btn-info">
                                <i class="icon-search"></i> Cari
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row ">
    <div class="col-md-12 col-lg-12 col-xs-12 align-self-center">
        <div class="card">
            <div class="card-body">
                <div class="m-t-40 table-responsive">
                <a href="{{ route('portal.license.add') }}">
                        <button type="submit" class="btn btn-success float-right">
                            <i class="icon-plus"></i> Tambah
                        </button>
                    </a>
                    <table id="license-datatable" class="table display table-bordered table-striped" style='text-align:justify'>
                        <thead>
                            <tr style="text-align:center">
                                <th style="vertical-align:middle">Bil.</th>
                                <th style="vertical-align:middle">Perkara</th>
                                <th style="vertical-align:middle">Status</th>
                                <th style="vertical-align:middle" width="300">Tindakan</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('css')
<link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}">
<style>
    .uniqueClassName {
        text-align: center;
    }
</style>
@endpush

@push('js')
<script src="{{ asset('DataTables/datatables.min.js') }}"></script>

<script>
    $("#filter-license").submit(function(event) {
        event.preventDefault();
        form = $('#filter-license').serializeArray();
        formdata = {}
        for (x = 0; x < form.length; x++) {
            formdata[form[x].name] = form[x].value;
        }
        $('#license-datatable').DataTable().destroy();
        $('#license-datatable').DataTable(
            loadConfig(formdata)
        );
    });

    $('#license-datatable').DataTable(
        loadConfig()
    );

    function loadConfig(form) {
        if (form == null) {
            form = {};
        }
        form["src"] = "datatable";
        var json = {
            "language": {
                "url": "{{ asset('DataTables/lang/malay.json') }}"
            },
            "lengthMenu": [ 20, 50, 75, 100 ],
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "rowReorder": {
                selector: 'td:nth-child(2)'
            },
            "responsive": true,
            "processing": true,
            "serverSide": true,
            "bPaginate": true,
            "searching": false,
            "ordering": false,
            "ajax": {
                "url": "{{ route('api.portal.license.datatable') }}",
                "type": "GET",
                "dataType": 'json',
                "data": form,
                "dataSrc": "data"
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    className: 'uniqueClassName',
                },
                {
                    data: 'title',
                    name: 'title',

                },
                {
                    data: 'published',
                    name: 'published',
                    className: 'uniqueClassName',

                },
                {
                    data: 'action',
                    name: 'Tindakan',
                    className: 'uniqueClassName',
                },
            ]

        };

        return json;
    }
</script>
@endpush
