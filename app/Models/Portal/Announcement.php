<?php

namespace App\Models\Portal;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    public $guarded = ['id'];
    public $table = 'portal_announcements';
}
