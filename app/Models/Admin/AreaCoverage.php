<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class AreaCoverage extends Model
{
    public $guarded = ['id'];
}
