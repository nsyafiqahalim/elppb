<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\ApprovalLetterApplication\Company;
use App\Models\ApprovalLetterApplication\CompanyAddress;
use App\Models\ApprovalLetterApplication\CompanyPartner;
use App\Models\ApprovalLetterApplication\CompanyPremise;
use App\Models\ApprovalLetterApplication\CompanyStore;
use App\Models\Admin\Status;
use App\Models\Admin\LicenseType;
use App\Models\Admin\ApprovalLetterApplicationType;

class ApprovalLetterApplication extends Model
{
    public $guarded = ['id'];
    public $table = 'approval_letter_applications';

    public $dates = ['apply_date', 'expiry_date'];

    private const ACTIVE = 1;

    public function companyAddress()
    {
        return $this->hasOne(CompanyAddress::class, 'approval_letter_application_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class, 'status_id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'approval_letter_application_id');
    }

    public function companyPartners()
    {
        return $this->hasMany(CompanyPartner::class, 'approval_letter_application_id');
    }

    public function companyStores()
    {
        return $this->hasMany(CompanyStore::class, 'approval_letter_application_id');
    }

    public function companyPremise()
    {
        return $this->hasOne(CompanyPremise::class, 'approval_letter_application_id');
    }

    public function ApprovalLetterType()
    {
        return $this->belongsTo(LicenseType::class, 'approval_letter_type_id');
    }

    public function approval_letterApplicationType()
    {
        return $this->belongsTo(LicenseApplicationType::class, 'approval_letter_application_type_id');
    }
}
