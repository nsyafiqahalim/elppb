<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CompanyPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('nric');
            $table->string('passport')->nullable();
            $table->string('total_share');
            $table->string('share_percentage');
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('postcode');
            $table->tinyInteger('is_citizen')->default(1);
            //$table->unsignedBigInteger('ownership_type_id')->index();
            $table->unsignedBigInteger('state_id')->index();
            $table->unsignedBigInteger('race_id')->index();
            $table->unsignedBigInteger('race_type_id')->index();

            $table->unsignedBigInteger('company_id')->index();
            $table->timestamps();

            $table->foreign('state_id')->references('id')->on('states');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('race_type_id')->references('id')->on('race_types');
            //$table->foreign('ownership_type_id')->references('id')->on('ownership_types');
            $table->foreign('race_id')->references('id')->on('races');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
