<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:60,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        'StoreRoleValidation'    =>  \App\Http\Middleware\StoreRoleValidation::class,
        'StoreUserValidationMiddleware'    =>  \App\Http\Middleware\Admin\StoreUserValidationMiddleware::class,
        'UpdateUserValidationMiddleware'    =>  \App\Http\Middleware\Admin\UpdateUserValidationMiddleware::class,
        'StoreBuildingTypeValidationMiddleware'  =>  \App\Http\Middleware\StoreBuildingTypeValidationMiddleware::class,
        'StorePremiseTypeValidationMiddleware'   =>  \App\Http\Middleware\StorePremiseTypeValidationMiddleware::class,
        'StoreBusinessTypeValidationMiddleware'  =>  \App\Http\Middleware\StoreBusinessTypeValidationMiddleware::class,
        'StoreCompanyTypeValidationMiddleware'   =>  \App\Http\Middleware\StoreCompanyTypeValidationMiddleware::class,
        'StoreLandTypeValidationMiddleware'      =>  \App\Http\Middleware\StoreLandTypeValidationMiddleware::class,
        'StorePermitTypeValidationMiddleware'    =>  \App\Http\Middleware\StorePermitTypeValidationMiddleware::class,
        'StoreStoreTypeValidationMiddleware'     =>  \App\Http\Middleware\StoreStoreTypeValidationMiddleware::class,
        'StorePartnerTypeValidationMiddleware'   =>  \App\Http\Middleware\StorePartnerTypeValidationMiddleware::class,
        'StoreRiceTypeValidationMiddleware'      =>  \App\Http\Middleware\StoreRiceTypeValidationMiddleware::class,
        'StoreRiceGradeValidationMiddleware'      =>  \App\Http\Middleware\StoreRiceGradeValidationMiddleware::class,
        'StoreFactoryTypeValidationMiddleware'   =>  \App\Http\Middleware\StoreFactoryTypeValidationMiddleware::class,
        'StoreDunValidationMiddleware'           =>  \App\Http\Middleware\StoreDunValidationMiddleware::class,
        'StoreZoneValidationMiddleware'          =>  \App\Http\Middleware\StoreZoneValidationMiddleware::class,
        'StoreDistrictValidationMiddleware'      =>  \App\Http\Middleware\StoreDistrictValidationMiddleware::class,
        'StoreStateValidationMiddleware'         =>  \App\Http\Middleware\StoreStateValidationMiddleware::class,
        'StoreBranchValidationMiddleware'        =>  \App\Http\Middleware\StoreBranchValidationMiddleware::class,
        'StoreLicenseTypeValidationMiddleware'   =>  \App\Http\Middleware\StoreLicenseTypeValidationMiddleware::class,
        'StoreLandStatusValidationMiddleware'    =>  \App\Http\Middleware\StoreLandStatusValidationMiddleware::class,
        'StoreRiceCategoryValidationMiddleware'  =>  \App\Http\Middleware\StoreRiceCategoryValidationMiddleware::class,
        'StoreRaceValidationMiddleware'          =>  \App\Http\Middleware\StoreRaceValidationMiddleware::class,
        'StoreRaceTypeValidationMiddleware'      =>  \App\Http\Middleware\StoreRaceTypeValidationMiddleware::class,
        'StoreStockValidationMiddleware'         =>  \App\Http\Middleware\StoreStockValidationMiddleware::class,
        'StoreOwnershipStatusValidationMiddleware'   =>  \App\Http\Middleware\StoreOwnershipStatusValidationMiddleware::class,
        'StorePaymentTypeValidationMiddleware'  =>  \App\Http\Middleware\StorePaymentTypeValidationMiddleware::class,
        'StoreStatusValidationMiddleware'        =>  \App\Http\Middleware\StoreStatusValidationMiddleware::class,
        'StoreParliamentValidationMiddleware'    =>  \App\Http\Middleware\StoreParliamentValidationMiddleware::class,
        'StoreAreaCoverageValidationMiddleware'  =>  \App\Http\Middleware\StoreAreaCoverageValidationMiddleware::class,
        'StoreAnnoucementValidationMiddleware'  =>  \App\Http\Middleware\Portal\StoreAnnoucementValidationMiddleware::class,
        'StoreLicenseValidationMiddleware'  =>  \App\Http\Middleware\Portal\StoreLicenseValidationMiddleware::class,
        'StorePermitValidationMiddleware'  =>  \App\Http\Middleware\Portal\StorePermitValidationMiddleware::class,
        'StoreFaqValidationMiddleware'  =>  \App\Http\Middleware\Portal\StoreFaqValidationMiddleware::class,
    ];

    /**
     * The priority-sorted list of middleware.
     *
     * This forces non-global middleware to always be in the given order.
     *
     * @var array
     */
    protected $middlewarePriority = [
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\Authenticate::class,
        \Illuminate\Routing\Middleware\ThrottleRequests::class,
        \Illuminate\Session\Middleware\AuthenticateSession::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
        \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
