@extends('layouts.portal-layout')

@section('content')
<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h2 class="text-themecolor mb-0 mt-0">Galeri</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Laman Utama</li>
            <li class="breadcrumb-item"><a href="{{ route('portal.index') }}">Pengurusan Portal</a></li>
            <li class="breadcrumb-item">Galeri</a></li>
        </ol>
    </div>

    <div class="col-md-6 col-4 align-self-center">
        <a href="{{ route('portal.gallery.album.create') }}">
            <button type="submit" class="btn btn-success float-right">
                <i class="icon-plus"></i> Tambah Album
            </button>
        </a>
    </div>
</div>

@if (count($albums) > 0)
<div class="row">
    @foreach ($albums as $album)
    <div class="col-md-3 col-sm-3 col-lg-3">
        <div class="card mb-3">
            <div class="card-header text-white bg-default mb-3">
                <div class="float-right">
                    <a href="{{ route('album-destroy', encrypt($album->id)) }}" class="btn btn-sm btn-outline-danger deletion" ><i class="fa fa-trash"></i></a>
                </div>
            </div>
            <img src="{{ asset('assets/images/folder.png') }}"  height="200">
            <div class="card-body">
                <p class="text-center">
                    <a href="{{ route('album-show', encrypt($album->id)) }}" sstyle="margin-right:15px;color:black;text-decoration:underline">
                        {{ $album->name }}
                    </a>
                </p>
                <div class="d-flex justify-content-between align-items-center">
                   
                    <!--
                    <div class="btn-group">
                        <a href="{{ route('album-show', $album->id) }}" class="btn btn-sm btn-primary" style="margin-right:15px;">Papar</a>
                        
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div> <!-- End col-md-4 -->
    @endforeach
</div> <!-- End Row -->
@else
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h3 style="text-align:center;">Tiada album</h3>
            </div>
        </div>
    </div>
</div>
@endif

@endsection
