<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApprovalLetterApplicationBuyPaddies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_letter_application_buy_paddies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('license_application_buy_paddy_condition_id')->nullable()->index('alabp_labpc_idx');
            $table->unsignedBigInteger('approval_letter_application_id')->index('alabp_ala_idx');
            $table->foreign('approval_letter_application_id', 'fk_alabp_ala')->references('id')->on('approval_letter_applications');
            $table->foreign('license_application_buy_paddy_condition_id', 'fk_alabp_labpc')->references('id')->on('license_application_buy_paddy_conditions');
            $table->timestamps();
        });

        Schema::create('license_application_approval_letters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('remarks')->nullable();
            $table->unsignedBigInteger('license_application_id')->nullable()->index('laal_la_idx');
            $table->unsignedBigInteger('approval_letter_id')->index('laal_al_idx');
            $table->foreign('approval_letter_id', 'fk_laal_la')->references('id')->on('approval_letters');
            $table->foreign('license_application_id', 'fk_laal_al')->references('id')->on('license_applications');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
