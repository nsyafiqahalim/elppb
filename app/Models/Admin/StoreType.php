<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class StoreType extends Model
{
    public $guarded = ['id'];
}
