<?php

use Illuminate\Database\Seeder;

use App\Models\Portal\License;

class PortalLicenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        License::create([
            'title'   =>  'Lesen Runcit',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  '<div class="entry-content">
                                            <p>
                                                <b>
                                                    PANDUAN DAN SYARAT MEMOHON LESEN RUNCIT
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    A. Keterangan Ringkas
                                                </b>
                                            </p>
                                            <p>
                                                Suatu lesen yang dikeluarkan kepada mana-mana orang yang berkelayakan untuk menjual beras secara runcit dengan had muatan bermula 10,000kg
                                            </p>
                                            <p>
                                                <b>
                                                    B. Panduan Permohonan
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p>
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                                <br>
                                                    <span style="color: #0000ff;">
                                                        <a href="http://www.elesen.moa.gov.my">
                                                            http://www.elesen.moa.gov.my
                                                        </a>
                                                    </span>
                                                <br>
                                            </p>
                                            <p>
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                i. Pemohon hendaklah warganegara Malaysia berumur 18 tahun dan ke atas atau syarikat milik warganegara Malaysia
                                                <br>
                                                    sahaja.
                                                <br>
                                            </p>
                                            <p>
                                                ii. Mempunyai premis yang sesuai dan munasabah (terletak di aras bawah,bebas dari kawasan banjir, atau syarikat milik warganegara Malaysia sahaja.
                                            </p>
                                            <p>
                                                iii. Tiada Lesen Runcit Beras lain yang wujud di alamat yang sama.
                                            </p>
                                            <p>
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p>
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise (Milik Tunggal / Perkongsian)
                                                </span>
                                            </p>
                                            <p style=" text-align: justify;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” bagi negeri Sarawak.
                                            </p>
                                            <p style=" text-align: justify;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Borong” bagi negeri Sabah.
                                            </p>
                                            <p style="text-align: justify;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p>
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style=" text-align: justify;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan “Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style=" text-align: justify;">
                                                iii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p>
                                                a. Salinan Sijil Pendaftaran.
                                            </p>
                                            <p>
                                                b. Salinan Buku Undang-Undang Kecil Tubuh.
                                            </p>
                                            <p style="text-align: justify; ">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Runcit Beras dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut.
                                            </p>
                                            <p>
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="text-align: justify;">
                                                i. Salinan surat kebenaran daripada Pihak Berkuasa Tempatan yang membenarkan perniagaan dijalankan di alamat tersebut (jika berkaitan).
                                            </p>
                                            <p style="text-align: justify;">
                                                <b>
                                                    C. Bayaran
                                                </b>
                                            </p>
                                            <p style=" text-align: justify;">
                                                1. RM20 setahun bagi 10,000kg yang pertama atau mana-mana bahagian daripadanya.
                                            </p>
                                            <p style=" text-align: justify;">
                                                2. RM10 setahun bagi setiap 10,000kg berikutnya atau mana-mana bahagian daripadanya.
                                            </p>
                                            <p>
                                                <b>
                                                    D. Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="text-align: justify;">
                                                1. Pelesen hendaklah melekatkan label atau tag yang menunjukkan gred dan harga beras pada kampit atau bekas beras berkenaan yang ditawarkan untuk jualan dan hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan tersebut.
                                            </p>
                                            <p style=" text-align: justify;">
                                                2. Pemegang Lesen Runcit Beras tidak boleh mempunyai dalam stok berasnya yang lebih daripada had muatan yang ditetapkan dalam lesennya pada satu-satu masa.
                                            </p>
                                            <p style=" text-align: justify;">
                                                3. Lesen yang lengkap ditandatangani dan cop rasmi oleh pemilik lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan dalam lesen itu.
                                            </p>
                                            <p style=" text-align: justify;">
                                                4. Lesen tidak boleh dipindah milik atau diserah hak kepada orang lain.
                                            </p>
                                            <p style=" text-align: justify;">
                                                5. Pelesen tidak dibenarkan membuat sebarang pindaan pada lesen tanpa kelulusan Ketua Pengarah Pengawalan Padi dan Beras.
                                            </p>
                                            <p style=" text-align: justify;">
                                                6. Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify;">
                                                7. Pelesen hendaklah menyediakan suatu akaun harian bagi beras yang diperolehi atau dibeli yang disokong dengan invois, jumlah jualan dan stok dalam tangan.
                                            </p>
                                            <p style=" text-align: justify;">
                                                8. Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen. Lesen akan terbatal dengan sendirinya selepas tamat tarikh sah tempoh.
                                            </p>
                                        </div>'
        ]);

        License::create([
            'title'   =>  'Lesen Borong',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  '  <p><b>
                                    PANDUAN DAN SYARAT MEMOHON LESEN BORONG
                                    <br><br></b>
                                    </p>
                                    <p><b>
                                    a) Keterangan Ringkas</b>
                                    </p>
                                      <p style="padding-left:30px;">
                                        Suatu lesen yang dikeluarkan kepada mana-mana orang yang berkelayakan untuk menjual beras secara borong dengan had muatan bermula 100 tan metrik
                                      </p>
                                            <p>
                                                <b>
                                                    b) Panduan Permohonan
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                            </p>
                                            <p style="padding-left: 90px;">
                                                <span style="color: #0000ff;">
                                                    <a href="http://www.elesen.moa.gov.my">
                                                        http://www.elesen.moa.gov.my
                                                    </a>
                                                </span>
                                            </p>
                                            <p>
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                i. Pemohon hendaklah warganegara Malaysia berumur 21 tahun dan ke atas atau syarikat milik warganegara Malaysia sahaja.
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                ii. Mempunyai modal semasa sekurang-kurangnya RM100,000 bagi Bumiputera atau RM150,000 bagi Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                iii. Mempunyai stor yang sesuai dan munasabah (terletak di aras bawah, bebas dari kawasan banjir, akses keluar masuk premis tidak menganggu aliran trafik).
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                iv. Tiada lesen berkaitan beras yang lain yang wujud di alamat yang sama.
                                            </p>
                                            <p>
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px; text-align: justify;">
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise (Milik Tunggal / Perkongsian)
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Borong Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Borong Beras” bagi negeri Sarawak.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras Borong” bagi negeri Sabah.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama pemohon atau perniagaan yang didaftarkan dengan baki tidak kurang daripada RM100,000 untuk pemohon Bumiputera atau RM150,000 untuk pemohon Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan “Borong Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Mempunyai modal berbayar sekurang-kurangnya RM100,000 bagi syarikat bertaraf Bumiputera dan RM150,000 bagi syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama perniagaan yang didaftarkan dengan baki tidak kurang daripada RM100,000 untuk syarikat bertaraf Bumiputera atau RM150,000 untuk syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                iii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan Sijil Pendaftaran Koperasi/ Syarikat Kerjasama/Pertubuhan Peladang.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Senaskah Buku Undang-Undang Kecil Pertubuhan.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Borong Beras dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut sekurang-kurangnya RM100,000 bagi perniagaan bertaraf Bumiputera dan RM150,000 bagi perniagaan bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                d. Senarai nama Ahli Jawatankuasa yang menunjukkan Jawatan, No. Kad Pengenalan dan alamat.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                e. Pemilikan syarikat hendaklah daripada warganegara sahaja.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                <b>
                                                    i.
                                                </b>
                                                <b>
                                                    Premis / Stor
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan perjanjian sewa/beli premis.
                                            </p>
                                            <p style="padding-left: 150px; text-align: justify;">
                                                * Pemohon hendaklah menyediakan premis yang sesuai bagi menjalankan urusniaga beras dan premis tersebut belum memiliki sebarang lesen beras.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Menyediakan gambar stor yang terdiri daripada pandangan hadapan, dalam dan belakang stor.
                                            </p>
                                            <p>
                                                <b>
                                                    B. Bayaran
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                1. RM200 setahun bagi 100 tan metrik yang pertama atau mana-mana bahagian daripadanya.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                2. RM10 setahun bagi setiap 100 tan metrik berikutnya atau mana-mana bahagian daripadanya
                                            </p>
                                            <p>
                                                <b>
                                                    c) Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                1. Pemohon hendaklah menyediakan premis dan stor yang sesuai bagi menjalankan urusniaga.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                2. Pemegang Lesen Borong Beras tidak boleh mempunyai dalam stok berasnya yang lebih daripada had muatan yang ditetapkan dalam lesennya pada satu-satu masa.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                3. Lesen yang lengkap ditandatangani dan cop rasmi oleh pemilik lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan dalam lesen itu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                4. Lesen tidak boleh dipindah milik atau diserah hak kepada orang lain.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                5. Pelesen tidak dibenarkan membuat sebarang pindaan pada lesen tanpa kelulusan Ketua Pengarah Pengawalan Padi dan Beras.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                6. Pelesen tidak boleh memperolehi atau membeli beras daripada mana-mana sumber selain daripada melalui import yang diluluskan oleh Kerajaan, tawaran Jabatan kerajaan atau lain-lain pemegang Lesen Borong Beras yang juga pengilang padi.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                7. Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                8. Pelesen tidak boleh menyimpan beku, menyembunyi atau memusnahkan beras.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                9. Pelesen tidak boleh menjual beras kepada mana-mana orang melainkan pemegang Lesen Borong Beras yang bukan pengilang padi, pemegang Lesen Runcit Beras, mana-mana pertubuhan yang mengkehendaki beras sebagai sebahagian daripada keluaran berkilang, pertubuhan-pertubuhan menyelia makanan dan pembekal beras di bawah kontrak.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                10. Pelesen yang terlibat dengan pembungkusan beras hendaklah mempamerkan label atau tag dalam Bahasa Malaysia yang menunjukkan gred, berat, harga, nama pemborong, nombor lesen dan peratus kandungan beras hancur pada pembungkus atau bekas beras itu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                11. Pelesen hendaklah menyelenggara suatu akaun harian bagi pembelian beras, yang didapati dari pengilangan (mengikut mana yang berkenaan), penjualan dan baki stok dalam tangan. Akaun hendaklah disediakn di premis perniagaan yang berkenaan untuk diperiksa oleh mana-mana pegawai yang diberi kuasa apabila diminta.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                12. Pelesen hendaklah menyediakan penyata laporan bulanan mengenai jumlah belian, jumlah jualan dan jumlah stok semasa beras dan dikemukakan ke pejabat cawangan yang berkenaan pada tiap-tiap awal bulan.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                13. Menyediakan invois jualan beras yang menyatakan nama perniagaan, alamat premis perniagaan dan no. lesen termasuklah tarikh jualan, nama dan alamat penuh pembeli, no. Lesen Borong Beras atau Lesen Runcit Beras pembeli dan jika pembeli bukan pemegang Lesen Borong Beras atau Lesen RUncit Beras, namakan jenis pertubuhan prniagaannya
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                14. Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen. Lesen akan terbatal dengan sendirinya selepas tamat tarikh sah tempoh.
                                            </p>'
        ]);

        License::create([
            'title'   =>  'Lesen Import',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  '            <p>
                                                <b>
                                                    PANDUAN DAN SYARAT MEMOHON LESEN IMPORT
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    a) Keterangan Ringkas
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                Suatu lesen yang dikeluarkan kepada mana-mana pemegang Lesen Borong Beras bagi tujuan pengimportan hasil sampingan berasaskan beras (Bihun, Kueteow, Laksa, dll yang dibenarkan oleh Ketua Pengarah Pengawalan Padi dan Beras)
                                            </p>
                                            <p>
                                                <b>
                                                    b) Panduan Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                                <br>
                                                    <span style="color: #0000ff;">
                                                        <a href="http://www.elesen.moa.gov.my">
                                                            http://www.elesen.moa.gov.my
                                                        </a>
                                                    </span>
                                                <br>
                                            </p>
                                            <p style="padding-left: 60px; text-align: justify;">
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                Hanya dibuka kepada pemohon yang telah memiliki Lesen Borong Beras yang masih sah tempoh sahaja
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                </b>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise (Milik Tunggal / Perkongsian)
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” bagi negeri Sarawak.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Borong” bagi negeri Sabah.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                iii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan Sijil Pendaftaran.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Buku Undang-Undang Kecil Tubuh.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Import.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                Sesalinan Lesen Borong Beras yang masih sah tempoh.
                                            </p>
                                            <p>
                                                <b>
                                                    c) Bayaran
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                RM200 setahun
                                            </p>
                                            <p>
                                                <b>
                                                    d) Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                1. Pelesen hanya dibenarkan mengimport produk seperti di dalam lesen sahaja.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                2. Pemegang Lesen Import tidak boleh mempunyai dalam stok berasnya yang lebih daripada had muatan yang ditetapkan dalam lesennya pada satu-satu masa.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                3. Lesen yang lengkap ditandatangani dan cop rasmi oleh pemilik lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan dalam lesen itu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                4. Lesen tidak boleh dipindah milik atau diserah hak kepada orang lain.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                5. Pelesen tidak dibenarkan membuat sebarang pindaan pada lesen tanpa kelulusan Ketua Pengarah Pengawalan Padi dan Beras.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                6. Pelesen tidak boleh menyetor atau menyimpan di tempat selain daripada yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                7. Pelesen hendaklah menyelenggara suatu akaun harian bagi aktiviti pengimportan yang didapati dari pengilangan (mengikut mana yang berkenaan), penjualan dan baki stok dalam tangan. Akaun hendaklah disediakan di premis perniagaan yang berkenaan untuk diperiksa oleh mana-mana pegawai yang diberi kuasa apabila diminta.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                8. Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen. Lesen akan terbatal dengan sendirinya selepas tamat tarikh sah tempoh.
                                            </p>'
        ]);

        License::create([
            'title'   =>  'Lesen Eksport',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  '  <p>
                                                <b>
                                                    PANDUAN DAN SYARAT MEMOHON LESEN EKSPORT
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    a) Keterangan Ringkas
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                Suatu lesen yang dikeluarkan kepada mana-mana pemegang Lesen Borong Beras bagi tujuan pengimportan hasil sampingan berasaskan beras (Bihun, Kueteow, Laksa, dll yang dibenarkan oleh Ketua Pengarah Pengawalan Padi dan Beras)
                                            </p>
                                            <p>
                                                <b>
                                                    b) Panduan Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px;">
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                                <br>
                                                    <span style="color: #0000ff;">
                                                        <a href="http://www.elesen.moa.gov.my">
                                                            http://www.elesen.moa.gov.my
                                                        </a>
                                                    </span>
                                                <br>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px;">
                                                <b>
                                                </b>
                                                Hanya dibuka kepada pemohon yang telah memiliki Lesen Borong Beras yang masih sah tempoh sahaja.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                </b>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p>
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise (Milik Tunggal / Perkongsian)
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Beras” bagi negeri Sarawak
                                            </p>
                                            <p style="padding-left: 120px;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Borong” bagi negeri Sabah.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                iii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan Sijil Pendaftaran.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Buku Undang-Undang Kecil Tubuh.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Eksport.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                Sesalinan Lesen Borong Beras yang masih sah tempoh.
                                            </p>
                                            <p>
                                                <b>
                                                    c) Bayaran
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                RM200 setahun
                                            </p>
                                            <p>
                                                <b>
                                                    d) Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px; text-align: justify;">
                                                <b>
                                                </b>
                                                1. Pelesen hanya dibenarkan mengeksport produk seperti di dalam lesen sahaja.
                                            </p>
                                            <p style="padding-left: 60px; text-align: justify;">
                                                2. Pemegang Lesen Eksport tidak boleh mempunyai dalam stok berasnya yang lebih daripada had muatan yang ditetapkan dalam lesennya pada satu-satu masa.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                3. Lesen yang lengkap ditandatangani dan cop rasmi oleh pemilik lesen hendaklah dipamerkan di tempat yang mudah dilihat di premis perniagaan yang dinyatakan dalam lesen itu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                4. Lesen tidak boleh dipindah milik atau diserah hak kepada orang lain.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                5. Pelesen tidak dibenarkan membuat sebarang pindaan pada lesen tanpa kelulusan Ketua Pengarah Pengawalan Padi dan Beras.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                6. Pelesen tidak boleh menyetor atau menyimpan di tempat selain daripada yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                7. Pelesen hendaklah menyelenggara suatu akaun harian bagi aktiviti pengeksportan yang didapati dari pengilangan (mengikut mana yang berkenaan), penjualan dan baki stok dalam tangan. Akaun hendaklah disediakan di premis perniagaan yang berkenaan untuk diperiksa oleh mana-mana pegawai yang diberi kuasa apabila diminta.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                8. Lesen hendaklah diperbaharui selewat-lewatnya 30 hari sebelum tamat tempoh sah lesen. Lesen akan terbatal dengan sendirinya selepas tamat tarikh sah tempoh.
                                            </p>'
        ]);

        License::create([
            'title'   =>  'Lesen Membeli Padi',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  ' <p>
                                                <b>
                                                    PANDUAN DAN SYARAT MEMOHON LESEN MEMBELI PADI
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    A. Keterangan Ringkas
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                Suatu lesen yang dikeluarkan kepada mana-mana orang yang berkelayakan untuk membeli padi.
                                            </p>
                                            <p>
                                                <b>
                                                    B. Panduan Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px;">
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                                <br>
                                                    <span style="color: #0000ff;">
                                                        <a href="http://www.elesen.moa.gov.my">
                                                            http://www.elesen.moa.gov.my
                                                        </a>
                                                    </span>
                                                <br>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                i. Pemohon hendaklah warganegara Malaysia berumur 21 tahun dan ke atas atau syarikat milik warganegara Malaysia sahaja.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                ii. Tapak pengumpulan padi hendaklah dikawasan yang bersesuaian yang tidak mengganggu kawasan awam.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                iii. Tidak menerima bantahan daripada penduduk sekitar.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise (Milik Tunggal / Perkongsian
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Beli/Jual Padi” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Beli/Jual Padi” bagi negeri Sarawak.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Beli/Jual Padi” bagi negeri Sabah.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama pemohon atau perniagaan yang didaftarkan dengan baki tidak kurang daripada RM20,000 untuk pemohon Bumiputera atau RM30,000 untuk pemohon Bukan Bumiputera.
                                            </p>
                                            <p>
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Beli/Jual Padi” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Mempunyai modal berbayar sekurang-kurangnya RM20,000 bagi syarikat bertaraf Bumiputera dan RM30,000 bagi syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama perniagaan yang didaftarkan dengan baki tidak kurang daripada RM20,000 untuk syarikat bertaraf Bumiputera atau RM30,000 untuk syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan Sijil Pendaftaran Koperasi/ Syarikat Kerjasama/Pertubuhan Peladang
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Senaskah Buku Undang-Undang Kecil Pertubuhan.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Membeli Padi dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut sekurang-kurangnya RM20,000 bagi perniagaan bertaraf Bumiputera dan RM30,000 bagi perniagaan bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Senarai nama Ahli Jawatankuasa yang menunjukkan Jawatan, No. Kad Pengenalan dan alamat.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                i. Salinan dokumen alat-alat timbang, geran lori dan lain-lain jika ada.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                ii. Salinan surat hak milik premis dan setor atau salinan perjanjian sewa.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                iii. Surat pengesahan daripada kilang padi yang akan membeli padi.
                                            </p>
                                            <p style="text-align: justify;">
                                                <strong>
                                                    C. Bayaran
                                                </strong>
                                            </p>
                                            <p style="text-align: left; padding-left: 60px;">
                                                1. RM200 setahun bagi 1000MT padi yang pertama atau mana-mana bahagian daripadanya (Semenanjung Malaysia); atau
                                            </p>
                                            <p style="padding-left: 60px; text-align: justify;">
                                                2. RM100 setahun bagi 1000MT padi yang pertama atau mana-mana bahagian daripadanya (Sabah / Sarawak); dan
                                            </p>
                                            <p style="padding-left: 60px;">
                                                3. RM10 setahun bagi setiap 1000MT padi berikutnya atau mana-mana bahagian daripadanya.
                                            </p>
                                            <p>
                                                <b>
                                                    D. Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                1. Pelesen tidak boleh membeli padi di tempat selain yang dinyatakan dalam lesen dan pembelian dibuktikan dengan resit.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                2. Pelesen tidak boleh menyetor atau menyimpan beras di tempat selain daripada yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                3. Pelesen tidak boleh membayar harga belian yang lebih rendah daripada harga yang telah ditetapkan oleh Ketua Pengarah Pengawalan Padi dan Beras.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                4. Pelesen hendaklah mengambilkira apa-apa potongan yang perlu dan munasabah yang dibuat oleh sebab kelembapan atau benda-benda asing yang terkandung dalamnya dan gred padi.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                5. Pelesen hendaklah membuat pembelian mengikut berat dan bukan mengikut isipadu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                6. Pelesen hendaklah membeli padi daripada petani dan membayar sepenuhnya secara tunai apabila selesai pembelian itu.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                7. Pelesen hendaklah menyelenggara suatu rekod mengenai pembeli dan butir-butir lain seperti tarikh belian, nama, nombor kad pengenalan dan alamat pembeli, berat padi dan harga jualan.
                                            </p>'
        ]);

        License::create([
            'title'   =>  'Lesen Kilang Padi',
            'created_by'    =>  '1',
            'is_published'   =>1,
            'updated_by'    =>  '1',
            'description'   =>  '  <p>
                                                <b>
                                                    PANDUAN DAN SYARAT MEMOHON LESEN KILANG PADI KOMERSIL
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p>
                                                <b>
                                                    a) Keterangan Ringkas
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px; text-align: justify;">
                                                Suatu lesen yang dikeluarkan kepada mana-mana orang yang berkelayakan untuk mengendalikan Kilang Beras bagi maksud komersil atau bagi maksud pengeringan atau pengilangan padi kepunyaan petani bagi kegunaan sendiri.
                                            </p>
                                            <p>
                                                <b>
                                                    b) Panduan Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    1. Cara Permohonan
                                                </b>
                                            </p>
                                            <p style="padding-left: 120px;">
                                                Permohonan boleh dibuat secara atas talian melalui laman web berikut:
                                                <br>
                                                    <span style="color: #0000ff;">
                                                        <a href="http://www.elesen.moa.gov.my">
                                                            http://www.elesen.moa.gov.my
                                                        </a>
                                                    </span>
                                                <br>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                <b>
                                                    2. Kriteria Kelayakan / Syarat-Syarat Permohonan
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                i. Pemohon hendaklah warganegara Malaysia berumur 21 tahun dan ke atas atau syarikat milik warganegara Malaysia sahaja.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                ii. Mempunyai modal semasa sekurang-kurangnya RM100,000 bagi Bumiputera atau RM200,000 bagi Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                iii. Tapak kilang yang sesuai dan munasabah (berhampiran kawasan penanaman padi, bebas dari kawasan banjir, akses keluar masuk premis tidak menganggu aliran trafik, mendapat kelulusan daripada Pihak Berkuasa Tempatan dan Jabatan Alam Sekitar).
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                iv. Tidak mendapat bantahan penduduk sekitar.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <b>
                                                    3. Dokumen Yang Diperlukan
                                                </b>
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian I – Maklumat Asas Syarikat
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                i.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Enterprise
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang masih sah tempoh dan menunjukkan jenis perniagaan “Kilang Padi” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan Borang 1 (I.R.D. No. 7), Borang R22 (Certificate of Registration) dan Extract of Business Names Registration yang masih sah tempoh dan menunjukkan jenis perniagaan “Kilang Padi” bagi negeri Sarawak.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                c. Salinan Trading Licence yang masih sah tempoh dan menunjukkan jenis perniagaan “Kilang Padi” bagi negeri Sabah.
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama pemohon atau perniagaan yang didaftarkan dengan baki tidak kurang daripada RM100,000 untuk pemohon Bumiputera atau RM200,000 untuk pemohon Bukan Bumiputera.
                                            </p>
                                            <p style="padding-left: 90px;">
                                                ii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Sdn Bhd / Bhd
                                                </span>
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan Memorandum and Articles of Association (MAA).
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Salinan print-out ringkasan maklumat perniagaan daripada Suruhanjaya Syarikat Malaysia (SSM) yang menunjukkan jenis perniagaan “Kilang Padi” serta alamat premis yang dipohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Mempunyai modal berbayar sekurang-kurangnya RM100,000 bagi syarikat bertaraf Bumiputera dan RM20,000 bagi syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Salinan Penyata Akaun Bank bagi tiga (3) bulan terkini atas nama perniagaan yang didaftarkan dengan baki tidak kurang daripada RM100,000 untuk syarikat bertaraf Bumiputera atau RM200,000 untuk syarikat bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 90px;">
                                                iii.
                                                <span style="text-decoration: underline;">
                                                    Perniagaan Jenis Koperasi / Pertubuhan Peladang
                                                </span>
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Salinan Sijil Pendaftaran Koperasi / Syarikat Kerjasama/Pertubuhan Peladang
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Senaskah Buku Undang-Undang Kecil Pertubuhan.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Salinan cabutan Minit Mesyuarat yang menunjukkan Ahli Lembaga Pengarah atau Ahli Jawatankuasa bersetuju untuk memohon Lesen Kilang Padi dengan menyatakan peruntukan modal bagi menjalankan perniagaan tersebut sekurang-kurangnya RM100,000 bagi perniagaan bertaraf Bumiputera dan RM200,000 bagi perniagaan bertaraf Bukan Bumiputera.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Senarai nama Ahli Jawatankuasa yang menunjukkan Jawatan, No. Kad Pengenalan dan alamat.
                                            </p>
                                            <p style="padding-left: 60px;">
                                                <span style="text-decoration: underline;">
                                                    Bahagian II – Maklumat Tambahan
                                                </span>
                                            </p>
                                            <p style="padding-left: 90px;">
                                                i. Bagi Pemohon Lesen Kilang Padi Komersil
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                a. Salinan kad pengenalan atau JPN 1/9 pemohon.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                b. Satu salinan surat kelulusan Jabatan Alam Sekitar.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                c. Satu salinan surat kelulusan ubah syarat tapak kilang kepada Industri / kilang padi.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                d. Satu salinan surat kelulusan mendirikan kilang padi daripada Pihak Berkuasa Tempatan.
                                            </p>
                                            <p style="text-align: justify; padding-left: 120px;">
                                                e. Satu salinan surat kelulusan daripada Jabatan Bomba dan Penyelamat Malaysia.
                                            </p>
                                            <p>
                                                <b>
                                                    c) Bayaran
                                                    <br>
                                                    <br>
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                1. RM200 setahun bagi 1000MT padi yang pertama atau mana-mana bahagian daripadanya (Semenanjung Malaysia); atau
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                2. RM100 setahun bagi 1000MT padi yang pertama atau mana-mana bahagian daripadanya (Sabah / Sarawak); dan
                                            </p>
                                            <p style="padding-left: 60px;">
                                                3. RM10 setahun bagi setiap 1000MT padi berikutnya atau mana-mana bahagian daripadanya.
                                            </p>
                                            <p>
                                                <b>
                                                    d) Kewajipan Pemohon
                                                </b>
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                1. Tidak boleh mengendalikan mana-mana kilang padi kecuali di tempat yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                2. Tidak boleh menyimpan atau menstor padi atau beras kecuali tempat yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                3. Tidak boleh menyimpan atau menstor padi melebihi amaun yang dinyatakan dalam lesen.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                4. Lesen tidak boleh dipindah milik atau diserah hak kepada orang lain.
                                            </p>
                                            <p style="text-align: justify; padding-left: 60px;">
                                                5. Pelesen hendaklah menyelenggara suatu akaun harian bagi :
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                a. Kelas dan jumlah berat padi yang dikilang
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                b. Jumlah berat dan gred beras yang diperolehi daripada pengilang padi
                                            </p>
                                            <p style="padding-left: 120px; text-align: justify;">
                                                c. Jumlah kuantiti dedak dan beras pecah yang diperolehi daripada pengilangan padi.
                                            </p>'
        ]);
    }
}
