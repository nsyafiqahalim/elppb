<?php

namespace App\DataObjects\ReferenceData;

use DB;
use Auth;

use App\Models\ReferenceData\Company;

class CompanyDataObject
{
    public static function createNewCompany($param)
    {
        return Company::create([
            'name'                  =>  $param['name'],
            'company_type_id'       =>  $param['company_type_id'],
            'registration_number'   =>  $param['mycoid'],
            'expiry_date'           =>  $param['expiry_date'],
            'paidup_capital'        =>  $param['paidup_capital'],
            'user_id'               =>  $param['user_id'],
        ]);
    }

    public static function findAllActiveCompanys()
    {
        $Companys = Company::activeOnly()->get();

        return $Companys;
    }

    public static function findAllCompanyUsingDatatableFormat($param)
    {
        $companies =  Company::distinct()
                    ->when(isset($param['user_id']), function ($company) use ($param) {
                        $company->where('user_id', $param['user_id']);
                    });
                
        return datatables()->of($companies)
        ->addIndexColumn()
        ->addColumn('action', function ($company) {
            // return view('admin.company-type.partials.datatable-button', compact('company'))->render();
        })
        ->addColumn('status', function ($company) {
            return ($company->is_active == 1)? 'Aktif' : 'Tidak Aktif';
        })
        ->addColumn('address', function ($company) {
            if (isset($company->address)) {
                return $company->address->address_1.','
                    .$company->address->address_2.','
                    .$company->address->address_3.','
                    .$company->address->postcode.','
                    .$company->address->state->name.',';
            } else {
                return '-';
            }
        })->addColumn('company_type', function ($company) {
            return $company->companyType->name ?? '-';
        })
        ->addColumn('expiry_date', function ($company) {
            return $company->expiry_date->format('d/m/Y') ?? '-';
        })
        ->addColumn('action', function ($company) {
            return view(
                'license-application.component.company.partials.radiobutton',
                [
                'id'    =>  encrypt($company->id)
            ]
            )->render();
        })->make(true);
    }

    public static function findCompanyById($id)
    {
        return Company::find($id);
    }
}
