<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class StoreOwnershipType extends Model
{
    public $guarded = ['id'];

    public $table = 'store_ownership_types';
}
