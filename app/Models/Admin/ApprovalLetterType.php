<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class ApprovalLetterType extends Model
{
    public $guarded = ['id'];
}
