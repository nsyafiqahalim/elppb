<?php

namespace App\DataObjects\Admin;

use DB;
use Spatie\Permission\Models\Permission;

use App\Models\Admin\District;

class DistrictDataObject
{
    public static function addDistrict($param)
    {
        $district = null;

        DB::transaction(function () use ($param, &$district) {
            $district = District::create([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'state_id'      =>  $param['state_id'],
                'updated_at'    =>  $param['updated_at'],
                'created_at'    =>  $param['created_at']
            ]);
        });
        return $district;
    }

    public static function updateDistrict($param, $id)
    {
        $district = null;

        DB::transaction(function () use ($param, $id,  &$district) {
            $district = District::find($id);

            $district->update([
                'name'          =>  $param['name'],
                'code'          =>  $param['code'],
                'is_active'     =>  $param['is_active'],
                'state_id'      =>  $param['state_id'],
                'updated_at'    =>  $param['updated_at'],
            ]);
        });
        return $district;
    }

    public static function findAllDistrictUsingDatatableFormat($param)
    {
        $districts = District::distinct()->orderby('id', 'desc')
                        ->when(isset($param['code']) && $param['code'], function ($query) use ($param) {
                            $query->where('code', 'like', '%'.$param['code'].'%');
                        })
                        ->when(isset($param['name']) && $param['name'], function ($query) use ($param) {
                            $query->where('name', 'like', '%'.$param['name'].'%');
                        })
                        ->when(isset($param['is_active']), function ($query) use ($param) {
                            $query->where('is_active', $param['is_active']);
                        });

        return datatables()->of($districts)
        ->addIndexColumn()
        ->addColumn('action', function ($district) {
            return view('admin.district.partials.datatable-button', compact('district'))->render();
        })
        ->addColumn('status', function ($district) {
            $status= ($district->is_active == 1)? 'Aktif' : 'Tidak Aktif';

            return view('admin.district.partials.status', compact('status'))->render();
        })
        ->rawColumns(['status','action'])
        ->make(true);
    }

    public static function findDistrictById($id)
    {
        return District::find($id);
    }

    public static function findAllActiveDistricts()
    {
        return District::activeOnly()->get();
    }
}
