<?php

namespace App\Http\Controllers\API\V1\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\DataObjects\Admin\StoreTypeDataObject;

class StoreTypeController extends Controller
{
    //

    public function __construct()
    {
        //   $this->middleware(['auth', 'verified']);
    }

    public function addStoreType(Request $request)
    {
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['created_at'] = $currentDateTime;
        $param['updated_at'] = $currentDateTime;

        $storeType = StoreTypeDataObject::addStoreType($param);

        return response()->json([
            'message'   =>  'Jenis Stor berjaya ditambah',
            'data'      =>  $storeType,
            'success'   =>  true,
            'route'     =>  route('admin.store_type.index')
        ], 200);
    }

    public function updateStoreType(Request $request, $id)
    {
        $decryptedId = decrypt($id);
        $currentDateTime = Carbon::now();

        $param = $request->all();
        $param['updated_at'] = $currentDateTime;

        $storeType = StoreTypeDataObject::updateStoreType($param, $decryptedId);

        return response()->json([
            'message'   =>  'Jenis Stor berjaya dikemaskini',
            'data'      =>  $storeType,
            'success'   =>  true,
            'route'     =>  route('admin.store_type.index')
        ], 200);
    }

    public function datatable(Request $request)
    {
        $param = $request->all();
        return StoreTypeDataObject::findAllStoreTypeUsingDatatableFormat($param);
    }
}
