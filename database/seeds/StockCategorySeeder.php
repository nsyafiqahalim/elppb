<?php

use Illuminate\Database\Seeder;

use App\Models\Admin\StockCategory;

class StockCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'Kilang',
          'Borong',
      ];

      foreach ($data as $item) {
          StockCategory::create([
              'name'  =>  $item,
              'code'  =>  strtoupper(str_replace(' ', '_', $item)),
              'description'   =>  'Data ini merujuk kepada data jenis '.$item,
              'is_active' => 1
          ]);
      }
    }
}
