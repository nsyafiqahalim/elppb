<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rice_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('old_id')->nullable();
            $table->bigInteger('rice_category_id')->nullable()->unsigned()->index();
            $table->string('code', 30);
            $table->string('name', 255);
            $table->text('description');
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();

            //foreign key
            $table->foreign('rice_category_id')->references('id')->on('rice_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rice_types');
    }
}
