<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\DataObjects\Admin\StatusDataObject;

class StatusController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        return view('admin.status.index');
    }

    public function add()
    {
        return view('admin.status.add');
    }

    public function edit($id)
    {
        $decryptedId = decrypt($id);

        $status = StatusDataObject::findStatusById($decryptedId);

        return view('admin.status.edit', compact('status'));
    }
}
