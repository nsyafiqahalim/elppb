<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ApprovalLetterAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approval_letter_application_attachments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('file_path');
            $table->string('code');
            $table->text('file_original_name');
            $table->unsignedBigInteger('approval_letter_application_id')->index('alaa_ala_idx');
            $table->timestamps();
            $table->foreign('approval_letter_application_id', 'fk_alaa_ala')->references('id')->on('approval_letter_applications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
